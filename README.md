#Setup on laptop, for using the nexus server you need to be on vpn

Add the following settings.xml to you .m2 folder fo maven
```
<?xml version="1.0" encoding="UTF-8"?>
<settings xmlns="http://maven.apache.org/SETTINGS/1.1.0"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.1.0 http://maven.apache.org/xsd/settings-1.1.0.xsd">
  <mirrors>
    <mirror>
      <!--This sends everything else to /public -->
      <id>nexus</id>
      <mirrorOf>*</mirrorOf>
      <url>http://15.15.3.4:8084/repository/maven-central/</url>
    </mirror>
  </mirrors>
  <profiles>
    <profile>
      <id>nexus</id>
      <!--Enable snapshots for the built in central repo to direct -->
      <!--all requests to nexus via the mirror -->
      <repositories>
        <repository>
          <id>central</id>
          <url>http://central</url>
          <releases><enabled>true</enabled></releases>
          <snapshots><enabled>true</enabled></snapshots>
        </repository>
      </repositories>
     <pluginRepositories>
        <pluginRepository>
          <id>central</id>
          <url>http://central</url>
          <releases><enabled>true</enabled></releases>
          <snapshots><enabled>true</enabled></snapshots>
        </pluginRepository>
      </pluginRepositories>
    </profile>
  </profiles>
  <activeProfiles>
    <!--make the profile active all the time -->
    <activeProfile>nexus</activeProfile>
  </activeProfiles>
# Goals

Contains code base for goals. 
Check https://teamvymo.atlassian.net/wiki/spaces/ENGG/pages/617873494/Goals for more info
Check PRD specs https://teamvymo.atlassian.net/wiki/spaces/PROD/pages/678658122/Team+Self+Goals+-+Use+cases

## Setup

1. Ensure Config service & Metrics rest is running.
2. Create MYsql tables by running patches in resources folder - create_tables.sql
3. Copy the elastic search cluster name installed on your setup. It is usually found in
`/usr/local/Cellar/elasticsearch@5.6/5.6.5/libexec/config/elasticsearch.yml` line 17 as `cluster.name: elasticsearch_chandrasekhar`.
4. Copy the cluster name and replace it in application.properties `spring.data.elasticsearch.cluster-name`.
5. Create ES dynamic template by running the following command
```bash
curl --request PUT \
  --url http://localhost:9200/_template/external_achievements_template \
  --header 'content-type: application/json' \
  --data '{
	"template": "*external_achievements",
	"mappings": {
		"achievements": {
			"dynamic_templates": [
				{
					"date_fields": {
						"match_pattern": "regex",
						"path_match": "achievementsMap.*",
						"match": "/_c_date$|/_c_Date$|/_c_time$|/_c_Time$/g",
						"mapping": {
							"type": "date"
						}
					}
				},
				{
					"number_fields": {
						"match_pattern": "regex",
						"path_match": "achievementsMap.*",
						"match": "/_c_number$|/_c_Number$/g",
						"mapping": {
							"type": "number"
						}
					}
				},
				{
					"other_text_fields": {
						"match_mapping_type": "string",
						"mapping": {
							"type": "keyword"
						}
					}
				}
			],
			"properties": {
				"startDate": {
					"type": "date",
					"format": "YYYY-MM-DD HH:mm:ss||YYYY-MM-DD||epoch_millis"
				},
				"endDate": {
					"type": "date",
					"format": "YYYY-MM-DD HH:mm:ss||YYYY-MM-DD||epoch_millis"
				},
				"deleted": {
					"type": "boolean"
				},
				"isActive": {
					"type": "boolean"
				},
				"createdDate": {
					"type": "date",
					"format": "YYYY-MM-DD HH:mm:ss||YYYY-MM-DD||epoch_millis"
				},
				"lastUpdatedDate": {
					"type": "date",
					"format": "YYYY-MM-DD HH:mm:ss||YYYY-MM-DD||epoch_millis"
				},
				"userId": {
					"type": "keyword"
				},
				"metric": {
					"type": "keyword"
				},
				"recurringFrequency": {
					"type": "keyword"
				},
				"clientId": {
					"type": "keyword"
				},
				"createdBy": {
					"type": "keyword"
				},
				"goalDefinitionCode": {
					"type": "keyword"
				},
				"id": {
					"type": "keyword"
				},
				"lastUpdatedBy": {
					"type": "keyword"
				},
				"achievement": {
					"type": "long"
				}
			}
		}
	},
	"aliases": {
		"{index}-alias": {}
	}
}'
```
6. Import the project in Intellij and create debug configuration for the Goals application and pass environment variables
```vymo_configuration_url=http://localhost:8082;metrics_rest_url=http://localhost:3003;newrelic.config.license_key=dcfbe7d9a56f816cec4e983e14080089f376NRAL;newrelic.config.app_name=goals;ELASTIC_SEARCH=http://localhost:9200;ELASTIC_HOSTS=127.0.0.1:9200``` in Environment variables tab.
    Check reference image [here](https://imgur.com/a/H4Q2OPk)

7. Start the application using intellij

or

Invoke using (make sure set the vymo_configuration_url & metrics_rest_url in application.properties file)
```
./gradlew bootRun
```
## Troubleshooting

1. Getting build failures & errors related to config model classes
- Ensure you are using the latest branches of config-service-model, config-server & config-java-client
- Delete config-service-java-client & model in `~/.m2/repository/in/vymo/config` & `~/.m2/repository/in/vymo/core`
- Rebuild config-service-java-client & config-service-model and do
```
gradle clean ; gradle build; gradle publishToMavenLocal ;
```
in both config-service-java-client and config-service-model folders.
- Go to goals service folder and delete gradle cache. `rm -rf $HOME/.gradle/caches/`
- Re-run goals service. It will install everything from scratch

2. Errors related to Elasticsearch
```
{"errors":["None of the configured nodes are available: [{#transport#-1}{Glf9Vxf0RquCmi_8NgXnHA}{localhost}{127.0.0.1:9300}]"],"message":"Something went wrong with us. We are on it!"}
```
- Ensure you are running Elasticsearch
- Ensure you've configured the correct cluster-name in the application properties. (Refer Step 3 of installation)

3. Errors related to `org.hibernate.exception.SQLGrammarException: could not extract ResultSet`
- You might have missed  Step 2 (Applying mysql patches)

4. Errors related to Config poll
- The branch used for config-model might be outdated. Update config-model inside config-service-server as well.
- If problem persists follow steps in Troubleshooting scenario 1.

