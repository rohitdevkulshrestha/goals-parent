--liquibase formatted sql
--changeset rohitdev:1_0_0
--labels: Goals service

--CREATE DATABASE `targets_service_v1` CHARACTER SET utf8 COLLATE utf8_unicode_ci

use targets_service_v1;

--sched lock
CREATE TABLE shedlock(name VARCHAR(64) NOT NULL, lock_until TIMESTAMP(3) NOT NULL,
    locked_at TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3), 
    locked_by VARCHAR(255) NOT NULL, PRIMARY KEY (name));

CREATE TABLE if not exists `goal_assignees` (
  `id` varchar(36) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `current_version` varchar(36) NOT NULL,
  `created_by` varchar(255) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated_date` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `goal_definition_code` varchar(255) DEFAULT NULL,
  `client_id` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `current_version_index` (`current_version`),
  KEY `u_client_v` (`user_id`,`client_id`,`current_version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE if not exists `goal_assignee_settings_versions` (
  `id` varchar(36) NOT NULL,
  `goal_assignee_id` varchar(36) NOT NULL,
  `recurring_frequency` varchar(255) DEFAULT NULL,
  `user_id` varchar(255) NOT NULL,
  `target` int(11) DEFAULT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `is_recurring` tinyint(1) NOT NULL,
  `version` varchar(36) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated_by` varchar(255) NOT NULL,
  `created_by` varchar(255) NOT NULL,
  `last_updated_date` datetime NOT NULL,
  `goal_definition_code` varchar(255) DEFAULT NULL,
  `client_id` varchar(255) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `start_date_index` (`start_date`),
  KEY `end_date_index` (`end_date`),
  KEY `version_index` (`version`),
  KEY `g_v_s_e` (`goal_assignee_id`,`version`,`start_date`,`end_date`),
  CONSTRAINT `FK6bv3u7xthf7hgh7cf7bqnp9we` 
  FOREIGN KEY (`goal_assignee_id`) REFERENCES `goal_assignees` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE if not exists `goal_assignment_versions` (
  `id` varchar(36) NOT NULL,
  `version` varchar(36) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `entry_type` varchar(255) DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `created_by` varchar(255) NOT NULL,
  `last_updated_date` datetime NOT NULL,
  `last_updated_by` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'SUCCESS',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE if not exists `goal_instances` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` varchar(255) NOT NULL,
  `goal_assignee_id` varchar(36) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `target` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `created_by` varchar(255) NOT NULL,
  `last_updated_date` datetime NOT NULL,
  `last_updated_by` varchar(255) NOT NULL,
  `is_recurring` tinyint(1) NOT NULL DEFAULT '0',
  `recurring_frequency` varchar(255) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `goal_definition_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `goal_instance_constraint` 
  (`goal_assignee_id`,`start_date`,`end_date`,`is_recurring`,`recurring_frequency`),
  CONSTRAINT `FK2iduriarr26h0ft6v6yo6mvon` 
  FOREIGN KEY (`goal_assignee_id`) REFERENCES `goal_assignees` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=521 DEFAULT CHARSET=utf8;