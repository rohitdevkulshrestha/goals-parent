package com.getvymo.goals.utils;

import static org.junit.Assert.assertEquals;

import com.getvymo.goals.dal.repository.GoalAssigneeSettingsVersionsRepository;
import com.getvymo.goals.dal.repository.GoalAssignmentOperationVersionsRepository;
import com.getvymo.platform.goals.GoalsApplication;
import in.vymo.core.config.model.core.DATE_GRANULARITY;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest(properties = "spring.profiles.active=test", classes = GoalsApplication.class)
public class GoalsUtilsTest {

  @Autowired GoalsUtils goalsUtils;

  @Autowired private GoalAssigneeSettingsVersionsRepository goalAssigneeSettingsVersionsRepository;

  @Autowired
  private GoalAssignmentOperationVersionsRepository goalAssignmentOperationVersionsRepository;

  @Before
  public void setUp() throws Exception {}

  @After
  public void tearDown() throws Exception {}

  @Test
  public void getOffsetCorrectedDate() {
    String timeZoneString = "+0530";
    TimeZone timeZone = TimeZone.getTimeZone("GMT" + timeZoneString);
    Date date = new Date(1588271400000L); // 2020-05-01 00:00:00
    Date offSetCorrectedDate = new Date(1588251600000L); // 2020-04-30 18:30:00
    assertEquals(goalsUtils.getOffsetCorrectedDate(date, timeZone), offSetCorrectedDate);

    TimeZone timeZone2 = TimeZone.getTimeZone("GMT" + "+0700");
    Date date2 = new Date(1588271400000L); // 2020-05-01 00:00:00
    Date offSetCorrectedDate2 = new Date(1588246200000L); // 2020-04-30 17:00:00
    assertEquals(goalsUtils.getOffsetCorrectedDate(date2, timeZone2), offSetCorrectedDate2);
  }

  @Test
  public void getNumberOfGoalInstancesWithoutOffsetCorrection() throws ParseException {
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-mm-dd hh:mm", Locale.ENGLISH);
    DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ", Locale.ENGLISH);
    Date startDate = format.parse("2020-04-01T00:00:00.000+0000");
    Date endDate = format.parse("2020-07-01T00:00:00.000+0000");
    assertEquals(
        goalsUtils.getNumberOfGoalInstances(startDate, endDate, DATE_GRANULARITY.month), 3);
    assertEquals(
        goalsUtils.getNumberOfGoalInstances(startDate, endDate, DATE_GRANULARITY.quarter), 1);
  }

  @Test
  public void getNumberOfGoalInstancesWithOffsetCorrection() throws ParseException {
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-mm-dd hh:mm", Locale.ENGLISH);
    DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ", Locale.ENGLISH);
    Date startDate = format.parse("2020-03-31T18:30:00.000+0000");
    Date endDate = format.parse("2020-06-30T18:30:00.000+0000");
    assertEquals(
        goalsUtils.getNumberOfGoalInstances(startDate, endDate, DATE_GRANULARITY.month), 3);
    assertEquals(
        goalsUtils.getNumberOfGoalInstances(startDate, endDate, DATE_GRANULARITY.quarter), 1);
  }

  @Test
  public void roundOffMillisInDate() {}

  @Test
  public void getUserIdsWithoutGoals() {}

  @Test
  public void getDateDiffInQuarters() {}

  @Test
  public void getDateDiffInYears() {}

  @Test
  public void getDateDiffInMonths() {}

  @Test
  public void createGoalAssigneeEntitiesForUsersWithoutGoalAssignments() {}

  @Test
  public void getStartAndEndDateBoundaries() {}

  @Test
  public void getListOfStartAndEndDatesAfterOffsetCorrection() {}

  @Test
  public void getOffsetCorrectedGoalAssigneeSettingsVersionsEntities() {}

  @Test
  public void createGoalInstancesFromGoalAssigneeSettingsVersionEntities() {}

  @Test
  public void createNewGoalAssigneeSettingsVersionEntities() {}

  @Test
  public void getUserGoalAssignmentFromGoalAssigneeSettingsVersionEntity() {}

  @Test
  public void getGoalDefinitionSettingsFromGoalDefinitionEntity() {}

  @Test
  public void getGoalAssignmentFromGoalAssigneeAndSettings() {}

  @Test
  public void getDateGranularity() {}

  @Test
  public void getClientTimezone() {}

  @Test
  public void getGoalDefinitionFromEntity() {}

  @Test
  public void getGoalDefinitionFromEntityAndSettings() {}

  @Test
  public void getGoalAssignnmentFromGoalInstanceEntity() {}

  @Test
  public void getTotalGoalInstanceEntitiesIncludingMissingUsers() {}

  @Test
  public void getExternalAchievementFromEntity() {}

  @Test
  public void prettyPrintStringFromJSON() {}
}
