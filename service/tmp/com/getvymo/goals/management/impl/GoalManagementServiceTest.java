package com.getvymo.goals.management.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.jsontype.NamedType;
import com.getvymo.goals.common.*;
import com.getvymo.goals.config.api.ConfigClient;
import com.getvymo.goals.dal.entity.GoalAssigneeSettingsVersionsEntity;
import com.getvymo.goals.dal.entity.GoalAssigneesEntity;
import com.getvymo.goals.dal.entity.GoalAssignmentOperationVersionsEntity;
import com.getvymo.goals.dal.repository.*;
import com.getvymo.goals.evaluation.api.impl.GoalEvaluationService;
import com.getvymo.goals.management.api.ActivateVersionResponse;
import com.getvymo.goals.management.api.GoalAssignment;
import com.getvymo.goals.utils.ConfigUtils;
import com.getvymo.goals.utils.GoalsUtils;
import com.getvymo.platform.goals.GoalsApplication;
import com.getvymo.platform.goals.app.errors.GoalManagementServiceException;
import in.vymo.configuration.api.ConfigurationClient;
import in.vymo.core.config.model.core.DATE_GRANULARITY;
import in.vymo.core.config.model.goals.GoalDefinition;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.aspectj.lang.annotation.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * FIXME: UTs case 1: One time goal
 *
 * <p>case 2: Recurring goal
 *
 * <p>1. Create GD 2. Create a version 3. Set goals for users (date range = today-2 months to today
 * + 6 months 4. Activate & validate GIs 5. create a new version for date range = today+1 month to
 * today + 3 months 6. activate & validate GIs
 */
@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@AutoConfigureTestDatabase(replace = Replace.NONE)
@SpringBootTest(properties = "spring.profiles.active=test", classes = GoalsApplication.class)
public class GoalManagementServiceTest {

  private static final ObjectMapper MAPPER = new ObjectMapper();
  private static ConfigurationClient client;

  static {
    MAPPER.registerSubtypes(new NamedType(RecurringGoalAssigneeSettings.class, "recurring"));
    MAPPER.registerSubtypes(new NamedType(OneTimeGoalAssigneeSettings.class, "one_time"));
    client = ConfigClient.client();
  }

  @Rule public ExpectedException exceptionRule = ExpectedException.none();

  @Autowired GoalAssigneeRepository goalAssigneeRepository;

  @Autowired GoalAssigneeSettingsVersionsRepository goalAssigneeSettingsVersionsRepository;

  @Autowired GoalInstanceRepository goalInstanceRepository;

  @Autowired GoalAssignmentOperationVersionsRepository goalAssignmentOperationVersionsRepository;

  @MockBean GoalInstanceCustomRepository goalInstanceCustomRepository;

  @Autowired GoalEvaluationService goalEvaluationService;

  @Autowired GoalManagementService goalManagementService;

  @Autowired GoalsUtils goalsUtils;

  @MockBean ConfigUtils configUtils;

  @Autowired ExternalAchievementRepository externalAchievementRepository;

  @PersistenceContext private EntityManager em;
  private Logger logger = LoggerFactory.getLogger(this.getClass());

  @Before
  public void setUp() {
    when(configUtils.getClientTimezone("demo")).thenReturn(TimeZone.getTimeZone("GMT+0530"));
    when(configUtils.getDateGranularity("monthly", "demo")).thenReturn(DATE_GRANULARITY.month);
    when(configUtils.getDateGranularity("yearly", "demo")).thenReturn(DATE_GRANULARITY.year);
    when(configUtils.getDateGranularity("weekly", "demo")).thenReturn(DATE_GRANULARITY.week);
    when(configUtils.getDateGranularity("quarterly", "demo")).thenReturn(DATE_GRANULARITY.quarter);
    when(configUtils.getDateGranularity("daily", "demo")).thenReturn(DATE_GRANULARITY.day);
    //        GoalDefinitionEntity savedGoalDefinitionEntity =
    // goalDefinitionRepository.save(dummyGoalDefinitionEntity);
    //        logger.info("Saved Goal definition is {}", savedGoalDefinitionEntity);
    Mockito.when(configUtils.getClientTimezone("demo"))
        .thenReturn(TimeZone.getTimeZone("GMT+0530"));
  }

  @Test
  public void testMockitoClientZone() {
    GoalsUtils goalsUtils = Mockito.mock(GoalsUtils.class);
    Mockito.when(configUtils.getClientTimezone("demo"))
        .thenReturn(TimeZone.getTimeZone("GMT+0530"));
    TimeZone demo = configUtils.getClientTimezone("demo");
    System.out.println(demo);
  }

  //	@Test
  //	public void testGoalDefinitionCreation() {
  //		GoalDefinition goalDefinition = createTestGD();
  //		logger.info("created goalDefinition", goalDefinition);
  //		GoalDefinitionSettings createdSettings = goalDefinition.getSettings();
  //		assertEquals(goalDefinition.getRow() != null, true);
  //		assertEquals(goalDefinition.getCode(), "demo_new_leads");
  //		assertEquals(createdSettings.getDescription(), "test description");
  //		assertEquals(createdSettings.getName(), "Test");
  //		assertEquals(createdSettings.getGoalType(), GoalType.individual);
  //		assertEquals(goalDefinition.getCreatedBy(), "test");
  //
  //	}

  @After(value = "")
  public void cleanup() {
    //        em.getTransaction().begin();
    //        em.createNativeQuery("truncate table goal_definition").executeUpdate();
    //        em.getTransaction().commit();
  }

  @Test
  public void testOnetimeGoalAssignmentWithoutProvidingVersion() {
    Date startDate = new Date(1568287883077L);
    Date endDate = new Date(1570707083077L);
    List<String> userIds = Arrays.asList("test");
    GoalDefinition gDefinition =
        goalManagementService.getGoalDefinitionByCode("demo_new_leads", "demo");
    OneTimeGoalAssigneeSettings oneTimeGoalAssigneeSettings =
        new OneTimeGoalAssigneeSettings(
            GoalOccurrence.one_time, 20d, startDate, endDate, null, "test", gDefinition.getCode());
    GoalAssigneeSettingsWithoutVersion gSettingsWithoutVersion =
        new GoalAssigneeSettingsWithoutVersion(oneTimeGoalAssigneeSettings, null);
    ActivateVersionResponse gAList =
        goalManagementService.setGoalForUsersWithoutVersion(
            "demo", userIds, gDefinition.getCode(), gSettingsWithoutVersion, "admin");
    assertEquals(gAList.getStatus(), "success");
    // GoalAssignment goalAssignment = gAList.get(0);
    // TODO: Will fail as activateGoalAssignmentOperationVersion in goalscustomrepository is mocked
    // assertEquals(true, goalAssignment.isActive());
  }

  @Test
  public void testRecurringGoalAssignmentWithoutProvidingVersion() {
    Date startDate = new Date(1568287883077L);
    Date endDate = new Date(1570707083077L);
    List<String> userIds = Arrays.asList("test");
    GoalDefinition gDefinition = goalManagementService.getGoalDefinitionByCode("demo_test", "demo");
    RecurringGoalAssigneeSettings rGoalAssigneeSettings =
        new RecurringGoalAssigneeSettings(
            GoalOccurrence.recurring,
            20d,
            startDate,
            endDate,
            "weekly",
            null,
            "test",
            gDefinition.getCode());
    GoalAssigneeSettingsWithoutVersion gSettingsWithoutVersion =
        new GoalAssigneeSettingsWithoutVersion(rGoalAssigneeSettings, null);

    ActivateVersionResponse gAList =
        goalManagementService.setGoalForUsersWithoutVersion(
            "demo", userIds, gDefinition.getCode(), gSettingsWithoutVersion, "admin");
    assertEquals(gAList.getStatus(), "success");
    // TODO: Separate testcase for testing goal instances created
    //        StandardListResponse allGoalAssignments =
    //                goalManagementService.getAllGoalAssignments("demo", gDefinition.getRow(),
    // userIds, startDate, endDate, 0, 10);
    //        ArrayList<GoalAssignment> goalAssignments = allGoalAssignments.getData();
    //        assertEquals(4, goalAssignments.size());

  }

  @Test
  public void testOneTimeGoalAssignmentByProvidingVersion() {
    Date startDate = new Date(1568287883077L);
    Date endDate = new Date(1570707083077L);
    List<String> userIds = Arrays.asList("test");
    GoalDefinition gDefinition =
        goalManagementService.getGoalDefinitionByCode("demo_new_leads", "demo");
    OneTimeGoalAssigneeSettings oneTimeGoalAssigneeSettings =
        new OneTimeGoalAssigneeSettings(
            GoalOccurrence.one_time, 20d, startDate, endDate, null, "test", "demo_new_leads");

    List<GoalAssignment> gAList =
        goalManagementService.setGoalForUsersWithVersion(
            "demo",
            userIds,
            gDefinition.getCode(),
            oneTimeGoalAssigneeSettings,
            "vjx8t7ta9k9",
            "admin");

    assertEquals(gAList.size(), 1);
    GoalAssignment goalAssignment = gAList.get(0);

    assertEquals(false, goalAssignment.isActive());

    //        gAList = goalManagementService.activateGoalAssignmentOperationVersion("vjx8t7ta9k9",
    // "demo");
    //
    //        goalAssignment = gAList.get(0);
    //        assertEquals(goalAssignment.isDisabled(), false);
  }

  //	@Test
  //	public void testGetUserIdAndEndDateMap() {
  //		List<GoalInstanceEntity> existingGoalInstanceEntities = new ArrayList<GoalInstanceEntity>();
  //		Date startDate = new Date(1568287883077L);
  //		Date endDate = new Date(1570707083077L);
  //		List<Map<String, Date>> startAndEndDateBoundaries = goalsUtils
  //				.getStartAndEndDateBoundaries(startDate, endDate, DATE_GRANULARITY.week, 4);
  //
  //		String userName = "testUser";
  //
  //		for (Iterator<Map<String, Date>> eachEntryBounds = startAndEndDateBoundaries.iterator();
  // eachEntryBounds
  //				.hasNext();) {
  //			Map<String, Date> startAndEndDateMap = (Map<String, Date>) eachEntryBounds.next();
  //			Date goalStartDate = startAndEndDateMap.get("startDate");
  //			Date goalEndDate = startAndEndDateMap.get("endDate");
  //			existingGoalInstanceEntities.add(new GoalInstanceEntity("demo", userName, goalStartDate,
  // goalEndDate,
  //					20, userName, userName, false, true, "weekly", true));
  //		}
  //
  //		List<String> userIds = Arrays.asList(userName);
  //
  //		Map<String, Date> userIdAndEndDateMap =
  // goalsUtils.getUserIdAndEndDateMap(existingGoalInstanceEntities, userIds);
  //		Date newEndDate = userIdAndEndDateMap.get(userName);
  //		assertEquals(1570707083077L, newEndDate.getTime());
  //	}

  @Test
  public void testRecurringGoalAssignmentByProvidingVersion() {
    Date startDate = new Date(1568287883077L);
    Date endDate = new Date(1570707083077L);
    List<String> userIds = Arrays.asList("test");
    GoalDefinition gDefinition =
        goalManagementService.getGoalDefinitionByCode("demo_new_leads", "demo");
    RecurringGoalAssigneeSettings rGoalAssigneeSettings =
        new RecurringGoalAssigneeSettings(
            GoalOccurrence.recurring,
            20d,
            startDate,
            endDate,
            "weekly",
            null,
            "test",
            "demo_new_leads");
    List<GoalAssignment> gAList =
        goalManagementService.setGoalForUsersWithVersion(
            "demo", userIds, gDefinition.getCode(), rGoalAssigneeSettings, "vjx8t7ta9k9", "admin");
    assertEquals(gAList.size(), 1);
    // TODO: Check goal_instances creation
    GoalAssignment goalAssignment = gAList.get(0);
    assertEquals(goalAssignment.isActive(), false);
    // TODO: activateGoalAssignmentOperationVersion is mocked so will not activate in real
    //        gAList = goalManagementService.activateGoalAssignmentOperationVersion("qweet",
    // "demo");
    //        goalAssignment = gAList.get(0);
    //        assertEquals(goalAssignment.isDisabled(), false);
    //        StandardListResponse
    //                allGoalAssignments = goalManagementService.getAllGoalAssignments("demo",
    // "demo_new_leads", userIds, startDate, endDate, 0, 10);
    //        ArrayList<GoalAssignment> goalAssignments = allGoalAssignments.getData();
    //        assertEquals(4, goalAssignments.size());

  }

  public GoalAssigneesEntity getDummyGoalAssigneesEntity() {
    return new GoalAssigneesEntity("abcdf", "userId", "demo", "createdBy", false, true);
  }

  @Test
  public void testGetUserIdsWithoutGoals() {
    List<GoalAssigneeSettingsVersionsEntity> existingGoalInstanceEntities =
        new ArrayList<GoalAssigneeSettingsVersionsEntity>();
    Date startDate = new Date(1568287883077L);
    Date endDate = new Date(1570707083077L);
    List<Map<String, Date>> startAndEndDateBoundaries =
        goalsUtils.getStartAndEndDateBoundaries(
            startDate, endDate, DATE_GRANULARITY.week, 4, "demo");

    String userName = "testUser";

    for (Iterator<Map<String, Date>> eachEntryBounds = startAndEndDateBoundaries.iterator();
        eachEntryBounds.hasNext(); ) {
      Map<String, Date> startAndEndDateMap = (Map<String, Date>) eachEntryBounds.next();
      Date goalStartDate = startAndEndDateMap.get("startDate");
      Date goalEndDate = startAndEndDateMap.get("endDate");
      existingGoalInstanceEntities.add(
          new GoalAssigneeSettingsVersionsEntity(
              "testgoal",
              getDummyGoalAssigneesEntity(),
              "weekly",
              20d,
              goalStartDate,
              goalEndDate,
              true,
              "abcdef",
              "abcdef",
              userName,
              "demo",
              userName,
              false));
    }
    List<String> userIds = Arrays.asList(userName, "testUser2", "testUser3");
    List<String> userIdsWithoutGoals =
        goalsUtils.getUserIdsWithoutGoals(existingGoalInstanceEntities, userIds);
    assertEquals(true, userIdsWithoutGoals.contains("testUser2"));
    assertEquals(true, userIdsWithoutGoals.contains("testUser3"));
    assertEquals(false, userIdsWithoutGoals.contains(userName));
  }

  public GoalAssignmentOperationVersionsEntity getDummyGoalAssignmentOperationVersionsEntity() {
    GoalAssignmentOperationVersionsEntity goalAssignmentOperationVersionsEntity =
        new GoalAssignmentOperationVersionsEntity(
            "abcdef", "admin", "admin", "description", GoalEntryType.BULK_UPLOAD);
    return goalAssignmentOperationVersionsEntity;
  }

  //    public GoalDefinitionSettings getDummyTeamDefinitionSettings() {
  //        return new TeamGoalDefinitionSettings(GoalType.team, "vo_won", "Demo goal", "Demo goal",
  // "demo_code", null,
  //                TeamDefinitionType.org_hierarchy, "value", false, false, 10);
  //    }

  public RecurringGoalAssigneeSettings getDummyRecurringGoalAssigneeSettings(
      Date startDate, Date endDate, String recurringFreq) {
    return new RecurringGoalAssigneeSettings(
        GoalOccurrence.recurring,
        100d,
        startDate,
        endDate,
        recurringFreq,
        false,
        "test",
        "demo_new_leads");
  }

  @Test
  public void testSetGoalsForUser() {}

  @Test
  public void testGetNumberOfGoalInstancesForRecurringWeekly() {
    Date startDate = new Date(1568287883077L);
    Date endDate = new Date(1570707083077L);
    int expected = goalsUtils.getNumberOfGoalInstances(startDate, endDate, DATE_GRANULARITY.week);
    assertEquals(expected, 4);
  }

  @Test(expected = GoalManagementServiceException.class)
  public void testGetNumberOfGoalInstancesForRecurringWeeklyThrowsException() {
    Date startDate = new Date(1568287883077L);
    Date endDate = new Date(1570707083077L);
    goalsUtils.getNumberOfGoalInstances(startDate, endDate, DATE_GRANULARITY.quarter);
    exceptionRule.expect(GoalManagementServiceException.class);
    exceptionRule.expectMessage("Recurring granularity doesn't fit with start & end date bounds");
  }

  @Test
  public void testGetNumberOfGoalInstancesForRecurringDaily() {
    Date startDate = new Date(1568287883077L);
    Date endDate = new Date(1570707083077L);
    int expected = goalsUtils.getNumberOfGoalInstances(startDate, endDate, DATE_GRANULARITY.day);
    assertEquals(28, expected);
  }

  @Test
  public void testGetNumberOfGoalInstancesForRecurringMonthly() {
    Date startDate = new Date(1570609663034L);
    Date endDate = new Date(1578558463034L);
    int expected = goalsUtils.getNumberOfGoalInstances(startDate, endDate, DATE_GRANULARITY.month);
    assertEquals(3, expected);
  }

  @Test
  public void testGetNumberOfGoalInstancesForRecurringQuarterly() {
    Date startDate = new Date(1570609663034L);
    Date endDate = new Date(1578558463034L);
    int actual = goalsUtils.getNumberOfGoalInstances(startDate, endDate, DATE_GRANULARITY.quarter);
    assertEquals(1, actual);
  }

  @Test
  public void testGetDateDiffInQuarters() {
    Date startDate = new Date(1570609663034L);
    Date endDate = new Date(1578558463034L);
    long actual = goalsUtils.getDateDiffInQuarters(startDate, endDate);
    assertEquals(1L, actual);
  }

  @Test
  public void testGetDateDiffInYears() {
    Date startDate = new Date(1570609663034L);
    Date endDate = new Date(1665304063034L);
    long actual = goalsUtils.getDateDiffInYears(startDate, endDate);
    assertEquals(3L, actual);
  }

  @Test
  public void testGetDateDiffInMonths() {
    Date startDate = new Date(1570609663034L);
    Date endDate = new Date(1665304063034L);
    long actual = goalsUtils.getDateDiffInMonths(startDate, endDate);
    assertEquals(36L, actual);
  }

  @Test
  public void testGetStartAndEndDateBoundariesForWeekly() throws ParseException {
    DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ", Locale.ENGLISH);
    Date startDate = format.parse("2020-01-01T05:30:00.000+0530");
    Date endDate = format.parse("2020-12-30T05:30:00.000+0530");

    List<Map<String, Date>> startAndEndDateBoundaries =
        goalsUtils.getStartAndEndDateBoundaries(
            startDate, endDate, DATE_GRANULARITY.week, 52, "demo");
    assertEquals(startAndEndDateBoundaries.size(), 52);
    DateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.ENGLISH);
    logger.info("Got " + startAndEndDateBoundaries + " date boundaries for ");
    assertEquals(
        startAndEndDateBoundaries.get(0).get("startDate"), formatter2.parse("2020-01-01 00:00:00"));
    assertEquals(
        startAndEndDateBoundaries.get(0).get("endDate"), formatter2.parse("2020-01-07 23:59:58"));

    assertEquals(
        startAndEndDateBoundaries.get(1).get("startDate"), formatter2.parse("2020-01-08 00:00:00"));
    assertEquals(
        startAndEndDateBoundaries.get(1).get("endDate"), formatter2.parse("2020-01-14 23:59:58"));

    // TODO: Sort the start & end date before checking this
    assertEquals(
        startAndEndDateBoundaries.get(51).get("startDate"),
        formatter2.parse("2020-12-23 00:00:00"));
    assertEquals(
        startAndEndDateBoundaries.get(51).get("endDate"), formatter2.parse("2020-12-29 23:59:58"));
  }

  @Test
  public void testGetStartAndEndDateBoundariesForMonthly() throws ParseException {
    DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ", Locale.ENGLISH);
    Date startDate = format.parse("2020-01-01T05:30:00.000+0530");
    Date endDate = format.parse("2021-01-01T05:30:00.000+0530");
    List<Map<String, Date>> startAndEndDateBoundaries =
        goalsUtils.getStartAndEndDateBoundaries(
            startDate, endDate, DATE_GRANULARITY.month, 12, "demo");
    assertEquals(startAndEndDateBoundaries.size(), 12);

    DateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.ENGLISH);
    logger.info("Got " + startAndEndDateBoundaries + " date boundaries for ");

    assertEquals(
        startAndEndDateBoundaries.get(0).get("startDate"), formatter2.parse("2020-01-01 00:00:00"));
    assertEquals(
        startAndEndDateBoundaries.get(0).get("endDate"), formatter2.parse("2020-01-31 23:59:58"));

    logger.error("{} expected", formatter2.parse("2020-02-01 00:00:00"));
    logger.error("{} actual", startAndEndDateBoundaries.get(1).get("startDate"));
    assertEquals(
        startAndEndDateBoundaries.get(1).get("startDate"), formatter2.parse("2020-02-01 00:00:00"));
    assertEquals(
        startAndEndDateBoundaries.get(1).get("endDate"), formatter2.parse("2020-02-29 23:59:58"));
  }
}
