

--INSERT INTO `goal_assignment_versions` (`id`, `version`, `description`, `entry_type`, `created_date`, `created_by`, `last_updated_date`, `last_updated_by`) VALUES
--('ed57aaff-693e-4c55-ba7d-d33dd9846ffd', 'vjx8t7ta9k9', 'Bulk upload using commandline', '0', '2020-04-16 15:57:35', 'SU', '2020-04-16 15:57:35', 'SU');

--For testing getUserIdsWithoutGoals
--INSERT INTO `goal_assignee_settings_versions` (`id`, `goal_definition_id`, `goal_assignee_id`, `recurring_frequency`, `user_id`, `target`, `start_date`, `end_date`, `is_recurring`, `version`, `created_date`, `last_updated_by`, `created_by`, `last_updated_date`) VALUES
--('2174cdc6-d9d5-43a7-8362-42a5e544c405', '89e089f4-d033-4d61-a5a0-d23ac8ab599c', '45684f5c-0df0-4c27-adf5-2da11ee94a11', 'monthly', 'lala201', '49', '2020-01-01 00:00:00', '2021-01-01 00:00:00', '1', '36ecf3nxw29', '2020-05-26 16:47:49', 'SU', 'SU', '2020-05-26 16:47:49'),
--('21affeda-710f-457d-9c09-6c2628354052', 'cdaa42d8-95a9-47f3-946d-85c059809bdf', '16de7b23-7be0-47ac-a12e-86b224effe21', 'weekly', 'test', '20', '2019-09-12 11:31:23', '2019-10-10 11:31:23', '1', '4a4e47472b', '2020-05-26 13:25:09', 'admin', 'admin', '2020-05-26 13:25:09'),
--('3179111d-d3dd-44e7-8818-2de5b4935109', 'cdaa42d8-95a9-47f3-946d-85c059809bdf', '56f41fc4-745e-475c-bd1c-ed9d5828e8ef', 'monthly', 'SU', '55', '2020-01-01 00:00:00', '2021-01-01 00:00:00', '1', 'xv2ick2csor', '2020-05-21 14:07:58', 'SU', 'SU', '2020-05-21 14:07:58'),
--('46a15186-b5ee-4b32-a079-a64bbe766f4e', '89e089f4-d033-4d61-a5a0-d23ac8ab599c', 'b6bcf575-fdeb-442a-82c3-0582a5fa6c3f', 'monthly', 'shazam', '49', '2020-01-01 00:00:00', '2021-01-01 00:00:00', '1', 'y2tvox647vi', '2020-05-26 16:39:26', 'SU', 'SU', '2020-05-26 16:39:26');