package tests.ct.com.getvymo.platform.goals.app.sample;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.getvymo.platform.goals.springconfig.constants.AppConstants;
import com.getvymo.platform.goals.springconfig.database.DatabaseConfig;
import com.getvymo.platform.goals.springconfig.database.JPAConfig;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.UUID;
import lombok.extern.log4j.Log4j2;
import org.jeasy.random.EasyRandom;
import org.jeasy.random.EasyRandomParameters;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import tests.ct.com.getvymo.platform.goals.app.sample.SampleTests2.SampleTestsConfig;

@SpringBootTest(
    webEnvironment = WebEnvironment.MOCK,
    properties = {"spring.cloud.config.enabled=false"})
@TestPropertySource(locations = {"classpath:tests/ct/configserver/app/sample/sample.properties"})
@ContextConfiguration(classes = {SampleTestsConfig.class})
@ActiveProfiles("ct")
@Log4j2
public class SampleTests2 {

  // @Test
  public void importValidConfigToRepository() throws Exception {
    String uuid = UUID.randomUUID().toString();
    // given
    HttpHeaders headers = new HttpHeaders();
    headers.add(AppConstants.USERID, "12345");
    headers.add(AppConstants.CUSTOMERID, "0123456789");
    headers.add(AppConstants.TENANTID, "test@gmail.com");
    headers.add(AppConstants.CORRID, uuid);
    headers.add(AppConstants.IDEMPOTENCYKEY, uuid);
    headers.add(AppConstants.TRACEID, uuid);

    //    File sampleDataFile =
    //        new ClassPathResource(
    //                "tests/ct/configserver/app/sample/fec.json",
    // SampleTests.class.getClassLoader())
    //            .getFile();
    //
    //    String data = Files.asCharSource(sampleDataFile, Charset.forName("UTF-8")).read();

    MvcResult result =
        this.mockMvc
            .perform(
                post("/vo-ss-config-service/v1/configurations")
                    .headers(headers)
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .content(""))
            .andExpect(status().isCreated())
            .andExpect(jsonPath("$.id").exists())
            .andReturn();
    log.debug(
        "response :{}",
        () -> {
          try {
            return result.getResponse().getContentAsString();
          } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
          }
        });
  }

  @BeforeEach
  void setup(WebApplicationContext wac) {
    this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
  }

  private MockMvc mockMvc;

  private EasyRandomParameters parameters =
      new EasyRandomParameters()
          .seed(123L)
          .objectPoolSize(100)
          .randomizationDepth(3)
          .charset(Charset.forName("UTF-8"))
          .stringLengthRange(5, 10)
          .collectionSizeRange(1, 3)
          .scanClasspathForConcreteTypes(true)
          .overrideDefaultInitialization(false)
          .ignoreRandomizationErrors(true);
  private EasyRandom easyRandom = new EasyRandom(parameters);

  /**
   * Define test configurations here
   *
   * @author rohitdevindreshkulshestha
   */
  @Configuration
  @ComponentScan(
      basePackages = {
        "com.getvymo.platform.goals.springconfig.common",
        "com.getvymo.platform.goals.app.api.internal.migrate",
        "com.getvymo.platform.goals.app.service.migrate"
      })
  @Import(value = {DatabaseConfig.class, JPAConfig.class})
  // @EnableConfigurationProperties(value = {CommonConfigs.class, DatabaseConfig.class})
  static class SampleTestsConfig {}
}
