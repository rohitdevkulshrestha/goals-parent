package tests.ut.archunit;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.lang.ArchRule;
import com.tngtech.archunit.lang.syntax.ArchRuleDefinition;

public class BasicArchConstraintsTests {

  // @ArchTest
  public static final ArchRule myRule1 =
      ArchRuleDefinition.classes()
          .that()
          .resideInAPackage("com.getvymo.platform.goals.app.api")
          .should()
          .accessClassesThat()
          .resideInAnyPackage(
              "com.getvymo.platform.goals.app.service", "com.getvymo.platform.goals.app.utils");

  // @ArchTest
  public static final ArchRule myRule2 =
      ArchRuleDefinition.noClasses()
          .that()
          .resideInAPackage("com.getvymo.platform.goals.app.api")
          .should()
          .accessClassesThat()
          .resideInAnyPackage(
              "com.getvymo.platform.goals.app.persistence",
              "com.getvymo.platform.goals.app.config",
              "com.getvymo.platform.goals.app.api");

  // @ArchTest
  public static final ArchRule myRule3 =
      noClasses()
          .that()
          .resideInAPackage("com.getvymo.platform.goals.app.persistence")
          .and()
          .resideInAPackage("com.getvymo.platform.goals.app.config")
          .and()
          .resideInAPackage("com.getvymo.platform.goals.app.api")
          .should()
          .accessClassesThat()
          .resideInAnyPackage("com.getvymo.platform.goals.app.api");

  private static final JavaClasses classes =
      new ClassFileImporter().importPackages("com.getvymo.platform.goals.app");
}
