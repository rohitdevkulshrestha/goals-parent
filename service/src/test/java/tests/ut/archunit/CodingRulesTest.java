package tests.ut.archunit;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;
import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.fields;
import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.methods;
import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;
import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noMethods;
import static com.tngtech.archunit.library.GeneralCodingRules.NO_CLASSES_SHOULD_ACCESS_STANDARD_STREAMS;
import static com.tngtech.archunit.library.GeneralCodingRules.NO_CLASSES_SHOULD_THROW_GENERIC_EXCEPTIONS;

import java.sql.SQLException;

import org.apache.logging.log4j.Logger;
import org.springframework.http.ResponseEntity;

import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.lang.ArchRule;
import com.tngtech.archunit.lang.CompositeArchRule;

@AnalyzeClasses(packages = "com.getvymo.platform.goals")
public class CodingRulesTest {
  // @ArchTest
  private final ArchRule loggersShouldBePrivateStaticFinal =
      fields()
          .that()
          .haveRawType(Logger.class)
          .should()
          .bePrivate()
          .andShould()
          .beStatic()
          .andShould()
          .beFinal()
          .because("we agreed on this convention");

  // @ArchTest private final ArchRule noJavaUtilLogging = NO_CLASSES_SHOULD_USE_JAVA_UTIL_LOGGING;

  // @ArchTest private final ArchRule noJodaTime = NO_CLASSES_SHOULD_USE_JODATIME;

  // @ArchTest
  static final ArchRule noClassesShouldAccessStandardStreamsOrThrowGenericExceptions =
      CompositeArchRule.of(NO_CLASSES_SHOULD_ACCESS_STANDARD_STREAMS)
          .and(NO_CLASSES_SHOULD_THROW_GENERIC_EXCEPTIONS);

  // @ArchTest
  static final ArchRule DaosMustResideInADaoPackage =
      classes()
          .that()
          .haveNameMatching(".*Repository")
          .should()
          .resideInAPackage("..persistence.dao..")
          .as("DAOs should reside in a package '..domain.repositories..'");

  //  @ArchTest
  //  static final ArchRule entitiesMustResideInADomainPackage =
  //      classes()
  //          .that()
  //          .areAnnotatedWith(Entity.class)
  //          .should()
  //          .resideInAPackage("..domain.data..")
  //          .as("Entities should reside in a package '..domain.data..'");
  //
  //  @ArchTest
  //  static final ArchRule only_DAOs_may_use_the_EntityManager =
  //      noClasses()
  //          .that()
  //          .resideOutsideOfPackage("..domain.repositories..")
  //          .should()
  //          .accessClassesThat()
  //          .areAssignableTo(EntityManager.class)
  //          .as("Only DAOs may use the " + EntityManager.class.getSimpleName());

  // @ArchTest
  static final ArchRule DAOs_must_not_throw_SQLException =
      noMethods()
          .that()
          .areDeclaredInClassesThat()
          .haveNameMatching(".*Dao")
          .should()
          .declareThrowableOfType(SQLException.class);

  // @ArchTest
  static final ArchRule interfaces_should_not_have_names_ending_with_the_word_interface =
      noClasses().that().areInterfaces().should().haveNameMatching(".*Interface");

  // @ArchTest
  static final ArchRule interfaces_should_not_have_names_ending_with_the_word_intf =
      noClasses().that().areInterfaces().should().haveNameMatching(".*Intf");

  //  @ArchTest
  static final ArchRule
      interfaces_should_not_have_simple_class_names_containing_the_word_interface =
          noClasses().that().areInterfaces().should().haveSimpleNameContaining("Interface");

  // @ArchTest
  static final ArchRule interfaces_must_not_be_placed_in_implementation_packages =
      noClasses().that().resideInAPackage("..impl..").should().beInterfaces();

  // @ArchTest
  static final ArchRule api_should_not_access_dao =
      noClasses()
          .that()
          .resideInAPackage("..app.api..")
          .should()
          .accessClassesThat()
          .resideInAPackage("..persistence.dao..");

  // @ArchTest
  static ArchRule all_public_methods_in_the_controller_layer_should_return_API_response_wrappers =
      methods()
          .that()
          .areDeclaredInClassesThat()
          .resideInAPackage("..app.api..")
          .and()
          .arePublic()
          .should()
          .haveRawReturnType(ResponseEntity.class)
          .because(
              "we do not want to couple the client code directly to the return types of the encapsulated module");
}
