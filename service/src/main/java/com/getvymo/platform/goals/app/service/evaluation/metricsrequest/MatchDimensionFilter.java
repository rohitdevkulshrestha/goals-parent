package com.getvymo.platform.goals.app.service.evaluation.metricsrequest;

public class MatchDimensionFilter extends DimensionFilter {
  private String value;

  public MatchDimensionFilter(String type, String dimension, String value, String attribute) {
    super(type, dimension, attribute);
    this.value = value;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }
}
