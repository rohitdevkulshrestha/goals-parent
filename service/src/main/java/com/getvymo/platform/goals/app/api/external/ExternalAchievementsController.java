package com.getvymo.platform.goals.app.api.external;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.getvymo.platform.goals.app.common.models.BulkExternalAchievementRequest;
import com.getvymo.platform.goals.app.common.models.ExternalAchievement;
import com.getvymo.platform.goals.app.common.models.StandardListRequest;
import com.getvymo.platform.goals.app.service.BulkExternalAchievementResponse;
import com.getvymo.platform.goals.app.service.ExternalAchievementApiObject;
import com.getvymo.platform.goals.app.service.external.achievement.IExternalAchievementsService;

@RestController
@RequestMapping("/goals/v1/clients/{clientId}/external")
public class ExternalAchievementsController {

  @Autowired private IExternalAchievementsService iExternalAchievementsService;

  @PostMapping(name = "/", consumes = "application/json")
  public ExternalAchievementApiObject saveExternalAchievement(
      @Valid @RequestBody ExternalAchievement externalAchievementSettings,
      @Valid @PathVariable String clientId) {
    return iExternalAchievementsService.saveExternalAchievement(
        clientId, externalAchievementSettings);
  }

  @PostMapping(value = "/bulk", consumes = "application/json")
  public BulkExternalAchievementResponse saveExternalAchievementInBulk(
      @Valid @RequestBody
          StandardListRequest<BulkExternalAchievementRequest> externalAchievementSettingsList,
      @Valid @PathVariable String clientId) {
    return iExternalAchievementsService.bulkSaveExternalAchievements(
        clientId, externalAchievementSettingsList);
  }
}
