package com.getvymo.platform.goals.app.common.models;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import java.util.Date;

@JsonDeserialize(as = OneTimeGoalAssigneeSettings.class)
public class OneTimeGoalAssigneeSettings extends GoalAssigneeSettings {
  public OneTimeGoalAssigneeSettings() {}

  public OneTimeGoalAssigneeSettings(
      GoalOccurrence goalOccurrence,
      Double target,
      Date startDate,
      Date endDate,
      Boolean deleted,
      String userId,
      String goalDefinitionCode) {
    super(goalOccurrence, target, startDate, endDate, deleted, userId, goalDefinitionCode);
  }
}
