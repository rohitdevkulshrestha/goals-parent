package com.getvymo.platform.goals.app.persistence.entities;

import java.util.Date;
import javax.persistence.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class GoalAuditableEntity {

  // created_by, updated_by added manually to required columns. The time stamps are automatically
  // added by spring boot

  @CreationTimestamp
  @Basic(optional = false)
  @Temporal(TemporalType.TIMESTAMP)
  @Column(
      name = "created_date",
      updatable = false,
      insertable = true,
      columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
  private Date createdDate;

  @UpdateTimestamp
  @Basic(optional = false)
  @Temporal(TemporalType.TIMESTAMP)
  @Column(
      name = "last_updated_date",
      updatable = true,
      insertable = true,
      columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
  private Date lastUpdatedDate;

  public Date getCreatedDate() {
    return createdDate;
  }

  public Date getLastUpdatedDate() {
    return lastUpdatedDate;
  }

  public void setCreatedDate(Date createdDate) {
    this.createdDate = createdDate;
  }

  public void setLastUpdatedDate(Date lastUpdatedDate) {
    this.lastUpdatedDate = lastUpdatedDate;
  }
}
