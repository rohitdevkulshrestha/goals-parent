/** */
package com.getvymo.platform.goals.app.utils.azurevault;

import com.microsoft.azure.keyvault.KeyVaultClient;
import com.microsoft.azure.keyvault.models.SecretBundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/** @author Aman */
@Configuration
public class AzureVaultConfigImpl implements AzureVaultConfig {

  @Value("${azure.vault.name}")
  private String vaultName;

  @Value("${azure.clientId}")
  private String clientId;

  @Value("${azure.clientKey}")
  private String clientKey;

  @Value("${azure.key.suffix}")
  private String passwordKeySuffix;

  @Value("${config.elastic.user}")
  private String esUser;

  private AzureKeyVaultStorage vaultStorage;

  private static Logger logger =
      LoggerFactory.getLogger(AzureVaultConfigImpl.class.getCanonicalName());

  @Bean
  public KeyVaultClient getKeyVaultClient() {
    return new KeyVaultClient(new ClientSecretKeyVaultCredential(clientId, clientKey));
  }

  private String getVaultUrl() {
    StringBuilder sb = new StringBuilder();
    sb.append("https://");
    sb.append(vaultName);
    sb.append(".vault.azure.net");
    return sb.toString();
  }

  private String fetchValueForKey(String key) {
    SecretBundle secret = getKeyVaultClient().getSecret(getVaultUrl(), key);
    if (secret != null) {
      return secret.value();
    }
    logger.error("unable to retrieve secret value for key: " + key);
    return "";
  }

  @Override
  @Bean
  public AzureKeyVaultStorage initialiseVaultStorage() throws IllegalAccessException {
    AzureKeyVaultStorage storage = new AzureKeyVaultStorage();
    logger.info("esUser is:" + getEsUser());
    String esPasswordKey = getEsPasswordKey();
    String value = fetchValueForKey(esPasswordKey);
    if (value == "") {
      logger.error(
          "ES user's password could not be retrieved, ES calls might fail, if auth is enabled");
    }
    storage.setSecretKeyValue(esPasswordKey, value);
    setVaultStorage(storage);
    return vaultStorage;
  }

  @Override
  public AzureKeyVaultStorage getVaultStorage() {
    return vaultStorage;
  }

  @Override
  public String getESPassword() {
    return vaultStorage.getSecretKeyValue(getEsPasswordKey());
  }

  private String getEsPasswordKey() {
    String esPasswordKey = esUser + passwordKeySuffix;
    logger.info("esPassword key is:" + esPasswordKey);
    return esPasswordKey;
  }

  public String getVaultName() {
    return vaultName;
  }

  public void setVaultName(String vaultName) {
    this.vaultName = vaultName;
  }

  public String getClientId() {
    return clientId;
  }

  public void setClientId(String clientId) {
    this.clientId = clientId;
  }

  public String getClientKey() {
    return clientKey;
  }

  public void setClientKey(String clientKey) {
    this.clientKey = clientKey;
  }

  public void setVaultStorage(AzureKeyVaultStorage vaultStorage) {
    this.vaultStorage = vaultStorage;
  }

  public String getEsUser() {
    return esUser;
  }

  public void setEsUser(String esUser) {
    this.esUser = esUser;
  }
}
