package com.getvymo.platform.goals.app.service.evaluation.metricsresponse;

public class GroupByKey {
  public String code;

  public GroupByKey() {}

  public GroupByKey(String code) {
    this.code = code;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }
}
