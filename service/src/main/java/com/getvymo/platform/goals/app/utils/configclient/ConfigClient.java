package com.getvymo.platform.goals.app.utils.configclient;

import in.vymo.configuration.api.Change;
import in.vymo.configuration.api.ConfigurationChangeKafkaListener;
import in.vymo.configuration.api.ConfigurationChangeListener;
import in.vymo.configuration.api.ConfigurationClient;
import in.vymo.configuration.lib.ConfigurationFactory;
import in.vymo.configuration.lib.exceptions.ConfigClientException;
import in.vymo.configuration.lib.models.ConfigChangeEvent;
import in.vymo.core.config.model.client.Client;
import lombok.extern.log4j.Log4j2;

@Log4j2
public enum ConfigClient {
  INSTANCE;
  private static ConfigurationClient configClient = ConfigurationFactory.newClient();

  static boolean successful = true;

  static {
    try {
      long startTime = System.currentTimeMillis();
      System.out.println(
          "Starting application "
              + startTime
              + " with config url "
              + System.getenv("vymo_configuration_url"));
      // TODO: Replace with env variables
      configClient.initialize(
          new ConfigurationFactory.ConfigurationClientBuilder()
              .pollInterval(900000)
              .configServiceURL(System.getenv("vymo_configuration_url"))
              .kafkaUrl("http://localhost:9092"));
      configClient.registerKafkaListener(
          new ConfigurationChangeKafkaListener() {

            public void onConfigChange(ConfigChangeEvent arg0) throws ConfigClientException {
              if (successful) {
                log.info("Received event " + arg0);
                successful = false;
              } else {
                log.info("Received false event " + arg0);
                successful = true;
                throw new ConfigClientException("Returned false");
              }
            }
          });

      configClient.registerListener(
          new ConfigurationChangeListener() {

            public void onPartnerGlobalReferenceChange(Change arg0) {
              // TODO Auto-generated method stub
              log.info("onPartnerGlobalReferenceChange " + arg0);
            }

            public void onModuleReferenceChange(Change arg0) {
              // TODO Auto-generated method stub
              log.info("onModuleReferenceChange " + arg0);
            }

            public void onModuleModified(Change arg0) {
              // TODO Auto-generated method stub
              log.info("onModuleModified " + arg0);
            }

            public void onModuleAdded(Change arg0) {
              // TODO Auto-generated method stub
              log.info("onModuleAdded " + arg0);
            }

            public void onLeadGlobalReferenceChange(Change arg0) {
              // TODO Auto-generated method stub
              log.info("onLeadGlobalReferenceChange " + arg0);
            }

            public void onClientReferenceChange(Change arg0) {
              // TODO Auto-generated method stub
              log.info("onClientReferenceChange " + arg0);
            }

            public void onClientModified(Change arg0) {
              // TODO Auto-generated method stub
              log.info("onClientModified " + arg0);
            }

            public void onClientGlobalReferenceChange(Change arg0) {
              // TODO Auto-generated method stub
              log.info("onClientGlobalReferenceChange " + arg0);
            }

            public void onClientDeactivated(Change arg0) {
              // TODO Auto-generated method stub
              log.info("onClientDeactivated " + arg0);
            }

            public void onClientAdded(Change arg0) {
              // TODO Auto-generated method stub
              log.info("onClientAdded " + arg0);
            }

            public void onClientActivated(Change arg0) {
              // TODO Auto-generated method stub
              log.info("onClientActivated " + arg0);
            }

            public void onActivityGlobalReferenceChange(Change arg0) {
              // TODO Auto-generated method stub
              log.info("onActivityGlobalReferenceChange " + arg0);
            }
          });
      log.info("Starting to poll");
      configClient.startPolling();
      log.info(
          "Ending poll at {} with current version {}" + (System.currentTimeMillis() - startTime),
          configClient.getCurrentVersion().getVersion());
      Client client = configClient.getClient("demo");
      log.info("Client details successfully returned {}", client.getName());
    } catch (Exception e) {
      log.error("Exception while config initialisation-> {}", e);
      // when config init failed we are not allowing the application to start
      System.exit(2); // 2 is for critical
    }
  }

  public static ConfigurationClient client() {
    return configClient;
  }
}
