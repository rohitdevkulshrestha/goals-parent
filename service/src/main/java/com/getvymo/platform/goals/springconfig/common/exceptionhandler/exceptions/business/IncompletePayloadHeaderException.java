package com.getvymo.platform.goals.springconfig.common.exceptionhandler.exceptions.business;

import com.getvymo.platform.goals.springconfig.common.exceptionhandler.exceptions.BusinessException;
import java.net.URI;
import org.zalando.problem.StatusType;

public class IncompletePayloadHeaderException extends BusinessException {

  public IncompletePayloadHeaderException(
      URI type, String title, StatusType status, String detail) {
    super(type, title, status, detail);
    // TODO Auto-generated constructor stub
  }
}
