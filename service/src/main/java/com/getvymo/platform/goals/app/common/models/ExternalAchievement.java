package com.getvymo.platform.goals.app.common.models;

import java.util.Date;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

public class ExternalAchievement {

  private String goalDefinitionCode;

  private String userId;

  @Temporal(TemporalType.TIMESTAMP)
  private Date startDate;

  @Temporal(TemporalType.TIMESTAMP)
  private Date endDate;

  private Double achievement;

  private Boolean isRecurring;

  private String recurringFrequency;

  private String lastUpdatedBy;

  private Date achievementUpdatedDate;

  private Boolean deleted;

  private String typeOfAchievement;

  private String dataType;

  public ExternalAchievement() {}

  public ExternalAchievement(
      String goalDefinitionCode,
      String userId,
      Date startDate,
      Date endDate,
      Double achievement,
      Boolean isRecurring,
      String recurringFrequency,
      String lastUpdatedBy,
      Date achievementUpdateDate,
      Boolean deleted,
      String typeOfAchievement,
      String dataType) {
    this.goalDefinitionCode = goalDefinitionCode;
    this.userId = userId;
    this.startDate = startDate;
    this.endDate = endDate;
    this.achievement = achievement;
    this.isRecurring = isRecurring;
    this.recurringFrequency = recurringFrequency;
    this.lastUpdatedBy = lastUpdatedBy;
    this.achievementUpdatedDate = achievementUpdateDate;
    this.deleted = deleted;
    this.typeOfAchievement = typeOfAchievement;
    this.dataType = dataType;
  }

  public String getDataType() {
    return dataType;
  }

  public void setDataType(String dataType) {
    this.dataType = dataType;
  }

  public String getTypeOfAchievement() {
    return typeOfAchievement;
  }

  public void setTypeOfAchievement(String typeOfAchievement) {
    this.typeOfAchievement = typeOfAchievement;
  }

  public String getGoalDefinitionCode() {
    return goalDefinitionCode;
  }

  public void setGoalDefinitionCode(String goalDefinitionCode) {
    this.goalDefinitionCode = goalDefinitionCode;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public Date getStartDate() {
    return startDate;
  }

  public void setStartDate(Date startDate) {
    this.startDate = startDate;
  }

  public Date getEndDate() {
    return endDate;
  }

  public void setEndDate(Date endDate) {
    this.endDate = endDate;
  }

  public Double getAchievement() {
    return achievement;
  }

  public void setAchievement(Double achievement) {
    this.achievement = achievement;
  }

  public Boolean getRecurring() {
    return isRecurring;
  }

  public void setRecurring(Boolean recurring) {
    isRecurring = recurring;
  }

  public String getRecurringFrequency() {
    return recurringFrequency;
  }

  public void setRecurringFrequency(String recurringFrequency) {
    this.recurringFrequency = recurringFrequency;
  }

  public String getLastUpdatedBy() {
    return lastUpdatedBy;
  }

  public void setLastUpdatedBy(String lastUpdatedBy) {
    this.lastUpdatedBy = lastUpdatedBy;
  }

  public Date getAchievementUpdatedDate() {
    return achievementUpdatedDate;
  }

  public void setAchievementUpdatedDate(Date achievementUpdatedDate) {
    this.achievementUpdatedDate = achievementUpdatedDate;
  }

  public Boolean getDeleted() {
    return deleted;
  }

  public void setDeleted(Boolean deleted) {
    this.deleted = deleted;
  }
}
