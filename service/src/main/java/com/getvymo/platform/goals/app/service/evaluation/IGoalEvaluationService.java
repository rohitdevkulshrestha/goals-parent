package com.getvymo.platform.goals.app.service.evaluation;

import com.getvymo.platform.goals.app.service.evaluation.metricsrequest.BusinessMetricsSummaryRequest;
import com.getvymo.platform.goals.app.service.evaluation.metricsresponse.MetricsResponse;
import in.vymo.configuration.lib.exceptions.ConfigClientException;
import java.util.Date;
import java.util.List;

public interface IGoalEvaluationService {

  public MetricsResponse getAchievementDataForGoal(
      BusinessMetricsSummaryRequest metricsSummaryRequest);

  public MetricsResponse getAchievementDataForExternalGoal(
      String clientId,
      String userId,
      List<String> teamUserIds,
      String goalDefinitionCode,
      Date startDate,
      Date endDate,
      Boolean isRecurring,
      String recurringFrequency,
      Boolean isUserGroupBy,
      Boolean isDateGroupBy)
      throws ConfigClientException;
}
