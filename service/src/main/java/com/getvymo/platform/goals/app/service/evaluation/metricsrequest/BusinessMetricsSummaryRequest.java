package com.getvymo.platform.goals.app.service.evaluation.metricsrequest;

import java.util.List;

public class BusinessMetricsSummaryRequest {
  private String client;
  private List<String> regions; // userIds
  private List<String> dataSet;
  private List<String> metrics;
  private List<Filter> filters;
  private List<GroupBy> groupBys;

  public BusinessMetricsSummaryRequest() {}

  public BusinessMetricsSummaryRequest(
      String client,
      List<String> regions,
      List<String> dataSet,
      List<String> metrics,
      List<Filter> filters,
      DateRangeObject date,
      List<GroupBy> groupBys) {
    this.client = client;
    this.regions = regions;
    this.dataSet = dataSet;
    this.metrics = metrics;
    this.filters = filters;
    //        this.date = date;
    this.groupBys = groupBys;
  }

  public List<GroupBy> getGroupBys() {
    return groupBys;
  }

  public void setGroupBys(List<GroupBy> groupBys) {
    this.groupBys = groupBys;
  }

  public String getClient() {
    return client;
  }

  public void setClient(String client) {
    this.client = client;
  }

  public List<String> getRegions() {
    return regions;
  }

  public void setRegions(List<String> regions) {
    this.regions = regions;
  }

  public List<String> getDataSet() {
    return dataSet;
  }

  public void setDataSet(List<String> dataSet) {
    this.dataSet = dataSet;
  }

  public List<String> getMetrics() {
    return metrics;
  }

  public void setMetrics(List<String> metrics) {
    this.metrics = metrics;
  }

  public List<Filter> getFilters() {
    return filters;
  }

  public void setFilters(List<Filter> filters) {
    this.filters = filters;
  }

  @Override
  public String toString() {
    return "BusinessMetricsSummaryRequest{"
        + "client='"
        + client
        + '\''
        + ", regions="
        + regions
        + ", dataSet="
        + dataSet
        + ", metrics="
        + metrics
        + ", filters="
        + filters
        + ", groupBys="
        + groupBys
        + '}';
  }
  //    public DateRangeObject getDate() {
  //        return date;
  //    }
  //
  //    public void setDate(DateRangeObject date) {
  //        this.date = date;
  //    }
}
