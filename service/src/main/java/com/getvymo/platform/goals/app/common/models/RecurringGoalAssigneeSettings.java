package com.getvymo.platform.goals.app.common.models;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import java.util.Date;

@JsonDeserialize(as = RecurringGoalAssigneeSettings.class)
public class RecurringGoalAssigneeSettings extends GoalAssigneeSettings {
  // Comes from config service
  private String recurringFrequency;

  public RecurringGoalAssigneeSettings() {}

  public String getRecurringFrequency() {
    return recurringFrequency;
  }

  public void setRecurringFrequency(String recurringFrequency) {
    this.recurringFrequency = recurringFrequency;
  }

  public RecurringGoalAssigneeSettings(
      GoalOccurrence goalOccurrence,
      Double target,
      Date startDate,
      Date endDate,
      String recurringFrequency,
      Boolean deleted,
      String userId,
      String goalDefinitionCode) {
    super(goalOccurrence, target, startDate, endDate, deleted, userId, goalDefinitionCode);
    this.recurringFrequency = recurringFrequency;
  }
}
