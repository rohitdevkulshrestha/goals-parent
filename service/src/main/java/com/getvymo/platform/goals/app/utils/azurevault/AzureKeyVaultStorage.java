package com.getvymo.platform.goals.app.utils.azurevault;

import java.util.HashMap;
import java.util.Map;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class AzureKeyVaultStorage {

  Map<String, String> secretKeyMap = new HashMap<>();

  public String getSecretKeyValue(String key) {
    if (!secretKeyMap.containsKey(key)) {
      log.error("secret key " + key + " is not present in the map, returning empty value");
      return "";
    }
    String secretKeyValue = secretKeyMap.get(key);
    return secretKeyValue;
  }

  public void setSecretKeyValue(String key, String value) {
    if (secretKeyMap.containsKey(key)) {
      log.warn(
          "secret key " + key + " is already present in the map, overriding the secret key value");
    }
    secretKeyMap.put(key, value);
  }
}
