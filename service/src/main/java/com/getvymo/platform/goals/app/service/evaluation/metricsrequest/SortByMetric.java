package com.getvymo.platform.goals.app.service.evaluation.metricsrequest;

public class SortByMetric extends SortBy {
  private String metric;

  public SortByMetric(String metric) {
    super();
    this.metric = metric;
  }

  public SortByMetric(String type, boolean asc, String metric) {
    super(type, asc);
    this.metric = metric;
  }

  public String getMetric() {
    return metric;
  }

  public void setMetric(String metric) {
    this.metric = metric;
  }
}
