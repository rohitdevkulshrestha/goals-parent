package com.getvymo.platform.goals.app.service.evaluation.goalsresponse;

public class GoalsGroupedByDate {
  private Double value;
  private String date;
  private String dataType;

  public GoalsGroupedByDate() {}

  public GoalsGroupedByDate(Double value, String date, String dataType) {
    this.value = value;
    this.date = date;
    this.dataType = dataType;
  }

  public String getDataType() {
    return dataType;
  }

  public void setDataType(String dataType) {
    this.dataType = dataType;
  }

  public Double getValue() {
    return value;
  }

  public void setValue(Double value) {
    this.value = value;
  }

  public String getDate() {
    return date;
  }

  public void setDate(String date) {
    this.date = date;
  }
}
