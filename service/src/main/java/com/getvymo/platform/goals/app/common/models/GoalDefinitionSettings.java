package com.getvymo.platform.goals.app.common.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import in.vymo.core.config.model.goals.*;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(
    use = JsonTypeInfo.Id.NAME,
    include = JsonTypeInfo.As.PROPERTY,
    property = "type",
    visible = true)
@JsonSubTypes({
  @JsonSubTypes.Type(value = VymoMetricGoalDefinitionSettings.class, name = "internal"),
  @JsonSubTypes.Type(value = ExternalGoalDefinitionSettings.class, name = "external"),
  @JsonSubTypes.Type(value = ComputedGoalDefinitionSettings.class, name = "computed")
})
public abstract class GoalDefinitionSettings {

  @Enumerated(EnumType.STRING)
  private String name;

  private String description;
  private String metric;
  private GoalScope scope;
  private GoalType type;
  private String goalDefinitionCode;
  private Boolean external;
  private Boolean showAchievementList;
  private Integer goalOrder;
  private Boolean managerDrillDownEnabled;
  private Boolean hideShortfall;

  public GoalDefinitionSettings() {}

  public GoalDefinitionSettings(
      String name,
      String description,
      String metric,
      GoalScope scope,
      GoalType type,
      String goalDefinitionCode,
      Boolean external,
      Boolean showAchievementList,
      Integer goalOrder,
      Boolean managerDrillDownEnabled,
      Boolean hideShortfall) {
    this.name = name;
    this.description = description;
    this.metric = metric;
    this.scope = scope;
    this.type = type;
    this.goalDefinitionCode = goalDefinitionCode;
    this.external = external;
    this.showAchievementList = showAchievementList;
    this.goalOrder = goalOrder;
    this.managerDrillDownEnabled = managerDrillDownEnabled;
    this.hideShortfall = hideShortfall;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getMetric() {
    return metric;
  }

  public void setMetric(String metric) {
    this.metric = metric;
  }

  public GoalScope getScope() {
    return scope;
  }

  public void setScope(GoalScope scope) {
    this.scope = scope;
  }

  public GoalType getType() {
    return type;
  }

  public void setType(GoalType type) {
    this.type = type;
  }

  public String getGoalDefinitionCode() {
    return goalDefinitionCode;
  }

  public void setGoalDefinitionCode(String goalDefinitionCode) {
    this.goalDefinitionCode = goalDefinitionCode;
  }

  public Boolean getExternal() {
    return external;
  }

  public void setExternal(Boolean external) {
    this.external = external;
  }

  public Boolean getShowAchievementList() {
    return showAchievementList;
  }

  public void setShowAchievementList(Boolean showAchievementList) {
    this.showAchievementList = showAchievementList;
  }

  public Integer getGoalOrder() {
    return goalOrder;
  }

  public void setGoalOrder(Integer goalOrder) {
    this.goalOrder = goalOrder;
  }

  public Boolean getManagerDrillDownEnabled() {
    return managerDrillDownEnabled;
  }

  public void setManagerDrillDownEnabled(Boolean managerDrillDownEnabled) {
    this.managerDrillDownEnabled = managerDrillDownEnabled;
  }

  public Boolean getHideShortfall() {
    return hideShortfall;
  }

  public void setHideShortfall(Boolean hideShortfall) {
    this.hideShortfall = hideShortfall;
  }

  @Override
  public String toString() {
    return "GoalDefinitionSettings{"
        + "name='"
        + name
        + '\''
        + ", description='"
        + description
        + '\''
        + ", metric='"
        + metric
        + '\''
        + ", scope="
        + scope
        + ", type="
        + type
        + ", goalDefinitionCode='"
        + goalDefinitionCode
        + '\''
        + ", external="
        + external
        + ", showAchievementList="
        + showAchievementList
        + ", goalOrder="
        + goalOrder
        + '}';
  }
}
