package com.getvymo.platform.goals.app.common.models;

import java.util.ArrayList;

public class StandardListRequest<T> {
  ArrayList<T> data;

  public StandardListRequest() {}

  public StandardListRequest(ArrayList<T> data) {
    this.data = data;
  }

  public ArrayList<T> getData() {
    return data;
  }

  public void setData(ArrayList<T> data) {
    this.data = data;
  }
}
