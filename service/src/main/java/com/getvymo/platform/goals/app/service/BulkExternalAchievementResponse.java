package com.getvymo.platform.goals.app.service;

import java.util.List;

public class BulkExternalAchievementResponse {
  private String status;
  private List<BulkGoalAchievement> results;

  public BulkExternalAchievementResponse() {}

  public BulkExternalAchievementResponse(String status, List<BulkGoalAchievement> results) {
    this.status = status;
    this.results = results;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public List<BulkGoalAchievement> getResults() {
    return results;
  }

  public void setResults(List<BulkGoalAchievement> results) {
    this.results = results;
  }
}
