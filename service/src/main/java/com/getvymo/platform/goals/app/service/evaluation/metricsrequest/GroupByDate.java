package com.getvymo.platform.goals.app.service.evaluation.metricsrequest;

import in.vymo.core.config.model.core.DATE_GRANULARITY;

/** Created by debashish on 19/08/19. */
public class GroupByDate extends GroupBy {
  private String dateDimension; // created_date, schedule_date
  private DATE_GRANULARITY granularity;

  public GroupByDate(String dateDimension, DATE_GRANULARITY granularity) {
    this.dateDimension = dateDimension;
    this.granularity = granularity;
  }

  public GroupByDate(String type, String dateDimension, DATE_GRANULARITY granularity) {
    super(type);
    this.dateDimension = dateDimension;
    this.granularity = granularity;
  }

  public String getDateDimension() {
    return dateDimension;
  }

  public void setDateDimension(String dateDimension) {
    this.dateDimension = dateDimension;
  }

  public DATE_GRANULARITY getGranularity() {
    return granularity;
  }

  public void setGranularity(DATE_GRANULARITY granularity) {
    this.granularity = granularity;
  }

  /*
  {type: 'date', dateDimension: 'created_date', granularity : 'day'}
   */
}
