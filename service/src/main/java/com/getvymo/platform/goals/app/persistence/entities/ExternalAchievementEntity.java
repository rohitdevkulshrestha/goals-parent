package com.getvymo.platform.goals.app.persistence.entities;

import com.getvymo.platform.goals.app.common.models.ExternalAchievementEntry;
import com.getvymo.platform.goals.app.service.utils.GoalsUtils;
import java.util.Date;
import java.util.HashMap;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.stereotype.Component;

@Component
// TODO: Dynamic index based on client id
public class ExternalAchievementEntity {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private String id;

  private String goalDefinitionCode;

  private String clientId;

  private String userId;

  @Temporal(TemporalType.TIMESTAMP)
  private Date startDate;

  @Temporal(TemporalType.TIMESTAMP)
  private Date endDate;

  private Double achievement;

  private Boolean isRecurring;

  private String recurringFrequency;

  private String metric;

  private HashMap<Long, ExternalAchievementEntry> achievementsMap;

  private String createdBy;

  private String lastUpdatedBy;

  private Boolean deleted;

  private Boolean isActive;

  @CreationTimestamp
  @Basic(optional = false)
  @Temporal(TemporalType.TIMESTAMP)
  @Column(
      name = "created_date",
      updatable = false,
      insertable = true,
      columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
  private Date createdDate;

  @Temporal(TemporalType.TIMESTAMP)
  private Date lastUpdatedDate;

  private Date achievementUpdatedDate;
  private String dataType;

  public ExternalAchievementEntity() {}

  public ExternalAchievementEntity(
      String goalDefinitionCode,
      String clientId,
      String userId,
      Date startDate,
      Date endDate,
      Double achievement,
      Boolean isRecurring,
      String recurringFrequency,
      String metric,
      HashMap<Long, ExternalAchievementEntry> achievementsMap,
      String lastUpdatedBy,
      Boolean deleted,
      String dataType,
      Date achievementUpdatedDate) {
    this.userId = userId;
    this.metric = metric;
    this.achievementsMap = achievementsMap;
    this.dataType = dataType == null ? "number" : dataType;
    this.id = GoalsUtils.generateRandomStringId();
    this.goalDefinitionCode = goalDefinitionCode;
    this.clientId = clientId;
    this.startDate = startDate;
    this.endDate = endDate;
    this.achievement = achievement;
    this.isRecurring = isRecurring;
    this.recurringFrequency = recurringFrequency;
    this.createdBy = lastUpdatedBy;
    this.lastUpdatedBy = lastUpdatedBy;
    this.deleted = deleted;
    this.achievementUpdatedDate = achievementUpdatedDate;
    this.lastUpdatedDate = new Date();
    this.createdDate = new Date();
  }

  public Date getAchievementUpdatedDate() {
    return achievementUpdatedDate;
  }

  public void setAchievementUpdatedDate(Date achievementUpdatedDate) {
    this.achievementUpdatedDate = achievementUpdatedDate;
  }

  public String getDataType() {
    return dataType;
  }

  public void setDataType(String dataType) {
    this.dataType = dataType;
  }

  public Double getAchievement() {
    return achievement;
  }

  public void setAchievement(Double achievement) {
    this.achievement = achievement;
  }

  public String getMetric() {
    return metric;
  }

  public void setMetric(String metric) {
    this.metric = metric;
  }

  public HashMap<Long, ExternalAchievementEntry> getAchievementsMap() {
    return achievementsMap;
  }

  public void setAchievementsMap(HashMap<Long, ExternalAchievementEntry> achievementsMap) {
    this.achievementsMap = achievementsMap;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getId() {
    return id;
  }

  public String getGoalDefinitionCode() {
    return goalDefinitionCode;
  }

  public void setGoalDefinitionCode(String goalDefinitionCode) {
    this.goalDefinitionCode = goalDefinitionCode;
  }

  public String getClientId() {
    return clientId;
  }

  public void setClientId(String clientId) {
    this.clientId = clientId;
  }

  public Date getStartDate() {
    return startDate;
  }

  public void setStartDate(Date startDate) {
    this.startDate = startDate;
  }

  public Date getEndDate() {
    return endDate;
  }

  public void setEndDate(Date endDate) {
    this.endDate = endDate;
  }

  public Boolean getRecurring() {
    return isRecurring;
  }

  public void setRecurring(Boolean recurring) {
    isRecurring = recurring;
  }

  public String getRecurringFrequency() {
    return recurringFrequency;
  }

  public void setRecurringFrequency(String recurringFrequency) {
    this.recurringFrequency = recurringFrequency;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public String getLastUpdatedBy() {
    return lastUpdatedBy;
  }

  public void setLastUpdatedBy(String lastUpdatedBy) {
    this.lastUpdatedBy = lastUpdatedBy;
  }

  public Boolean getDeleted() {
    return deleted;
  }

  public void setDeleted(Boolean deleted) {
    this.deleted = deleted;
  }

  public Boolean getActive() {
    return isActive;
  }

  public void setActive(Boolean active) {
    isActive = active;
  }

  public Date getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(Date createdDate) {
    this.createdDate = createdDate;
  }

  public Date getLastUpdatedDate() {
    return lastUpdatedDate;
  }

  public void setLastUpdatedDate(Date lastUpdatedDate) {
    this.lastUpdatedDate = lastUpdatedDate;
  }

  @Override
  public String toString() {
    return "ExternalAchievementEntity{"
        + "id='"
        + id
        + '\''
        + ", goalDefinitionCode='"
        + goalDefinitionCode
        + '\''
        + ", clientId='"
        + clientId
        + '\''
        + ", startDate="
        + startDate
        + ", endDate="
        + endDate
        + ", achievement="
        + achievement
        + ", isRecurring="
        + isRecurring
        + ", recurringFrequency='"
        + recurringFrequency
        + '\''
        + ", createdBy='"
        + createdBy
        + '\''
        + ", lastUpdatedBy='"
        + lastUpdatedBy
        + '\''
        + ", deleted="
        + deleted
        + ", isActive="
        + isActive
        + ", createdDate="
        + createdDate
        + ", lastUpdatedDate="
        + lastUpdatedDate
        + '}';
  }
}
