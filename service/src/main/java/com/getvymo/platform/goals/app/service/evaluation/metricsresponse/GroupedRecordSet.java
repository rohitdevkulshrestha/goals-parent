package com.getvymo.platform.goals.app.service.evaluation.metricsresponse;

import java.util.List;

public class GroupedRecordSet {
  public String type;
  public GROUP_BY_TYPE group_by_type;
  public List<MetricAchievementObject> metrics;
  public GroupByKey group_by_key;

  public GroupedRecordSet() {}

  public GroupedRecordSet(
      String type,
      GROUP_BY_TYPE group_by_type,
      List<MetricAchievementObject> metrics,
      GroupByKey group_by_key) {
    this.type = type;
    this.group_by_type = group_by_type;
    this.metrics = metrics;
    this.group_by_key = group_by_key;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public GROUP_BY_TYPE getGroup_by_type() {
    return group_by_type;
  }

  public void setGroup_by_type(GROUP_BY_TYPE group_by_type) {
    this.group_by_type = group_by_type;
  }

  public List<MetricAchievementObject> getMetrics() {
    return metrics;
  }

  public void setMetrics(List<MetricAchievementObject> metrics) {
    this.metrics = metrics;
  }

  public GroupByKey getGroup_by_key() {
    return group_by_key;
  }

  public void setGroup_by_key(GroupByKey group_by_key) {
    this.group_by_key = group_by_key;
  }

  public enum GROUP_BY_TYPE {
    user,
    date
  }
}
