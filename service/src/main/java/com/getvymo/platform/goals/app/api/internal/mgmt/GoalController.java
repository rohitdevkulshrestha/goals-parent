package com.getvymo.platform.goals.app.api.internal.mgmt;

import java.util.Collections;
import java.util.Map;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GoalController {

  @GetMapping(path = "/health", produces = "application/json")
  public Map<String, String> getServiceHealth() {

    return Collections.singletonMap("health", "success");
  }

  @GetMapping(path = "/goals/health", produces = "application/json")
  public Map<String, String> getGoalsServiceHealth() {

    return Collections.singletonMap("health", "success");
  }
}
