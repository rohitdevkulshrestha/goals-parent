package com.getvymo.platform.goals.app.service.goalsmgmt;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.getvymo.platform.goals.app.common.models.GoalAchievementSettingsWithoutAssignment;
import com.getvymo.platform.goals.app.common.models.GoalAssigneeSettings;
import com.getvymo.platform.goals.app.common.models.GoalAssigneeSettingsWithoutVersion;
import com.getvymo.platform.goals.app.common.models.GoalAssignmentOperationMetaData;
import com.getvymo.platform.goals.app.common.models.GoalEntryType;
import com.getvymo.platform.goals.app.common.models.GoalGroupSettings;
import com.getvymo.platform.goals.app.common.models.GoalOccurrence;
import com.getvymo.platform.goals.app.common.models.RecurringGoalAssigneeSettings;
import com.getvymo.platform.goals.app.common.models.StandardListRequest;
import com.getvymo.platform.goals.app.common.models.StandardListResponse;
import com.getvymo.platform.goals.app.errors.GoalManagementServiceException;
import com.getvymo.platform.goals.app.errors.RecordNotFoundException;
import com.getvymo.platform.goals.app.persistence.dao.GoalAssigneeRepository;
import com.getvymo.platform.goals.app.persistence.dao.GoalAssigneeSettingsVersionsRepository;
import com.getvymo.platform.goals.app.persistence.dao.GoalAssignmentOperationVersionsRepository;
import com.getvymo.platform.goals.app.persistence.dao.GoalInstanceCustomRepository;
import com.getvymo.platform.goals.app.persistence.dao.GoalInstanceRepository;
import com.getvymo.platform.goals.app.persistence.entities.GoalAssigneeSettingsVersionsEntity;
import com.getvymo.platform.goals.app.persistence.entities.GoalAssigneesEntity;
import com.getvymo.platform.goals.app.persistence.entities.GoalAssignmentOperationVersionsEntity;
import com.getvymo.platform.goals.app.persistence.entities.GoalInstanceEntity;
import com.getvymo.platform.goals.app.service.ActivateVersionResponse;
import com.getvymo.platform.goals.app.service.BulkGoalAssignment;
import com.getvymo.platform.goals.app.service.GoalAssignment;
import com.getvymo.platform.goals.app.service.GoalAssignmentOperationVersion;
import com.getvymo.platform.goals.app.service.UserGoalAssignment;
import com.getvymo.platform.goals.app.service.evaluation.GoalEvaluationService;
import com.getvymo.platform.goals.app.service.evaluation.goalsresponse.GoalAchievement;
import com.getvymo.platform.goals.app.service.evaluation.goalsresponse.UserObject;
import com.getvymo.platform.goals.app.service.evaluation.metricsrequest.BusinessMetricsSummaryRequest;
import com.getvymo.platform.goals.app.service.evaluation.metricsrequest.DateFilter;
import com.getvymo.platform.goals.app.service.evaluation.metricsrequest.Filter;
import com.getvymo.platform.goals.app.service.evaluation.metricsrequest.GroupBy;
import com.getvymo.platform.goals.app.service.evaluation.metricsrequest.GroupByDate;
import com.getvymo.platform.goals.app.service.evaluation.metricsrequest.GroupByUser;
import com.getvymo.platform.goals.app.service.evaluation.metricsrequest.UserFilter;
import com.getvymo.platform.goals.app.service.evaluation.metricsrequest.UserFilterEnum;
import com.getvymo.platform.goals.app.service.evaluation.metricsrequest.UserGroupByEnum;
import com.getvymo.platform.goals.app.service.evaluation.metricsresponse.GroupByKey;
import com.getvymo.platform.goals.app.service.evaluation.metricsresponse.GroupedRecordSet;
import com.getvymo.platform.goals.app.service.evaluation.metricsresponse.MetricAchievementObject;
import com.getvymo.platform.goals.app.service.evaluation.metricsresponse.MetricsResponse;
import com.getvymo.platform.goals.app.service.evaluation.metricsresponse.MetricsResultObject;
import com.getvymo.platform.goals.app.service.utils.GoalsUtils;
import com.getvymo.platform.goals.app.utils.configclient.ConfigClient;
import com.getvymo.platform.goals.app.utils.configclient.ConfigUtil;
import com.getvymo.platform.goals.app.utils.configclient.ConfigUtils;
import com.getvymo.platform.goals.app.utils.kafka.KafkaMessage;
import com.getvymo.platform.goals.app.utils.kafka.KafkaProducer;
import in.vymo.configuration.api.ConfigurationClient;
import in.vymo.configuration.lib.exceptions.ConfigClientException;
import in.vymo.core.config.model.client.Client;
import in.vymo.core.config.model.core.DATE_GRANULARITY;
import in.vymo.core.config.model.core.metric.Metric;
import in.vymo.core.config.model.goals.ComputedGoalDefinition;
import in.vymo.core.config.model.goals.GoalAssignmentRBACSettings;
import in.vymo.core.config.model.goals.GoalDefinition;
import in.vymo.core.config.model.goals.GoalDefinitionMappings;
import in.vymo.core.config.model.goals.GoalGroupDefinition;
import in.vymo.core.config.model.goals.GoalMetricFilter;
import in.vymo.core.config.model.goals.GoalType;
import in.vymo.core.config.model.goals.GoalsConfig;
import in.vymo.core.config.model.goals.VymoMetricGoalDefinition;
import in.vymo.core.config.model.utils.JSONUtils;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TimeZone;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;
import javax.transaction.Transactional;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
@Log4j2
public class GoalManagementService implements IGoalManagementService {
  @Autowired GoalAssigneeRepository goalAssigneeRepository;
  @Autowired GoalAssigneeSettingsVersionsRepository goalAssigneeSettingsVersionsRepository;
  @Autowired GoalInstanceRepository goalInstanceRepository;
  @Autowired GoalAssignmentOperationVersionsRepository goalAssignmentOperationVersionsRepository;
  @Autowired GoalInstanceCustomRepository goalInstanceCustomRepository;
  @Autowired GoalEvaluationService goalEvaluationService;
  @Autowired GoalsUtils goalsUtils;
  ConfigUtil configUtil = new ConfigUtil();
  @Autowired private ConfigUtils configUtils;
  @Autowired private ConfigClient configClient;
  @Autowired private KafkaProducer kafkaProducer;

  private ConfigurationClient client = ConfigClient.client();
  private String NOTIFICATION_KAFKA_TOPIC = "goal_notifications";

  @Override
  public GoalDefinition getGoalDefinitionByCode(String goalDefinitionCode, String clientId) {
    GoalDefinition goalDefinition =
        configUtil.getGoalDefinitionByCode(clientId, goalDefinitionCode);
    if (goalDefinition == null) {
      log.error("Goal definition doesn't exist for id {} ", goalDefinitionCode);
      throw new RecordNotFoundException("Goal definition doesn't exist " + goalDefinitionCode);
    }
    return goalDefinition;
  }

  @Override
  public StandardListResponse getAllGoalDefinitions(String clientId, int pageNumber, int pageSize) {
    List<GoalDefinition> allGoalDefinitions = new ArrayList<>();

    Pageable page = PageRequest.of(pageNumber, pageSize);

    Map<String, GoalDefinition> allDefinitionEntities =
        configUtil.getAllGoalDefinitionsByClient(clientId);

    long totalResultsCount = allDefinitionEntities.size();
    log.info("Total available result are => {}", totalResultsCount);
    for (GoalDefinition goalDefinition : allDefinitionEntities.values()) {
      allGoalDefinitions.add(goalDefinition);
    }

    StandardListResponse slr =
        new StandardListResponse(
            totalResultsCount,
            page.getPageNumber(),
            page.getPageSize(),
            (ArrayList<GoalDefinition>) allGoalDefinitions);

    return slr;
  }

  @Transactional
  public GoalAssignmentOperationVersion createGoalAssignmentOperationVersion(
      GoalAssignmentOperationMetaData auditInfo) throws GoalManagementServiceException {
    log.info(
        "In createGoalAssignmentOperationVersion with for GoalAssignmentOperationMetaData",
        auditInfo.toString());
    GoalAssignmentOperationVersionsEntity goalAssignmentOperationVersionsEntity =
        new GoalAssignmentOperationVersionsEntity(
            auditInfo.getVersion(),
            auditInfo.getUpdater(),
            auditInfo.getUpdater(),
            auditInfo.getDescription(),
            auditInfo.getEntryType());

    GoalAssignmentOperationVersionsEntity savedEntity =
        goalAssignmentOperationVersionsRepository.save(goalAssignmentOperationVersionsEntity);
    log.info("Saved GoalAssignmentOperationVersionsEntity");
    log.info(
        "Returning from createGoalAssignmentOperationVersion with GoalAssignmentOperationVersion");
    return new GoalAssignmentOperationVersion(
        savedEntity.getVersion(),
        savedEntity.getCreatedDate(),
        savedEntity.getLastUpdatedDate(),
        savedEntity.getCreatedBy(),
        savedEntity.getLastUpdatedBy());
  }

  /**
   * Called by the REST api to create goal assignments for the batch of users We take userID +
   * goal_definition_code + recurring frequency + start_date + end_date as unique constraint If
   * there are any assignment matching the unique constraints the goal assignment will be updated &
   * versioned If there aren't any, new ones will be created
   *
   * @param clientId
   * @param userIds
   * @return
   * @throws GoalManagementServiceException
   */
  @Override
  @Transactional
  public ActivateVersionResponse setGoalForUsersWithoutVersion(
      String clientId,
      List<String> userIds,
      String goalDefinitionCode,
      GoalAssigneeSettingsWithoutVersion goalAssigneeSettingsWithoutVersion,
      String userUpdating)
      throws GoalManagementServiceException {

    log.info("In setGoalForUsersWithoutVersion for goalDefinitionCode {}, userIds {}", userIds);
    GoalAssigneeSettings goalAssigneeSettings =
        goalAssigneeSettingsWithoutVersion.getGoalAssigneeSettings();
    String version = GoalsUtils.generateRandomShortString();

    log.info("Creating GoalAssignmentOperationVersionsEntity with version {}", version);
    GoalAssignmentOperationVersionsEntity goalAssignmentOperationVersionsEntity =
        new GoalAssignmentOperationVersionsEntity(
            version,
            userUpdating,
            userUpdating,
            "Goal assignment via UI",
            GoalEntryType.USER_ASSIGNMENT);
    GoalAssignmentOperationVersionsEntity savedOperationVersionsEntity =
        goalAssignmentOperationVersionsRepository.saveAndFlush(
            goalAssignmentOperationVersionsEntity);
    log.info("Created GoalAssignmentOperationVersionsEntity");
    // Set goals
    setGoalsWithRetry(
        clientId, userIds, goalDefinitionCode, goalAssigneeSettings, savedOperationVersionsEntity);
    // Activate them
    return activateGoalAssignmentOperationVersion(version, clientId);
  }

  @Transactional
  private void createKafkaMessageAndSend(
      String version, List<GoalAssigneesEntity> newGoalAssigneeEntities) {
    if (newGoalAssigneeEntities == null) {
      log.warn("Can't find the version ", version);
      throw new RecordNotFoundException("Given version: " + version + " doesn't exist");
    }
    List<String> goalAssigneeEntityIds = new ArrayList<>();
    for (GoalAssigneesEntity newGoalAssigneeEntity : newGoalAssigneeEntities) {
      goalAssigneeEntityIds.add(newGoalAssigneeEntity.getId());
    }
    List<GoalAssigneesEntity> goalAssigneesEntities =
        goalAssigneeRepository.findAllById(goalAssigneeEntityIds);

    try {
      for (GoalAssigneesEntity goalAssigneesEntity : goalAssigneesEntities) {
        if (!goalAssigneesEntity.getDeleted() && goalAssigneesEntity.getActive()) {
          KafkaMessage kafkaMessage =
              new KafkaMessage(
                  goalAssigneesEntity.getId(),
                  goalAssigneesEntity.getClientId(),
                  goalAssigneesEntity.getGoalDefinitionCode(),
                  goalAssigneesEntity.getUserId(),
                  goalAssigneesEntity.getCreatedBy(),
                  "success",
                  "GOAL_ASSIGNED");
          kafkaProducer.sendMessage(
              JSONUtils.mapper().writeValueAsString(kafkaMessage), NOTIFICATION_KAFKA_TOPIC);
        }
      }
    } catch (JsonProcessingException e) {
      log.error("Unable to process message before sending {}");
    } catch (Exception e) {
      log.error("Unable to do kafka Send process->{}", e.getMessage());
    }
  }

  private List<GoalAssignment> setGoalsWithRetry(
      String clientId,
      List<String> userIds,
      String goalDefinitionCode,
      GoalAssigneeSettings goalAssigneeSettings,
      GoalAssignmentOperationVersionsEntity savedOperationVersionsEntity) {
    List<GoalAssignment> goalAssignments = new ArrayList<>();
    int i = 0;
    // Retry logic to deal with LockAcquisitionException
    while (i < 3) {
      try {
        log.warn("Trying setGoalsForUsers for {} time ", i);
        goalAssignments =
            setGoalsForUsers(
                clientId,
                userIds,
                goalDefinitionCode,
                goalAssigneeSettings,
                savedOperationVersionsEntity);
        break;
      } catch (DataAccessException e) {
        log.error("In exception block of setGoalsWithRetry due to {}", e.getLocalizedMessage(), e);
        if (i == 3) {
          throw e;
        }
        try {
          Thread.sleep(500 * (i + 1));
        } catch (InterruptedException ex) {
          log.error("Exception while setGoalsWithRetry ", ex);
        }
        log.error("RETRYING due to LockAcquisitionException for {} time", i);
        i++;
      } catch (Exception e) {
        log.error("Catching other unknown exception {}", e.getLocalizedMessage(), e);
        throw e;
      }
    }
    return goalAssignments;
  }

  @Override
  @Transactional
  public List<GoalAssignment> setGoalForUsersWithVersion(
      String clientId,
      List<String> userIds,
      String goalDefinitionCode,
      GoalAssigneeSettings goalAssigneeSettings,
      String version,
      String userUpdating)
      throws GoalManagementServiceException {
    log.info("In setGoalForUsersWithVersion for version {} and userIds {} ", version, userIds);
    // Fetch the object
    GoalAssignmentOperationVersionsEntity savedOperationVersionsEntity =
        goalAssignmentOperationVersionsRepository.findByVersion(version);
    if (savedOperationVersionsEntity == null) {
      log.error("Can't find specified version " + version);
      throw new GoalManagementServiceException("Specified version " + version + " doesn't exist");
    }
    return setGoalsWithRetry(
        clientId, userIds, goalDefinitionCode, goalAssigneeSettings, savedOperationVersionsEntity);
  }

  @Override
  @Transactional
  public List<GoalAssignment> bulkSetGoalForUsersWithVersion(
      String clientId,
      StandardListRequest<GoalAssigneeSettings> goalAssigneeSettingsList,
      String version,
      String userUpdating)
      throws GoalManagementServiceException {

    log.info(
        "In bulkSetGoalForUsersWithVersion for version {} and goalAssigneeSettingsList of size {} ",
        version,
        goalAssigneeSettingsList.getData().size());
    // Fetch the object
    GoalAssignmentOperationVersionsEntity savedOperationVersionsEntity =
        goalAssignmentOperationVersionsRepository.findByVersion(version);
    if (savedOperationVersionsEntity == null) {
      log.error("Can't find specified version " + version);
      throw new GoalManagementServiceException("Specified version " + version + " doesn't exist");
    }

    ArrayList<GoalAssigneeSettings> assigneeSettingsListData = goalAssigneeSettingsList.getData();

    return bulkSetGoalsForUsers(
        clientId, userUpdating, assigneeSettingsListData, savedOperationVersionsEntity);
  }

  @Transactional
  public List<GoalAssignment> bulkSetGoalsForUsers(
      String clientId,
      String userUpdating,
      List<GoalAssigneeSettings> goalAssigneeSettingsList,
      GoalAssignmentOperationVersionsEntity goalAssignmentOperationVersionsEntity) {

    /**
     * 1. For all users check if there is an existing goal_assignee_id which exists for the matching
     * criteria for the current version 2. Create entries in goal_assignees table table with given
     * version for users who do not have a goal assignee id - bulk save 3. For all users create a
     * goal_assignee_settings_versions entry with the new version - bulk save 4. Create goal
     * instances for all goal_assignee_id/start_date/end_date tuples (insert IGNORE)
     */
    List<GoalAssigneesEntity> allGoalAssigneeEntities = new ArrayList<>();
    List<GoalAssignment> allGoalAssignments = new ArrayList<>();
    List<GoalInstanceEntity> allGoalInstanceEntities = new ArrayList<>();
    List<GoalAssigneeSettingsVersionsEntity>
        allOffsetCorrectedGoalAssigneeSettingsVersionsEntities = new ArrayList<>();
    int counter = 0;
    GoalAssigneeSettings firstGoalAssignment = goalAssigneeSettingsList.get(0);
    Integer beginningRowIndex = firstGoalAssignment.getRow();
    if (null == beginningRowIndex) {
      log.error("Row/Index id is sent as null. Setting it to 0");
      beginningRowIndex = beginningRowIndex != null ? beginningRowIndex : 0;
    }

    log.info(
        "In setGoalsForUsers with version entity "
            + goalAssignmentOperationVersionsEntity.getVersion());
    String version = goalAssignmentOperationVersionsEntity.getVersion();

    try {
      for (GoalAssigneeSettings goalAssigneeSettings : goalAssigneeSettingsList) {
        counter++;
        String goalDefinitionCode = goalAssigneeSettings.getGoalDefinitionCode();
        List<String> userIds = Arrays.asList(goalAssigneeSettings.getUserId());

        log.info("goalAssigneeSettings is {} ", goalAssigneeSettings);
        log.info("goalDefinitionCode is {} & userIds are {} ", goalDefinitionCode, userIds);

        Double target = goalAssigneeSettings.getTarget();
        String createdBy = goalAssignmentOperationVersionsEntity.getCreatedBy();
        String lastUpdatedBy = goalAssignmentOperationVersionsEntity.getLastUpdatedBy();

        // If passed existing goal assignments will be marked deleted & new ones will be created as
        // deleted
        boolean shouldDeleteGoalAssignments =
            goalAssigneeSettings.getDeleted() != null ? goalAssigneeSettings.getDeleted() : false;

        log.info("shouldDeleteGoalAssignments is " + shouldDeleteGoalAssignments);

        // We are doing this to prevent null values, if not passed consider as false
        goalAssigneeSettings.setDeleted(shouldDeleteGoalAssignments);

        // Date from where the goal assignment starts
        Date startDate = goalsUtils.roundOffMillisInDate(goalAssigneeSettings.getStartDate());

        // Date where the goal assignment ends
        Date endDate = goalsUtils.roundOffMillisInDate(goalAssigneeSettings.getEndDate());

        // is set to true if it's recurring (from rest API params)
        boolean isRecurring = goalAssigneeSettings.getGoalOccurrence() == GoalOccurrence.recurring;

        // Initialisations
        String recurringFrequency = null;

        DATE_GRANULARITY granularity = null;
        RecurringGoalAssigneeSettings rGoalAssigneeSettings = null;
        TimeZone clientTimezone = configUtils.getClientTimezone(clientId);

        // Get goaldefinition entity for given id
        GoalDefinition goalDefinition = null;
        if (shouldDeleteGoalAssignments) {
          goalDefinition = configUtil.getGoalDefinitionByCode(clientId, goalDefinitionCode);
        } else {
          goalDefinition = configUtil.getEnabledGoalDefinitionByCode(clientId, goalDefinitionCode);
        }

        // Check if self assignment is disabled
        // Ignore this check for SU
        GoalsConfig goalsConfig = configUtil.getGoalsConfig(clientId);
        GoalAssignmentRBACSettings goalAssignmentRBACSettings =
            goalsConfig.getGoalAssignmentRBACSettings();
        boolean isSelfAssignmentEnabled = false;
        if (goalAssignmentRBACSettings != null) {
          isSelfAssignmentEnabled = goalAssignmentRBACSettings.isEnableSelfAssignments();
        }
        if (userUpdating.equals("SU")) {
          isSelfAssignmentEnabled = true;
        }

        if (!isSelfAssignmentEnabled && userUpdating.equals(goalAssigneeSettings.getUserId())) {
          throw new GoalManagementServiceException(
              "Assigning goals for self is not allowed. Please contact your Vymo admin for more info");
        }

        // Check if given goal definition id exists
        // for recurring goals get granularity, frequency and number of instances to be
        // created
        if (isRecurring) {
          log.debug("It's a recurring goal");
          rGoalAssigneeSettings = (RecurringGoalAssigneeSettings) goalAssigneeSettings;
          recurringFrequency = rGoalAssigneeSettings.getRecurringFrequency();
          granularity = configUtils.getDateGranularity(recurringFrequency, clientId);
          log.debug("Fetched granularity -> {} ", granularity);
        }

        // Initialise the array of goal assignments to be created

        List<GoalAssigneeSettingsVersionsEntity> existingGoalAssignments =
            goalsUtils.getExistingGoalAssigneeSettingsVersionEntities(
                isRecurring,
                userIds,
                recurringFrequency,
                goalDefinitionCode,
                startDate,
                endDate,
                clientId);

        goalsUtils.validateNewGoalAssignmentWithExistingAssignments(
            existingGoalAssignments, goalAssigneeSettings, clientId);

        // We create update & create new goal assignments - and merge them together
        // before sending

        // Get users without existing goal assignments - create goal assignments
        List<String> usersWithoutGoals =
            goalsUtils.getUserIdsWithoutGoals(existingGoalAssignments, userIds);

        List<GoalAssigneesEntity> goalAssigneeEntities = new ArrayList<GoalAssigneesEntity>();
        List<GoalAssigneesEntity> goalAssigneeEntitiesForUsersWithoutGoalAssignments =
            goalsUtils.createGoalAssigneeEntitiesForUsersWithoutGoalAssignments(
                usersWithoutGoals,
                clientId,
                goalDefinitionCode,
                version,
                createdBy,
                shouldDeleteGoalAssignments);

        List<GoalInstanceEntity> existingGoalInstanceEntities = new ArrayList<>();
        // append existing goal_assignee entities
        log.debug(
            "Appending existing goal_assignee entities from {} existingGoalAssignments ",
            existingGoalAssignments.size());
        for (GoalAssigneeSettingsVersionsEntity goalAssigneeSettingsVersionsEntity :
            existingGoalAssignments) {
          GoalAssigneesEntity assigneesEntity =
              goalAssigneeSettingsVersionsEntity.getGoalAssigneesEntity();
          //                    assigneesEntity.setDeleted(goalAssigneeSettings.getDeleted());
          //
          // existingGoalInstanceEntities.addAll(assigneesEntity.getGoalInstances());
          goalAssigneeEntities.add(assigneesEntity);
        }

        // Disable all the goal instances in the matching/overlapping period
        for (GoalInstanceEntity existingGoalInstanceEntity : existingGoalInstanceEntities) {
          //                    existingGoalInstanceEntity.setDeleted(true);
        }

        // Append new goal_assignee entites to be created
        log.debug(
            "Append new {} goal_assignee entites (goalAssigneeEntitiesForUsersWithoutGoalAssignments) to goalAssigneeEntities to "
                + "be created ",
            goalAssigneeEntitiesForUsersWithoutGoalAssignments.size());
        goalAssigneeEntities.addAll(goalAssigneeEntitiesForUsersWithoutGoalAssignments);

        List<GoalAssigneeSettingsVersionsEntity> createdGoalAssigneeSettingsVersions =
            goalsUtils.createNewGoalAssigneeSettingsVersionEntities(
                goalAssigneeEntities,
                goalDefinitionCode,
                recurringFrequency,
                target,
                startDate,
                endDate,
                isRecurring,
                createdBy,
                lastUpdatedBy,
                version,
                clientId,
                shouldDeleteGoalAssignments);

        log.info(
            "There are => {} usersWithoutGoals out of given {} userIds",
            usersWithoutGoals.size(),
            userIds.size());

        // create goal_instance for all goal_assignee_settings given:
        log.debug("create goal_instance for all goal_assignee_settings given");
        List<GoalInstanceEntity> goalInstanceEntities =
            goalsUtils.createGoalInstancesFromGoalAssigneeSettingsVersionEntities(
                createdGoalAssigneeSettingsVersions,
                isRecurring,
                createdBy,
                lastUpdatedBy,
                granularity,
                clientId,
                goalDefinitionCode);

        List<GoalAssigneeSettingsVersionsEntity>
            offsetCorrectedGoalAssigneeSettingsVersionsEntities =
                goalsUtils.getOffsetCorrectedGoalAssigneeSettingsVersionsEntities(
                    createdGoalAssigneeSettingsVersions, clientTimezone);
        allGoalAssigneeEntities.addAll(goalAssigneeEntities);
        allGoalInstanceEntities.addAll(goalInstanceEntities);
        allOffsetCorrectedGoalAssigneeSettingsVersionsEntities.addAll(
            offsetCorrectedGoalAssigneeSettingsVersionsEntities);
      }
    } catch (Exception e) {
      String existingErrorMessage = e.getLocalizedMessage();
      int actualRowIndex = beginningRowIndex + counter;
      log.error(
          "Error while processing " + actualRowIndex + "th row. Error is " + existingErrorMessage,
          e);
      throw new GoalManagementServiceException(
          "Error while processing " + actualRowIndex + "th row. Error is " + existingErrorMessage);
    }

    log.info("Beggining index is {} ", beginningRowIndex);

    return persistGoalAssignments(
        clientId,
        allGoalAssigneeEntities,
        allGoalAssignments,
        allGoalInstanceEntities,
        allOffsetCorrectedGoalAssigneeSettingsVersionsEntities,
        beginningRowIndex,
        version);
  }

  @Transactional
  private List<GoalAssignment> setGoalsForUsers(
      String clientId,
      List<String> userIds,
      String goalDefinitionCode,
      GoalAssigneeSettings goalAssigneeSettings,
      GoalAssignmentOperationVersionsEntity goalAssignmentOperationVersionsEntity)
      throws GoalManagementServiceException {

    /**
     * 1. For all users check if there is an existing goal_assignee_id which exists for the matching
     * criteria for the current version 2. Create entries in goal_assignees table table with given
     * version for users who do not have a goal assignee id - bulk save 3. For all users create a
     * goal_assignee_settings_versions entry with the new version - bulk save 4. Create goal
     * instances for all goal_assignee_id/start_date/end_date tuples (insert IGNORE)
     */
    log.info(
        "In setGoalsForUsers with version entity "
            + goalAssignmentOperationVersionsEntity.getVersion());
    log.info("goalAssigneeSettings is {} ", goalAssigneeSettings);
    log.info("goalDefinitionCode is {} & userIds are {} ", goalDefinitionCode, userIds);

    Double target = goalAssigneeSettings.getTarget();
    String version = goalAssignmentOperationVersionsEntity.getVersion();

    String createdBy = goalAssignmentOperationVersionsEntity.getCreatedBy();
    String lastUpdatedBy = goalAssignmentOperationVersionsEntity.getLastUpdatedBy();

    // If passed existing goal assignments will be marked deleted & new ones will be created as
    // deleted
    boolean shouldDeleteGoalAssignments =
        goalAssigneeSettings.getDeleted() != null ? goalAssigneeSettings.getDeleted() : false;

    log.info("shouldDeleteGoalAssignments is " + shouldDeleteGoalAssignments);

    // We are doing this to prevent null values, if not passed consider as false
    goalAssigneeSettings.setDeleted(shouldDeleteGoalAssignments);

    // Date from where the goal assignment starts
    Date startDate = goalsUtils.roundOffMillisInDate(goalAssigneeSettings.getStartDate());

    // Date where the goal assignment ends
    Date endDate = goalsUtils.roundOffMillisInDate(goalAssigneeSettings.getEndDate());

    // is set to true if it's recurring (from rest API params)
    boolean isRecurring = goalAssigneeSettings.getGoalOccurrence() == GoalOccurrence.recurring;

    // Initialisations
    String recurringFrequency = null;

    DATE_GRANULARITY granularity = null;
    RecurringGoalAssigneeSettings rGoalAssigneeSettings = null;
    TimeZone clientTimezone = configUtils.getClientTimezone(clientId);

    // Get goaldefinition entity for given id
    GoalDefinition goalDefinition = null;
    if (shouldDeleteGoalAssignments) {
      goalDefinition = configUtil.getGoalDefinitionByCode(clientId, goalDefinitionCode);
    } else {
      goalDefinition = configUtil.getEnabledGoalDefinitionByCode(clientId, goalDefinitionCode);
    }
    // Check if given goal definition id exists
    if (goalDefinition == null) {
      log.info("Couldn't find goalDefinitionEntity for code " + goalDefinitionCode);
      throw new GoalManagementServiceException(
          "Goal definition code " + goalDefinitionCode + " not found");
    }

    log.info("goalDefinitionEntity found is " + goalDefinitionCode);

    // for recurring goals get granularity, frequency and number of instances to be
    // created
    if (isRecurring) {
      log.debug("It's a recurring goal");
      rGoalAssigneeSettings = (RecurringGoalAssigneeSettings) goalAssigneeSettings;
      recurringFrequency = rGoalAssigneeSettings.getRecurringFrequency();
      granularity = configUtils.getDateGranularity(recurringFrequency, clientId);
      log.debug("Fetched granularity -> {} ", granularity);
    }

    // Initialise the array of goal assignments to be created

    List<GoalAssigneeSettingsVersionsEntity> existingGoalAssignments =
        goalsUtils.getExistingGoalAssigneeSettingsVersionEntities(
            isRecurring,
            userIds,
            recurringFrequency,
            goalDefinitionCode,
            startDate,
            endDate,
            clientId);

    // We create update & create new goal assignments - and merge them together
    // before sending

    // Get users without existing goal assignments - create goal assignments
    List<String> usersWithoutGoals =
        goalsUtils.getUserIdsWithoutGoals(existingGoalAssignments, userIds);

    List<GoalAssigneesEntity> goalAssigneeEntities = new ArrayList<GoalAssigneesEntity>();
    List<GoalAssigneesEntity> goalAssigneeEntitiesForUsersWithoutGoalAssignments =
        goalsUtils.createGoalAssigneeEntitiesForUsersWithoutGoalAssignments(
            usersWithoutGoals,
            clientId,
            goalDefinitionCode,
            version,
            createdBy,
            shouldDeleteGoalAssignments);

    List<GoalInstanceEntity> existingGoalInstanceEntities = new ArrayList<>();
    // append existing goal_assignee entities
    log.debug(
        "Appending existing goal_assignee entities from {} existingGoalAssignments ",
        existingGoalAssignments.size());
    for (GoalAssigneeSettingsVersionsEntity goalAssigneeSettingsVersionsEntity :
        existingGoalAssignments) {
      GoalAssigneesEntity assigneesEntity =
          goalAssigneeSettingsVersionsEntity.getGoalAssigneesEntity();
      // Validating date when non deleted exiting goal assignment present and
      // shouldDeleteGoalAssignments=false
      //            if (!assigneesEntity.getDeleted() && !shouldDeleteGoalAssignments) {
      //                validateGoalPeriod(goalAssigneeSettingsVersionsEntity, startDate,
      // granularity, clientTimezone);
      //            }
      //            assigneesEntity.setDeleted(goalAssigneeSettings.getDeleted());
      //            existingGoalInstanceEntities.addAll(assigneesEntity.getGoalInstances());
      goalAssigneeEntities.add(assigneesEntity);
    }

    // Disable all the goal instances in the matching/overlapping period
    for (GoalInstanceEntity existingGoalInstanceEntity : existingGoalInstanceEntities) {
      //            existingGoalInstanceEntity.setDeleted(true);
    }

    // Append new goal_assignee entites to be created
    log.debug(
        "Append new {} goal_assignee entites (goalAssigneeEntitiesForUsersWithoutGoalAssignments) to goalAssigneeEntities to "
            + "be created ",
        goalAssigneeEntitiesForUsersWithoutGoalAssignments.size());
    goalAssigneeEntities.addAll(goalAssigneeEntitiesForUsersWithoutGoalAssignments);

    List<GoalAssigneeSettingsVersionsEntity> createdGoalAssigneeSettingsVersions =
        goalsUtils.createNewGoalAssigneeSettingsVersionEntities(
            goalAssigneeEntities,
            goalDefinitionCode,
            recurringFrequency,
            target,
            startDate,
            endDate,
            isRecurring,
            createdBy,
            lastUpdatedBy,
            version,
            clientId,
            shouldDeleteGoalAssignments);

    log.info(
        "There are => {} usersWithoutGoals out of given {} userIds",
        usersWithoutGoals.size(),
        userIds.size());

    List<GoalAssignment> allGoalAssignments = new ArrayList<>();

    // create goal_instance for all goal_assignee_settings given:
    log.debug("create goal_instance for all goal_assignee_settings given");
    List<GoalInstanceEntity> goalInstanceEntities =
        goalsUtils.createGoalInstancesFromGoalAssigneeSettingsVersionEntities(
            createdGoalAssigneeSettingsVersions,
            isRecurring,
            createdBy,
            lastUpdatedBy,
            granularity,
            clientId,
            goalDefinitionCode);

    List<GoalAssigneeSettingsVersionsEntity> offsetCorrectedGoalAssigneeSettingsVersionsEntities =
        goalsUtils.getOffsetCorrectedGoalAssigneeSettingsVersionsEntities(
            createdGoalAssigneeSettingsVersions, clientTimezone);

    return persistGoalAssignments(
        clientId,
        goalAssigneeEntities,
        allGoalAssignments,
        goalInstanceEntities,
        offsetCorrectedGoalAssigneeSettingsVersionsEntities,
        0,
        version);
  }

  private void validateGoalPeriod(
      GoalAssigneeSettingsVersionsEntity goalAssigneeSettingsVersionsEntity,
      Date startDate,
      DATE_GRANULARITY granularity,
      TimeZone clientTimezone) {
    Date existingStartDate = goalAssigneeSettingsVersionsEntity.getStartDate();
    int timezoneOffsetInMillis = goalsUtils.getTimezoneOffset(startDate, clientTimezone);
    Date newStartDate = new Date(startDate.getTime() - (long) timezoneOffsetInMillis);
    if (existingStartDate.getTime() > startDate.getTime()) {
      goalsUtils.getNumberOfGoalInstances(newStartDate, existingStartDate, granularity);
    } else {
      goalsUtils.getNumberOfGoalInstances(existingStartDate, newStartDate, granularity);
    }
  }

  @Transactional
  private List<GoalAssignment> persistGoalAssignments(
      String clientId,
      List<GoalAssigneesEntity> goalAssigneeEntities,
      List<GoalAssignment> allGoalAssignments,
      List<GoalInstanceEntity> goalInstanceEntities,
      List<GoalAssigneeSettingsVersionsEntity> offsetCorrectedGoalAssigneeSettingsVersionsEntities,
      Integer beginningRowIndex,
      String version) {
    /*
     * Step 1: Save all the goal assignee entitiess
     * Step 2: Save all the Goal Assignee Settings Versions Entities
     * Step 3: Get goal assignment POJOs from the saved GoalAssigneesEntity & GoalAssigneeSettingsVersionsEntity
     * Step 4: Insert ignore Goal Instance entities
     * */

    log.info(
        "In persistGoalAssignments to save {} goalAssigneeEntities, {} allGoalAssignments, {} goalInstanceEntities, {} "
            + "offsetCorrectedGoalAssigneeSettingsVersionsEntities for {} client & {} beginningRowIndex",
        goalAssigneeEntities.size(),
        allGoalAssignments.size(),
        goalInstanceEntities.size(),
        offsetCorrectedGoalAssigneeSettingsVersionsEntities.size(),
        clientId,
        beginningRowIndex);
    log.info("Saving goalAssigneeEntities to goalAssigneeRepository");

    List<GoalAssigneesEntity> savedGoalAssigneeEntities =
        goalAssigneeRepository.saveAll(goalAssigneeEntities);

    log.info(
        "Saving createdGoalAssigneeSettingsVersions to goalAssigneeSettingsVersionsRepository");

    List<GoalAssigneeSettingsVersionsEntity> savedGoalAssigneeSettingsVersionsEntities =
        goalAssigneeSettingsVersionsRepository.saveAll(
            offsetCorrectedGoalAssigneeSettingsVersionsEntities);
    log.debug("Getting  goalAssignments to save");

    List<BulkGoalAssignment> goalAssignments =
        goalsUtils.getGoalAssignmentFromGoalAssigneeAndSettings(
            savedGoalAssigneeEntities,
            savedGoalAssigneeSettingsVersionsEntities,
            clientId,
            beginningRowIndex);

    log.info("Got {} goalAssignments to save ", goalAssignments.size());
    goalAssigneeRepository.flush();
    goalAssigneeSettingsVersionsRepository.flush();

    // TODO: Fix the issue where duplicate goal instance entities are created either hard delete
    // them or add constraint on deleted flag
    // goalInstanceRepository.saveAll(existingGoalInstanceEntities);
    goalInstanceCustomRepository.insertIgnoreGoalInstancesWithQuery(goalInstanceEntities);
    log.info("Finished wiring");

    List<String> userIds = new ArrayList<String>(goalAssignments.size());
    for (GoalAssignment goalAssignment : goalAssignments) {
      userIds.add(goalAssignment.getUserId());
    }
    if (!userIds.isEmpty()) {
      goalInstanceCustomRepository.activateGoalInstancesForGivenVersion(version, userIds);
    }
    createKafkaMessageAndSend(version, goalAssigneeEntities);
    allGoalAssignments.addAll(goalAssignments);
    log.info("Returning {} allGoalAssignments ", allGoalAssignments.size());
    return allGoalAssignments;
  }

  @Override
  @Transactional
  public ActivateVersionResponse activateGoalAssignmentOperationVersion(
      String version, String clientId) throws GoalManagementServiceException {
    log.info("IN activateGoalAssignmentOperationVersion for version {}", version);
    log.debug("Searchin for current active version {} ", version);
    GoalAssignmentOperationVersionsEntity entityIdAndVersion =
        goalAssignmentOperationVersionsRepository.findByVersion(version);
    if (entityIdAndVersion == null) {
      log.warn("Can't find the version ", version);
      throw new RecordNotFoundException("Given version: " + version + " doesn't exist");
    }
    log.debug("Activating the goalInstances matching version ", version);
    ActivateVersionResponse activateVersionResponse = new ActivateVersionResponse("success");
    return activateVersionResponse;
  }

  @Override
  @Transactional
  public List<GoalAssignment> disableGoalForUsers(
      String clientId,
      List<String> userIds,
      String goalDefinitionCode,
      Date startDate,
      Date endDate)
      throws GoalManagementServiceException {
    log.info("In disableGoalForUsers");
    log.debug(
        "Fetching goalInstanceEntities from repository matchin userIds {}, clientId {}, goalDefinitionCode {} ",
        userIds,
        clientId,
        goalDefinitionCode);
    List<GoalInstanceEntity> goalInstanceEntities =
        goalInstanceRepository.findByUserIdInAndClientIdAndGoalDefinitionCode(
            userIds, clientId, goalDefinitionCode);
    List<GoalAssignment> goalAssignments = new ArrayList<>();
    if (goalInstanceEntities.size() == 0) {
      log.warn("No goals to disable for given parameters");
      throw new RecordNotFoundException("No goals to disable for given parameters");
    }
    log.debug("Iterating over {} goalInstanceEntities", goalInstanceEntities.size());
    for (GoalInstanceEntity goalInstanceEntity : goalInstanceEntities) {
      if (goalInstanceEntity.getDeleted() == true) {
        throw new GoalManagementServiceException(
            "Goal instance " + goalInstanceEntity.getId() + " is already disabled");
      }
      goalInstanceEntity.setDeleted(true);
      goalAssignments.add(
          goalsUtils.getGoalAssignnmentFromGoalInstanceEntity(
              goalInstanceRepository.save(goalInstanceEntity)));
    }
    log.info("Returning {} goalAssignments", goalAssignments.size());
    return goalAssignments;
  }

  @Override
  @Transactional
  public StandardListResponse getAllGoalAssignments(
      String clientId,
      String goalDefinitionCode,
      List<String> userIds,
      Date startDate,
      Date endDate,
      int pageNumber,
      int pageSize)
      throws GoalManagementServiceException {
    Pageable page = PageRequest.of(pageNumber, pageSize);
    log.info(
        "In getAllGoalAssignments for userIds {} clientId {} goalDefinitionCode {}",
        userIds,
        clientId,
        goalDefinitionCode);
    Page<GoalInstanceEntity> goalInstanceEntitiesPage =
        goalInstanceRepository.findByUserIdInAndClientIdAndGoalDefinitionCode(
            userIds, clientId, goalDefinitionCode, page);
    List<GoalInstanceEntity> goalInstanceEntities = goalInstanceEntitiesPage.getContent();
    ArrayList<GoalAssignment> goalAssignments = new ArrayList<>();
    log.debug("Iterating over goalInstanceEntities");
    for (GoalInstanceEntity goalInstanceEntity : goalInstanceEntities) {
      goalAssignments.add(goalsUtils.getGoalAssignnmentFromGoalInstanceEntity(goalInstanceEntity));
    }
    StandardListResponse slr =
        new StandardListResponse(
            goalInstanceEntitiesPage.getTotalElements(), pageNumber, pageSize, goalAssignments);
    log.info(
        "Returning from getAllGoalAssignments with {} goalAssignments",
        goalInstanceEntitiesPage.getTotalElements());
    return slr;
  }

  @Override
  @Transactional
  public StandardListResponse getAllGoalAssignmentsForUser(
      String clientId, String userId, int pageNumber, int pageSize)
      throws GoalManagementServiceException {
    Pageable page = PageRequest.of(pageNumber, pageSize);
    log.info("In getAllGoalAssignmentsForUser for  userId {} clientId {}", userId, clientId);
    Page<GoalInstanceEntity> goalInstanceEntitiesPage =
        goalInstanceRepository.findByUserIdAndClientId(userId, clientId, page);
    List<GoalInstanceEntity> goalInstanceEntities = goalInstanceEntitiesPage.getContent();
    ArrayList<GoalAssignment> goalAssignments = new ArrayList<>();
    log.info("Iterating over goalInstanceEntities");
    for (GoalInstanceEntity goalInstanceEntity : goalInstanceEntities) {
      goalAssignments.add(goalsUtils.getGoalAssignnmentFromGoalInstanceEntity(goalInstanceEntity));
    }
    StandardListResponse slr =
        new StandardListResponse(
            goalInstanceEntitiesPage.getTotalElements(), pageNumber, pageSize, goalAssignments);
    log.info(
        "Returning from getAllGoalAssignmentsForUser with {} ",
        goalInstanceEntitiesPage.getTotalElements());
    return slr;
  }

  @Override
  @Transactional
  public StandardListResponse getUserActiveGoalAssignments(
      String clientId,
      String userId,
      Boolean isActive,
      Date startDate,
      Date endDate,
      int limit,
      int offset,
      List<String> frequency,
      List<String> goalDefinitionCode) {
    startDate = startDate == null ? new Date() : startDate;
    endDate = endDate == null ? new Date() : endDate;
    isActive = isActive == null ? true : false;
    List<Boolean> deletedIn = Arrays.asList(false);
    frequency = Objects.isNull(frequency) ? null : frequency;
    goalDefinitionCode = Objects.isNull(goalDefinitionCode) ? null : goalDefinitionCode;
    log.info(
        "IN getUserActiveGoalAssignments for userId -> {}, startDate -> {} and endDAte -> {} ",
        userId,
        startDate,
        endDate);

    // Format the search query string to mysql date format for native queries
    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    log.debug("Limit is {} and offset is {} ", limit, offset);
    List<GoalDefinition> childGoals = configUtil.getAllChildGoalsForClient(clientId);
    List<String> childGoalCodes =
        childGoals.stream().map(goal -> goal.getCode()).collect(Collectors.toList());
    List<String> allGoalDefinitionsByClientInOrder =
        configUtil.getAllGoalDefinitionsByClientInOrder(clientId);
    List<String> goalFrequencyInOrder =
        Arrays.asList("daily", "weekly", "monthly", "quarterly", "yearly");
    List<GoalAssigneeSettingsVersionsEntity> activeUserGoalAssignments =
        goalAssigneeSettingsVersionsRepository.findMatchingGoalAssignmentsForUsers(
            Arrays.asList(userId),
            clientId,
            dateFormat.format(startDate),
            dateFormat.format(endDate),
            isActive,
            deletedIn,
            limit,
            offset,
            frequency,
            goalDefinitionCode,
            allGoalDefinitionsByClientInOrder,
            goalFrequencyInOrder,
            childGoalCodes);

    ArrayList<UserGoalAssignment> userGoalAssignments = new ArrayList<UserGoalAssignment>();

    log.info("Iterating over {} activeUserGoalAssignments", activeUserGoalAssignments.size());
    for (GoalAssigneeSettingsVersionsEntity goalAssigneeSettingsVersionsEntity :
        activeUserGoalAssignments) {
      userGoalAssignments.add(
          goalsUtils.getUserGoalAssignmentFromGoalAssigneeSettingsVersionEntity(
              goalAssigneeSettingsVersionsEntity, clientId));
    }
    //        Collections.sort(userGoalAssignments, new GoalsUtils.GoalOrderComparator());
    log.info("Getting countOfMatchingGoalUserAssignments");
    Integer countOfMatchingGoalUserAssignments =
        goalAssigneeSettingsVersionsRepository.getCountOfMatchingGoalUserAssignments(
            userId,
            clientId,
            dateFormat.format(startDate),
            dateFormat.format(endDate),
            isActive,
            frequency,
            goalDefinitionCode,
            childGoalCodes);
    StandardListResponse slr =
        new StandardListResponse(
            countOfMatchingGoalUserAssignments, limit, offset, userGoalAssignments);
    log.info("Returning {} countOfMatchingGoalUserAssignments", countOfMatchingGoalUserAssignments);
    return slr;
  }

  // Supports user groupby
  // Returns goal achievement for the give goal assignee Id which is active at current point of time
  // Doesn't support date range filter
  @Override
  @Transactional
  public GoalAchievement getGoalAchievement(
      String clientId,
      String userId,
      String goalAssigneeId,
      List<String> teamUserIds,
      String groupByType,
      Date startDate,
      Date endDate,
      List<String> progress) {

    log.info(
        "In getGoalAchievement for userId {} , clientId {} , goalAssigneeId {}, groupByType {}, startDate {}, endDate {}",
        userId,
        clientId,
        goalAssigneeId,
        groupByType,
        startDate,
        endDate);

    startDate = new Date();
    endDate = new Date();

    Boolean withDateGroupBy = groupByType.equals("date");
    Boolean withUserGroupBy = groupByType.equals("user");

    List<GoalInstanceEntity> goalInstanceEntities = new ArrayList<>();

    log.info(
        "In getGoalAchievement for userId -> {}, goalAssigneeId -> {} & teamUserIds -> {} and withUserGroupBy -> {}",
        userId,
        goalAssigneeId,
        teamUserIds,
        withUserGroupBy);

    log.info(
        "Fetching current goalInstanceEntity for given userId {} , clientId {} , goalAssigneeId {}, startDate {} , endDate "
            + "{}",
        userId,
        clientId,
        goalAssigneeId,
        startDate,
        endDate);

    GoalInstanceEntity goalInstanceEntity =
        goalInstanceRepository
            .findByUserIdAndClientIdAndGoalAssigneeIdAndStartDateLessThanEqualAndEndDateGreaterThanEqualAndDeleted(
                userId, clientId, goalAssigneeId, startDate, endDate, false);

    if (goalInstanceEntity == null) {
      log.info(
          "No goal instances active currently for goal_assignee_id {} and user_id {}",
          goalAssigneeId,
          userId);
      return new GoalAchievement();
    }

    String goalDefinitionCode = goalInstanceEntity.getGoalDefinitionCode();
    GoalDefinition goalDefinition =
        configUtil.getGoalDefinitionByCode(clientId, goalDefinitionCode);
    if (goalDefinition == null) {
      log.error("Goal definition doesn't exist for id {} ", goalDefinitionCode);
      return new GoalAchievement();
    }
    if (withUserGroupBy == true) {
      log.info("withUserGroupBy is {} ", withUserGroupBy);
      log.info(
          "Fetching goalinstances for team users -> {} and goal definition Id and current start date and end date",
          teamUserIds,
          goalDefinitionCode);
      goalInstanceEntities =
          goalInstanceRepository
              .findByUserIdInAndGoalDefinitionCodeAndClientIdAndStartDateLessThanEqualAndEndDateGreaterThanEqualAndRecurringFrequencyAndDeleted(
                  teamUserIds,
                  goalDefinitionCode,
                  clientId,
                  startDate,
                  endDate,
                  goalInstanceEntity.getRecurringFrequency(),
                  false);
      goalInstanceEntities =
          goalsUtils.getTotalGoalInstanceEntitiesIncludingMissingUsers(
              teamUserIds, goalInstanceEntities);
    }

    log.info("goalInstanceEntity is -> {}", goalInstanceEntity);
    log.info("goalInstanceEntities size -> {}", goalInstanceEntities.size());

    startDate = goalInstanceEntity.getStartDate();
    endDate = goalInstanceEntity.getEndDate();

    MetricsResponse achievementDataForGoal = null;
    try {
      achievementDataForGoal =
          getAchievementDataForGoalByType(
              goalDefinition,
              clientId,
              startDate,
              endDate,
              withDateGroupBy,
              withUserGroupBy,
              teamUserIds,
              goalInstanceEntity.getRecurringFrequency(),
              userId);
    } catch (Exception e) {
      log.error("Exception while getAchievementDataForGoalByType ", e);
    }

    GoalInstanceEntity managerSelfGoalInstanceEntity = null;
    MetricAchievementObject managerSelfAchievement = null;
    if (withUserGroupBy && configUtil.isGoalDefinitionHasChild(goalDefinition, clientId)) {
      List<GoalDefinition> childGoals =
          configUtil.getChildGoals(goalDefinition.getCode(), clientId);
      // As of now child goals are only used for manager self contribution, so considering only one
      // child which is manager self goal
      GoalDefinition childGoalDefinition = childGoals.get(0);
      managerSelfGoalInstanceEntity =
          getManagerSelfGoalInstanceEntityFromParentGoalDefinition(
              childGoalDefinition,
              clientId,
              startDate,
              endDate,
              userId,
              goalInstanceEntity.getRecurringFrequency());
      if (Objects.nonNull(managerSelfGoalInstanceEntity)) {
        managerSelfAchievement =
            getManagerSelfAchievementFromParentGoalDefinition(
                childGoalDefinition,
                clientId,
                startDate,
                endDate,
                userId,
                goalInstanceEntity.getRecurringFrequency());
      }
    }

    if (withUserGroupBy) {
      log.info(
          "With user groupby: Getting goal achievement from metrics response and goal instances");
      return goalsUtils.getGoalAchievementFromResponseAndAssignment(
          goalInstanceEntities,
          achievementDataForGoal,
          goalAssigneeId,
          userId,
          clientId,
          progress,
          managerSelfGoalInstanceEntity,
          managerSelfAchievement);
    } else if (withDateGroupBy) {
      return goalsUtils.getGoalAchievementFromResponseAndAssignmentForGroupedByDate(
          goalInstanceEntity,
          achievementDataForGoal,
          goalAssigneeId,
          userId,
          goalDefinition,
          clientId,
          progress);
    } else {
      log.info(
          "Without user groupby: Getting goal achievement from metrics response and goal instances");
      return goalsUtils.getGoalAchievementFromResponseAndAssignment(
          Arrays.asList(goalInstanceEntity),
          achievementDataForGoal,
          goalAssigneeId,
          userId,
          clientId,
          progress,
          null,
          null);
    }
  }

  private MetricAchievementObject getManagerSelfAchievementFromParentGoalDefinition(
      GoalDefinition childGoalDefinition,
      String clientId,
      Date startDate,
      Date endDate,
      String userId,
      String recurringFrequency) {

    try {
      MetricsResponse achievementDataForGoal =
          getAchievementDataForGoalByType(
              childGoalDefinition,
              clientId,
              startDate,
              endDate,
              false,
              false,
              new ArrayList<>(),
              recurringFrequency,
              userId);
      List<MetricAchievementObject> metrics = achievementDataForGoal.getResult().getMetrics();
      return metrics.get(0);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }

  @Transactional
  private GoalInstanceEntity getManagerSelfGoalInstanceEntityFromParentGoalDefinition(
      GoalDefinition childGoalDefinition,
      String clientId,
      Date startDate,
      Date endDate,
      String userId,
      String recurringFrequency) {

    List<GoalInstanceEntity> goalInstanceEntities =
        goalInstanceRepository
            .findByUserIdInAndGoalDefinitionCodeAndClientIdAndStartDateLessThanEqualAndEndDateGreaterThanEqualAndRecurringFrequencyAndDeleted(
                Arrays.asList(userId),
                childGoalDefinition.getCode(),
                clientId,
                startDate,
                endDate,
                recurringFrequency,
                false);
    if (goalInstanceEntities.size() > 0) {
      return goalInstanceEntities.get(0);
    }
    return null;
  }

  private MetricsResponse getAchievementDataForGoalByType(
      GoalDefinition goalDefinition,
      String clientId,
      Date startDate,
      Date endDate,
      Boolean withDateGroupBy,
      Boolean withUserGroupBy,
      List<String> teamUserIds,
      String recurringFrequency,
      String userId)
      throws Exception {
    MetricsResponse achievementDataForGoal = null;

    if (goalDefinition.getType() == GoalType.vymoMetric) {

      List<Filter> metricFilters = new ArrayList<>();
      log.info("getting goal metric filters");

      List<GoalMetricFilter> goalMetricFilters = new ArrayList<>();
      VymoMetricGoalDefinition vymoMetricGoalDefinition = (VymoMetricGoalDefinition) goalDefinition;
      goalMetricFilters = vymoMetricGoalDefinition.getMetricFilters();

      log.info("Constructing datefilter for startDate -> {}, endDate -> {}", startDate, endDate);
      DateFilter dateFilter = new DateFilter("date", "last_state_update_time", startDate, endDate);
      metricFilters.add(dateFilter);
      if (goalMetricFilters != null && !goalMetricFilters.isEmpty()) {
        metricFilters.addAll(
            goalsUtils.getGoalMetricFiltersFromEntities(goalMetricFilters, clientId));
      }
      List<String> dataSets = getDataSetsForMetric(clientId, goalDefinition.getMetric());
      List<GroupBy> groupBys = new ArrayList<>();
      List<String> metrics = Arrays.asList(goalDefinition.getMetric());
      log.info("Metrics are {} ", metrics);
      String scope = goalDefinition.getScope().getType();
      UserFilterEnum userFilterEnum =
          scope.equals("team") ? UserFilterEnum.team : UserFilterEnum.self;

      if (withUserGroupBy) {
        log.info("Adding groupByUser for teamUserIds -> {} ", teamUserIds);
        GroupBy groupByUser =
            new GroupByUser(
                "user",
                scope.equals("team") ? UserGroupByEnum.team : UserGroupByEnum.self,
                teamUserIds);
        groupBys.add(groupByUser);
      } else if (withDateGroupBy == true) {
        log.info("Adding groupByDate");
        DATE_GRANULARITY granularity = configUtils.getDateGranularity(recurringFrequency, clientId);
        log.info("Adding groupByDate for granularity {} ", granularity);
        GroupBy groupByDate = new GroupByDate("date", null, granularity);
        groupBys.add(groupByDate);
      }

      if (withUserGroupBy) {
        log.info("Adding userFilter for userId with groupby user", userId);
        UserFilter userFilter = new UserFilter("user", UserFilterEnum.team, Arrays.asList(userId));
        metricFilters.add(userFilter);
      } else {
        log.info("Adding userFilter for userId ", userId);
        UserFilter userFilter = new UserFilter("user", userFilterEnum, Arrays.asList(userId));
        metricFilters.add(userFilter);
      }

      log.info("Constructing businessMetricsSummaryRequest");
      BusinessMetricsSummaryRequest businessMetricsSummaryRequest =
          new BusinessMetricsSummaryRequest(
              clientId, Arrays.asList(userId), dataSets, metrics, metricFilters, null, groupBys);

      log.info("Making call to metrics_rest");
      achievementDataForGoal =
          goalEvaluationService.getAchievementDataForGoal(businessMetricsSummaryRequest);
      log.info("Received response from metrics_rest");
    } else if (goalDefinition.getType() == GoalType.external) {
      achievementDataForGoal =
          goalEvaluationService.getAchievementDataForExternalGoal(
              clientId,
              userId,
              teamUserIds,
              goalDefinition.getCode(),
              startDate,
              endDate,
              recurringFrequency == null,
              recurringFrequency,
              withUserGroupBy,
              withDateGroupBy);
    } else {
      ComputedGoalDefinition computedGoalDefinition = (ComputedGoalDefinition) goalDefinition;
      achievementDataForGoal =
          getAchievementDataForComputedGoal(
              computedGoalDefinition,
              clientId,
              startDate,
              endDate,
              withDateGroupBy,
              withUserGroupBy,
              teamUserIds,
              recurringFrequency,
              userId);
    }
    goalsUtils.prettyPrintStringFromJSON(
        achievementDataForGoal,
        "Returning achievementDataForGoal for goal " + goalDefinition.getCode());
    return achievementDataForGoal;
  }

  public MetricsResponse getAchievementDataForComputedGoal(
      ComputedGoalDefinition computedGoalDefinition,
      String clientId,
      Date startDate,
      Date endDate,
      Boolean withDateGroupBy,
      Boolean withUserGroupBy,
      List<String> teamUserIds,
      String recurringFrequency,
      String userId)
      throws Exception {

    Map<String, MetricsResponse> goalAchievementsComputed = new HashMap<>();
    goalAchievementsComputed =
        goalAchievementsComputedForGoalDefinitionsInvolved(
            computedGoalDefinition,
            clientId,
            startDate,
            endDate,
            withDateGroupBy,
            withUserGroupBy,
            teamUserIds,
            recurringFrequency,
            userId);

    log.info("Computed goals goalAchievementsComputed size " + goalAchievementsComputed.size());
    Map<String, Object> vars = new HashMap<String, Object>();
    for (Map.Entry<String, MetricsResponse> entry : goalAchievementsComputed.entrySet()) {
      log.info(
          "goalAchievementsComputed Key = " + entry.getKey() + ", Value = " + entry.getValue());
      MetricsResponse value = entry.getValue();
      List<MetricAchievementObject> metrics = value.getResult().getMetrics();
      vars.put(entry.getKey(), metrics.get(0).getValue());
    }

    MetricsResultObject achievementDataForGoalResult =
        goalsUtils.createAchievementDataForComputedGoal(
            goalsUtils.evaluateExpression(computedGoalDefinition.getExpression(), vars),
            withUserGroupBy || withDateGroupBy);
    log.info("Populating setGrouped_record_sets for " + computedGoalDefinition.getCode());
    achievementDataForGoalResult.setGrouped_record_sets(
        createAchievementDataForComputedGoalGroupedRecordSets(
            computedGoalDefinition,
            goalAchievementsComputed,
            withUserGroupBy || withDateGroupBy
                ? withUserGroupBy
                    ? GroupedRecordSet.GROUP_BY_TYPE.user
                    : GroupedRecordSet.GROUP_BY_TYPE.date
                : null));
    MetricsResponse achievementDataForGoal = new MetricsResponse();
    achievementDataForGoal.setResult(achievementDataForGoalResult);

    return achievementDataForGoal;
  }

  private Map<String, MetricsResponse> goalAchievementsComputedForGoalDefinitionsInvolved(
      ComputedGoalDefinition computedGoalDefinition,
      String clientId,
      Date startDate,
      Date endDate,
      Boolean withDateGroupBy,
      Boolean withUserGroupBy,
      List<String> teamUserIds,
      String recurringFrequency,
      String userId)
      throws Exception {
    Map<String, MetricsResponse> goalAchievementsComputed = new HashMap<>();
    ArrayList<CompletableFuture<MetricsResponse>> listOfMetricsResponseFutures = new ArrayList<>();
    for (String goalDefinitionInvolved : computedGoalDefinition.getGoalDefinitionsInvolved()) {
      GoalDefinition goalDefinitionAdded =
          configUtil.getGoalDefinitionByCode(clientId, goalDefinitionInvolved);
      if (goalDefinitionAdded == null) {
        log.error("Goal definition doesn't exist for id {} ", goalDefinitionInvolved);
        throw new GoalManagementServiceException("Goal definition doesn't exist for id");
      }

      CompletableFuture<MetricsResponse> goalAchievementCompletableFuture =
          getAchievementDataForGoalByTypeAsync(
                  goalDefinitionAdded,
                  clientId,
                  startDate,
                  endDate,
                  withDateGroupBy,
                  withUserGroupBy,
                  teamUserIds,
                  recurringFrequency,
                  userId)
              .thenApply(
                  metricsResponse -> {
                    try {
                      return metricsResponse;
                    } catch (Exception e) {
                      log.error(
                          "Error while goalAchievementsComputedForGoalDefinitionsInvolved ", e);
                      return null;
                    }
                  });
      listOfMetricsResponseFutures.add(goalAchievementCompletableFuture);
    }

    CompletableFuture<Void> allFutures =
        CompletableFuture.allOf(
            listOfMetricsResponseFutures.toArray(
                new CompletableFuture[listOfMetricsResponseFutures.size()]));

    CompletableFuture<List<MetricsResponse>> listCompletableFuture =
        allFutures.thenApply(
            goalAchievement -> {
              return listOfMetricsResponseFutures.stream()
                  .map(metricsResponseFuture -> metricsResponseFuture.join())
                  .collect(Collectors.toList());
            });

    try {
      log.info("Trying to resolve all futures");
      List<MetricsResponse> metricsResponseList = listCompletableFuture.get();
      for (int i = 0; i < computedGoalDefinition.getGoalDefinitionsInvolved().size(); i++)
        goalAchievementsComputed.put(
            computedGoalDefinition.getGoalDefinitionsInvolved().get(i), metricsResponseList.get(i));
    } catch (InterruptedException e) {
      log.error(
          "InterruptedException while goalAchievementsComputedForGoalDefinitionsInvolved ", e);
    } catch (ExecutionException e) {
      log.error("ExecutionException while goalAchievementsComputedForGoalDefinitionsInvolved ", e);
    } finally {
      log.error("Error while resolving all futures");
    }
    return goalAchievementsComputed;
  }

  private List<GroupedRecordSet> createAchievementDataForComputedGoalGroupedRecordSets(
      ComputedGoalDefinition computedGoalDefinition,
      Map<String, MetricsResponse> goalAchievementsComputed,
      GroupedRecordSet.GROUP_BY_TYPE group_by_type) {
    log.info("Inside createAchievementDataForComputedGoalGroupedRecordSets");
    List<GroupedRecordSet> groupedRecordSets = new ArrayList<>();
    Map<String, Map<String, Object>> userIdMetricMapping = new HashMap<>();
    if (group_by_type == GroupedRecordSet.GROUP_BY_TYPE.user) {
      for (Map.Entry<String, MetricsResponse> entry : goalAchievementsComputed.entrySet()) {
        log.info(
            "createAchievementDataForComputedGoalGroupedRecordSets Key = "
                + entry.getKey()
                + ", Value = "
                + entry.getValue());
        MetricsResponse value = entry.getValue();
        List<GroupedRecordSet> grouped_record_sets = value.getResult().getGrouped_record_sets();
        if (Objects.nonNull(grouped_record_sets)) {
          log.info("grouped_record_sets size = " + grouped_record_sets.size());
          for (GroupedRecordSet record : grouped_record_sets) {
            Map<String, Object> objectMap = new HashMap<>();
            if (userIdMetricMapping.keySet().contains(record.group_by_key.code)) {
              objectMap = userIdMetricMapping.get(record.group_by_key.code);
              objectMap.put(entry.getKey(), record.getMetrics().get(0).getValue());
            } else {
              objectMap.put(entry.getKey(), record.getMetrics().get(0).getValue());
              userIdMetricMapping.put(record.group_by_key.code, objectMap);
            }
          }
        }
      }
      log.info("Iterating over userIdMetricMapping " + userIdMetricMapping.size());
      if (userIdMetricMapping.size() > 0) {
        for (String user : userIdMetricMapping.keySet()) {
          Map<String, Object> obj = userIdMetricMapping.get(user);
          Object result =
              goalsUtils.evaluateExpression(computedGoalDefinition.getExpression(), obj);

          GroupedRecordSet groupedRecordSet = new GroupedRecordSet();
          groupedRecordSet.setType("terminal");

          groupedRecordSet.setGroup_by_type(group_by_type);
          groupedRecordSet.setMetrics(
              Arrays.asList(
                  new MetricAchievementObject(
                      "number",
                      "computed",
                      goalsUtils.getAchievementValueFromObject(result),
                      new Date())));

          GroupByKey groupByKey = new GroupByKey(user);
          groupedRecordSet.setGroup_by_key(groupByKey);

          groupedRecordSets.add(groupedRecordSet);
        }
      }
    } else if (group_by_type == GroupedRecordSet.GROUP_BY_TYPE.date) {
      // TODO: group by date logic for computed goals
    }
    log.info("Returing from createAchievementDataForComputedGoalGroupedRecordSets");
    return groupedRecordSets;
  }

  @Override
  @Deprecated
  @Transactional
  // Use getGoalAchievementReportForUsers instead
  public StandardListResponse<GoalAchievement> getGoalAchievementWithDateRange(
      String clientId,
      String userId,
      Date startDate,
      Date endDate,
      List<String> progress,
      List<String> status) {
    // doesn't support groupby users
    // doesn't support groupby date
    // Returns array of Goal achievements matching a goal assignee id in specified date range
    log.info(
        "In getGoalAchievementWithDateRange for userId {}  clientId {}  startDate) {} endDate",
        userId,
        clientId,
        startDate,
        endDate);
    StandardListResponse standardListResponse = new StandardListResponse();
    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    log.info("Fetching findMatchingWithinDateRangeAndNoGoalAssigneeId");

    List<GoalDefinition> childGoals = configUtil.getAllChildGoalsForClient(clientId);
    List<String> childGoalCodes =
        childGoals.stream().map(goal -> goal.getCode()).collect(Collectors.toList());
    List<String> allGoalDefinitionsByClientInOrder =
        configUtil.getAllGoalDefinitionsByClientInOrder(clientId);
    List<String> goalFrequencyInOrder =
        Arrays.asList("daily", "weekly", "monthly", "quarterly", "yearly");

    ArrayList<Boolean> deletedIn = new ArrayList<>();

    if (status.isEmpty()) {
      deletedIn.add(false);
    } else {
      status.stream()
          .forEach(
              s -> {
                if (s.equals("disabled")) {
                  deletedIn.add(true);
                } else {
                  deletedIn.add(false);
                }
              });
    }

    // @TODO Pagination support has to be done
    List<GoalAssigneeSettingsVersionsEntity> goalAssigneeSettingsVersionsEntities =
        goalAssigneeSettingsVersionsRepository.findMatchingGoalAssignmentsForUsers(
            Arrays.asList(userId),
            clientId,
            dateFormat.format(startDate),
            dateFormat.format(endDate),
            true,
            deletedIn,
            1000,
            0,
            null,
            null,
            allGoalDefinitionsByClientInOrder,
            goalFrequencyInOrder,
            childGoalCodes);

    log.info(
        "Fetched {} goalAssigneeSettingsVersionsEntities",
        goalAssigneeSettingsVersionsEntities.size());
    if (goalAssigneeSettingsVersionsEntities.size() == 0) {
      standardListResponse.setCount(0);
      standardListResponse.setData((ArrayList) goalAssigneeSettingsVersionsEntities);
      return standardListResponse;
    }

    ArrayList<String> allAssigneeVersionEntityIds = new ArrayList<>();
    ArrayList<String> activeAssigneeVersionEntityIds = new ArrayList<>();

    goalAssigneeSettingsVersionsEntities.stream()
        .forEach(
            goalAssigneeSettingsVersionsEntity -> {
              GoalAssigneesEntity assigneeEntity =
                  goalAssigneeSettingsVersionsEntity.getGoalAssigneesEntity();
              allAssigneeVersionEntityIds.add(assigneeEntity.getId());
              if (!assigneeEntity.getDeleted()) {
                activeAssigneeVersionEntityIds.add(assigneeEntity.getId());
              }
            });

    List<GoalInstanceEntity> goalInstanceEntities =
        goalInstanceRepository.findMatchingWithinDateRangeForUsersAndGoalAssigneeId(
            Arrays.asList(userId),
            clientId,
            dateFormat.format(startDate),
            dateFormat.format(endDate),
            allAssigneeVersionEntityIds);

    log.info("Fetched {} goalInstanceEntities", goalInstanceEntities.size());
    if (goalInstanceEntities.size() == 0) {
      standardListResponse.setCount(0);
      standardListResponse.setData((ArrayList) goalInstanceEntities);
      return standardListResponse;
    }

    ArrayList<CompletableFuture<GoalAchievement>> listOfGoalAchievementFutures = new ArrayList<>();

    log.info("Iterating over goalInstanceEntities for making parallel calls");
    for (GoalInstanceEntity goalInstanceEntity : goalInstanceEntities) {
      CompletableFuture<GoalAchievement> goalAchievementCompletableFuture =
          getAchievementForGoalInstanceEntity(goalInstanceEntity)
              .thenApply(
                  metricsResponse -> {
                    try {
                      return goalsUtils.getGoalAchievementFromResponseAndAssignment(
                          Arrays.asList(goalInstanceEntity),
                          metricsResponse,
                          goalInstanceEntity.getGoalAssignee().getId(),
                          userId,
                          clientId,
                          progress,
                          null,
                          null);
                    } catch (Exception e) {
                      log.error("Exception while getGoalAchievementWithDateRange ", e);
                      return null;
                    }
                  });
      listOfGoalAchievementFutures.add(goalAchievementCompletableFuture);
    }

    CompletableFuture<Void> allFutures =
        CompletableFuture.allOf(
            listOfGoalAchievementFutures.toArray(
                new CompletableFuture[listOfGoalAchievementFutures.size()]));

    CompletableFuture<List<GoalAchievement>> listCompletableFuture =
        allFutures.thenApply(
            goalAchievement -> {
              return listOfGoalAchievementFutures.stream()
                  .map(goalAchievementFuture -> goalAchievementFuture.join())
                  .collect(Collectors.toList());
            });

    List<GoalAchievement> goalAchievements = null;
    try {
      log.info("Trying to resolve all futures");
      goalAchievements = listCompletableFuture.get();
    } catch (InterruptedException e) {
      log.error("InterruptedException while getGoalAchievementWithDateRange ", e);
    } catch (ExecutionException e) {
      log.error("ExecutionException while getGoalAchievementWithDateRange ", e);
    } catch (Exception e) {
      log.error("Error while resolving all futures", e);
      throw new GoalManagementServiceException("Error while fetching goals in date range");
    }

    if (goalAchievements != null) {
      goalAchievements.stream()
          .forEach(
              goalAchievement -> {
                if (activeAssigneeVersionEntityIds.contains(goalAchievement.getGoalAssigneeId())) {
                  goalAchievement.setStatus("1");
                } else {
                  goalAchievement.setStatus("0");
                }
              });
    }

    standardListResponse.setData((ArrayList) goalAchievements);
    log.info("Returning from getGoalAchievementWithDateRange");
    return standardListResponse;
  }

  @Override
  @Transactional
  public StandardListResponse<GoalAchievement> getGoalAchievementReportForUsers(
      String clientId,
      List<String> userIds,
      Map<String, String> userCodeVsName,
      Date startDate,
      Date endDate,
      String goalDefinitionCode,
      String goalRecurringFrequency,
      List<String> status) {
    log.info(
        "In getGoalAchievementReportForUsers for clientId {}  startDate{}  endDate{}, userIds {}, goalDefinitionCode {}, "
            + "goalRecurringFrequency {}",
        clientId,
        startDate,
        endDate,
        userIds,
        goalDefinitionCode,
        goalRecurringFrequency);
    StandardListResponse standardListResponse = new StandardListResponse();
    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    log.info("Fetching findMatchingWithinDateRangeAndNoGoalAssigneeId");
    String fromDateFormatted = dateFormat.format(startDate);
    String toDateFormatted = dateFormat.format(endDate);

    List<GoalDefinition> childGoals = configUtil.getAllChildGoalsForClient(clientId);
    List<String> childGoalCodes =
        childGoals.stream().map(goal -> goal.getCode()).collect(Collectors.toList());
    List<String> allGoalDefinitionsByClientInOrder =
        configUtil.getAllGoalDefinitionsByClientInOrder(clientId);
    List<String> goalFrequencyInOrder =
        Arrays.asList("daily", "weekly", "monthly", "quarterly", "yearly");

    ArrayList<Boolean> deletedIn = new ArrayList<>();

    if (status.isEmpty()) {
      deletedIn.add(false);
    } else {
      status.stream()
          .forEach(
              s -> {
                if (s.equals("disabled")) {
                  deletedIn.add(true);
                } else {
                  deletedIn.add(false);
                }
              });
    }

    // @TODO Pagination support has to be done
    List<GoalAssigneeSettingsVersionsEntity> goalAssigneeSettingsVersionsEntities =
        goalAssigneeSettingsVersionsRepository.findMatchingGoalAssignmentsForUsers(
            userIds,
            clientId,
            dateFormat.format(startDate),
            dateFormat.format(endDate),
            true,
            deletedIn,
            1000,
            0,
            null,
            null,
            allGoalDefinitionsByClientInOrder,
            goalFrequencyInOrder,
            childGoalCodes);

    log.info(
        "Fetched {} goalAssigneeSettingsVersionsEntities",
        goalAssigneeSettingsVersionsEntities.size());
    if (goalAssigneeSettingsVersionsEntities.size() == 0) {
      standardListResponse.setCount(0);
      standardListResponse.setData((ArrayList) goalAssigneeSettingsVersionsEntities);
      return standardListResponse;
    }

    ArrayList<String> allAssigneeVersionEntityIds = new ArrayList<>();
    ArrayList<String> activeAssigneeVersionEntityIds = new ArrayList<>();

    goalAssigneeSettingsVersionsEntities.stream()
        .forEach(
            goalAssigneeSettingsVersionsEntity -> {
              GoalAssigneesEntity assigneeEntity =
                  goalAssigneeSettingsVersionsEntity.getGoalAssigneesEntity();
              allAssigneeVersionEntityIds.add(assigneeEntity.getId());
              if (!assigneeEntity.getDeleted()) {
                activeAssigneeVersionEntityIds.add(assigneeEntity.getId());
              }
            });

    List<GoalInstanceEntity> goalInstanceEntities =
        goalInstanceRepository.findMatchingWithinDateRangeForUsersAndGoalAssigneeId(
            userIds,
            clientId,
            dateFormat.format(startDate),
            dateFormat.format(endDate),
            allAssigneeVersionEntityIds);

    log.info("Fetched {} goalInstanceEntities", goalInstanceEntities.size());
    if (goalInstanceEntities.size() == 0) {
      standardListResponse.setCount(0);
      standardListResponse.setData((ArrayList) goalInstanceEntities);
      return standardListResponse;
    }

    ArrayList<CompletableFuture<GoalAchievement>> listOfGoalAchievementFutures = new ArrayList<>();

    log.info("Iterating over goalInstanceEntities for making parallel calls");
    for (GoalInstanceEntity goalInstanceEntity : goalInstanceEntities) {
      CompletableFuture<GoalAchievement> goalAchievementCompletableFuture =
          getAchievementForGoalInstanceEntity(goalInstanceEntity)
              .thenApply(
                  metricsResponse -> {
                    try {
                      return goalsUtils.getGoalAchievementFromResponseAndAssignment(
                          Arrays.asList(goalInstanceEntity),
                          metricsResponse,
                          goalInstanceEntity.getGoalAssignee().getId(),
                          goalInstanceEntity.getUserId(),
                          clientId,
                          new ArrayList<>(),
                          null,
                          null);
                    } catch (Exception e) {
                      log.error("Exception while getGoalAchievementReportForUsers ", e);
                      return null;
                    }
                  });
      listOfGoalAchievementFutures.add(goalAchievementCompletableFuture);
    }

    CompletableFuture<Void> allFutures =
        CompletableFuture.allOf(
            listOfGoalAchievementFutures.toArray(
                new CompletableFuture[listOfGoalAchievementFutures.size()]));

    CompletableFuture<List<GoalAchievement>> listCompletableFuture =
        allFutures.thenApply(
            goalAchievement -> {
              return listOfGoalAchievementFutures.stream()
                  .map(goalAchievementFuture -> goalAchievementFuture.join())
                  .collect(Collectors.toList());
            });

    List<GoalAchievement> goalAchievements = null;
    try {
      log.info("Trying to resolve all futures");
      goalAchievements = listCompletableFuture.get();
    } catch (InterruptedException e) {
      log.error("InterruptedException while getGoalAchievementReportForUsers ", e);
    } catch (ExecutionException e) {
      log.error("ExecutionException while getGoalAchievementReportForUsers ", e);
    } catch (Exception e) {
      log.error("Exception while getGoalAchievementReportForUsers ", e);
      log.error("Error while resolving all futures", e);
      throw new GoalManagementServiceException("Error while fetching goals in date range");
    }

    if (goalAchievements != null) {
      goalAchievements.stream()
          .forEach(
              goalAchievement -> {
                UserObject user = goalAchievement.getUser();
                if (userCodeVsName.containsKey(user.getCode())) {
                  user.setName(userCodeVsName.get(user.getCode()));
                } else {
                  user.setName("");
                }
                goalAchievement.setUser(user);
                if (activeAssigneeVersionEntityIds.contains(goalAchievement.getGoalAssigneeId())) {
                  goalAchievement.setStatus("1");
                } else {
                  goalAchievement.setStatus("0");
                }
              });
    }

    standardListResponse.setData((ArrayList) goalAchievements);
    log.info("Returning from getGoalAchievementWithDateRange");
    return standardListResponse;
  }

  @Override
  @Transactional
  public GoalAchievement getGoalAchievementWithoutGoalAssigneeId(
      String clientId,
      String userId,
      GoalAchievementSettingsWithoutAssignment goalAchievementSettingsWithoutAssignment,
      List<String> teamUserIds,
      List<String> progress) {

    Date startDate = goalAchievementSettingsWithoutAssignment.getStartDate();
    Date endDate = goalAchievementSettingsWithoutAssignment.getEndDate();
    String groupByType = goalAchievementSettingsWithoutAssignment.getGroupByType();
    String goalDefinitionCode = goalAchievementSettingsWithoutAssignment.getGoalDefinitionCode();
    String recurringFrequency = goalAchievementSettingsWithoutAssignment.getRecurringFrequency();
    Boolean withDateGroupBy = groupByType.equals("date");
    Boolean withUserGroupBy = groupByType.equals("user");

    List<GoalInstanceEntity> goalInstanceEntities = new ArrayList<>();

    log.info(
        "In getGoalAchievementWithoutGoalAssigneeId for userId -> {}, teamUserIds -> {} and groupByType -> {}",
        userId,
        teamUserIds,
        groupByType);

    log.info("Fetching current goalInstanceEntity for given goal_assignee_id");

    GoalDefinition goalDefinition =
        configUtil.getGoalDefinitionByCode(clientId, goalDefinitionCode);

    if (goalDefinition == null) {
      log.info("No goalDefinition active currently for user_id {}", userId);
      return new GoalAchievement();
    }

    GoalInstanceEntity dummyGoalInstanceEntity = new GoalInstanceEntity();
    dummyGoalInstanceEntity.setGoalDefinitionCode(goalDefinitionCode);
    dummyGoalInstanceEntity.setRecurringFrequency(recurringFrequency);
    dummyGoalInstanceEntity.setStartDate(startDate);
    dummyGoalInstanceEntity.setEndDate(endDate);
    dummyGoalInstanceEntity.setRecurring(recurringFrequency == null);
    dummyGoalInstanceEntity.setTarget(0d);
    dummyGoalInstanceEntity.setClientId(clientId);
    dummyGoalInstanceEntity.setUserId(userId);
    dummyGoalInstanceEntity.setDeleted(false);

    if (withUserGroupBy == true) {
      log.info("withUserGroupBy is {} ", withUserGroupBy);
      log.info(
          "Fetching goalinstances for team users -> {} and goal definition code and current start date and end date",
          teamUserIds,
          goalDefinitionCode);
      goalInstanceEntities =
          goalInstanceRepository
              .findByUserIdInAndGoalDefinitionCodeAndClientIdAndStartDateLessThanEqualAndEndDateGreaterThanEqualAndRecurringFrequencyAndDeleted(
                  teamUserIds,
                  goalDefinitionCode,
                  clientId,
                  new Date(),
                  new Date(),
                  recurringFrequency,
                  false);
      goalInstanceEntities.add(dummyGoalInstanceEntity);
      goalInstanceEntities =
          goalsUtils.getTotalGoalInstanceEntitiesIncludingMissingUsers(
              teamUserIds, goalInstanceEntities);
    }

    log.info("goalInstanceEntities size -> {}", goalInstanceEntities.size());

    MetricsResponse achievementDataForGoal = null;
    try {
      achievementDataForGoal =
          getAchievementDataForGoalByType(
              goalDefinition,
              clientId,
              startDate,
              endDate,
              withDateGroupBy,
              withUserGroupBy,
              teamUserIds,
              recurringFrequency,
              userId);
    } catch (Exception e) {
      log.error("Exception while getAchievementDataForGoalByType ", e);
    }

    GoalInstanceEntity managerSelfGoalInstanceEntity = null;
    MetricAchievementObject managerSelfAchievement = null;
    if (withUserGroupBy && configUtil.isGoalDefinitionHasChild(goalDefinition, clientId)) {
      List<GoalDefinition> childGoals =
          configUtil.getChildGoals(goalDefinition.getCode(), clientId);
      // As of now child goals are only used for manager self contribution, so considering only one
      // child which is manager self goal
      GoalDefinition childGoalDefinition = childGoals.get(0);
      managerSelfGoalInstanceEntity =
          getManagerSelfGoalInstanceEntityFromParentGoalDefinition(
              childGoalDefinition, clientId, startDate, endDate, userId, recurringFrequency);
      if (Objects.nonNull(managerSelfGoalInstanceEntity)) {
        managerSelfAchievement =
            getManagerSelfAchievementFromParentGoalDefinition(
                childGoalDefinition, clientId, startDate, endDate, userId, recurringFrequency);
      }
    }

    log.info("Received response from metrics_rest");
    if (withUserGroupBy) {
      log.info(
          "With user groupby: Getting goal achievement from metrics response and goal instances");
      return goalsUtils.getGoalAchievementFromResponseAndAssignment(
          goalInstanceEntities,
          achievementDataForGoal,
          null,
          userId,
          clientId,
          progress,
          managerSelfGoalInstanceEntity,
          managerSelfAchievement);
    } else if (withDateGroupBy) {
      return goalsUtils.getGoalAchievementFromResponseAndAssignmentForGroupedByDate(
          dummyGoalInstanceEntity,
          achievementDataForGoal,
          null,
          userId,
          goalDefinition,
          clientId,
          progress);
    } else {
      log.info(
          "Without user groupby: Getting goal achievement from metrics response and goal instances");
      return goalsUtils.getGoalAchievementFromResponseAndAssignment(
          Arrays.asList(dummyGoalInstanceEntity),
          achievementDataForGoal,
          null,
          userId,
          clientId,
          progress,
          null,
          null);
    }
  }

  public CompletableFuture<MetricsResponse> getAchievementForGoalInstanceEntity(
      GoalInstanceEntity goalInstanceEntity) {
    log.info("In getAchievementForGoalInstanceEntity");
    String clientId = goalInstanceEntity.getClientId();
    String userId = goalInstanceEntity.getUserId();
    GoalDefinition goalDefinition =
        configUtil.getGoalDefinitionByCode(clientId, goalInstanceEntity.getGoalDefinitionCode());
    Date startDate = goalInstanceEntity.getStartDate();
    Date endDate = goalInstanceEntity.getEndDate();

    CompletableFuture<MetricsResponse> achievementDataForGoalAsync = new CompletableFuture<>();

    try {
      achievementDataForGoalAsync =
          getAchievementDataForGoalByTypeAsync(
              goalDefinition,
              clientId,
              startDate,
              endDate,
              false,
              false,
              new ArrayList<>(),
              goalInstanceEntity.getRecurringFrequency(),
              userId);
    } catch (Exception e) {
      log.error("Exception while getAchievementDataForGoalByType ", e);
    }

    log.info("Returing from getAchievementForGoalInstanceEntity");
    return achievementDataForGoalAsync;
  }

  public List<String> getDataSetsForMetric(String clientId, String metric) {
    List<String> dataSets = new ArrayList<>();
    log.info("In getDataSetsForMetric for clientId {} and metric {}", clientId, metric);
    try {
      Client clientInfo = null;
      if (client == null) {
        log.error("Config client is not initialised");
      }
      clientInfo = client.getClient(clientId);

      Map<String, Metric> availableMetrics = clientInfo.getMetrics();
      Metric configuredMetric = availableMetrics.get(metric);
      dataSets = configuredMetric.getDataSet();

      if (dataSets == null || dataSets.size() == 0) {
        throw new GoalManagementServiceException("Data set not configured for metric " + metric);
      }

      if (configuredMetric == null) {
        throw new GoalManagementServiceException("Given metric code " + metric + " doesn't exist");
      }
    } catch (ConfigClientException e) {
      log.error("Error in config service ", e);
    }
    log.info("Returning from getDataSetsForMetric");
    return dataSets;
  }

  @Override
  @Transactional
  public GoalAssignment editGoalInstance(
      String goalInstanceId, String clientId, GoalAssigneeSettings goalAssigneeSettings)
      throws GoalManagementServiceException {
    GoalInstanceEntity goalInstanceEntity = goalInstanceRepository.findById(goalInstanceId).get();
    if (goalInstanceEntity == null) {
      throw new RecordNotFoundException("Goal instance doesn't exist");
    }

    if (goalInstanceEntity.getEndDate().getTime() < new Date().getTime()) {
      throw new GoalManagementServiceException("Can't edit past goals");
    }

    if (goalInstanceEntity.getDeleted() == true) {
      throw new GoalManagementServiceException("Can't edit deleted goals. Enable them to edit it");
    }

    goalInstanceEntity.setTarget(goalAssigneeSettings.getTarget());
    GoalAssignment goalAssignment =
        goalsUtils.getGoalAssignnmentFromGoalInstanceEntity(
            goalInstanceRepository.save(goalInstanceEntity));
    return goalAssignment;
  }

  @Async
  public CompletableFuture<MetricsResponse> getAchievementDataForGoalByTypeAsync(
      GoalDefinition goalDefinition,
      String clientId,
      Date startDate,
      Date endDate,
      Boolean withDateGroupBy,
      Boolean withUserGroupBy,
      List<String> teamUserIds,
      String recurringFrequency,
      String userId)
      throws Exception {
    log.info("In getAchievementDataForGoalByTypeAsync");
    return CompletableFuture.completedFuture(
        getAchievementDataForGoalByType(
            goalDefinition,
            clientId,
            startDate,
            endDate,
            withDateGroupBy,
            withUserGroupBy,
            teamUserIds,
            recurringFrequency,
            userId));
  }

  @SuppressWarnings("rawtypes")
  @Override
  public StandardListResponse getAllGoalGroups(String clientId)
      throws GoalManagementServiceException {

    List<GoalGroupDefinition> goalGroups = configUtil.getAllGoalGroupsByClient(clientId);
    if (goalGroups == null || goalGroups.size() <= 0) {
      return new StandardListResponse();
    }

    List<GoalGroupSettings> goalGroupSettings = new ArrayList<GoalGroupSettings>();

    for (GoalGroupDefinition goalGroup : goalGroups) {
      List<GoalDefinitionMappings> mappings = goalGroup.getSubGoals();
      List<String> goalIds = new ArrayList<String>();
      for (GoalDefinitionMappings mapping : mappings) {
        goalIds.add(mapping.getGoalDefinitionCode());
      }
      GoalGroupSettings ggs =
          new GoalGroupSettings(
              goalGroup.getCode(), goalGroup.getName(), goalGroup.getDescription(), goalIds);
      goalGroupSettings.add(ggs);
    }
    StandardListResponse slr =
        new StandardListResponse(
            goalGroupSettings.size(), 0, 0, (ArrayList<GoalGroupSettings>) goalGroupSettings);
    return slr;
  }

  @Override
  public GoalGroupSettings getGoalGroupDetails(String clientId, String goalGroupId)
      throws GoalManagementServiceException {
    @SuppressWarnings("unchecked")
    ArrayList<GoalGroupSettings> goalGroups = getAllGoalGroups(clientId).getData();
    for (GoalGroupSettings goalGroupSettings : goalGroups) {
      if (goalGroupSettings.getId() != null
          && goalGroupSettings.getId().equalsIgnoreCase(goalGroupId)) {
        log.debug("Found match for goal group id ", goalGroupId, " . Returning.");
        return goalGroupSettings;
      }
    }
    log.info("No goal group match for specified id", goalGroupId);
    return null;
  }
}
