package com.getvymo.platform.goals.springconfig.constants;

public interface AppConstants {
  String USERID = "User-Id";
  String CUSTOMERID = "Customer-Id";
  String TENANTID = "Tenant-Id";
  String DEVICEID = "Device-Id";
  String CORRID = "Correlation-Id";
  String IDEMPOTENCYKEY = "Idempotency-Key";
  String TRACEID = "Trace-Id";
  String EVENT_TYPE = "Event-Type";
  String CREATEDON = "Created-On";
  String SIMULATED = "Simulated";
}
