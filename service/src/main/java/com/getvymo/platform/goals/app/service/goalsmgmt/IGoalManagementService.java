package com.getvymo.platform.goals.app.service.goalsmgmt;

import com.getvymo.platform.goals.app.common.models.GoalAchievementSettingsWithoutAssignment;
import com.getvymo.platform.goals.app.common.models.GoalAssigneeSettings;
import com.getvymo.platform.goals.app.common.models.GoalAssigneeSettingsWithoutVersion;
import com.getvymo.platform.goals.app.common.models.GoalAssignmentOperationMetaData;
import com.getvymo.platform.goals.app.common.models.GoalGroupSettings;
import com.getvymo.platform.goals.app.common.models.StandardListRequest;
import com.getvymo.platform.goals.app.common.models.StandardListResponse;
import com.getvymo.platform.goals.app.errors.GoalManagementServiceException;
import com.getvymo.platform.goals.app.service.ActivateVersionResponse;
import com.getvymo.platform.goals.app.service.GoalAssignment;
import com.getvymo.platform.goals.app.service.GoalAssignmentOperationVersion;
import com.getvymo.platform.goals.app.service.UserGoalAssignment;
import com.getvymo.platform.goals.app.service.evaluation.goalsresponse.GoalAchievement;
import in.vymo.core.config.model.goals.GoalDefinition;
import java.util.Date;
import java.util.List;
import java.util.Map;

// Goal management APIs
/* All the times are stored in provided timezone i.e., client time zone */
public interface IGoalManagementService {

  GoalDefinition getGoalDefinitionByCode(String goalDefinitionCode, String clientId);

  StandardListResponse getAllGoalDefinitions(String clientId, int pageNumber, int pageSize)
      throws GoalManagementServiceException;

  // Upsert API
  ActivateVersionResponse setGoalForUsersWithoutVersion(
      String clientId,
      List<String> userIds,
      String goalDefinitionCode,
      GoalAssigneeSettingsWithoutVersion goalAssigneeSettingsWithoutVersion,
      String userUpdating)
      throws GoalManagementServiceException;

  GoalAssignmentOperationVersion createGoalAssignmentOperationVersion(
      GoalAssignmentOperationMetaData auditInfo) throws GoalManagementServiceException;

  List<GoalAssignment> setGoalForUsersWithVersion(
      String clientId,
      List<String> userIds,
      String goalDefinitionCode,
      GoalAssigneeSettings goalAssigneeSettings,
      String version,
      String userUpdating)
      throws GoalManagementServiceException;

  List<GoalAssignment> bulkSetGoalForUsersWithVersion(
      String clientId,
      StandardListRequest<GoalAssigneeSettings> goalAssigneeSettingsList,
      String version,
      String userUpdating)
      throws GoalManagementServiceException;

  ActivateVersionResponse activateGoalAssignmentOperationVersion(String version, String clientId)
      throws GoalManagementServiceException;

  List<GoalAssignment> disableGoalForUsers(
      String clientId,
      List<String> userIds,
      String goalDefinitionCode,
      Date startDate,
      Date endDate)
      throws GoalManagementServiceException;

  StandardListResponse getAllGoalAssignments(
      String clientId,
      String goalDefinitionCode,
      List<String> userIds,
      Date startDate,
      Date endDate,
      int pageNumber,
      int pageSize)
      throws GoalManagementServiceException;

  StandardListResponse getAllGoalAssignmentsForUser(
      String clientId, String userId, int pageNumber, int pageSize)
      throws GoalManagementServiceException;

  GoalAssignment editGoalInstance(
      String goalInstanceId, String clientId, GoalAssigneeSettings goalAssigneeSettings)
      throws GoalManagementServiceException;

  StandardListResponse<UserGoalAssignment> getUserActiveGoalAssignments(
      String clientId,
      String userId,
      Boolean isActive,
      Date startDate,
      Date endDate,
      int limit,
      int offset,
      List<String> frequency,
      List<String> goalDefinitionCode);

  GoalAchievement getGoalAchievement(
      String clientId,
      String userId,
      String goalAssigneeId,
      List<String> teamUserIds,
      String groupByType,
      Date startDate,
      Date endDate,
      List<String> progress);

  StandardListResponse<GoalAchievement> getGoalAchievementWithDateRange(
      String clientId,
      String userId,
      Date startDate,
      Date endDate,
      List<String> progress,
      List<String> status);

  StandardListResponse<GoalAchievement> getGoalAchievementReportForUsers(
      String clientId,
      List<String> userIds,
      Map<String, String> userCodeVsName,
      Date fromDate,
      Date toDate,
      String goalDefinitionCode,
      String goalRecurringFrequency,
      List<String> status);

  GoalAchievement getGoalAchievementWithoutGoalAssigneeId(
      String clientId,
      String userId,
      GoalAchievementSettingsWithoutAssignment goalAchievementSettingsWithoutAssignment,
      List<String> userIds,
      List<String> progress);

  StandardListResponse getAllGoalGroups(String clientId) throws GoalManagementServiceException;

  GoalGroupSettings getGoalGroupDetails(String clientId, String goalGroupId)
      throws GoalManagementServiceException;
}
