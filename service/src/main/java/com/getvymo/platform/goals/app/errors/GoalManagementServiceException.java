package com.getvymo.platform.goals.app.errors;

public class GoalManagementServiceException extends RuntimeException {
  private String errorCode;

  public GoalManagementServiceException(String exception) {
    super(exception);
  }
}
