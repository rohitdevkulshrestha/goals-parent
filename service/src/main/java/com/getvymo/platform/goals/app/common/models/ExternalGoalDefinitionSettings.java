package com.getvymo.platform.goals.app.common.models;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import in.vymo.core.config.model.goals.GoalScope;
import in.vymo.core.config.model.goals.GoalType;

@JsonDeserialize(as = ExternalGoalDefinitionSettings.class)
public class ExternalGoalDefinitionSettings extends GoalDefinitionSettings {
  public ExternalGoalDefinitionSettings() {}

  public ExternalGoalDefinitionSettings(
      String name,
      String description,
      String metric,
      GoalScope scope,
      GoalType type,
      String goalDefinitionCode,
      Boolean external,
      Boolean showAchievementList,
      Integer goalOrder,
      Boolean managerDrillDownEnabled,
      Boolean hideShortfall) {
    super(
        name,
        description,
        metric,
        scope,
        type,
        goalDefinitionCode,
        external,
        showAchievementList,
        goalOrder,
        managerDrillDownEnabled,
        hideShortfall);
  }
}
