package com.getvymo.platform.goals.app.utils.es;

import com.getvymo.platform.goals.app.utils.azurevault.AzureVaultConfig;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.stereotype.Component;

/**
 * https://www.elastic.co/guide/en/elasticsearch/client/java-rest/7.3/java-rest-high-document-delete.html
 */
@Component
public class ElasticRestConnection {

  private static int TIMEOUT = 2;
  private static Long SCROLL_TIME = 10L;

  @Value("${config.elastic.user}")
  String esUser;

  @Autowired ApplicationArguments appArgs;
  RestHighLevelClient client;
  @Autowired private AzureVaultConfig vaultConfig;
  private int port;
  private String host = System.getenv("ELASTIC_SEARCH");
  private String esHosts = System.getenv("ELASTIC_HOSTS");
  private String scheme = "http";
  private Logger logger = LoggerFactory.getLogger(this.getClass());

  /** Get the host and port from the env configuration */
  @PostConstruct
  private void setUpEnv() {
    List<String> manualDeploy = appArgs.getOptionValues("manualDeploy");
    if (manualDeploy != null) {
      port = 9000;
      host = "localhost";
      logger.debug(String.format("The elastic host and port are - %s:%s", host, port));
    }
  }

  // We are expecting that the format to be - http://localhost:9200
  private void splitHostAndPortFromURL() {
    if (host.contains(scheme)) {
      final String[] elasticUrl = host.split(":");
      final int length = elasticUrl.length;
      port = Integer.valueOf(elasticUrl[length - 1]);
      final String[] hostUrl = elasticUrl[length - 2].split("//");
      host = hostUrl[hostUrl.length - 1];
      logger.info(
          "Setting hostUrl in splitHostAndPortFromURL host is {} scheme is {} and port is {}",
          host,
          scheme,
          port);
    }
  }

  private List<HttpHost> getListOfHttpHosts(String hostsString) {
    // split "15.1.23.5:9200,15.1.23.6:9200,15.1.23.7:9200" gives ["15.1.23.5:9200",
    // "15.1.23.6:9200"]
    String[] slittedHosts = hostsString.split(",");
    List<HttpHost> httpHosts = new ArrayList<>();
    for (int i = 0; i < slittedHosts.length; i++) {
      String hostString = slittedHosts[i];
      String[] splittedHost = hostString.split(":");
      HttpHost httpHost = new HttpHost(splittedHost[0], Integer.valueOf(splittedHost[1]), "http");
      httpHosts.add(httpHost);
    }
    return httpHosts;
  }

  public RestHighLevelClient getConnection() {
    splitHostAndPortFromURL();
    String esPassword = vaultConfig.getESPassword();
    final CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
    credentialsProvider.setCredentials(
        AuthScope.ANY, new UsernamePasswordCredentials(esUser, esPassword));
    logger.info("esHosts sent in env variable are {} ", esHosts);
    List<HttpHost> httpHostsArr = getListOfHttpHosts(esHosts);
    logger.info("httpHosts are {} ", httpHostsArr.toString());
    HttpHost[] httpHosts = httpHostsArr.stream().toArray(HttpHost[]::new);
    final RestClientBuilder builder =
        RestClient.builder(httpHosts)
            .setHttpClientConfigCallback(
                httpClientBuilder ->
                    httpClientBuilder.setDefaultCredentialsProvider(credentialsProvider));
    if (client == null) {
      client = new RestHighLevelClient(builder);
    }
    return client;
  }

  public void closeConnection(RestHighLevelClient client) {
    try {
      logger.debug("Closing Elastic connection");
      Field restClientField = RestHighLevelClient.class.getDeclaredField("client");
      restClientField.setAccessible(true);
      RestClient restclient = (RestClient) restClientField.get(client);
      restclient.close();
      this.client = null;
    } catch (NoSuchFieldException | IllegalAccessException | IOException e) {
      logger.debug("Not closing elastic connection");
    }
  }

  public String getIndexName(String clientId) {
    return clientId + "_external_achievements";
  }
}
