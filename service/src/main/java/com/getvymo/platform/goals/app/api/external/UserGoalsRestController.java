package com.getvymo.platform.goals.app.api.external;

import com.getvymo.platform.goals.app.common.models.GoalAchievementSettingsWithoutAssignment;
import com.getvymo.platform.goals.app.common.models.StandardListResponse;
import com.getvymo.platform.goals.app.service.UserGoalAssignment;
import com.getvymo.platform.goals.app.service.evaluation.goalsresponse.GoalAchievement;
import com.getvymo.platform.goals.app.service.goalsmgmt.IGoalManagementService;
import java.util.Date;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/goals/v1/clients/{clientId}/users/{userId}")
public class UserGoalsRestController {
  @Autowired private IGoalManagementService iGoalManagementService;

  @GetMapping(path = "/goals", consumes = "application/json")
  public StandardListResponse<UserGoalAssignment> getGoalsForUser(
      @Valid @PathVariable String clientId,
      @RequestParam(value = "startDate", required = false)
          @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
          Date startDate,
      @Valid @PathVariable String userId,
      @RequestParam(value = "isActive", required = false) Boolean isActive,
      @RequestParam(value = "endDate", required = false)
          @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
          Date endDate,
      @RequestParam(value = "limit", required = false, defaultValue = "20") int limit,
      @RequestParam(value = "offset", required = false, defaultValue = "0") int offset,
      @RequestParam(value = "frequency", required = false) List<String> frequency,
      @RequestParam(value = "goal_definition_code", required = false)
          List<String> goalDefinitionCode) {
    return iGoalManagementService.getUserActiveGoalAssignments(
        clientId,
        userId,
        isActive,
        startDate,
        endDate,
        limit,
        offset,
        frequency,
        goalDefinitionCode);
  }

  @GetMapping(path = "/goals/{goalAssigneeId}/achievements", consumes = "application/json")
  public GoalAchievement getGoalAchievement(
      @Valid @PathVariable String clientId,
      @Valid @PathVariable String userId,
      @Valid @PathVariable String goalAssigneeId,
      @RequestParam(value = "teamUserIds", required = false) List<String> teamUserIds,
      @RequestParam(value = "groupByType", required = false, defaultValue = "false")
          String groupByType,
      @RequestParam(value = "startDate", required = false)
          @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
          Date startDate,
      @RequestParam(value = "endDate", required = false)
          @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
          Date endDate,
      @RequestParam(value = "progress", required = false) List<String> progress) {

    return iGoalManagementService.getGoalAchievement(
        clientId, userId, goalAssigneeId, teamUserIds, groupByType, startDate, endDate, progress);
  }

  @GetMapping(path = "/achievements", consumes = "application/json")
  public StandardListResponse<GoalAchievement> getGoalAchievementsForDateRange(
      @Valid @PathVariable String clientId,
      @Valid @PathVariable String userId,
      @RequestParam(value = "startDate", required = false)
          @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
          Date startDate,
      @RequestParam(value = "endDate", required = false)
          @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
          Date endDate,
      @RequestParam(value = "progress", required = false) List<String> progress,
      @RequestParam(value = "status", required = false) List<String> status) {
    return iGoalManagementService.getGoalAchievementWithDateRange(
        clientId, userId, startDate, endDate, progress, status);
  }

  //        /goals/v1/clients/demo/users/shazam/goals/achievements_without_id
  @PostMapping(path = "/goals/achievements_without_id", consumes = "application/json")
  public GoalAchievement getGoalAchievementWithoutGoalAssigneeId(
      @Valid @RequestBody
          GoalAchievementSettingsWithoutAssignment goalAchievementSettingsWithoutAssignment,
      @RequestParam(value = "teamUserIds", required = false) List<String> teamUserIds,
      @Valid @PathVariable String clientId,
      @Valid @PathVariable String userId,
      @RequestParam(value = "progress", required = false) List<String> progress) {
    return iGoalManagementService.getGoalAchievementWithoutGoalAssigneeId(
        clientId, userId, goalAchievementSettingsWithoutAssignment, teamUserIds, progress);
  }
}
