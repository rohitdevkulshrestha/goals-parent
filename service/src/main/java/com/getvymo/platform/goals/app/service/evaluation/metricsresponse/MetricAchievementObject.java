package com.getvymo.platform.goals.app.service.evaluation.metricsresponse;

import com.getvymo.platform.goals.app.service.utils.GoalsUtils;
import java.util.Date;

public class MetricAchievementObject {
  private String data_type;
  private String metric;
  private Double value;
  private Date lastUpdatedDate;

  public MetricAchievementObject() {}

  public MetricAchievementObject(
      String dataType, String metric, Double value, Date lastUpdatedDate) {
    this.data_type = dataType;
    this.metric = metric;
    this.value = value;
    this.lastUpdatedDate = lastUpdatedDate;
  }

  public Date getLastUpdatedDate() {
    return lastUpdatedDate;
  }

  public void setLastUpdatedDate(Date lastUpdatedDate) {
    this.lastUpdatedDate = lastUpdatedDate;
  }

  public String getData_type() {
    return data_type;
  }

  public void setData_type(String data_type) {
    this.data_type = data_type;
  }

  public String getMetric() {
    return metric;
  }

  public void setMetric(String metric) {
    this.metric = metric;
  }

  public Double getValue() {
    return value;
  }

  public void setValue(Double value) {
    this.value = GoalsUtils.round(value, 2);
  }

  @Override
  public String toString() {
    return "MetricAchievementObject{"
        + "data_type='"
        + data_type
        + '\''
        + ", metric='"
        + metric
        + '\''
        + ", value='"
        + value
        + '\''
        + '}';
  }
}
