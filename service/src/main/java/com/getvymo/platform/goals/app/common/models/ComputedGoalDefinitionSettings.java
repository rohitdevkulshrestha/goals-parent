package com.getvymo.platform.goals.app.common.models;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import in.vymo.core.config.model.goals.GoalScope;
import in.vymo.core.config.model.goals.GoalType;
import java.util.List;

@JsonDeserialize(as = ComputedGoalDefinitionSettings.class)
public class ComputedGoalDefinitionSettings extends GoalDefinitionSettings {

  private String expression;
  private List<String> goalDefinitionsInvolved;

  public ComputedGoalDefinitionSettings() {}

  public ComputedGoalDefinitionSettings(
      String name,
      String description,
      String metric,
      GoalScope scope,
      GoalType type,
      String goalDefinitionCode,
      Boolean external,
      Boolean showAchievementList,
      Integer goalOrder,
      String expression,
      List<String> goalDefinitionsInvolved,
      Boolean managerDrillDownEnabled,
      Boolean hideShortfall) {
    super(
        name,
        description,
        metric,
        scope,
        type,
        goalDefinitionCode,
        external,
        showAchievementList,
        goalOrder,
        managerDrillDownEnabled,
        hideShortfall);
    this.expression = expression;
    this.goalDefinitionsInvolved = goalDefinitionsInvolved;
  }

  public String getExpression() {
    return this.expression;
  }

  public void setExpression(String expression) {
    this.expression = expression;
  }

  public List<String> getGoalDefinitionsInvolved() {
    return this.goalDefinitionsInvolved;
  }

  public void setGoalDefinitionsInvolved(List<String> goalDefinitionsInvolved) {
    this.goalDefinitionsInvolved = goalDefinitionsInvolved;
  }
}
