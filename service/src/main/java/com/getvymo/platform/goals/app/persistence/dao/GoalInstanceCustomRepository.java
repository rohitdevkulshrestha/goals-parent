package com.getvymo.platform.goals.app.persistence.dao;

import com.getvymo.platform.goals.app.persistence.entities.GoalInstanceEntity;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Log4j2
public class GoalInstanceCustomRepository {

  @PersistenceContext private EntityManager entityManager;

  private static String toNumeralString(final Boolean input) {
    if (input == null) {
      return "null";
    } else {
      return input.booleanValue() ? "1" : "0";
    }
  }

  public String getValuesStringFromEntities(List<GoalInstanceEntity> goalInstanceEntities) {
    log.info("In getValuesStringFromEntities");
    String valuesString = "";
    List<String> eachEntityValues = new ArrayList<String>(goalInstanceEntities.size());
    log.info("Date we needs is {} ", new java.sql.Date(new Date().getTime()));
    for (GoalInstanceEntity goalInstanceEntity : goalInstanceEntities) {
      String entityString = "(";
      entityString =
          entityString
              + "'"
              + goalInstanceEntity.getClientId()
              + "'"
              + ","
              + "'"
              + goalInstanceEntity.getUserId()
              + "'"
              + ","
              + "'"
              + new Timestamp(goalInstanceEntity.getStartDate().getTime())
              + "'"
              + ","
              + "'"
              + new Timestamp(goalInstanceEntity.getEndDate().getTime())
              + "'"
              + ","
              + "'"
              + goalInstanceEntity.getTarget()
              + "'"
              + ","
              + "'"
              + toNumeralString(goalInstanceEntity.isRecurring())
              + "'"
              + ","
              + "'"
              + goalInstanceEntity.getRecurringFrequency()
              + "'"
              + ","
              + "'"
              + goalInstanceEntity.getCreatedBy()
              + "'"
              + ","
              + "'"
              + goalInstanceEntity.getLastUpdatedBy()
              + "'"
              + ","
              + "'"
              + toNumeralString(goalInstanceEntity.getDeleted())
              + "'"
              + ","
              + "'"
              + goalInstanceEntity.getGoalAssignee().getId()
              + "'"
              + ","
              + "'"
              + goalInstanceEntity.getGoalDefinitionCode()
              + "'"
              + ","
              + "'"
              + new Timestamp(new Date().getTime())
              + "'"
              + ","
              + "'"
              + new Timestamp(new Date().getTime())
              + "'"
              + ","
              + "'"
              + toNumeralString(goalInstanceEntity.getActive())
              + "'"
              + ")";
      eachEntityValues.add(entityString);
    }
    valuesString = String.join(",", eachEntityValues);
    return valuesString;
  }

  @Transactional
  public void insertIgnoreGoalInstancesWithQuery(List<GoalInstanceEntity> goalInstanceEntityList) {
    log.info(
        "In insertIgnoreGoalInstancesWithQuery for {} goalInstanceEntityList ",
        goalInstanceEntityList.size());
    String query =
        " INSERT IGNORE INTO goal_instances (client_id, user_id, start_date, end_date, target, is_recurring, "
            + "recurring_frequency, created_by, last_updated_by, deleted, goal_assignee_id, goal_definition_code, created_date, "
            + "last_updated_date, is_active) VALUES ";
    String valuesString = getValuesStringFromEntities(goalInstanceEntityList);
    query = query + valuesString;
    //       log.info("Custom query is {}", query);
    log.info("Executing native query");
    entityManager.createNativeQuery(query).executeUpdate();
  }

  @Transactional
  public void activateGoalInstancesForGivenVersion(String version, List<String> userIds) {

    log.info("In activateGoalInstancesForGivenVersion for version {}", version);
    // Step 1: Disable all the goal_instances with matching goal_assignee_id which matches the
    // version
    log.info(
        "Step 1: Disable all the goal_instances with matching goal_assignee_id which matches the version {} ",
        version);
    String query1 =
        "UPDATE goal_instances gi \n"
            + "    INNER JOIN goal_assignee_settings_versions gav ON gav.goal_assignee_id = gi.goal_assignee_id \n"
            + "    INNER JOIN goal_assignees gaa ON gaa.id = gav.goal_assignee_id \n"
            + "    SET gi.is_active = 0\n"
            + "    WHERE gav.goal_assignee_id=gi.goal_assignee_id \n"
            + "        AND gi.end_date > current_date()\n"
            + "        AND gav.version = '"
            + version
            + "'";

    // Step 2: Update the goal_instances to copy the target from needed
    log.info("Step 2: Update the goal_instances to copy the target from needed");
    String query2 =
        "UPDATE goal_instances gi\n"
            + "INNER JOIN goal_assignee_settings_versions gav ON gav.goal_assignee_id = gi.goal_assignee_id\n"
            + "INNER JOIN goal_assignees gaa ON gaa.id = gav.goal_assignee_id\n"
            + "SET gi.target = gav.target, gaa.current_version = gav.version, gaa.active=1, gaa.deleted=gav.deleted, gi.deleted = gav"
            + ".deleted WHERE gav.goal_assignee_id=gi.goal_assignee_id AND gi.end_date > current_date() AND gav.version = '"
            + version
            + "'";
    //       log.info("Query 1 to be executed is {} ", query1);
    //       log.info("Query 2 to be executed is {}", query2);

    boolean hasUsersIds = userIds != null && !userIds.isEmpty();

    if (hasUsersIds) {
      query1 += " AND gav.user_id IN (:userIds)";
      query2 += " AND gav.user_id IN (:userIds)";
    }

    Query q1 = entityManager.createNativeQuery(query1);
    Query q2 = entityManager.createNativeQuery(query2);

    if (hasUsersIds) {
      q1.setParameter("userIds", userIds);
      q2.setParameter("userIds", userIds);
    }

    q1.executeUpdate();
    q2.executeUpdate();
    // Clearing persistence context to freshly fetch active goal assignee entities
    // in the createKafkaMessageAndSend function to send notifications for the active goals
    // Else hibernate will return cached goal assignees which has active as false (initially set as
    // false in the goal assignment flow)
    // and kafka notification won't be triggered because active is still false in cache
    entityManager.clear();
    log.info("Finished activateGoalInstancesForGivenVersion for version {}", version);
  }
}
