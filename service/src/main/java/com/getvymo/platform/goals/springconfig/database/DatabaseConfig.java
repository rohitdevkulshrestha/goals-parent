package com.getvymo.platform.goals.springconfig.database;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import javax.sql.DataSource;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
@Log4j2
public class DatabaseConfig {
  @Bean
  public DataSource getDataSource(final Environment env) {
    log.debug("Dump env config ::", () -> env.toString());
    HikariConfig config = getHikariConfig(env);
    HikariDataSource ds = new HikariDataSource(config);
    return ds;
  }

  private HikariConfig getHikariConfig(final Environment env) {
    HikariConfig config = new HikariConfig();
    config.setJdbcUrl(env.getProperty("app.db.pgsqljdbcuri"));
    config.setUsername(env.getProperty("app.db.dba.username"));
    config.setPassword(env.getProperty("app.db.dba.password"));
    config.setDriverClassName(env.getProperty("app.db.jdbcDriverName"));
    config.addDataSourceProperty("currentSchema", env.getProperty("app.db.schema"));
    config.addDataSourceProperty("logUnclosedConnections", "true");
    config.setMaximumPoolSize(Integer.parseInt(env.getProperty("app.db.cpSize")));
    config.setPoolName(env.getProperty("spring.application.name"));
    // mgmt
    config.setAllowPoolSuspension(true);
    config.addHealthCheckProperty("connectivityCheckTimeoutMs", "1000");
    config.addHealthCheckProperty("expected99thPercentileMs", "10");
    return config;
  }
}
