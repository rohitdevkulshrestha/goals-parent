package com.getvymo.platform.goals.springconfig.database;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Objects;
import java.util.Properties;
import javax.sql.DataSource;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
    basePackages = {"com.getvymo.platform.goals.app.persistence.dao"},
    entityManagerFactoryRef = "emf")
@Log4j2
public class JPAConfig {
  @Bean(name = "emf")
  public LocalContainerEntityManagerFactoryBean getLocalContainerEntityManagerFactoryBean(
      final DataSource dataSource, final Environment env, final ObjectMapper om) {
    LocalContainerEntityManagerFactoryBean emFactoryBean =
        new LocalContainerEntityManagerFactoryBean();
    emFactoryBean.setDataSource(dataSource);
    emFactoryBean.setPackagesToScan(
        new String[] {
          "com.getvymo.platform.goals.app.persistence.entities",
        });
    HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
    vendorAdapter.setDatabase(Database.MYSQL);
    emFactoryBean.setJpaVendorAdapter(vendorAdapter);
    emFactoryBean.setJpaProperties(hibernateAdditionalProperties(env));
    return emFactoryBean;
  }

  @Bean
  public PlatformTransactionManager transactionManager(
      LocalContainerEntityManagerFactoryBean emFactoryBean) {
    return new JpaTransactionManager(Objects.requireNonNull(emFactoryBean.getObject()));
  }

  private static Properties hibernateAdditionalProperties(final Environment env) {
    Properties properties = new Properties();
    // This will recreate the data base
    properties.setProperty("hibernate.hbm2ddl.auto", env.getProperty("hibernate.hbm2ddl.auto"));
    properties.setProperty("hibernate.default_schema", env.getProperty("app.db.schema"));
    properties.setProperty("hibernate.enable_lazy_load_no_trans", "true");
    properties.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQL55Dialect");
    properties.setProperty("hibernate.show_sql", "true");
    properties.setProperty("hibernate.globally_quoted_identifiers", "true");
    properties.setProperty("hibernate.globally_quoted_identifiers_skip_column_definitions", "true");
    return properties;
  }
}
