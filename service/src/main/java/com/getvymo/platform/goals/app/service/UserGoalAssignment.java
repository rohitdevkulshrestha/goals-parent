package com.getvymo.platform.goals.app.service;

import com.getvymo.platform.goals.app.common.models.GoalAssigneeSettings;
import com.getvymo.platform.goals.app.common.models.GoalDefinitionSettings;
import com.getvymo.platform.goals.app.service.evaluation.goalsresponse.UserObject;

public class UserGoalAssignment {

  private GoalAssigneeSettings goalAssigneeSettings;

  private GoalDefinitionSettings goalDefinitionSettings;

  private UserObject user;

  // TODO: Keeping this as goalDefinitionId even though it is goalDefinitionCode technically so that
  // App contract doesn't break
  private String goalDefinitionId;

  private String goalAssigneeId;

  public UserGoalAssignment(
      GoalAssigneeSettings goalAssigneeSettings,
      GoalDefinitionSettings goalDefinitionSettings,
      UserObject user,
      String goalDefinitionId,
      String goalAssigneeId) {
    this.goalAssigneeSettings = goalAssigneeSettings;
    this.goalDefinitionSettings = goalDefinitionSettings;
    this.user = user;
    this.goalDefinitionId = goalDefinitionId;
    this.goalAssigneeId = goalAssigneeId;
  }

  public UserGoalAssignment() {}

  public GoalAssigneeSettings getGoalAssigneeSettings() {
    return goalAssigneeSettings;
  }

  public void setGoalAssigneeSettings(GoalAssigneeSettings goalAssigneeSettings) {
    this.goalAssigneeSettings = goalAssigneeSettings;
  }

  public GoalDefinitionSettings getGoalDefinitionSettings() {
    return goalDefinitionSettings;
  }

  public void setGoalDefinitionSettings(GoalDefinitionSettings goalDefinitionSettings) {
    this.goalDefinitionSettings = goalDefinitionSettings;
  }

  public UserObject getUser() {
    return user;
  }

  public void setUser(UserObject user) {
    this.user = user;
  }

  public String getGoalDefinitionId() {
    return goalDefinitionId;
  }

  public void setGoalDefinitionId(String goalDefinitionId) {
    this.goalDefinitionId = goalDefinitionId;
  }

  public String getGoalAssigneeId() {
    return goalAssigneeId;
  }

  public void setGoalAssigneeId(String goalAssigneeId) {
    this.goalAssigneeId = goalAssigneeId;
  }
}
