package com.getvymo.platform.goals.app.service;

public class BulkGoalAchievement extends ExternalAchievementApiObject {
  private String msg;
  private String row;
  private String status;
  private String vymoCode;

  public BulkGoalAchievement() {}

  public BulkGoalAchievement(String msg, String row, String status, String vymoCode) {
    this.msg = msg;
    this.row = row;
    this.status = status;
    this.vymoCode = vymoCode;
  }

  public String getMsg() {
    return msg;
  }

  public void setMsg(String msg) {
    this.msg = msg;
  }

  public String getRow() {
    return row;
  }

  public void setRow(String row) {
    this.row = row;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getVymoCode() {
    return vymoCode;
  }

  public void setVymoCode(String vymoCode) {
    this.vymoCode = vymoCode;
  }
}
