package com.getvymo.platform.goals.app.service.evaluation.metricsrequest;

import java.util.List;

public class IncludeDimensionFilter extends DimensionFilter {
  private List<String> values;

  public IncludeDimensionFilter(
      String type, String dimension, String attribute, List<String> values) {
    super(type, dimension, attribute);
    this.values = values;
  }

  public IncludeDimensionFilter(String dimension) {
    super(dimension);
  }

  public List<String> getValues() {
    return values;
  }

  public void setValues(List<String> values) {
    this.values = values;
  }
}
