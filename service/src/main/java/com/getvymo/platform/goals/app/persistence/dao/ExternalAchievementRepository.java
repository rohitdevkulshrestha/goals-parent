package com.getvymo.platform.goals.app.persistence.dao;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.getvymo.platform.goals.app.persistence.entities.ExternalAchievementEntity;
import com.getvymo.platform.goals.app.service.utils.GoalsUtils;
import com.getvymo.platform.goals.app.utils.es.ElasticRestConnection;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import lombok.extern.log4j.Log4j2;
import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
@Log4j2
public class ExternalAchievementRepository implements IExternalAchievementRepository {

  @Autowired private ElasticRestConnection elasticRestConnection;
  @Autowired private GoalsUtils goalsUtils;

  //    private final RestHighLevelClient client = elasticRestConnection.getConnection();

  @Override
  public List<ExternalAchievementEntity> getMatchingExternalAchievementEntitiesForOneUser(
      String userId,
      String goalDefinitionCode,
      String clientId,
      Long startDate,
      Long endDate,
      Boolean isRecurring,
      String recurringFrequency) {
    log.info("In getMatchingExternalAchievementEntitiesForOneUser 1 ");
    List<ExternalAchievementEntity> externalAchievementEntities = new ArrayList<>();
    String indexName = elasticRestConnection.getIndexName(clientId);
    BoolQueryBuilder query =
        QueryBuilders.boolQuery()
            .must(QueryBuilders.termQuery("goalDefinitionCode", goalDefinitionCode))
            .must(QueryBuilders.termQuery("userId", userId))
            .must(QueryBuilders.termQuery("recurringFrequency", recurringFrequency))
            .must(QueryBuilders.termQuery("startDate", startDate))
            .must(QueryBuilders.termQuery("endDate", endDate));

    SearchRequest searchRequest = new SearchRequest(indexName);
    SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
    searchSourceBuilder.query(query);
    searchSourceBuilder.size(10000);
    searchRequest.source(searchSourceBuilder);

    try {
      RestHighLevelClient client = elasticRestConnection.getConnection();
      SearchResponse searchResponse = client.search(searchRequest);
      externalAchievementEntities =
          getExternalAchievementEntitiesFromSearchResponse(searchResponse);

    } catch (IOException e) {
      log.error("Exception while getMatchingExternalAchievementEntitiesForOneUser", e);
    }

    //        SearchQuery searchQuery = new
    // NativeSearchQueryBuilder().withQuery(query).withIndices(indexName).build();
    //        List<ExternalAchievementEntity> externalAchievementEntities =
    // esTemplate.queryForList(searchQuery, ExternalAchievementEntity.class);
    //        return externalAchievementEntities;
    return externalAchievementEntities;
  }

  @Override
  public String saveExternalAchievement(ExternalAchievementEntity externalAchievementEntity) {

    String indexName = elasticRestConnection.getIndexName(externalAchievementEntity.getClientId());
    String responseString = "";
    try {
      IndexRequest indexRequest = new IndexRequest(indexName, "achievements");
      indexRequest.id(externalAchievementEntity.getId());
      ObjectMapper objectMapper = new ObjectMapper();
      objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
      String jsonString =
          objectMapper
              .writerWithDefaultPrettyPrinter()
              .writeValueAsString(externalAchievementEntity);
      indexRequest.source(jsonString, XContentType.JSON);
      RestHighLevelClient client = elasticRestConnection.getConnection();
      IndexResponse indexResponse = client.index(indexRequest);
      responseString = indexResponse.getId();
      //            goalsUtils.prettyPrintStringFromJSON(indexResponse, "Indexing response");
    } catch (IOException e) {
      log.error("Exception while saveExternalAchievement ", e);
    }
    return responseString;
  }

  @Override
  public List<ExternalAchievementEntity> findByIdsIn(
      List<String> listOfAchievementIds, String clientId) {
    log.info(
        "In findByIdsIn for clientId {} and listOfAchievementIds {} ",
        clientId,
        listOfAchievementIds);
    RestHighLevelClient client = elasticRestConnection.getConnection();
    String indexName = elasticRestConnection.getIndexName(clientId);
    List<ExternalAchievementEntity> externalAchievementEntities = new ArrayList<>();
    try {
      HashSet<String> idsSet = new HashSet<>(listOfAchievementIds);
      BoolQueryBuilder query =
          QueryBuilders.boolQuery().filter(QueryBuilders.termsQuery("_id", idsSet));
      SearchRequest searchRequest = new SearchRequest(indexName);
      SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
      searchSourceBuilder.query(query);
      searchSourceBuilder.size(10000);
      searchRequest.source(searchSourceBuilder);
      SearchResponse searchResponse = client.search(searchRequest);
      externalAchievementEntities =
          getExternalAchievementEntitiesFromSearchResponse(searchResponse);

    } catch (IOException e) {
      log.error("IOException while findByIdsIn ", e);
    }
    return externalAchievementEntities;
  }

  @Override
  public List<ExternalAchievementEntity> getMatchingEntitiesForSetOfUsersAndCurrentDateRange(
      List<String> userIds,
      String goalDefinitionCode,
      String clientId,
      Long currentDate1,
      Long currentDate2,
      String recurringFrequency) {
    String indexName = elasticRestConnection.getIndexName(clientId);
    HashSet<String> userIdSet = new HashSet<>(userIds);
    List<ExternalAchievementEntity> externalAchievementEntities = new ArrayList<>();
    try {
      BoolQueryBuilder query =
          QueryBuilders.boolQuery()
              .filter(QueryBuilders.termsQuery("userId", userIdSet))
              .must(QueryBuilders.termQuery("goalDefinitionCode", goalDefinitionCode))
              .must(QueryBuilders.termQuery("recurringFrequency", recurringFrequency))
              .must(QueryBuilders.rangeQuery("startDate").lte(currentDate1))
              .must(QueryBuilders.rangeQuery("endDate").gte(currentDate2));
      SearchRequest searchRequest = new SearchRequest(indexName);
      SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
      searchSourceBuilder.query(query);
      searchSourceBuilder.size(10000);
      searchRequest.source(searchSourceBuilder);
      RestHighLevelClient client = elasticRestConnection.getConnection();
      SearchResponse searchResponse = client.search(searchRequest);
      externalAchievementEntities =
          getExternalAchievementEntitiesFromSearchResponse(searchResponse);
    } catch (IOException e) {
      log.error("Exception while getMatchingEntitiesForSetOfUsersAndCurrentDateRange ", e);
    }
    return externalAchievementEntities;
  }

  @Override
  public List<ExternalAchievementEntity> getMatchingEntitiesForSetOfUsersAndAnyDateRange(
      List<String> userIds,
      String goalDefinitionCode,
      String clientId,
      Long startDate,
      Long endDate,
      String recurringFrequency) {
    log.info(
        "In getMatchingEntitiesForSetOfUsersAndAnyDateRange for userIds {} startDate {} endDate {} ",
        userIds,
        startDate,
        endDate);
    String indexName = elasticRestConnection.getIndexName(clientId);
    HashSet<String> userIdSet = new HashSet<>(userIds);

    List<ExternalAchievementEntity> externalAchievementEntities = new ArrayList<>();
    try {
      BoolQueryBuilder query =
          QueryBuilders.boolQuery()
              .filter(QueryBuilders.termsQuery("userId", userIdSet))
              .must(QueryBuilders.termQuery("goalDefinitionCode", goalDefinitionCode))
              .must(QueryBuilders.termQuery("recurringFrequency", recurringFrequency))
              .must(QueryBuilders.termQuery("startDate", startDate))
              .must(QueryBuilders.termQuery("endDate", endDate));
      SearchRequest searchRequest = new SearchRequest(indexName);
      SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
      searchSourceBuilder.query(query);
      searchSourceBuilder.size(10000);
      searchRequest.source(searchSourceBuilder);
      SearchResponse searchResponse = null;
      RestHighLevelClient client = elasticRestConnection.getConnection();
      searchResponse = client.search(searchRequest);
      externalAchievementEntities =
          getExternalAchievementEntitiesFromSearchResponse(searchResponse);
    } catch (IOException e) {
      log.error("Exception while  getMatchingEntitiesForSetOfUsersAndAnyDateRange ", e);
    } catch (ElasticsearchException exception) {
      log.error("ElasticsearchException ", exception);
    } catch (Exception ex) {
      log.error("Exception while  getMatchingEntitiesForSetOfUsersAndAnyDateRange ", ex);
    }

    log.info(
        "Returning from getMatchingEntitiesForSetOfUsersAndAnyDateRange with {} externalAchievementEntities",
        externalAchievementEntities.size());
    return externalAchievementEntities;
  }

  @Override
  public void bulkSaveExternalAchievementEntities(
      List<ExternalAchievementEntity> externalAchievementEntities, String clientId) {
    String indexName = elasticRestConnection.getIndexName(clientId);

    try {
      BulkRequest bulkRequest = new BulkRequest();
      for (ExternalAchievementEntity ea : externalAchievementEntities) {
        IndexRequest indexRequest = new IndexRequest(indexName, "achievements");
        indexRequest.id(ea.getId());
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        String jsonString = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(ea);
        indexRequest.source(jsonString, XContentType.JSON);

        bulkRequest.add(indexRequest);
      }
      RestHighLevelClient client = elasticRestConnection.getConnection();
      BulkResponse bulkResponse = client.bulk(bulkRequest);
      //            goalsUtils.prettyPrintStringFromJSON(bulkResponse, "bulkResponse");
      log.info(
          "Successfully indexed {} externalAchievementEntities",
          externalAchievementEntities.size());
      if (bulkResponse.hasFailures()) {
        log.error("Bulk upload failed with message {}", bulkResponse.buildFailureMessage());
      }
    } catch (Exception e) {
      log.error("Failed bulk upload for {} documents", externalAchievementEntities.size(), e);
    }
  }

  private List<ExternalAchievementEntity> getExternalAchievementEntitiesFromSearchResponse(
      SearchResponse searchResponse) {
    log.info("In getExternalAchievementEntitiesFromSearchResponse for searchResponse");
    List<ExternalAchievementEntity> externalAchievementEntitiesList = new ArrayList<>();

    SearchHit[] hits = searchResponse.getHits().getHits();
    log.info("Found {} hits ", hits.length);
    ObjectMapper mapper = new ObjectMapper();
    try {
      for (SearchHit hit : hits) {
        externalAchievementEntitiesList.add(
            mapper.readValue(hit.getSourceAsString(), ExternalAchievementEntity.class));
      }
    } catch (JsonProcessingException e) {
      log.error(
          "JsonProcessingException while getExternalAchievementEntitiesFromSearchResponse ", e);
    }

    //        goalsUtils.prettyPrintStringFromJSON(externalAchievementEntitiesList,
    // "listOfObjectHits");
    log.info(
        "Returing from getExternalAchievementEntitiesFromSearchResponse with {} externalAchievementEntities",
        externalAchievementEntitiesList.size());
    return externalAchievementEntitiesList;
  }
}
