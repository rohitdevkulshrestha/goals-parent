/** */
package com.getvymo.platform.goals.app.persistence.entities;

import com.getvymo.platform.goals.app.service.utils.GoalsUtils;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/** @author chandrasekhar */
@Entity
@Table(name = "goal_assignees")
public class GoalAssigneesEntity extends GoalAuditableEntity {

  @Id
  // do you need varchar(36) here ?
  // isn't there a auto incremented id (primary key) ?
  @Column(name = "id", columnDefinition = "VARCHAR(36)")
  private String id;

  private String goalDefinitionCode;

  private String userId;

  private String clientId;

  private String currentVersion;

  private String createdBy;

  private Boolean deleted;

  private Boolean active;

  @OneToMany(mappedBy = "goalAssignee")
  private List<GoalInstanceEntity> goalInstances = new ArrayList();

  protected GoalAssigneesEntity() {}

  public GoalAssigneesEntity(
      String currentVersion,
      String userId,
      String clientId,
      String createdBy,
      Boolean deleted,
      Boolean active) {
    super();
    this.clientId = clientId;
    this.id = GoalsUtils.generateRandomStringId();
    this.currentVersion = currentVersion;
    this.createdBy = createdBy;
    this.userId = userId;
    this.deleted = deleted;
    this.active = active;
  }

  public String getId() {
    return id;
  }

  public String getCurrentVersion() {
    return currentVersion;
  }

  public void setCurrentVersion(String currentVersion) {
    this.currentVersion = currentVersion;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public String getGoalDefinitionCode() {
    return goalDefinitionCode;
  }

  public void setGoalDefinitionCode(String goalDefinitionCode) {
    this.goalDefinitionCode = goalDefinitionCode;
  }

  public List<GoalInstanceEntity> getGoalInstances() {
    return goalInstances;
  }

  public void assignGoalInstance(GoalInstanceEntity goalInstance) {
    this.goalInstances.add(goalInstance);
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public Boolean getDeleted() {
    return deleted;
  }

  public void setDeleted(Boolean deleted) {
    this.deleted = deleted;
  }

  public Boolean getActive() {
    return active;
  }

  public void setActive(Boolean active) {
    this.active = active;
  }

  public String getClientId() {
    return clientId;
  }

  public void setClientId(String clientId) {
    this.clientId = clientId;
  }
}
