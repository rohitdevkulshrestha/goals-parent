package com.getvymo.platform.goals.app.service;

import java.util.Date;

public interface IAuditable {
  Date getCreatedDate();

  Date getLastUpdatedDate();

  String getCreatedBy();

  String getLastUpdatedBy();
}
