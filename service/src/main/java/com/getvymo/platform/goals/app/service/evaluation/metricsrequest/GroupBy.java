package com.getvymo.platform.goals.app.service.evaluation.metricsrequest;

/** Created by debashish on 19/08/19. */
public abstract class GroupBy {
  private String type; // user, date, dimension

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public GroupBy() {}

  public GroupBy(String type) {
    this.type = type;
  }
}
