package com.getvymo.platform.goals.app.common.models;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import in.vymo.core.config.model.goals.TeamDefinitionType;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@JsonDeserialize(as = TeamGoalDefinitionSettings.class)
public class TeamGoalDefinitionSettings extends GoalDefinitionSettings {

  @Enumerated(EnumType.STRING)
  private TeamDefinitionType teamDefinitionType;

  private String teamDefinitionValue;

  //    public TeamGoalDefinitionSettings(GoalType goalType, String metric, String name, String
  // description, String goalDefinitionCode,
  //                                      List<GoalMetricFilter> metricFilters, TeamDefinitionType
  // teamDefinitionType,
  //                                      String teamDefinitionValue, Boolean external, Boolean
  // showAchievementList, Integer goalOrder) {
  //        super(goalType, metric, name, description, goalDefinitionCode, metricFilters, external,
  // showAchievementList, goalOrder);
  //        this.teamDefinitionType = teamDefinitionType;
  //        this.teamDefinitionValue = teamDefinitionValue;
  //    }

  public TeamGoalDefinitionSettings() {}

  public TeamDefinitionType getTeamDefinitionType() {
    return teamDefinitionType;
  }

  public void setTeamDefinitionType(TeamDefinitionType teamDefinitionType) {
    this.teamDefinitionType = teamDefinitionType;
  }

  public String getTeamDefinitionValue() {
    return teamDefinitionValue;
  }

  public void setTeamDefinitionValue(String teamDefinitionValue) {
    this.teamDefinitionValue = teamDefinitionValue;
  }
}
