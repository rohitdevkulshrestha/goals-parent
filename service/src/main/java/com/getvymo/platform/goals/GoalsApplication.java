package com.getvymo.platform.goals;

import com.getvymo.platform.goals.app.utils.configclient.ConfigClient;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan({"com.getvymo.goals.api", "com.getvymo.goals.*"})
@EntityScan("com.getvymo.goals.dal.entity")
@EnableJpaRepositories(
    value = "com.getvymo.goals.dal.repository",
    entityManagerFactoryRef = "entityManagerFactory")
@EnableJpaAuditing
// @EnableAutoConfiguration(
//    exclude = {ElasticsearchAutoConfiguration.class, RestClientAutoConfiguration.class})
@Log4j2
public class GoalsApplication {

  public static void main(String[] args) {
    // Logger logger = LoggerFactory.getLogger("Goals Application");

    log.info("initialising new relic");

    String license = System.getenv("newrelic.config.license_key");

    String appName = System.getenv("newrelic.config.newrelic_app_name");

    if (license != null) {
      System.setProperty(
          "newrelic.config.license_key", System.getenv("newrelic.config.license_key"));
      log.info("liscenseKey set to=", System.getProperty("newrelic.config.license_key"));
    }

    if (appName != null) {
      System.setProperty(
          "newrelic.config.app_name", System.getenv("newrelic.config.newrelic_app_name"));
      log.info("new relic environment  set to=", System.getProperty("newrelic.config.app_name"));
    }
    SpringApplication.run(GoalsApplication.class, args);
  }

  @Bean
  public ConfigClient configClient() {
    return ConfigClient.INSTANCE;
  }
}
