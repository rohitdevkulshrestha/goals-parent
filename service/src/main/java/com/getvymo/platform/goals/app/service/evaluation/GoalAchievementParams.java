package com.getvymo.platform.goals.app.service.evaluation;

import java.util.Date;
import java.util.List;

public class GoalAchievementParams {
  private Date startDate;
  private Date endDate;
  private List<String> teamUserIds;
  private Boolean withGroupby;

  public GoalAchievementParams(
      Date startDate, Date endDate, List<String> teamUserIds, Boolean withGroupby) {
    this.startDate = startDate;
    this.endDate = endDate;
    this.teamUserIds = teamUserIds;
    this.withGroupby = withGroupby;
  }

  public GoalAchievementParams() {}

  public Boolean getWithGroupby() {
    return withGroupby;
  }

  public void setWithGroupby(Boolean withGroupby) {
    this.withGroupby = withGroupby;
  }

  public List<String> getTeamUserIds() {
    return teamUserIds;
  }

  public void setTeamUserIds(List<String> teamUserIds) {
    this.teamUserIds = teamUserIds;
  }

  public Date getStartDate() {
    return startDate;
  }

  public void setStartDate(Date startDate) {
    this.startDate = startDate;
  }

  public Date getEndDate() {
    return endDate;
  }

  public void setEndDate(Date endDate) {
    this.endDate = endDate;
  }
}
