package com.getvymo.platform.goals.app.service.evaluation.goalsresponse;

public class UserObject {
  private String code;
  private String name;

  public UserObject(String code) {
    this.code = code;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
