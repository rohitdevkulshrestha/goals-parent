package com.getvymo.platform.goals.app.common.models;

import java.util.ArrayList;

public class StandardListResponse<Object> {
  long count;
  long pageNumber;
  long pageSize;

  ArrayList data = new ArrayList();

  public StandardListResponse() {}

  public StandardListResponse(long count, long pageNumber, long pageSize, ArrayList data) {
    super();
    this.count = count;
    this.pageNumber = pageNumber;
    this.pageSize = pageSize;
    this.data = data;
  }

  public ArrayList getData() {
    return data;
  }

  public void setData(ArrayList data) {
    this.data = data;
  }

  public void setCount(long count) {
    this.count = count;
  }

  public void setPageNumber(long pageNumber) {
    this.pageNumber = pageNumber;
  }

  public void setPageSize(long pageSize) {
    this.pageSize = pageSize;
  }

  public long getCount() {
    return count;
  }

  public void setCount(int count) {
    this.count = count;
  }

  public long getPageNumber() {
    return pageNumber;
  }

  public void setPageNumber(int pageNumber) {
    this.pageNumber = pageNumber;
  }

  public long getPageSize() {
    return pageSize;
  }

  public void setPageSize(int pageSize) {
    this.pageSize = pageSize;
  }
}
