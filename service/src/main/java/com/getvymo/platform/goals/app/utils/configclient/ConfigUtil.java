package com.getvymo.platform.goals.app.utils.configclient;

import com.getvymo.platform.goals.app.errors.GoalManagementServiceException;
import in.vymo.configuration.api.ConfigurationClient;
import in.vymo.configuration.lib.exceptions.ConfigClientException;
import in.vymo.core.config.model.client.Client;
import in.vymo.core.config.model.core.dimensionV2.Dimension;
import in.vymo.core.config.model.core.inputfield.GenericInputField;
import in.vymo.core.config.model.core.inputfield.InputField;
import in.vymo.core.config.model.core.inputfield.SavedListSifgInputField;
import in.vymo.core.config.model.core.inputfield.StaticSifgInputField;
import in.vymo.core.config.model.core.metric.Metric;
import in.vymo.core.config.model.goals.GoalDefinition;
import in.vymo.core.config.model.goals.GoalDefinitionMappings;
import in.vymo.core.config.model.goals.GoalGroupDefinition;
import in.vymo.core.config.model.goals.GoalType;
import in.vymo.core.config.model.goals.GoalsConfig;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Log4j2
public class ConfigUtil {

  @Autowired private ConfigurationClient client = ConfigClient.client();
  private int ERROR_CODE = 101;

  public GoalsConfig getGoalsConfig(String clientId) {
    log.info("Inside getGoalsConfig for client " + clientId);
    try {
      Client clientConfig = client.getClient(clientId);
      if (clientConfig == null) {
        log.error("Not able to get client config for client " + clientId);
        throw new GoalManagementServiceException(
            "Not able to get client config for client " + clientId);
      }
      log.info("Inside getGoalDefinitionsByClient. Got Client Config for client " + clientId);
      GoalsConfig goalsConfig = clientConfig.getGoalsConfig();
      if (goalsConfig == null) {
        log.error("Goal config null from config");
        throw new GoalManagementServiceException(
            "Not able to get goals config for client " + clientId);
      }

      return goalsConfig;
    } catch (ConfigClientException e) {
      log.error("Error while fetching from client config", e);
      throw new GoalManagementServiceException(e.getMessage());
    }
  }

  public GoalDefinition getGoalDefinitionByCode(String clientId, String goalDefinitionCode) {
    log.info(
        "Inside getGoalDefinitionByCode for client "
            + clientId
            + " Trying to get goal definition for "
            + goalDefinitionCode);

    GoalsConfig goalsConfig = getGoalsConfig(clientId);
    Map<String, GoalDefinition> goalDefinitions = goalsConfig.getGoalDefinitions();

    if (goalDefinitions.isEmpty()) {
      log.error("Empty goal definitions for client " + clientId);
      throw new GoalManagementServiceException("Empty goal definitions for client " + clientId);
    }
    if (goalDefinitions.get(goalDefinitionCode) == null) {
      log.error("No goal definition found for " + goalDefinitionCode);
      throw new GoalManagementServiceException(
          "No goal definition found for " + goalDefinitionCode);
    }
    return goalDefinitions.get(goalDefinitionCode);
  }

  public GoalDefinition getEnabledGoalDefinitionByCode(String clientId, String goalDefinitionCode) {
    log.info(
        "Inside getGoalDefinitionByCode for client "
            + clientId
            + " Trying to get goal definition for "
            + goalDefinitionCode);

    Map<String, GoalDefinition> goalDefinitions = getEnabledGoalDefinitionsByClient(clientId);
    if (goalDefinitions.isEmpty()) {
      log.error("Empty goal definitions for client " + clientId);
      throw new GoalManagementServiceException("Empty goal definitions for client " + clientId);
    }
    if (goalDefinitions.get(goalDefinitionCode) == null) {
      log.error("No goal definition  found for " + goalDefinitionCode);
      throw new GoalManagementServiceException(
          "No goal definition  found for " + goalDefinitionCode);
    }
    return goalDefinitions.get(goalDefinitionCode);
  }

  public Map<String, GoalDefinition> getAllGoalDefinitionsByClient(String clientId) {
    log.info("Inside getAllGoalDefinitionsByClient for client " + clientId);
    try {
      GoalsConfig goalsConfig = getGoalsConfig(clientId);

      Map<String, GoalDefinition> goalDefinitionMap = goalsConfig.getGoalDefinitions();
      // Excluding goal groups
      Map<String, GoalDefinition> result = new HashMap<>();
      for (GoalDefinition goalDefinition : goalDefinitionMap.values()) {
        if (goalDefinition.getType() != GoalType.goalGroup) {
          result.put(goalDefinition.getCode(), goalDefinition);
        }
      }

      return result;
    } catch (Exception e) {
      log.error("Error while fetching from client config", e);
      throw new GoalManagementServiceException(e.getMessage());
    }
  }

  public Map<String, GoalDefinition> getEnabledGoalDefinitionsByClient(String clientId) {
    Map<String, GoalDefinition> goalDefinitionsEnabled = new HashMap<>();
    Map<String, GoalDefinition> goalDefinitions = getAllGoalDefinitionsByClient(clientId);
    List<GoalDefinitionMappings> goalDefinitionMappings = getGoalDefinitionsMappings(clientId);
    for (GoalDefinitionMappings goalDefinitionMapping : goalDefinitionMappings) {
      String goalDefinitionCode = goalDefinitionMapping.getGoalDefinitionCode();
      if (!goalDefinitionMapping.isDisabled() && goalDefinitions.containsKey(goalDefinitionCode)) {
        goalDefinitionsEnabled.put(goalDefinitionCode, goalDefinitions.get(goalDefinitionCode));
      }
    }
    return goalDefinitionsEnabled;
  }

  public List<GoalDefinitionMappings> getGoalDefinitionsMappings(String clientId) {
    log.info("Inside getGoalDefinitionsMappings");
    try {
      GoalsConfig goalsConfig = getGoalsConfig(clientId);

      List<GoalDefinitionMappings> goalDefinitionMappingResult =
          new ArrayList<GoalDefinitionMappings>();

      List<GoalDefinitionMappings> goalDefinitionMappings = goalsConfig.getGoalDefinitionMappings();
      Map<String, GoalDefinition> goalDefinitionMap = goalsConfig.getGoalDefinitions();

      if (goalDefinitionMap.isEmpty()) {
        log.error("Empty goal definitions for client " + clientId);
        throw new GoalManagementServiceException("Empty goal definitions for client " + clientId);
      }
      for (GoalDefinitionMappings goalDefinitionMapping : goalDefinitionMappings) {
        if (goalDefinitionMap.get(goalDefinitionMapping.getGoalDefinitionCode()) == null) {
          log.error(
              "No goal definition found for " + goalDefinitionMapping.getGoalDefinitionCode());
          throw new GoalManagementServiceException(
              "No goal definition found for " + goalDefinitionMapping.getGoalDefinitionCode());
        }
        GoalDefinition goalDefinition =
            goalDefinitionMap.get(goalDefinitionMapping.getGoalDefinitionCode());
        if (goalDefinition.getType() != GoalType.goalGroup) {
          goalDefinitionMappingResult.add(goalDefinitionMapping);
        }
      }
      return goalDefinitionMappingResult;
    } catch (Exception e) {
      log.error("Error while fetching from client config", e);
      throw new GoalManagementServiceException(e.getMessage());
    }
  }

  public Metric getMetric(String clientId, String metric) {
    try {
      Client clientConfig = client.getClient(clientId);
      if (clientConfig == null) {
        log.error("Not able to get client config for client " + clientId);
        throw new GoalManagementServiceException(
            "Not able to get client config for client " + clientId);
      }
      log.info("Inside getMetric. Got Client Config for client " + clientId);
      Map<String, Metric> metrics = clientConfig.getMetrics();
      if (metrics == null) {
        log.error("Not able to get metrics config for client " + clientId);
        throw new GoalManagementServiceException(
            "Not able to get metrics config for client " + clientId);
      }
      return metrics.get(metric);
    } catch (ConfigClientException e) {
      log.error("Error while fetching from client config", e);
      throw new GoalManagementServiceException(e.getMessage());
    }
  }

  public Integer getGoalOrder(GoalDefinition goalDefinition, String clientId) {
    List<GoalDefinitionMappings> goalDefinitionMappings = getGoalDefinitionsMappings(clientId);
    Optional<GoalDefinitionMappings> goalDefinitionMapping1 =
        goalDefinitionMappings.stream()
            .filter(
                entry ->
                    StringUtils.equals(entry.getGoalDefinitionCode(), goalDefinition.getCode()))
            .findFirst();
    if (goalDefinitionMapping1.isPresent()) {
      return goalDefinitionMapping1.get().getOrder();
    }

    return 0;
  }

  public Dimension getDimensionInfo(String dimensionCode, String clientId) {
    try {
      Client clientConfig = client.getClient(clientId);
      if (clientConfig == null) {
        log.error("Not able to get client config for client " + clientId);
        throw new GoalManagementServiceException(
            "Not able to get client config for client " + clientId);
      }
      log.info("Inside getDimensionInfo. Got Client Config for client " + clientId);
      Map<String, Dimension> dimensions = clientConfig.getDimensionsV2();
      if (dimensions == null) {
        log.error("Not able to get Dimensions config for client " + clientId);
        throw new GoalManagementServiceException(
            "Not able to get Dimensions config for client " + clientId);
      }
      return dimensions.get(dimensionCode);
    } catch (ConfigClientException e) {
      log.error("Error while fetching from client config", e);
      throw new GoalManagementServiceException(e.getMessage());
    }
  }

  public List<GoalGroupDefinition> getAllGoalGroupsByClient(String clientId) {
    log.info("Fetching all goal groups for client ", clientId);

    try {
      GoalsConfig goalsConfig = getGoalsConfig(clientId);
      List<GoalGroupDefinition> result = new ArrayList<GoalGroupDefinition>();
      List<GoalGroupDefinition> goalGroups = new ArrayList<GoalGroupDefinition>();
      Map<String, GoalDefinition> goalDefinitions = goalsConfig.getGoalDefinitions();
      if (Objects.nonNull(goalDefinitions)) {
        List<GoalDefinition> goalDefinitionList =
            goalDefinitions.values().stream()
                .filter(goalDefinition -> goalDefinition.getType() == GoalType.goalGroup)
                .collect(Collectors.toList());
        goalGroups = (List<GoalGroupDefinition>) (List<?>) goalDefinitionList;
      }
      List<GoalDefinitionMappings> goalGroupMappings = orderByGoalGroups(clientId, goalGroups);

      for (GoalDefinitionMappings goalDefinitionMapping : goalGroupMappings) {
        Optional<GoalGroupDefinition> group =
            goalGroups.stream()
                .filter(
                    entry ->
                        StringUtils.equals(
                            entry.getCode(), goalDefinitionMapping.getGoalDefinitionCode()))
                .findFirst();
        if (group.isPresent()) {
          result.add(group.get());
        }
      }

      return result;
    } catch (Exception e) {
      log.error("Error while fetching from client config", e);
      throw new GoalManagementServiceException(e.getMessage());
    }
  }

  public List<String> getAllGoalDefinitionsByClientInOrder(String clientId) {
    List<GoalDefinitionMappings> goalDefinitionMappings = getGoalDefinitionsMappings(clientId);
    Collections.sort(goalDefinitionMappings, new GoalOrderComparator());

    List<String> goalDefinitionCodeList =
        goalDefinitionMappings.stream()
            .map(goalDefinitionMapping -> goalDefinitionMapping.getGoalDefinitionCode())
            .collect(Collectors.toList());

    List<GoalGroupDefinition> goalGroups = getAllGoalGroupsByClient(clientId);
    if (Objects.nonNull(goalGroups)) {
      List<GoalDefinitionMappings> orderedGoalGroups = orderByGoalGroups(clientId, goalGroups);
      int index = 0;
      for (GoalDefinitionMappings goalDefinitionMapping : orderedGoalGroups) {
        GoalGroupDefinition goalGroupDefinition =
            (GoalGroupDefinition)
                getGoalDefinitionByCode(clientId, goalDefinitionMapping.getGoalDefinitionCode());
        List<GoalDefinitionMappings> goalDefinitionMappingsByGoalGroups =
            goalGroupDefinition.getSubGoals();
        Collections.sort(goalDefinitionMappingsByGoalGroups, new GoalOrderComparator());

        for (GoalDefinitionMappings subGoalDefinitionMapping : goalDefinitionMappingsByGoalGroups) {
          goalDefinitionCodeList.remove(subGoalDefinitionMapping.getGoalDefinitionCode());
          goalDefinitionCodeList.add(index, subGoalDefinitionMapping.getGoalDefinitionCode());
          index++;
        }
      }
    }

    return goalDefinitionCodeList;
  }

  public List<GoalDefinitionMappings> orderByGoalGroups(
      String clientId, List<GoalGroupDefinition> goalGroups) {
    log.info("Inside OrderByGoalGroups");
    try {

      GoalsConfig goalsConfig = getGoalsConfig(clientId);

      List<GoalDefinitionMappings> goalDefinitionMappings = goalsConfig.getGoalDefinitionMappings();
      List<String> goalGroupCodes =
          goalGroups.stream().map(goalGroup -> goalGroup.getCode()).collect(Collectors.toList());
      List<GoalDefinitionMappings> result =
          goalDefinitionMappings.stream()
              .filter(
                  goalDefinitionMapping ->
                      goalGroupCodes.contains(goalDefinitionMapping.getGoalDefinitionCode()))
              .collect(Collectors.toList());

      Collections.sort(result, new GoalOrderComparator());
      return result;

    } catch (Exception e) {
      log.error("Exception while OrderByGoalGroups", e);
      throw new GoalManagementServiceException(e.getMessage());
    }
  }

  public boolean isGoalDefinitionHasChild(GoalDefinition parentGoalDefinition, String clientId) {
    String parentGoalCode = parentGoalDefinition.getCode();
    List<GoalDefinition> childGoals = getChildGoals(parentGoalCode, clientId);
    if (childGoals.size() > 0) {
      return true;
    }
    return false;
  }

  public List<GoalDefinition> getChildGoals(String parentGoalCode, String clientId) {
    GoalsConfig goalsConfig = getGoalsConfig(clientId);
    Map<String, GoalDefinition> goalDefinitions = goalsConfig.getGoalDefinitions();
    List<GoalDefinition> goalDefinitionList =
        goalDefinitions.values().stream()
            .filter(
                goalDefinition ->
                    StringUtils.equals(goalDefinition.getParentGoalCode(), parentGoalCode))
            .collect(Collectors.toList());
    return goalDefinitionList;
  }

  public List<GoalDefinition> getAllChildGoalsForClient(String clientId) {
    GoalsConfig goalsConfig = getGoalsConfig(clientId);
    Map<String, GoalDefinition> goalDefinitions = goalsConfig.getGoalDefinitions();
    List<GoalDefinition> goalDefinitionList =
        goalDefinitions.values().stream()
            .filter(goalDefinition -> goalDefinition.getIsChild())
            .collect(Collectors.toList());
    return goalDefinitionList;
  }

  public static class GoalOrderComparator implements Comparator {
    public int compare(Object o1, Object o2) {
      GoalDefinitionMappings s1 = (GoalDefinitionMappings) o1;
      GoalDefinitionMappings s2 = (GoalDefinitionMappings) o2;
      if (Objects.nonNull(s1.getOrder())
          && Objects.nonNull(s2.getOrder())
          && s1.getOrder() > 0
          && s2.getOrder() > 0) {
        return s1.getOrder().compareTo(s2.getOrder());
      }
      return 0;
    }
  }

  public String getAttributeFromField(String clientId, String field) {
    try {
      Client clientConfig = client.getClient(clientId);
      Map<String, InputField> inputFieldMap = clientConfig.getInputFields();
      InputField inputField = inputFieldMap.get(field);
      if (Objects.nonNull(inputField)) {
        String type = inputField.getType();
        if (StringUtils.equals(type, "static-sifg")) {
          StaticSifgInputField staticSifgInputField = (StaticSifgInputField) inputField;
          return staticSifgInputField.getSelectionAttribute();
        } else if (StringUtils.equals(type, "saved-list-sifg")) {
          SavedListSifgInputField savedListSifgInputField = (SavedListSifgInputField) inputField;
          return savedListSifgInputField.getSavedListAttribute();
        } else {
          GenericInputField genericInputField = (GenericInputField) inputField;
          return genericInputField.getInputType().getCode();
        }
      } else {
        return field;
      }

    } catch (ConfigClientException e) {
      e.printStackTrace();
    }
    return field;
  }
}
