package com.getvymo.platform.goals.app.common.models;

public enum GoalOccurrence {
  one_time,
  recurring
}
