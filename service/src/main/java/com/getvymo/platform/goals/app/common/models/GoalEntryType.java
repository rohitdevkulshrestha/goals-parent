/** */
package com.getvymo.platform.goals.app.common.models;

/** @author chandrasekhar */
public enum GoalEntryType {
  BULK_UPLOAD,
  USER_ASSIGNMENT
}
