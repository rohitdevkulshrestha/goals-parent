package com.getvymo.platform.goals.app.utils.kafka;

public class KafkaMessage {
  private String id;
  private String client_id;
  private String goal_definition_code;
  private String user_id;
  private String last_updated_by;
  private String status;
  private String type;

  public KafkaMessage(
      String id,
      String client_id,
      String goal_definition_code,
      String user_id,
      String last_updated_by,
      String status,
      String type) {
    this.id = id;
    this.client_id = client_id;
    this.goal_definition_code = goal_definition_code;
    this.user_id = user_id;
    this.last_updated_by = last_updated_by;
    this.status = status;
    this.type = type;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getClient_id() {
    return client_id;
  }

  public void setClient_id(String client_id) {
    this.client_id = client_id;
  }

  public String getGoal_definition_code() {
    return goal_definition_code;
  }

  public void setGoal_definition_code(String goal_definition_code) {
    this.goal_definition_code = goal_definition_code;
  }

  public String getUser_id() {
    return user_id;
  }

  public void setUser_id(String user_id) {
    this.user_id = user_id;
  }

  public String getLast_updated_by() {
    return last_updated_by;
  }

  public void setLast_updated_by(String last_updated_by) {
    this.last_updated_by = last_updated_by;
  }
}
