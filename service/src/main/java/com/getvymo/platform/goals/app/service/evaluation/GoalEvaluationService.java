package com.getvymo.platform.goals.app.service.evaluation;

import com.getvymo.platform.goals.app.common.models.ExternalAchievementEntry;
import com.getvymo.platform.goals.app.errors.GoalManagementServiceException;
import com.getvymo.platform.goals.app.persistence.dao.ExternalAchievementRepository;
import com.getvymo.platform.goals.app.persistence.entities.ExternalAchievementEntity;
import com.getvymo.platform.goals.app.service.evaluation.metricsrequest.BusinessMetricsSummaryRequest;
import com.getvymo.platform.goals.app.service.evaluation.metricsresponse.GroupByKey;
import com.getvymo.platform.goals.app.service.evaluation.metricsresponse.GroupedRecordSet;
import com.getvymo.platform.goals.app.service.evaluation.metricsresponse.MetricAchievementObject;
import com.getvymo.platform.goals.app.service.evaluation.metricsresponse.MetricsResponse;
import com.getvymo.platform.goals.app.service.evaluation.metricsresponse.MetricsResultObject;
import com.getvymo.platform.goals.app.service.utils.GoalsUtils;
import com.getvymo.platform.goals.app.utils.configclient.ConfigUtil;
import in.vymo.core.config.model.goals.GoalDefinition;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import lombok.extern.log4j.Log4j2;
import org.elasticsearch.index.IndexNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
@Log4j2
public class GoalEvaluationService implements IGoalEvaluationService {

  @Autowired GoalsUtils goalsUtils;

  ConfigUtil configUtil = new ConfigUtil();

  @Autowired ExternalAchievementRepository externalAchievementQueryRepository;

  @Autowired private Environment env;

  @Override
  public MetricsResponse getAchievementDataForGoal(
      BusinessMetricsSummaryRequest businessMetricsSummaryRequest) {

    log.info("Making REST call to metrics rest");

    final String metricsRestURL =
        System.getenv("metrics_rest_url")
            + "/v1/metrics/datasets/"
            + businessMetricsSummaryRequest.getDataSet().get(0);

    log.info("Metrics rest url is -> {}", metricsRestURL);
    goalsUtils.prettyPrintStringFromJSON(
        businessMetricsSummaryRequest, "Business metric request made is");
    RestTemplate restTemplate = new RestTemplate();
    try {

      MetricsResponse response =
          restTemplate.postForObject(
              metricsRestURL, businessMetricsSummaryRequest, MetricsResponse.class);
      log.info("Response from metrics API is ", response.toString());
      return response;
    } catch (Exception e) {
      log.error("Failed to fetch data from Metrics service", e.getLocalizedMessage(), e);
      throw new GoalManagementServiceException(
          "Failed to fetch data with Metrics Service " + e.getLocalizedMessage());
    }
  }

  @Override
  public MetricsResponse getAchievementDataForExternalGoal(
      String clientId,
      String userId,
      List<String> teamUserIds,
      String goalDefinitionCode,
      Date startDate,
      Date endDate,
      Boolean isRecurring,
      String recurringFrequency,
      Boolean isUserGroupBy,
      Boolean isDateGroupBy) {

    log.info("In getAchievementDataForExternalGoal");
    List<ExternalAchievementEntity> externalAchievementEntities = new ArrayList<>();
    List<String> allUserIds = new ArrayList<>(teamUserIds);

    allUserIds.add(userId);

    GoalDefinition goalDefinition =
        configUtil.getGoalDefinitionByCode(clientId, goalDefinitionCode);
    String metric = goalDefinition.getMetric();

    log.info(
        "Fetching externalAchievementEntities for clientId {}, goalDefinitionCode {}, recurringFrequency{}, userIds{}  "
            + "startDate {} and endDate {} ",
        clientId,
        goalDefinitionCode,
        recurringFrequency,
        allUserIds,
        startDate,
        endDate);

    // Get matching entites for all the teamUsers including the manager
    try {

      externalAchievementEntities =
          externalAchievementQueryRepository.getMatchingEntitiesForSetOfUsersAndAnyDateRange(
              allUserIds,
              goalDefinitionCode,
              clientId,
              startDate.getTime(),
              endDate.getTime(),
              recurringFrequency);
      log.info("Received {} externalAchievementEntities", externalAchievementEntities.size());
    } catch (IndexNotFoundException e) {
      log.error("Index not created yet. Upload achievement for the client", e);
    }

    //        goalsUtils.prettyPrintStringFromJSON(externalAchievementEntities, "Dummy entity");

    MetricsResponse metricsResponse = new MetricsResponse();
    MetricsResultObject metricsResultObject = new MetricsResultObject();
    List<MetricAchievementObject> metricAchievementObjects = new ArrayList<>();
    List<GroupedRecordSet> groupedRecordSets = new ArrayList<>();

    if (isUserGroupBy || isDateGroupBy) {
      metricsResultObject.setType("grouped");
      log.info("Is user groupby {} and date groupby {}", isUserGroupBy, isDateGroupBy);
    } else {
      metricsResultObject.setType("terminal");
    }

    MetricAchievementObject userMetricAchievementObject =
        new MetricAchievementObject("number", metric, 0d, null);
    Set<String> userIdsWithoutAchievementUploaded = new HashSet<String>(allUserIds);

    // Iterate over externalAchievementEntities to create metricAchievementObjects &
    // groupedRecordSets incase of groupbys
    for (ExternalAchievementEntity ea : externalAchievementEntities) {
      //            goalsUtils.prettyPrintStringFromJSON(ea, "Iterating over ea");
      metric = ea.getMetric();
      MetricAchievementObject metricAchievementObject =
          new MetricAchievementObject(
              ea.getDataType(), metric, ea.getAchievement(), ea.getAchievementUpdatedDate());
      if (ea.getUserId().equals(userId)) {
        userMetricAchievementObject = metricAchievementObject;
        if (isDateGroupBy) {
          log.info("Is date groupby");
          groupedRecordSets.addAll(getGroupByDateFromExternalAchievementEntity(ea));
        }
      }
      if (isUserGroupBy) {

        userIdsWithoutAchievementUploaded.remove(ea.getUserId());
        groupedRecordSets.add(
            getGroupedRecordSetForUserGroupByFromMetricAchievementObject(
                metricAchievementObject, ea));
      }

      metricAchievementObjects.add(metricAchievementObject);
    }
    if (isUserGroupBy) {
      // Add empty grouped record sets for users without achievement uploaded
      List<GroupedRecordSet> emptyGroupedRecordSets =
          getEmptyGroupedRecordSetForUserGroupbyFromUserIds(
              userIdsWithoutAchievementUploaded, metric);
      log.info(
          "There are {} users without achievment uploaded for the goal with {} code and {} recurringFrequency",
          emptyGroupedRecordSets.size(),
          goalDefinitionCode,
          recurringFrequency);
      groupedRecordSets.addAll(emptyGroupedRecordSets);
    }

    metricsResultObject.setTotal_records_count(externalAchievementEntities.size());
    metricsResultObject.setGrouped_record_sets(groupedRecordSets);
    metricsResultObject.setMetrics(Arrays.asList(userMetricAchievementObject));

    metricsResponse.setResult(metricsResultObject);
    goalsUtils.prettyPrintStringFromJSON(metricsResponse, "Metrics response for External");

    return metricsResponse;
  }

  private List<GroupedRecordSet> getEmptyGroupedRecordSetForUserGroupbyFromUserIds(
      Set<String> userIds, String metric) {
    log.info(
        "In getEmptyGroupedRecordSetForUserGroupbyFromUserIds for userIds {} and metric {}",
        userIds,
        metric);
    MetricAchievementObject metricAchievementObject =
        new MetricAchievementObject("number", metric, 0d, null);

    List<GroupedRecordSet> groupedRecordSets = new ArrayList<>();
    log.info(
        "Creating empty user groupby object for userIds {} in getEmptyGroupedRecordSetForUserGroupbyFromUserId and metric {}",
        userIds,
        metric);
    for (String userId : userIds) {

      GroupedRecordSet groupedRecordSet = new GroupedRecordSet();
      groupedRecordSet.setType("terminal");

      groupedRecordSet.setGroup_by_type(GroupedRecordSet.GROUP_BY_TYPE.user);
      groupedRecordSet.setMetrics(Arrays.asList(metricAchievementObject));

      GroupByKey groupByKey = new GroupByKey(userId);
      groupedRecordSet.setGroup_by_key(groupByKey);
      groupedRecordSets.add(groupedRecordSet);
    }

    return groupedRecordSets;
  }

  private GroupedRecordSet getGroupedRecordSetForUserGroupByFromMetricAchievementObject(
      MetricAchievementObject metricAchievementObject, ExternalAchievementEntity ea) {
    log.info(
        "Creating user groupby object for userId {} in getGroupedRecordSetForUserGroupByFromMetricAchievementObject",
        ea.getUserId());
    GroupedRecordSet groupedRecordSet = new GroupedRecordSet();
    groupedRecordSet.setType("terminal");

    groupedRecordSet.setGroup_by_type(GroupedRecordSet.GROUP_BY_TYPE.user);
    groupedRecordSet.setMetrics(Arrays.asList(metricAchievementObject));

    GroupByKey groupByKey = new GroupByKey(ea.getUserId());
    groupedRecordSet.setGroup_by_key(groupByKey);

    return groupedRecordSet;
  }

  private List<GroupedRecordSet> getGroupByDateFromExternalAchievementEntity(
      ExternalAchievementEntity ea) {
    log.info("In getGroupByDateFromExternalAchievementEntity");
    log.info("Creating date groupby object for userId {} ", ea.getUserId());
    HashMap<Long, ExternalAchievementEntry> achievementsMap = ea.getAchievementsMap();
    List<GroupedRecordSet> groupedRecordSets = new ArrayList<>();
    log.info("Iterating over achievementsMap keys");
    // sort the dates in ascending orderg
    List<Long> updatedDatesList = new ArrayList<>(achievementsMap.keySet());
    log.info("Sorting dates in ascending order");
    Collections.sort(updatedDatesList);
    for (Long updatedDate : updatedDatesList) {
      ExternalAchievementEntry externalAchievementObject = achievementsMap.get(updatedDate);
      //            goalsUtils.prettyPrintStringFromJSON(externalAchievementObject, "
      // externalAchievementObject is");
      if (externalAchievementObject.isDeleted()) {
        log.info("external achievment is deleted");
        continue;
      }
      MetricAchievementObject metricAchievementObject =
          new MetricAchievementObject(
              ea.getDataType(),
              ea.getMetric(),
              externalAchievementObject.getValue(),
              ea.getAchievementUpdatedDate());
      GroupByKey groupByKey = new GroupByKey(updatedDate.toString());
      GroupedRecordSet groupedRecordSet = new GroupedRecordSet();
      groupedRecordSet.setGroup_by_key(groupByKey);
      groupedRecordSet.setGroup_by_type(GroupedRecordSet.GROUP_BY_TYPE.date);
      groupedRecordSet.setMetrics(Arrays.asList(metricAchievementObject));
      groupedRecordSet.setType("terminal");
      groupedRecordSets.add(groupedRecordSet);
    }
    log.info("Returning from getGroupByDateFromExternalAchievementEntity with grouped record sets");
    //        goalsUtils.prettyPrintStringFromJSON(groupedRecordSets, "returning
    // groupedRecordSets");
    return groupedRecordSets;
  }

  @Async
  public CompletableFuture<MetricsResponse> getAchievementDataForGoalAsync(
      BusinessMetricsSummaryRequest businessMetricsSummaryRequest) {
    log.info("In getAchievementDataForGoalAsync");
    return CompletableFuture.completedFuture(
        getAchievementDataForGoal(businessMetricsSummaryRequest));
  }

  @Async
  public CompletableFuture<MetricsResponse> getExternalAchievementDataForGoalAsync(
      String clientId,
      String userId,
      List<String> teamUserIds,
      String goalDefinitionCode,
      Date startDate,
      Date endDate,
      Boolean isRecurring,
      String recurringFrequency,
      Boolean isUserGroupBy,
      Boolean isDateGroupBy) {
    log.info("In getExternalAchievementDataForGoalAsync");
    return CompletableFuture.completedFuture(
        getAchievementDataForExternalGoal(
            clientId,
            userId,
            teamUserIds,
            goalDefinitionCode,
            startDate,
            endDate,
            isRecurring,
            recurringFrequency,
            false,
            false));
  }
}
