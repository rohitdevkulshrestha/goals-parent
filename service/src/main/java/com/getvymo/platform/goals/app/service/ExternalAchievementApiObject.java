package com.getvymo.platform.goals.app.service;

import java.util.Date;

public class ExternalAchievementApiObject {

  private String goalDefinitionCode;

  private String clientId;

  private String userId;

  private Date startDate;

  private Date endDate;

  private Double achievement;

  private Boolean isRecurring;

  private String recurringFrequency;

  private String createdBy;

  private String lastUpdatedBy;

  private Boolean deleted;

  private Boolean isActive;

  private Date createdDate;

  private Date lastUpdatedDate;

  public ExternalAchievementApiObject() {}

  public ExternalAchievementApiObject(
      String goalDefinitionCode,
      String clientId,
      String userId,
      Date startDate,
      Date endDate,
      Double achievement,
      Boolean isRecurring,
      String recurringFrequency,
      String createdBy,
      String lastUpdatedBy,
      Boolean deleted,
      Boolean isActive,
      Date createdDate,
      Date lastUpdatedDate) {
    this.goalDefinitionCode = goalDefinitionCode;
    this.clientId = clientId;
    this.userId = userId;
    this.startDate = startDate;
    this.endDate = endDate;
    this.achievement = achievement;
    this.isRecurring = isRecurring;
    this.recurringFrequency = recurringFrequency;
    this.createdBy = createdBy;
    this.lastUpdatedBy = lastUpdatedBy;
    this.deleted = deleted;
    this.isActive = isActive;
    this.createdDate = createdDate;
    this.lastUpdatedDate = lastUpdatedDate;
  }

  public String getGoalDefinitionCode() {
    return goalDefinitionCode;
  }

  public void setGoalDefinitionCode(String goalDefinitionCode) {
    this.goalDefinitionCode = goalDefinitionCode;
  }

  public String getClientId() {
    return clientId;
  }

  public void setClientId(String clientId) {
    this.clientId = clientId;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public Date getStartDate() {
    return startDate;
  }

  public void setStartDate(Date startDate) {
    this.startDate = startDate;
  }

  public Date getEndDate() {
    return endDate;
  }

  public void setEndDate(Date endDate) {
    this.endDate = endDate;
  }

  public Double getAchievement() {
    return achievement;
  }

  public void setAchievement(Double achievement) {
    this.achievement = achievement;
  }

  public Boolean getRecurring() {
    return isRecurring;
  }

  public void setRecurring(Boolean recurring) {
    isRecurring = recurring;
  }

  public String getRecurringFrequency() {
    return recurringFrequency;
  }

  public void setRecurringFrequency(String recurringFrequency) {
    this.recurringFrequency = recurringFrequency;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public String getLastUpdatedBy() {
    return lastUpdatedBy;
  }

  public void setLastUpdatedBy(String lastUpdatedBy) {
    this.lastUpdatedBy = lastUpdatedBy;
  }

  public Boolean getDeleted() {
    return deleted;
  }

  public void setDeleted(Boolean deleted) {
    this.deleted = deleted;
  }

  public Boolean getActive() {
    return isActive;
  }

  public void setActive(Boolean active) {
    isActive = active;
  }

  public Date getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(Date createdDate) {
    this.createdDate = createdDate;
  }

  public Date getLastUpdatedDate() {
    return lastUpdatedDate;
  }

  public void setLastUpdatedDate(Date lastUpdatedDate) {
    this.lastUpdatedDate = lastUpdatedDate;
  }
}
