package com.getvymo.platform.goals.springconfig.common.exceptionhandler.exceptions.business;

import com.getvymo.platform.goals.springconfig.common.exceptionhandler.exceptions.BusinessException;
import java.net.URI;
import org.zalando.problem.StatusType;

public class InvalidParametersException extends BusinessException {

  public InvalidParametersException(URI type, String title, StatusType status) {
    super(type, title, status);
    // TODO Auto-generated constructor stub
  }
}
