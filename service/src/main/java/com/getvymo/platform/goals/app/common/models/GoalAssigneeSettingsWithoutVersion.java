package com.getvymo.platform.goals.app.common.models;

public class GoalAssigneeSettingsWithoutVersion {
  GoalAssigneeSettings goalAssigneeSettings;
  GoalAssignmentOperationMetaData goalAssignmentOperationMetaData;

  public GoalAssigneeSettingsWithoutVersion(
      GoalAssigneeSettings goalAssigneeSettings,
      GoalAssignmentOperationMetaData goalAssignmentOperationMetaData) {
    this.goalAssigneeSettings = goalAssigneeSettings;
    this.goalAssignmentOperationMetaData = goalAssignmentOperationMetaData;
  }

  public GoalAssigneeSettingsWithoutVersion() {}

  public GoalAssigneeSettings getGoalAssigneeSettings() {
    return goalAssigneeSettings;
  }

  public void setGoalAssigneeSettings(GoalAssigneeSettings goalAssigneeSettings) {
    this.goalAssigneeSettings = goalAssigneeSettings;
  }

  public GoalAssignmentOperationMetaData getGoalAssignmentOperationMetaData() {
    return goalAssignmentOperationMetaData;
  }

  public void setGoalAssignmentOperationMetaData(
      GoalAssignmentOperationMetaData goalAssignmentOperationMetaData) {
    this.goalAssignmentOperationMetaData = goalAssignmentOperationMetaData;
  }
}
