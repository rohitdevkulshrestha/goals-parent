package com.getvymo.platform.goals.app.service.evaluation.metricsrequest;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

public class DateRangeObject {

  @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
  private Date start;

  @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
  private Date end;

  private String type;

  public DateRangeObject(Date start, Date end) {
    this.start = start;
    this.end = end;
    this.type = "date";
  }

  public DateRangeObject() {}

  public Date getStart() {
    return start;
  }

  public void setStart(Date start) {
    this.start = start;
  }

  public Date getEnd() {
    return end;
  }

  public void setEnd(Date end) {
    this.end = end;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }
}
