package com.getvymo.platform.goals.app.service.external.achievement;

import com.getvymo.platform.goals.app.common.models.BulkExternalAchievementRequest;
import com.getvymo.platform.goals.app.common.models.ExternalAchievement;
import com.getvymo.platform.goals.app.common.models.StandardListRequest;
import com.getvymo.platform.goals.app.service.BulkExternalAchievementResponse;
import com.getvymo.platform.goals.app.service.ExternalAchievementApiObject;

public interface IExternalAchievementsService {

  ExternalAchievementApiObject saveExternalAchievement(
      String clientId, ExternalAchievement externalAchievementSettings);

  BulkExternalAchievementResponse bulkSaveExternalAchievements(
      String clientId,
      StandardListRequest<BulkExternalAchievementRequest> externalAchievementSettingsList);
}
