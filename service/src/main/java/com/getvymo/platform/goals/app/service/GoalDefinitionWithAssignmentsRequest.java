package com.getvymo.platform.goals.app.service;

import com.getvymo.platform.goals.app.common.models.GoalAssigneeSettings;
import com.getvymo.platform.goals.app.common.models.GoalDefinitionSettings;

public class GoalDefinitionWithAssignmentsRequest {

  private GoalDefinitionSettings goalDefinitionSettings;

  private GoalAssigneeSettings goalAssigneeSettings;

  public GoalDefinitionSettings getGoalDefinitionSettings() {
    return goalDefinitionSettings;
  }

  public GoalAssigneeSettings getGoalAssigneeSettings() {
    return goalAssigneeSettings;
  }

  @Override
  public String toString() {
    return "GoalDefinitionWithAssignmentsRequest [goalDefinitionSettings="
        + goalDefinitionSettings
        + ", goalAssigneeSettings="
        + goalAssigneeSettings
        + "]";
  }
}
