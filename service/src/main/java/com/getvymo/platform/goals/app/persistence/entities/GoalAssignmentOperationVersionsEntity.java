package com.getvymo.platform.goals.app.persistence.entities;

import com.getvymo.platform.goals.app.common.models.GoalEntryType;
import com.getvymo.platform.goals.app.service.utils.GoalsUtils;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "goal_assignment_versions")
public class GoalAssignmentOperationVersionsEntity extends GoalAuditableEntity {

  @Id
  @Column(name = "id", columnDefinition = "VARCHAR(36)")
  private String id;

  private String version;

  private String lastUpdatedBy;

  private String createdBy;

  private String description;

  // Not exactly source but the way the version was created - can be LMS API/ external API, file etc
  @Enumerated(EnumType.STRING)
  private GoalEntryType entryType;

  public GoalAssignmentOperationVersionsEntity() {}

  public GoalAssignmentOperationVersionsEntity(
      String version,
      String lastUpdatedBy,
      String createdBy,
      String description,
      GoalEntryType entryType) {
    this.id = GoalsUtils.generateRandomStringId();
    this.version = version;
    this.lastUpdatedBy = lastUpdatedBy;
    this.createdBy = createdBy;
    this.description = description;
    this.entryType = entryType;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public GoalEntryType getEntryType() {
    return entryType;
  }

  public void setEntryType(GoalEntryType entryType) {
    this.entryType = entryType;
  }

  public String getId() {
    return id;
  }

  public String getVersion() {
    return version;
  }

  public void setVersion(String version) {
    this.version = version;
  }

  public String getLastUpdatedBy() {
    return lastUpdatedBy;
  }

  public void setLastUpdatedBy(String lastUpdatedBy) {
    this.lastUpdatedBy = lastUpdatedBy;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }
}
