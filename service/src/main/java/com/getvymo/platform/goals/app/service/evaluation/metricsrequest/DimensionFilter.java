package com.getvymo.platform.goals.app.service.evaluation.metricsrequest;

public abstract class DimensionFilter extends Filter {
  private String dimension; // module, task_vo_attribute, task_attribute
  private String attribute;

  public DimensionFilter(String type, String dimension, String attribute) {
    super(type);
    this.dimension = dimension;
    this.attribute = attribute;
  }

  public DimensionFilter(String dimension) {
    this.dimension = dimension;
  }

  public String getAttribute() {
    return attribute;
  }

  public void setAttribute(String attribute) {
    this.attribute = attribute;
  }

  public String getDimension() {
    return dimension;
  }

  public void setDimension(String dimension) {
    this.dimension = dimension;
  }
}
