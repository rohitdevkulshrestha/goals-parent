package com.getvymo.platform.goals.app.persistence.dao;

import com.getvymo.platform.goals.app.persistence.entities.GoalAssignmentOperationVersionsEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GoalAssignmentOperationVersionsRepository
    extends JpaRepository<GoalAssignmentOperationVersionsEntity, String> {

  public GoalAssignmentOperationVersionsEntity findByVersion(String version);
}
