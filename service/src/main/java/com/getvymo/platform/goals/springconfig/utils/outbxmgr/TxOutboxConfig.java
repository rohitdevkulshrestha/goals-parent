package com.getvymo.platform.goals.springconfig.utils.outbxmgr;

import com.gruelbox.transactionoutbox.Dialect;
import com.gruelbox.transactionoutbox.Persistor;
import com.gruelbox.transactionoutbox.SpringInstantiator;
import com.gruelbox.transactionoutbox.SpringTransactionManager;
import com.gruelbox.transactionoutbox.TransactionOutbox;
import com.gruelbox.transactionoutbox.TransactionOutboxEntry;
import com.gruelbox.transactionoutbox.TransactionOutboxListener;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

/**
 * https://github.com/gruelbox/transaction-outbox#spring
 *
 * @author rohitdevindreshkulshestha
 */
@Configuration
@EnableScheduling
@Log4j2
public class TxOutboxConfig {

  @Bean
  @Lazy
  public TransactionOutbox transactionOutbox(
      SpringTransactionManager springTransactionManager,
      SpringInstantiator springInstantiator,
      Dialect dialect) {

    txObx =
        TransactionOutbox.builder()
            .instantiator(springInstantiator)
            .transactionManager(springTransactionManager)
            .persistor(Persistor.forDialect(dialect))
            .listener(
                new TransactionOutboxListener() {
                  @Override
                  public void blocked(TransactionOutboxEntry entry, Throwable cause) {
                    // Spring example
                    // applicationEventPublisher.publishEvent(new
                    // TransactionOutboxBlockedEvent(entry.getId(), cause);
                  }
                })
            .build();
    return txObx;
  }

  @Scheduled(fixedDelay = 1000, initialDelay = 1000)
  public void scheduleFixedDelayTask() {
    // Keep flushing work until there's nothing left to flush
    while (txObx.flush()) {}
  }

  private static TransactionOutbox txObx;
}
