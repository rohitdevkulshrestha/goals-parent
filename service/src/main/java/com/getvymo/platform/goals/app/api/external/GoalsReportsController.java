package com.getvymo.platform.goals.app.api.external;

import com.getvymo.platform.goals.app.common.models.StandardListResponse;
import com.getvymo.platform.goals.app.service.evaluation.goalsresponse.GoalAchievement;
import com.getvymo.platform.goals.app.service.goalsmgmt.IGoalManagementService;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/goals/v1/clients/{clientId}")
public class GoalsReportsController {
  @Autowired private IGoalManagementService iGoalManagementService;

  // Used for download goal report via DDP
  @PostMapping(path = "/reports", consumes = "application/json")
  public StandardListResponse<GoalAchievement> getGoalAchievementsReportsForDateRange(
      @Valid @PathVariable String clientId,
      @RequestParam(value = "startDate", required = true)
          @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
          Date startDate,
      @RequestParam(value = "endDate", required = true)
          @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
          Date endDate,
      @RequestParam(value = "userIds", required = true) List<String> userIds,
      @RequestParam(value = "goalDefinitionCode", required = false) String goalDefinitionCode,
      @RequestParam(value = "recurringFrequency", required = false) String recurringFrequency,
      @RequestParam(value = "status", required = false) List<String> status,
      @RequestBody Map<String, String> userCodeVsName) {
    return iGoalManagementService.getGoalAchievementReportForUsers(
        clientId,
        userIds,
        userCodeVsName,
        startDate,
        endDate,
        goalDefinitionCode,
        recurringFrequency,
        status);
  }

  @GetMapping(path = "/reports", consumes = "application/json")
  public StandardListResponse<GoalAchievement> getGoalAchievementsReportsForDateRange(
      @Valid @PathVariable String clientId,
      @RequestParam(value = "startDate", required = true)
          @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
          Date startDate,
      @RequestParam(value = "endDate", required = true)
          @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
          Date endDate,
      @RequestParam(value = "userIds", required = true) List<String> userIds,
      @RequestParam(value = "goalDefinitionCode", required = false) String goalDefinitionCode,
      @RequestParam(value = "recurringFrequency", required = false) String recurringFrequency,
      @RequestParam(value = "status", required = false) List<String> status) {
    return iGoalManagementService.getGoalAchievementReportForUsers(
        clientId,
        userIds,
        new HashMap<>(),
        startDate,
        endDate,
        goalDefinitionCode,
        recurringFrequency,
        status);
  }
}
