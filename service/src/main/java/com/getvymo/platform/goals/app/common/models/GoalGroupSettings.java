package com.getvymo.platform.goals.app.common.models;

import java.util.ArrayList;
import java.util.List;

public class GoalGroupSettings {

  private String id;
  private String name;
  private String description;
  private List<String> goalDefinitionCode = new ArrayList<String>();

  /**
   * @param name
   * @param description
   * @param goalDefinitionCode
   */
  public GoalGroupSettings(
      String id, String name, String description, List<String> goalDefinitionCode) {
    super();
    this.setId(id);
    this.setName(name);
    this.setDescription(description);
    this.setGoalDefinitionCode(goalDefinitionCode);
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public List<String> getGoalDefinitionCode() {
    return goalDefinitionCode;
  }

  public void setGoalDefinitionCode(List<String> goalDefinitionCode) {
    this.goalDefinitionCode = goalDefinitionCode;
  }
}
