package com.getvymo.platform.goals.app.utils.azurevault;

public interface AzureVaultConfig {

  AzureKeyVaultStorage initialiseVaultStorage() throws IllegalAccessException;

  AzureKeyVaultStorage getVaultStorage();

  String getESPassword();
}
