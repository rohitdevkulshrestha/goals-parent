package com.getvymo.platform.goals.app.service.evaluation.goalsresponse;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.getvymo.platform.goals.app.common.models.GoalAssigneeSettings;
import com.getvymo.platform.goals.app.common.models.GoalDefinitionSettings;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class GoalAchievement {
  private String goalAssigneeId;
  private GoalAssigneeSettings goalAssigneeSettings;
  private String goalDefinitionCode;
  private GoalDefinitionSettings goalDefinitionSettings;
  private UserObject user;
  private GoalAchievementObject achievement;
  private String status;

  @JsonFormat(shape = JsonFormat.Shape.NUMBER)
  private Date startDate;

  @JsonFormat(shape = JsonFormat.Shape.NUMBER)
  private Date endDate;

  private List<GoalsGroupedByUser> groupedByUser = new ArrayList<GoalsGroupedByUser>();

  private GoalsGroupedByUser managerSelfAchievement;

  private List<GoalsGroupedByDate> trend = new ArrayList<GoalsGroupedByDate>();

  public GoalAchievement() {}

  public GoalAchievement(
      String goalAssigneeId,
      GoalAssigneeSettings goalAssigneeSettings,
      String goalDefinitionCode,
      GoalDefinitionSettings goalDefinitionSettings,
      UserObject user,
      GoalAchievementObject achievement,
      List<GoalsGroupedByUser> groupedByUser,
      List<GoalsGroupedByDate> trend,
      Date startDate,
      Date endDate,
      GoalsGroupedByUser managerSelfAchievement) {
    this.goalAssigneeId = goalAssigneeId;
    this.goalAssigneeSettings = goalAssigneeSettings;
    this.goalDefinitionCode = goalDefinitionCode;
    this.goalDefinitionSettings = goalDefinitionSettings;
    this.user = user;
    this.achievement = achievement;
    this.groupedByUser = groupedByUser;
    this.trend = trend;
    this.startDate = startDate;
    this.endDate = endDate;
    this.managerSelfAchievement = managerSelfAchievement;
  }

  public Date getStartDate() {
    return startDate;
  }

  public void setStartDate(Date startDate) {
    this.startDate = startDate;
  }

  public Date getEndDate() {
    return endDate;
  }

  public void setEndDate(Date endDate) {
    this.endDate = endDate;
  }

  public List<GoalsGroupedByDate> getTrend() {
    return trend;
  }

  public void setTrend(List<GoalsGroupedByDate> trend) {
    this.trend = trend;
  }

  public String getGoalAssigneeId() {
    return goalAssigneeId;
  }

  public void setGoalAssigneeId(String goalAssigneeId) {
    this.goalAssigneeId = goalAssigneeId;
  }

  public GoalAssigneeSettings getGoalAssigneeSettings() {
    return goalAssigneeSettings;
  }

  public void setGoalAssigneeSettings(GoalAssigneeSettings goalAssigneeSettings) {
    this.goalAssigneeSettings = goalAssigneeSettings;
  }

  public String getGoalDefinitionCode() {
    return goalDefinitionCode;
  }

  public void setGoalDefinitionCode(String goalDefinitionCode) {
    this.goalDefinitionCode = goalDefinitionCode;
  }

  public GoalDefinitionSettings getGoalDefinitionSettings() {
    return goalDefinitionSettings;
  }

  public void setGoalDefinitionSettings(GoalDefinitionSettings goalDefinitionSettings) {
    this.goalDefinitionSettings = goalDefinitionSettings;
  }

  public UserObject getUser() {
    return user;
  }

  public void setUser(UserObject user) {
    this.user = user;
  }

  public GoalAchievementObject getAchievement() {
    return achievement;
  }

  public void setAchievement(GoalAchievementObject achievement) {
    this.achievement = achievement;
  }

  public List<GoalsGroupedByUser> getGroupedByUser() {
    return groupedByUser;
  }

  public void setGroupedByUser(List<GoalsGroupedByUser> groupedByUser) {
    this.groupedByUser = groupedByUser;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public GoalsGroupedByUser getManagerSelfAchievement() {
    return managerSelfAchievement;
  }

  public void setManagerSelfAchievement(GoalsGroupedByUser managerSelfAchievement) {
    this.managerSelfAchievement = managerSelfAchievement;
  }
}
