package com.getvymo.platform.goals.app.errors;

import java.util.List;

public class ErrorResponse {

  private List<String> errors;
  private String message;

  public ErrorResponse() {}

  public ErrorResponse(List<String> errors, String message) {
    super();
    this.errors = errors;
    this.message = message;
  }

  public List<String> getErrors() {
    return errors;
  }

  public void setErrors(List<String> errors) {
    this.errors = errors;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }
}
