package com.getvymo.platform.goals.app.common.models;

import java.util.Date;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

public class ExternalAchievementEntry {

  private Double value;

  @Temporal(TemporalType.TIMESTAMP)
  private Date lastUpdatedDate;

  private String type;
  private Boolean deleted;
  private String lastUpdatedBy;
  private String dataType;

  public ExternalAchievementEntry(
      Double value,
      Date lastUpdatedDate,
      String type,
      Boolean deleted,
      String lastUpdatedBy,
      String dataType) {
    this.value = value;
    this.lastUpdatedDate = lastUpdatedDate;
    this.type = type;
    this.deleted = deleted;
    this.lastUpdatedBy = lastUpdatedBy;
    this.dataType = dataType;
  }

  public ExternalAchievementEntry() {}

  public String getDataType() {
    return dataType;
  }

  public void setDataType(String dataType) {
    this.dataType = dataType;
  }

  public Boolean getDeleted() {
    return deleted;
  }

  public void setDeleted(Boolean deleted) {
    this.deleted = deleted;
  }

  public String getLastUpdatedBy() {
    return lastUpdatedBy;
  }

  public void setLastUpdatedBy(String lastUpdatedBy) {
    this.lastUpdatedBy = lastUpdatedBy;
  }

  public Boolean isDeleted() {
    return deleted;
  }

  public Double getValue() {
    return value;
  }

  public void setValue(Double value) {
    this.value = value;
  }

  public Date getLastUpdatedDate() {
    return lastUpdatedDate;
  }

  public void setLastUpdatedDate(Date lastUpdatedDate) {
    this.lastUpdatedDate = lastUpdatedDate;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }
}
