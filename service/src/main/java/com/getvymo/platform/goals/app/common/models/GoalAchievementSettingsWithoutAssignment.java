package com.getvymo.platform.goals.app.common.models;

import java.util.Date;

public class GoalAchievementSettingsWithoutAssignment {
  private Date startDate;
  private Date endDate;
  private String goalDefinitionCode;
  private String recurringFrequency;
  private String groupByType;
  private String occurrence;
  private String clientId;
  private String userId;

  public GoalAchievementSettingsWithoutAssignment(
      Date startDate,
      Date endDate,
      String goalDefinitionCode,
      String recurringFrequency,
      String groupByType,
      String occurrence,
      String clientId,
      String userId) {
    this.startDate = startDate;
    this.endDate = endDate;
    this.goalDefinitionCode = goalDefinitionCode;
    this.recurringFrequency = recurringFrequency;
    this.groupByType = groupByType;
    this.occurrence = occurrence;
    this.clientId = clientId;
    this.userId = userId;
  }

  public GoalAchievementSettingsWithoutAssignment() {}

  public String getClientId() {
    return clientId;
  }

  public void setClientId(String clientId) {
    this.clientId = clientId;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public Date getStartDate() {
    return startDate;
  }

  public void setStartDate(Date startDate) {
    this.startDate = startDate;
  }

  public Date getEndDate() {
    return endDate;
  }

  public void setEndDate(Date endDate) {
    this.endDate = endDate;
  }

  public String getGoalDefinitionCode() {
    return goalDefinitionCode;
  }

  public void setGoalDefinitionCode(String goalDefinitionCode) {
    this.goalDefinitionCode = goalDefinitionCode;
  }

  public String getRecurringFrequency() {
    return recurringFrequency;
  }

  public void setRecurringFrequency(String recurringFrequency) {
    this.recurringFrequency = recurringFrequency;
  }

  public String getOccurrence() {
    return occurrence;
  }

  public void setOccurrence(String occurrence) {
    this.occurrence = occurrence;
  }

  public String getGroupByType() {
    return groupByType;
  }

  public void setGroupByType(String groupByType) {
    this.groupByType = groupByType;
  }
}
