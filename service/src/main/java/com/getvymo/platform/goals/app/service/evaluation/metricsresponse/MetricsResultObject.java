package com.getvymo.platform.goals.app.service.evaluation.metricsresponse;

import java.util.List;

public class MetricsResultObject {
  private String type;
  private List<MetricAchievementObject> metrics;
  private Integer total_records_count;
  private List<GroupedRecordSet> grouped_record_sets;

  public MetricsResultObject() {}

  public MetricsResultObject(
      String type,
      List<MetricAchievementObject> metrics,
      Integer total_records_count,
      List<GroupedRecordSet> grouped_record_sets) {
    this.type = type;
    this.metrics = metrics;
    this.total_records_count = total_records_count;
    this.grouped_record_sets = grouped_record_sets;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public List<MetricAchievementObject> getMetrics() {
    return metrics;
  }

  public void setMetrics(List<MetricAchievementObject> metrics) {
    this.metrics = metrics;
  }

  public Integer getTotal_records_count() {
    return total_records_count;
  }

  public void setTotal_records_count(Integer total_records_count) {
    this.total_records_count = total_records_count;
  }

  public List<GroupedRecordSet> getGrouped_record_sets() {
    return grouped_record_sets;
  }

  public void setGrouped_record_sets(List<GroupedRecordSet> grouped_record_sets) {
    this.grouped_record_sets = grouped_record_sets;
  }
}
