package com.getvymo.platform.goals.app.service.evaluation.metricsrequest;

/** Created by debashish on 19/08/19. */
public class GroupByDimension extends GroupBy {
  private String dimension; // module, state

  public GroupByDimension(String dimension) {
    this.dimension = dimension;
  }

  public GroupByDimension(String type, String dimension) {
    super(type);
    this.dimension = dimension;
  }

  public String getDimension() {
    return dimension;
  }

  public void setDimension(String dimension) {
    this.dimension = dimension;
  }
}
