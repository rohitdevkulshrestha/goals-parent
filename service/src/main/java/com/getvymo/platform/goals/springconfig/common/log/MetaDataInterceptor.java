package com.getvymo.platform.goals.springconfig.common.log;

import com.getvymo.platform.goals.springconfig.constants.AppConstants;
import java.util.UUID;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.ui.ModelMap;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.context.request.WebRequestInterceptor;

// MDC for incoming requests
@Log4j2
public class MetaDataInterceptor implements WebRequestInterceptor {

  @Override
  public void preHandle(WebRequest request) throws Exception {
    String correlationId = request.getHeader(AppConstants.CORRID);
    if (!StringUtils.isNotBlank(correlationId)) {
      correlationId = UUID.randomUUID().toString();
      log.info("Request header missing {}", AppConstants.CORRID);
    }
    String idempotencyKey = request.getHeader(AppConstants.IDEMPOTENCYKEY);
    if (!StringUtils.isNotBlank(idempotencyKey)) {
      idempotencyKey = UUID.randomUUID().toString();
      log.info("Request header missing {}", AppConstants.IDEMPOTENCYKEY);
    }
    String traceId = request.getHeader(AppConstants.TRACEID);
    if (!StringUtils.isNotBlank(traceId)) {
      traceId = UUID.randomUUID().toString();
      log.info("Request header missing {}", AppConstants.TRACEID);
    }
    String user = request.getHeader(AppConstants.USERID);
    if (!StringUtils.isNotBlank(user)) {
      user = "uname_missing";
      log.info("Request header missing {}", AppConstants.USERID);
    }

    String deviceId = request.getHeader(AppConstants.DEVICEID);
    if (!StringUtils.isNotBlank(deviceId)) {
      deviceId = "deviceid_missing";
      log.info("Request header missing {}", AppConstants.DEVICEID);
    }

    String tenantId = request.getHeader(AppConstants.TENANTID);
    if (!StringUtils.isNotBlank(tenantId)) {
      tenantId = "tenantId_missing";
      log.info("Request header missing {}", AppConstants.TENANTID);
    }
    String sim = request.getHeader(AppConstants.SIMULATED);
    if (!StringUtils.isNotBlank(sim)) {
      sim = "false";
      log.info("Request header missing {}", AppConstants.SIMULATED);
    }
    final String operationPath = request.getContextPath();
    log.info(
        "correlationId={} user={} deviceId={} operationPath={} tenantId{}",
        correlationId,
        user,
        deviceId,
        operationPath,
        tenantId);

    ThreadContext.put(AppConstants.CORRID, correlationId);
    ThreadContext.put(AppConstants.IDEMPOTENCYKEY, idempotencyKey);
    ThreadContext.put(AppConstants.TRACEID, traceId);
    ThreadContext.put("path", operationPath);
    ThreadContext.put(AppConstants.USERID, user);
    ThreadContext.put(AppConstants.DEVICEID, deviceId);
    ThreadContext.put(AppConstants.TENANTID, tenantId);
    ThreadContext.put(AppConstants.SIMULATED, sim);
  }

  @Override
  public void afterCompletion(WebRequest request, Exception ex) throws Exception {

    log.info("Clearing ThreadContext");
    ThreadContext.clearMap();
  }

  @Override
  public void postHandle(WebRequest request, ModelMap model) throws Exception { // TODO
    // Auto-generated
    // method stub
  }
}
