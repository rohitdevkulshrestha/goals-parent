package com.getvymo.platform.goals.app.service;

public class BulkGoalAssignment extends GoalAssignment {
  private String msg;
  private int row;
  private String status;
  private String vymoCode;

  public BulkGoalAssignment() {}

  public BulkGoalAssignment(String msg, int row, String status, String vymoCode) {
    this.msg = msg;
    this.row = row;
    this.status = status;
    this.vymoCode = vymoCode;
  }

  public String getMsg() {
    return msg;
  }

  public void setMsg(String msg) {
    this.msg = msg;
  }

  public int getRow() {
    return row;
  }

  public void setRow(int row) {
    this.row = row;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getVymoCode() {
    return vymoCode;
  }

  public void setVymoCode(String vymoCode) {
    this.vymoCode = vymoCode;
  }
}
