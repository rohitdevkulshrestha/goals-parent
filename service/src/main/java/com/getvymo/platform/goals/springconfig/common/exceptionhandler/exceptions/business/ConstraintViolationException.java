package com.getvymo.platform.goals.springconfig.common.exceptionhandler.exceptions.business;

import com.getvymo.platform.goals.springconfig.common.exceptionhandler.exceptions.BusinessException;
import java.net.URI;
import java.util.Map;
import org.zalando.problem.StatusType;
import org.zalando.problem.ThrowableProblem;

public class ConstraintViolationException extends BusinessException {

  public ConstraintViolationException(
      URI type,
      String title,
      StatusType status,
      String detail,
      URI instance,
      ThrowableProblem cause,
      Map<String, Object> parameters) {
    super(type, title, status, detail, instance, cause, parameters);
    // TODO Auto-generated constructor stub
  }

  public ConstraintViolationException(
      URI type,
      String title,
      StatusType status,
      String detail,
      URI instance,
      ThrowableProblem cause) {
    super(type, title, status, detail, instance, cause);
    // TODO Auto-generated constructor stub
  }

  public ConstraintViolationException(
      URI type, String title, StatusType status, String detail, URI instance) {
    super(type, title, status, detail, instance);
    // TODO Auto-generated constructor stub
  }

  public ConstraintViolationException(URI type, String title, StatusType status, String detail) {
    super(type, title, status, detail);
    // TODO Auto-generated constructor stub
  }

  public ConstraintViolationException(URI type, String title, StatusType status) {
    super(type, title, status);
    // TODO Auto-generated constructor stub
  }

  public ConstraintViolationException(URI type, String title) {
    super(type, title);
    // TODO Auto-generated constructor stub
  }

  public ConstraintViolationException(URI type) {
    super(type);
    // TODO Auto-generated constructor stub
  }
}
