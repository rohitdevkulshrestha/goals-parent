package com.getvymo.platform.goals.app.service;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.getvymo.platform.goals.app.common.models.GoalAssigneeSettings;
import java.util.Date;

public class GoalAssignment implements IAuditable {
  private String id;

  private String userId;

  private boolean disabled;
  private boolean active;

  private String clientId;

  private String goalDefinitionCode;

  @JsonIgnore private String version;

  private Date createdDate;
  private Date lastUpdatedDate;
  private String createdBy;
  private String lastUpdatedBy;

  private GoalAssigneeSettings goalAssigneeSettings;

  public GoalAssignment() {}

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public boolean isDisabled() {
    return disabled;
  }

  public void setDisabled(boolean disabled) {
    this.disabled = disabled;
  }

  public String getVersion() {
    return version;
  }

  public void setVersion(String version) {
    this.version = version;
  }

  public GoalAssigneeSettings getGoalAssigneeSettings() {
    return goalAssigneeSettings;
  }

  public void setGoalAssigneeSettings(GoalAssigneeSettings goalAssigneeSettings) {
    this.goalAssigneeSettings = goalAssigneeSettings;
  }

  public String getClientId() {
    return clientId;
  }

  public void setClientId(String clientId) {
    this.clientId = clientId;
  }

  public String getGoalDefinitionCode() {
    return goalDefinitionCode;
  }

  public void setGoalDefinitionCode(String goalDefinitionCode) {
    this.goalDefinitionCode = goalDefinitionCode;
  }

  @Override
  public Date getCreatedDate() {
    return createdDate;
  }

  @Override
  public Date getLastUpdatedDate() {
    return lastUpdatedDate;
  }

  @Override
  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  @Override
  public String getLastUpdatedBy() {
    return lastUpdatedBy;
  }

  public void setLastUpdatedBy(String lastUpdatedBy) {
    this.lastUpdatedBy = lastUpdatedBy;
  }

  public boolean isActive() {
    return active;
  }

  public void setActive(boolean active) {
    this.active = active;
  }
}
