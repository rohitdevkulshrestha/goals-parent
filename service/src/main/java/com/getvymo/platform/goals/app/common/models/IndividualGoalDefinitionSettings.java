package com.getvymo.platform.goals.app.common.models;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonDeserialize(as = IndividualGoalDefinitionSettings.class)
public class IndividualGoalDefinitionSettings extends GoalDefinitionSettings {

  public IndividualGoalDefinitionSettings() {}

  //    public IndividualGoalDefinitionSettings(GoalType goalType, String metric, String name,
  // String description,
  //                                            String goalDefinitionCode, List<GoalMetricFilter>
  // metricFilters, Boolean external,
  //                                            Boolean showAchievementList, Integer goalOrder) {
  //        super(goalType, metric, name, description, goalDefinitionCode, metricFilters, external,
  // showAchievementList, goalOrder);
  //    }
}
