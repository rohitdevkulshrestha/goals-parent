package com.getvymo.platform.goals.app.service.evaluation.metricsrequest;

import java.util.List;

/** Created by debashish on 19/08/19. */
public class GroupByUser extends GroupBy {
  private UserGroupByEnum groupByType;
  private List<String> groupByRegions;

  public GroupByUser(UserGroupByEnum groupByType, List<String> groupByRegions) {
    this.groupByType = groupByType;
    this.groupByRegions = groupByRegions;
  }

  public GroupByUser(String type, UserGroupByEnum groupByType, List<String> groupByRegions) {
    super(type);
    this.groupByType = groupByType;
    this.groupByRegions = groupByRegions;
  }

  public UserGroupByEnum getGroupByType() {
    return groupByType;
  }

  public void setGroupByType(UserGroupByEnum groupByType) {
    this.groupByType = groupByType;
  }

  public List<String> getGroupByRegions() {
    return groupByRegions;
  }

  public void setGroupByRegions(List<String> groupByRegions) {
    this.groupByRegions = groupByRegions;
  }

  /*
  {type: 'user', group_by_type : 'immediate_hierarchy'}
   */

}
