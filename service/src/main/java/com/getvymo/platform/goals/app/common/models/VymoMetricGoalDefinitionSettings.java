package com.getvymo.platform.goals.app.common.models;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import in.vymo.core.config.model.goals.GoalMetricFilter;
import in.vymo.core.config.model.goals.GoalScope;
import in.vymo.core.config.model.goals.GoalType;
import java.util.List;

@JsonDeserialize(as = VymoMetricGoalDefinitionSettings.class)
public class VymoMetricGoalDefinitionSettings extends GoalDefinitionSettings {
  private List<GoalMetricFilter> metricFilters;

  public List<GoalMetricFilter> getMetricFilters() {
    return metricFilters;
  }

  public void setMetricFilters(List<GoalMetricFilter> metricFilters) {
    this.metricFilters = metricFilters;
  }

  public VymoMetricGoalDefinitionSettings() {}

  public VymoMetricGoalDefinitionSettings(
      String name,
      String description,
      String metric,
      GoalScope scope,
      GoalType type,
      String goalDefinitionCode,
      Boolean external,
      Boolean showAchievementList,
      Integer goalOrder,
      List<GoalMetricFilter> metricFilters,
      Boolean managerDrillDownEnabled,
      Boolean hideShortfall) {
    super(
        name,
        description,
        metric,
        scope,
        type,
        goalDefinitionCode,
        external,
        showAchievementList,
        goalOrder,
        managerDrillDownEnabled,
        hideShortfall);
    this.metricFilters = metricFilters;
  }
}
