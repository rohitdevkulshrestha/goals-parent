package com.getvymo.platform.goals.app.utils.configclient;

import com.getvymo.platform.goals.app.errors.GoalManagementServiceException;
import in.vymo.configuration.api.ConfigurationClient;
import in.vymo.configuration.lib.exceptions.ConfigClientException;
import in.vymo.core.config.model.client.Client;
import in.vymo.core.config.model.core.DATE_GRANULARITY;
import in.vymo.core.config.model.goals.GoalProgressTypeSettingMapping;
import in.vymo.core.config.model.goals.GoalsConfig;
import in.vymo.core.config.model.goals.RecurringFrequencyDefinition;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Log4j2
public class ConfigUtils {
  @Autowired private ConfigClient configClient;

  private ConfigurationClient client = configClient.client();

  private Map<String, List<GoalProgressTypeSettingMapping>>
      clientAndSortedGoalProgressTypeMappings = new HashMap<>();

  public DATE_GRANULARITY getDateGranularity(String recurringFrequency, String clientId) {
    log.info(
        " In getDateGranularity for recurringFrequency -> {} and clientId -> {}",
        recurringFrequency,
        clientId);

    DATE_GRANULARITY granularity = null;
    Client clientInfo;

    try {
      log.info("Trying to fetch client config in getDateGranularity");
      clientInfo = client.getClient(clientId);
      log.info("Trying to getGoalRecurringFrequencyDefinitions for clientId {} ", clientId);
      Map<String, RecurringFrequencyDefinition> clientFrequencyDefinitions =
          clientInfo.getGoalRecurringFrequencyDefinitions();

      RecurringFrequencyDefinition frequencyDefinition =
          clientFrequencyDefinitions.get(recurringFrequency);

      if (frequencyDefinition == null) {
        log.info("Couldn't find frequencyDefinition in client config");
        throw new GoalManagementServiceException(
            "Given frequency code " + recurringFrequency + " doesn't exist");
      }
      granularity = frequencyDefinition.getGranularity();
      log.info(
          "Found frequencyDefinition & granularity in client config for recurringFrequency {} and granularity {} ",
          recurringFrequency,
          granularity);

    } catch (ConfigClientException e) {
      log.error("Error while config fetch", e);
      throw new GoalManagementServiceException(
          "Problem with config parsing" + e.getLocalizedMessage());
    }

    return granularity;
  }

  public TimeZone getClientTimezone(String clientId) {
    log.info(" In getClientTimezone for clientId -> {}", clientId);

    DATE_GRANULARITY granularity = null;
    Client clientInfo;
    String ianaTimeZoneString = "Asia/Kolkata";

    TimeZone timeZone;

    try {
      log.info("Trying to fetch client config in getClientTimezone");
      clientInfo = client.getClient(clientId);

      if (clientInfo == null) {
        log.error("Client config not reachable");
      }

      if (clientInfo.getI18nConfig() == null) {
        log.error("Client i18n config is null so initialising it to {}", ianaTimeZoneString);
      } else {
        ianaTimeZoneString = clientInfo.getI18nConfig().getIanaTimezone();
      }

      log.debug(
          "Found ianaTimeZoneString in client config for ianaTimeZoneString {} ",
          ianaTimeZoneString);
      timeZone = TimeZone.getTimeZone(ianaTimeZoneString);
      log.debug("Timezone is {} & name is {}", timeZone.toString(), timeZone.getDisplayName());

    } catch (ConfigClientException e) {
      log.error("Error while config fetch", e);
      throw new GoalManagementServiceException(
          "Problem with config parsing" + e.getLocalizedMessage());
    }

    return timeZone;
  }

  public List<GoalProgressTypeSettingMapping> getGoalProgressTypeSettingMappings(String clientId) {
    log.info(" In getGoalProgressTypeSettingMappings for clientId -> {}", clientId);

    Client clientInfo;
    List<GoalProgressTypeSettingMapping> goalProgressTypeSettingMappings;

    try {
      log.info(
          "Trying to fetch client config in getGoalProgressTypeFromExpectedAndCurrentProgress");
      clientInfo = client.getClient(clientId);

      log.info("Trying to getGoalsConfig for clientId {} ", clientId);
      GoalsConfig goalsConfig = clientInfo.getGoalsConfig();

      if (goalsConfig != null) {
        goalProgressTypeSettingMappings = clientInfo.getGoalProgressTypeSettingMappings();
        log.info(
            "Found goalProgressTypeSettingMappings in client config for goalProgressTypeSettingMappings {}  ",
            goalProgressTypeSettingMappings.toString());
      } else {
        log.error("No goalsConfig & goalProgressTypeSettingMappings found for client {}", clientId);
        return null;
      }

    } catch (ConfigClientException e) {
      log.error("Error while config fetch", e);
      throw new GoalManagementServiceException(
          "Problem with config parsing" + e.getLocalizedMessage());
    }

    return goalProgressTypeSettingMappings;
  }
}
