/** */
package com.getvymo.platform.goals.app.persistence.dao;

import com.getvymo.platform.goals.app.persistence.entities.GoalAssigneesEntity;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/** @author chandrasekhar */
@Repository
public interface GoalAssigneeRepository extends JpaRepository<GoalAssigneesEntity, String> {
  public List<GoalAssigneesEntity> findByCurrentVersion(String currentVersion);
}
