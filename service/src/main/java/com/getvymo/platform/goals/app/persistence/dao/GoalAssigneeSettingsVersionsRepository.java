/** */
package com.getvymo.platform.goals.app.persistence.dao;

import com.getvymo.platform.goals.app.persistence.entities.GoalAssigneeSettingsVersionsEntity;
import com.getvymo.platform.goals.app.persistence.entities.GoalAssigneesEntity;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Repository;

/** @author chandrasekhar */
@Repository
public interface GoalAssigneeSettingsVersionsRepository
    extends JpaRepository<GoalAssigneeSettingsVersionsEntity, String> {
  public List<GoalAssigneeSettingsVersionsEntity> findByVersion(String version);

  public GoalAssigneeSettingsVersionsEntity findByGoalAssigneesEntityAndVersion(
      GoalAssigneesEntity goalAssigneesEntity, String version);

  // This query fetches goal_assignee_settings_versions with overlapping time ranges. Assignments
  // which aren't overlapping aren't fetched
  /* WHERE new_start < existing_end AND new_end   > existing_start;
  ((ns, ne, es, ee) = (new_start, new_end, existing_start, existing_end)):
  ns - ne - es - ee: doesn't overlap and doesn't match (because ne < es)
  ns - es - ne - ee: overlaps and matches
  es - ns - ee - ne: overlaps and matches
  es - ee - ns - ne: doesn't overlap and doesn't match (because ns > ee)
  es - ns - ne - ee: overlaps and matches
  ns - es - ee - ne: overlaps and matches
  * */
  @Query(
      value =
          "select gav.* from goal_assignee_settings_versions gav inner join goal_assignees ga on ga.current_version = gav"
              + ".version where gav.end_date  > :startDate and gav.start_date < :endDate and ga.user_id IN (:userIds) and gav.recurring_frequency= :recurringFrequency and ga.goal_definition_code="
              + " :goalDefinitionCode and ga.id=gav.goal_assignee_id and ga.client_id= :clientId",
      nativeQuery = true)
  public List<GoalAssigneeSettingsVersionsEntity>
      findLatestRowsForUserAndGoalDefinitionAndRecurringFrequency(
          @Param("userIds") List<String> userIds,
          @Param("recurringFrequency") String recurringFrequency,
          @Param("goalDefinitionCode") String goalDefinitionCode,
          @Param("startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) String startDate,
          @Param("endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) String endDate,
          @Param("clientId") String clientId);

  @Query(
      value =
          "select gav.* from goal_assignee_settings_versions gav inner join goal_assignees ga on ga.current_version = gav"
              + ".version where gav.start_date = :startDate and gav.end_date = :endDate and ga.user_id IN (:userIds) and gav"
              + ".recurring_frequency is null and ga.goal_definition_code= "
              + ":goalDefinitionCode and ga.id=gav.goal_assignee_id and ga.client_id= :clientId",
      nativeQuery = true)
  public List<GoalAssigneeSettingsVersionsEntity>
      findLatestRowsForUserAndGoalDefintionForOnetimeGoals(
          @Param("userIds") List<String> userIds,
          @Param("goalDefinitionCode") String goalDefinitionCode,
          @Param("startDate") String startDate,
          @Param("endDate") String endDate,
          @Param("clientId") String clientId);

  // Find the goal assignee which are less than given start date but end date greater than given end
  // date and are active and are the
  // latest version. It's pageable by spring data.
  @Query(
      value =
          ""
              + "select\n"
              + "        gav.* \n"
              + "    from\n"
              + "        goal_assignee_settings_versions gav \n"
              + "    inner join\n"
              + "        goal_assignees ga \n"
              + "            on ga.current_version = gav.version\n"
              + "    where\n"
              + "        gav.end_date  >= :startDate \n"
              + "        and gav.start_date <= :endDate \n"
              + "        and IF(CONCAT(:frequency) is NOT null, gav.recurring_frequency  IN (:frequency), gav.id IS NOT NULL) \n"
              + "        and IF(CONCAT(:childGoalCodes) is NOT null, gav.goal_definition_code NOT IN (:childGoalCodes), gav.id IS NOT NULL) \n"
              + "        and IF(CONCAT(:goalDefinitionCode) is NOT null, gav.goal_definition_code IN (:goalDefinitionCode), gav.id IS NOT NULL) \n"
              + "        and ga.user_id IN (:userIds) \n"
              + "        and ga.client_id = :clientId \n"
              + "        and ga.id=gav.goal_assignee_id \n"
              + "        and ga.deleted IN (:deleted) \n"
              + "        and ga.active= :active \n"
              + "    ORDER BY FIELD(gav.goal_definition_code, :goalDefinitionCodeInOrder) \n"
              + "           , FIELD(gav.recurring_frequency, :goalFrequencyInOrder) \n"
              + "    LIMIT :limit OFFSET :offset",
      nativeQuery = true)
  public List<GoalAssigneeSettingsVersionsEntity> findMatchingGoalAssignmentsForUsers(
      @Param("userIds") List<String> userIds,
      @Param("clientId") String clientId,
      @Param("startDate") String startDate,
      @Param("endDate") String endDate,
      @Param("active") Boolean active,
      @Param("deleted") List<Boolean> deleted,
      @Param("limit") Integer limit,
      @Param("offset") Integer offset,
      @Param("frequency") List<String> frequency,
      @Param("goalDefinitionCode") List<String> goalDefinitionCode,
      @Param("goalDefinitionCodeInOrder") List<String> goalDefinitionCodeInOrder,
      @Param("goalFrequencyInOrder") List<String> goalFrequencyInOrder,
      @Param("childGoalCodes") List<String> childGoalCodes);

  @Query(
      value =
          ""
              + "select\n"
              + "    count(*) \n"
              + "from\n"
              + "    goal_assignee_settings_versions gav \n"
              + "inner join\n"
              + "    goal_assignees ga \n"
              + "        on ga.current_version = gav.version \n"
              + "where\n"
              + "    gav.end_date  >= :startDate \n"
              + "    and gav.start_date <= :endDate \n"
              + "    and IF(CONCAT(:frequency) is NOT null, gav.recurring_frequency  IN (:frequency), gav.id IS NOT NULL) \n"
              + "    and IF(CONCAT(:childGoalCodes) is NOT null, gav.goal_definition_code NOT IN (:childGoalCodes), gav.id IS NOT NULL) \n"
              + "    and IF(CONCAT(:goalDefinitionCode) is NOT null, gav.goal_definition_code IN (:goalDefinitionCode), gav.id IS NOT NULL) \n"
              + "    and ga.user_id = :userId \n"
              + "    and ga.client_id = :clientId \n"
              + "    and ga.id=gav.goal_assignee_id \n"
              + "    and ga.deleted=false \n"
              + "    and ga.active= :active",
      nativeQuery = true)
  public Integer getCountOfMatchingGoalUserAssignments(
      @Param("userId") String userId,
      @Param("clientId") String clientId,
      @Param("startDate") String startDate,
      @Param("endDate") String endDate,
      @Param("active") Boolean active,
      @Param("frequency") List<String> frequency,
      @Param("goalDefinitionCode") List<String> goalDefinitionCode,
      @Param("childGoalCodes") List<String> childGoalCodes);
}
