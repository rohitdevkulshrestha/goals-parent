package com.getvymo.platform.goals.app.service;

public class ActivateVersionResponse {
  private String status;

  public ActivateVersionResponse() {}

  public ActivateVersionResponse(String status) {
    this.status = status;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }
}
