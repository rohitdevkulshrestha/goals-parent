package com.getvymo.platform.goals.app.errors;

import in.vymo.configuration.lib.exceptions.ConfigClientException;
import java.util.ArrayList;
import java.util.List;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import lombok.extern.log4j.Log4j2;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

@Log4j2
@RestControllerAdvice
public class CustomExceptionHandler {
  // private Logger logger = LoggerFactory.getLogger(this.getClass());

  @ExceptionHandler(Exception.class)
  public final ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request) {
    List<String> details = new ArrayList<>();
    details.add(ex.getLocalizedMessage());
    //        log.error("Unknown error encountered " + ex.toString());
    //        log.error(ex.getMessage());
    log.error("Exception Unknown error encountered ", ex);
    ErrorResponse errorResponse =
        new ErrorResponse(details, "Something went wrong with us. We are on it!");
    return new ResponseEntity<>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @ExceptionHandler(GoalManagementServiceException.class)
  public final ResponseEntity<Object> handleGoalManagementServiceException(
      Exception ex, WebRequest request) {
    List<String> details = new ArrayList<>();
    details.add(ex.getLocalizedMessage());
    //        log.error(ex.getMessage());
    log.error("GoalManagementServiceException in goal service ", ex);
    ErrorResponse errorResponse = new ErrorResponse(details, "Error in goals service");
    return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(RecordNotFoundException.class)
  public final ResponseEntity<Object> handleUserNotFoundException(
      RecordNotFoundException ex, WebRequest request) {
    List<String> details = new ArrayList<>();
    log.error("RecordNotFoundException in goal service ", ex);
    details.add(ex.getLocalizedMessage());
    ErrorResponse error = new ErrorResponse(details, "Record Not Found");
    return new ResponseEntity<Object>(error, HttpStatus.NOT_FOUND);
  }

  //    @ExceptionHandler(value = {IllegalArgumentException.class, IllegalStateException.class})
  //    public final ResponseEntity<Object> handleIllegalArgumentException(RuntimeException ex,
  // WebRequest metricsrequest) {
  //        List<String> details = new ArrayList<>();
  //        details.add(ex.getLocalizedMessage());
  //        ErrorResponse error = new ErrorResponse(details, "Invalid arguments passed");
  //        return new ResponseEntity<Object>(error, HttpStatus.BAD_REQUEST);
  //    }

  @ExceptionHandler(ConstraintViolationException.class)
  public final ResponseEntity<Object> handleConstraintViolationException(
      ConstraintViolationException ex, WebRequest request) {
    List<String> details = new ArrayList<>();
    log.error("ConstraintViolationException in goal service ", ex);
    for (ConstraintViolation<?> violation : ex.getConstraintViolations()) {
      details.add(violation.getMessage());
    }
    ErrorResponse error = new ErrorResponse(details, "Missing arguments");
    return new ResponseEntity<Object>(error, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(DataIntegrityViolationException.class)
  public final ResponseEntity<Object> handleDataIntegrityViolationException(
      DataIntegrityViolationException ex, WebRequest request) {
    List<String> details = new ArrayList<>();
    log.error("DataIntegrityViolationException in goal service ", ex);
    details.add(ex.getLocalizedMessage());
    ErrorResponse error = new ErrorResponse(details, "Duplicate record found");
    return new ResponseEntity<Object>(error, HttpStatus.CONFLICT);
  }

  @ExceptionHandler(ConfigClientException.class)
  public final ResponseEntity<Object> handleConfigClientException(
      DataIntegrityViolationException ex, WebRequest request) {
    List<String> details = new ArrayList<>();
    log.error("ConfigClientException in goal service ", ex);
    details.add(ex.getLocalizedMessage());
    ErrorResponse error = new ErrorResponse(details, "Config Exception");
    return new ResponseEntity<Object>(error, HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @ExceptionHandler(HttpMessageNotReadableException.class)
  public final ResponseEntity<Object> handleHttpMessageNotReadableException(
      HttpMessageNotReadableException ex, WebRequest request) {
    List<String> details = new ArrayList<>();
    log.error("HttpMessageNotReadableException in goal service ", ex);
    details.add(ex.getLocalizedMessage());
    ErrorResponse error = new ErrorResponse(details, "Invalid data format");
    return new ResponseEntity<Object>(error, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(MethodArgumentNotValidException.class)
  protected ResponseEntity<Object> handleMethodArgumentNotValid(
      MethodArgumentNotValidException ex,
      HttpHeaders headers,
      HttpStatus status,
      WebRequest request) {
    List<String> details = new ArrayList<>();
    log.error("MethodArgumentNotValidException in goal service ", ex);
    for (ObjectError error : ex.getBindingResult().getAllErrors()) {
      details.add(error.getDefaultMessage());
    }
    ErrorResponse error = new ErrorResponse(details, "Validation Failed");
    return new ResponseEntity<Object>(error, HttpStatus.BAD_REQUEST);
  }
}
