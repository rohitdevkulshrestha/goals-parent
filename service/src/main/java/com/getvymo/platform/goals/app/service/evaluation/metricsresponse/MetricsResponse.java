package com.getvymo.platform.goals.app.service.evaluation.metricsresponse;

public class MetricsResponse {

  public MetricsResultObject result;

  public MetricsResponse() {}

  public MetricsResponse(MetricsResultObject result) {
    this.result = result;
  }

  public MetricsResultObject getResult() {
    return result;
  }

  public void setResult(MetricsResultObject result) {
    this.result = result;
  }

  @Override
  public String toString() {
    return "MetricsResponse{" + "result=" + result + '}';
  }
}
