package com.getvymo.platform.goals.app.service;

import java.util.Date;

public class GoalAssignmentOperationVersion implements IAuditable {
  private String version;
  private Date createdDate;
  private Date lastUpdatedDate;
  private String createdBy;
  private String lastUpdatedBy;

  public GoalAssignmentOperationVersion(
      String version,
      Date createdDate,
      Date lastUpdatedDate,
      String createdBy,
      String lastUpdatedBy) {
    this.version = version;
    this.createdDate = createdDate;
    this.lastUpdatedDate = lastUpdatedDate;
    this.createdBy = createdBy;
    this.lastUpdatedBy = lastUpdatedBy;
  }

  @Override
  public Date getCreatedDate() {
    return null;
  }

  @Override
  public Date getLastUpdatedDate() {
    return null;
  }

  @Override
  public String getCreatedBy() {
    return null;
  }

  @Override
  public String getLastUpdatedBy() {
    return null;
  }

  public String getVersion() {
    return version;
  }

  public void setVersion(String version) {
    this.version = version;
  }
}
