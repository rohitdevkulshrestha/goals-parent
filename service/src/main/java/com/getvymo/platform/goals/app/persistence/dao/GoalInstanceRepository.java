/** */
package com.getvymo.platform.goals.app.persistence.dao;

import com.getvymo.platform.goals.app.persistence.entities.GoalInstanceEntity;
import java.util.Date;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/** @author chandrasekhar */
@Repository
public interface GoalInstanceRepository extends JpaRepository<GoalInstanceEntity, String> {

  public Page<GoalInstanceEntity> findByUserIdAndClientId(
      String userId, String clientId, Pageable page);

  public Page<GoalInstanceEntity> findByUserIdInAndClientIdAndGoalDefinitionCode(
      List<String> userId, String clientId, String goalDefinitionCode, Pageable page);

  public List<GoalInstanceEntity> findByUserIdInAndClientIdAndGoalDefinitionCode(
      List<String> userIds, String clientId, String goalDefinitionCode);

  public GoalInstanceEntity
      findByUserIdAndClientIdAndGoalAssigneeIdAndStartDateLessThanEqualAndEndDateGreaterThanEqualAndDeleted(
          String userId,
          String clientId,
          String goalAssigneeId,
          Date currentDate,
          Date endDate,
          Boolean deleted);

  public List<GoalInstanceEntity>
      findByUserIdInAndGoalDefinitionCodeAndClientIdAndStartDateLessThanEqualAndEndDateGreaterThanEqualAndRecurringFrequencyAndDeleted(
          List<String> userId,
          String goalDefinitionCode,
          String clientId,
          Date currentDate,
          Date endDate,
          String recurringFrequency,
          Boolean deleted);

  @Query(
      value =
          "select * from goal_instances where end_date >= :startDate and start_date <= :endDate and user_id IN (:userIds) and "
              + "client_id =:clientId and goal_assignee_id IN (:goalAssigneeIds)",
      nativeQuery = true)
  public List<GoalInstanceEntity> findMatchingWithinDateRangeForUsersAndGoalAssigneeId(
      @Param("userIds") List<String> userIds,
      @Param("clientId") String clientId,
      @Param("startDate") String startDate,
      @Param("endDate") String endDate,
      @Param("goalAssigneeIds") List<String> goalAssigneeIds);

  @Query(
      value =
          "select * from goal_instances where end_date >= :startDate and start_date <= :endDate and user_id =:userId and "
              + "client_id =:clientId",
      nativeQuery = true)
  public List<GoalInstanceEntity> findMatchingWithinDateRangeAndNoGoalAssigneeId(
      @Param("userId") String userId,
      @Param("clientId") String clientId,
      @Param("startDate") String startDate,
      @Param("endDate") String endDate);

  @Query(
      value =
          "select * from goal_instances where end_date >= :startDate and start_date <= :endDate and user_id IN (:userIds) and "
              + "client_id =:clientId",
      nativeQuery = true)
  public List<GoalInstanceEntity> findMatchingWithinDateRangeForUsersAndNoGoalAssigneeId(
      @Param("userIds") List<String> userIds,
      @Param("clientId") String clientId,
      @Param("startDate") String startDate,
      @Param("endDate") String endDate);

  public List<GoalInstanceEntity>
      findByUserIdInAndGoalDefinitionCodeAndClientIdAndStartDateLessThanEqualAndEndDateGreaterThanEqualAndDeleted(
          List<String> userIds,
          String goalDefinitionCode,
          String clientId,
          Date toDate,
          Date fromDate,
          Boolean deleted);

  public List<GoalInstanceEntity>
      findByUserIdInAndRecurringFrequencyAndClientIdAndStartDateLessThanEqualAndEndDateGreaterThanEqualAndDeleted(
          List<String> userIds,
          String recurringFrequency,
          String clientId,
          Date toDate,
          Date fromDate,
          Boolean deleted);
}
