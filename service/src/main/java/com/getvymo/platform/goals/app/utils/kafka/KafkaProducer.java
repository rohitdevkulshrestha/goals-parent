package com.getvymo.platform.goals.app.utils.kafka;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class KafkaProducer {

  @Autowired private KafkaTemplate<String, String> kafkaTemplate;

  public void sendMessage(String message, String topic) {
    log.info("#### -> Producing message " + message + " for topic " + topic);
    this.kafkaTemplate.send(topic, message);
  }
}
