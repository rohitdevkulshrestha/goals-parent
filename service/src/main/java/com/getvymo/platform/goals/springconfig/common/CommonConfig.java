package com.getvymo.platform.goals.springconfig.common;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.getvymo.platform.goals.springconfig.common.log.MetaDataInterceptor;
import com.getvymo.platform.goals.springconfig.common.web.RequestMetaDataHandlerMethodArgumentResolver;
import java.util.List;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.http.MediaType;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.zalando.problem.jackson.ProblemModule;

// dont touch this
@Configuration
@EnableAutoConfiguration(exclude = ErrorMvcAutoConfiguration.class)
public class CommonConfig implements WebMvcConfigurer {
  @Bean
  public ObjectMapper buildObjectMapper() {
    ObjectMapper mapper = new ObjectMapper();
    setUpObjectMapper(mapper);
    return mapper;
  }

  @Override
  public void addArgumentResolvers(List<HandlerMethodArgumentResolver> resolvers) {
    resolvers.add(new RequestMetaDataHandlerMethodArgumentResolver());
  }

  public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
    configurer.mediaType("json", MediaType.APPLICATION_JSON);
  }

  @Override
  public void addInterceptors(InterceptorRegistry registry) {
    registry.addWebRequestInterceptor(metaDataInterceptor()).order(Ordered.HIGHEST_PRECEDENCE);
  }

  @Bean
  public MetaDataInterceptor metaDataInterceptor() {
    return new MetaDataInterceptor();
  }

  public static final void setUpObjectMapper(final ObjectMapper mapper) {
    mapper.setSerializationInclusion(Include.NON_NULL);
    // mapper features
    mapper.enable(MapperFeature.ACCEPT_CASE_INSENSITIVE_ENUMS);
    // ser/deser features
    mapper.configure(SerializationFeature.WRITE_ENUMS_USING_TO_STRING, true);
    mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    mapper.configure(DeserializationFeature.USE_JAVA_ARRAY_FOR_JSON_ARRAY, true);
    // register problem module and any other modules
    mapper.findAndRegisterModules();
    mapper.registerModule(new ProblemModule().withStackTraces());
  }
}
