package com.getvymo.platform.goals.app.utils.azurevault;

import com.microsoft.aad.adal4j.AuthenticationContext;
import com.microsoft.aad.adal4j.AuthenticationResult;
import com.microsoft.aad.adal4j.ClientCredential;
import com.microsoft.azure.keyvault.authentication.KeyVaultCredentials;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ClientSecretKeyVaultCredential extends KeyVaultCredentials {

  private static final Logger LOG =
      LoggerFactory.getLogger(ClientSecretKeyVaultCredential.class.getCanonicalName());

  private String clientId;
  private String clientKey;

  public ClientSecretKeyVaultCredential(String clientId, String clientKey) {
    this.clientId = clientId;
    this.clientKey = clientKey;
  }

  @Override
  public String doAuthenticate(String authorization, String resource, String scope) {
    try {
      AuthenticationResult token =
          getAccessTokenFromClientCredentials(authorization, resource, clientId, clientKey);
      return token.getAccessToken();
    } catch (Exception e) {
      LOG.error(
          "Unable to authenticate with azure key vault , please recheck your provided credentials",
          e);
      LOG.error(e.getMessage());
      return "";
    }
  }

  private static AuthenticationResult getAccessTokenFromClientCredentials(
      String authorization, String resource, String clientId, String clientKey) throws Exception {
    AuthenticationContext context = null;
    AuthenticationResult result = null;
    ExecutorService service = null;
    try {
      service = Executors.newFixedThreadPool(1);
      context = new AuthenticationContext(authorization, false, service);
      ClientCredential credentials = new ClientCredential(clientId, clientKey);
      Future<AuthenticationResult> future = context.acquireToken(resource, credentials, null);
      result = future.get();
    } catch (Exception e) {
      LOG.error("Error while authenticating with azure key vault->{}", e.getMessage(), e);
      throw e;
    } finally {
      service.shutdown();
    }

    if (result == null) {
      LOG.error("Azure key vault authentication result was null, recheck the credentials");
      throw new Exception("authentication result was null");
    }
    return result;
  }
}
