package com.getvymo.platform.goals.app.persistence.entities;

import com.getvymo.platform.goals.app.service.utils.GoalsUtils;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "goal_instances")
public class GoalInstanceEntity extends GoalAuditableEntity {

  @Id @GeneratedValue private int id;

  @NotEmpty(message = "Client id cannot be empty")
  @Column(name = "client_id", nullable = false, updatable = false)
  private String clientId;

  @NotEmpty(message = "User id cannot be empty")
  @Column(name = "user_id", nullable = false, updatable = false)
  private String userId;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "start_date", nullable = false, updatable = true)
  private Date startDate;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "end_date", nullable = false, updatable = true)
  private Date endDate;

  @Column(name = "target", nullable = false, updatable = true)
  private Double target;

  @ManyToOne
  @JoinColumn(name = "goal_assignee_id")
  private GoalAssigneesEntity goalAssignee;

  private String goalDefinitionCode;

  private Boolean isRecurring;

  private String recurringFrequency;

  private String createdBy;

  private String lastUpdatedBy;

  private Boolean deleted;

  private Boolean isActive;

  public GoalInstanceEntity() {}

  public GoalInstanceEntity(
      String clientId,
      String userId,
      Date startDate,
      Date endDate,
      Double target,
      String createdBy,
      String lastUpdatedBy,
      Boolean deleted,
      Boolean isRecurring,
      String recurringFrequency,
      Boolean isActive) {
    super();
    this.clientId = clientId;
    this.userId = userId;
    this.startDate = startDate;
    this.endDate = endDate;
    this.target = target;
    this.createdBy = createdBy;
    this.lastUpdatedBy = lastUpdatedBy;
    this.deleted = deleted;
    this.isRecurring = isRecurring;
    this.recurringFrequency = recurringFrequency;
    this.isActive = isActive;
  }

  public String getClientId() {
    return clientId;
  }

  public void setClientId(String clientId) {
    this.clientId = clientId;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public Date getStartDate() {
    return startDate;
  }

  public void setStartDate(Date startDate) {
    this.startDate = startDate;
  }

  public Date getEndDate() {
    return endDate;
  }

  public void setEndDate(Date endDate) {
    this.endDate = endDate;
  }

  public Double getTarget() {
    return target;
  }

  public void setTarget(Double target) {
    this.target = GoalsUtils.round(target, 2);
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public String getLastUpdatedBy() {
    return lastUpdatedBy;
  }

  public void setLastUpdatedBy(String lastUpdatedBy) {
    this.lastUpdatedBy = lastUpdatedBy;
  }

  public void setUpdatedBy(String updatedBy) {
    this.lastUpdatedBy = updatedBy;
  }

  public Boolean getDeleted() {
    return deleted;
  }

  public void setDeleted(Boolean deleted) {
    this.deleted = deleted;
  }

  public int getId() {
    return id;
  }

  public Boolean isRecurring() {
    return isRecurring;
  }

  public void setRecurring(Boolean isRecurring) {
    this.isRecurring = isRecurring;
  }

  public String getRecurringFrequency() {
    return recurringFrequency;
  }

  public void setRecurringFrequency(String recurringFrequency) {
    this.recurringFrequency = recurringFrequency;
  }

  public String getGoalDefinitionCode() {
    return goalDefinitionCode;
  }

  public void setGoalDefinitionCode(String goalDefinitionCode) {
    this.goalDefinitionCode = goalDefinitionCode;
  }

  public GoalAssigneesEntity getGoalAssignee() {
    return goalAssignee;
  }

  public void setGoalAssignee(GoalAssigneesEntity goalAssignee2) {
    this.goalAssignee = goalAssignee2;
  }

  @Override
  public String toString() {
    return "GoalInstanceEntity{"
        + "id="
        + id
        + ", clientId='"
        + clientId
        + '\''
        + ", userId='"
        + userId
        + '\''
        + ", startDate="
        + startDate
        + ", endDate="
        + endDate
        + ", target="
        + target
        + ", goalAssignee="
        + goalAssignee
        + ", goalDefinitionCode="
        + goalDefinitionCode
        + ", isRecurring="
        + isRecurring
        + ", recurringFrequency='"
        + recurringFrequency
        + '\''
        + ", createdBy='"
        + createdBy
        + '\''
        + ", lastUpdatedBy='"
        + lastUpdatedBy
        + '\''
        + ", deleted="
        + deleted
        + ", isActive="
        + isActive
        + '}';
  }

  public Boolean getActive() {
    return isActive;
  }

  public void setActive(Boolean active) {
    isActive = active;
  }
}
