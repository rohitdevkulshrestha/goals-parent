/** */
package com.getvymo.platform.goals.app.persistence.entities;

import com.getvymo.platform.goals.app.service.utils.GoalsUtils;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.validator.constraints.NotEmpty;

/** @author chandrasekhar */
@Entity
@Table(name = "goal_assignee_settings_versions")
public class GoalAssigneeSettingsVersionsEntity extends GoalAuditableEntity {

  @Id
  @Column(name = "id", columnDefinition = "VARCHAR(36)")
  private String id;

  private String goalDefinitionCode;

  @ManyToOne
  @JoinColumn(name = "goal_assignee_id", nullable = false)
  private GoalAssigneesEntity goalAssigneesEntity;

  private String recurringFrequency;

  private Double target;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "start_date", nullable = false, updatable = true)
  private Date startDate;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "end_date", nullable = false, updatable = true)
  private Date endDate;

  private boolean isRecurring;

  private String version;

  private String lastUpdatedBy;

  @NotEmpty(message = "User id cannot be empty")
  private String userId;

  @NotEmpty(message = "Client id cannot be empty")
  private String clientId;

  private String createdBy;

  private Boolean deleted;

  public GoalAssigneeSettingsVersionsEntity() {}

  public GoalAssigneeSettingsVersionsEntity(
      String goalDefinitionCode,
      GoalAssigneesEntity goalAssigneesEntity,
      String recurringFrequency,
      Double target,
      Date startDate,
      Date endDate,
      boolean isRecurring,
      String version,
      String lastUpdatedBy,
      String userId,
      String clientId,
      String createdBy,
      Boolean deleted) {
    super();
    this.deleted = deleted;
    this.id = GoalsUtils.generateRandomStringId();
    this.goalDefinitionCode = goalDefinitionCode;
    this.goalAssigneesEntity = goalAssigneesEntity;
    this.recurringFrequency = recurringFrequency;
    this.target = target;
    this.startDate = startDate;
    this.endDate = endDate;
    this.isRecurring = isRecurring;
    this.version = version;
    this.lastUpdatedBy = lastUpdatedBy;
    this.userId = userId;
    this.clientId = clientId;
    this.createdBy = createdBy;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getClientId() {
    return clientId;
  }

  public void setClientId(String clientId) {
    this.clientId = clientId;
  }

  public String getId() {
    return id;
  }

  public String getRecurringFrequencyDefinitionId() {
    return recurringFrequency;
  }

  public void setRecurringFrequencyDefinitionId(String recurringFrequencyDefinitionId) {
    this.recurringFrequency = recurringFrequencyDefinitionId;
  }

  public Double getTarget() {
    return target;
  }

  public void setTarget(Double target) {
    this.target = target;
  }

  public Date getStartDate() {
    return startDate;
  }

  public void setStartDate(Date startDate) {
    this.startDate = startDate;
  }

  public Date getEndDate() {
    return endDate;
  }

  public void setEndDate(Date endDate) {
    this.endDate = endDate;
  }

  public boolean isRecurring() {
    return isRecurring;
  }

  public void setRecurring(boolean isRecurring) {
    this.isRecurring = isRecurring;
  }

  public String getVersion() {
    return version;
  }

  public void setVersion(String version) {
    this.version = version;
  }

  public String getLastUpdatedBy() {
    return lastUpdatedBy;
  }

  public void setLastUpdatedBy(String lastUpdatedBy) {
    this.lastUpdatedBy = lastUpdatedBy;
  }

  public String getGoalDefinitionCode() {
    return goalDefinitionCode;
  }

  public void setGoalDefinitionCode(String goalDefinitionCode) {
    this.goalDefinitionCode = goalDefinitionCode;
  }

  public GoalAssigneesEntity getGoalAssigneesEntity() {
    return goalAssigneesEntity;
  }

  public void setGoalAssigneesEntity(GoalAssigneesEntity goalAssigneesEntity) {
    this.goalAssigneesEntity = goalAssigneesEntity;
  }

  public String getRecurringFrequency() {
    return recurringFrequency;
  }

  public void setRecurringFrequency(String recurringFrequency) {
    this.recurringFrequency = recurringFrequency;
  }

  public Boolean getDeleted() {
    return deleted;
  }

  public void setDeleted(Boolean deleted) {
    this.deleted = deleted;
  }
}
