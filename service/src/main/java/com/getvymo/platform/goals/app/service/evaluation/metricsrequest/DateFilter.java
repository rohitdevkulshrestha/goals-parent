package com.getvymo.platform.goals.app.service.evaluation.metricsrequest;

import com.fasterxml.jackson.annotation.JsonFormat;

/** Created by debashish on 19/08/19. */
public class DateFilter extends Filter {
  private String dateDimension; // created_date, schedule_date

  @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
  private Object start;

  @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
  private Object end;

  public DateFilter(String type, String dateDimension, Object start, Object end) {
    super(type);
    this.dateDimension = dateDimension;
    this.start = start;
    this.end = end;
  }

  public DateFilter(String dateDimension, Object start, Object end) {
    this.dateDimension = dateDimension;
    this.start = start;
    this.end = end;
  }

  public String getDateDimension() {
    return dateDimension;
  }

  public void setDateDimension(String dateDimension) {
    this.dateDimension = dateDimension;
  }

  public Object getStart() {
    return start;
  }

  public void setStart(Object start) {
    this.start = start;
  }

  public Object getEnd() {
    return end;
  }

  public void setEnd(Object end) {
    this.end = end;
  }

  /*
  {type:'range',dimension:'created_date',start:"", end: ""}
   */
}
