package com.getvymo.platform.goals.app.persistence.dao;

import com.getvymo.platform.goals.app.persistence.entities.ExternalAchievementEntity;
import java.util.List;

public interface IExternalAchievementRepository {

  public List<ExternalAchievementEntity> getMatchingExternalAchievementEntitiesForOneUser(
      String userId,
      String goalDefinitionCode,
      String clientId,
      Long startDate,
      Long endDate,
      Boolean isRecurring,
      String recurringFrequency);

  public String saveExternalAchievement(ExternalAchievementEntity externalAchievementEntity);

  public List<ExternalAchievementEntity> findByIdsIn(
      List<String> listOfAchievementIds, String clientId);

  public List<ExternalAchievementEntity> getMatchingEntitiesForSetOfUsersAndCurrentDateRange(
      List<String> userIds,
      String goalDefinitionCode,
      String clientId,
      Long currentDate1,
      Long currentDate2,
      String recurringFrequency);

  public List<ExternalAchievementEntity> getMatchingEntitiesForSetOfUsersAndAnyDateRange(
      List<String> userIds,
      String goalDefinitionCode,
      String clientId,
      Long startDate,
      Long endDate,
      String recurringFrequency);

  public void bulkSaveExternalAchievementEntities(
      List<ExternalAchievementEntity> externalAchievementEntities, String clientId);
}
