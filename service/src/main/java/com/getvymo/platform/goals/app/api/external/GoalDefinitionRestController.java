package com.getvymo.platform.goals.app.api.external;

import com.getvymo.platform.goals.app.common.models.StandardListResponse;
import com.getvymo.platform.goals.app.service.goalsmgmt.IGoalManagementService;
import in.vymo.core.config.model.goals.GoalDefinition;
import javax.validation.Valid;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/goals/v1/clients/{clientId}/goaldefinitions")
@Log4j2
public class GoalDefinitionRestController {
  @Autowired private IGoalManagementService iGoalManagementService;

  @GetMapping(path = "/{goalDefinitionCode}", consumes = "application/json")
  public GoalDefinition getGoalDefinitionByCode(
      @Valid @PathVariable(value = "goalDefinitionCode") String goalDefinitionCode,
      @Valid @PathVariable String clientId) {
    return iGoalManagementService.getGoalDefinitionByCode(goalDefinitionCode, clientId);
  }

  @GetMapping(path = "/all", consumes = "application/json")
  public StandardListResponse getAllGoalDefinitions(
      @Valid @PathVariable String clientId,
      @RequestParam(value = "pageNumber", required = false, defaultValue = "0") int pageNumber,
      @RequestParam(value = "pageSize", required = false, defaultValue = "20") int pageSize) {
    return iGoalManagementService.getAllGoalDefinitions(clientId, pageNumber, pageSize);
  }
}
