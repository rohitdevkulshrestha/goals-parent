package com.getvymo.platform.goals.app.service.external.achievement;

import com.getvymo.platform.goals.app.common.models.BulkExternalAchievementRequest;
import com.getvymo.platform.goals.app.common.models.ExternalAchievement;
import com.getvymo.platform.goals.app.common.models.ExternalAchievementEntry;
import com.getvymo.platform.goals.app.common.models.StandardListRequest;
import com.getvymo.platform.goals.app.errors.GoalManagementServiceException;
import com.getvymo.platform.goals.app.persistence.dao.ExternalAchievementRepository;
import com.getvymo.platform.goals.app.persistence.entities.ExternalAchievementEntity;
import com.getvymo.platform.goals.app.service.BulkExternalAchievementResponse;
import com.getvymo.platform.goals.app.service.BulkGoalAchievement;
import com.getvymo.platform.goals.app.service.ExternalAchievementApiObject;
import com.getvymo.platform.goals.app.service.utils.GoalsUtils;
import com.getvymo.platform.goals.app.utils.configclient.ConfigUtil;
import com.getvymo.platform.goals.app.utils.configclient.ConfigUtils;
import in.vymo.core.config.model.goals.GoalDefinition;
import in.vymo.core.config.model.goals.GoalType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.stream.Collectors;
import lombok.extern.log4j.Log4j2;
import org.elasticsearch.ElasticsearchStatusException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Log4j2
public class ExternalAchievementService implements IExternalAchievementsService {

  @Autowired GoalsUtils goalsUtils;

  ConfigUtil configUtil = new ConfigUtil();

  @Autowired private ConfigUtils configUtils;

  @Autowired ExternalAchievementRepository externalAchievementQueryRepository;

  /*
   * 1. Get goal definition details using code from External Achievement Settings
   * 2. Fetch the current external achievements matching userId, goal definition, recurring frequency, start & end dates
   * 3. If there are any achievements matching our constraints, update the existing achievement map with new achievement
   * (replace/create) using key (achievement calculated date)
   * 4. Store the copy of the achievement value (latest calculated date in the parent - External Ach Entity)
   * 5. In case of deleting particular upload - fetch the achievement from map by key and mark as deleted, update the parent entity
   * with next best date's achievement value
   * 6. Save the entity (upsert - not new entry)
   * */
  @Override
  public ExternalAchievementApiObject saveExternalAchievement(
      String clientId, ExternalAchievement eas) {

    GoalDefinition goalDefinition =
        configUtil.getGoalDefinitionByCode(clientId, eas.getGoalDefinitionCode());
    Double achievement = eas.getAchievement();
    Date achievementUpdateDate = eas.getAchievementUpdatedDate();
    Boolean deleted = eas.getDeleted();
    String typeOfAchievement = eas.getTypeOfAchievement();

    HashMap<Long, ExternalAchievementEntry> achievementsMap = new HashMap<>();
    log.info("Received goal definition code {} ", eas.getGoalDefinitionCode());
    List<ExternalAchievementEntity> externalAchievementEntityList = new ArrayList<>();
    //        goalsUtils.prettyPrintStringFromJSON(eas, "EAS");

    TimeZone clientTimezone = configUtils.getClientTimezone(clientId);
    Date easStartDate = goalsUtils.getOffsetCorrectedDate(eas.getStartDate(), clientTimezone);
    Date easEndDate = goalsUtils.getOffsetCorrectedDate(eas.getEndDate(), clientTimezone);
    easEndDate = new Date(easEndDate.getTime() - 2000);
    try {
      externalAchievementEntityList =
          externalAchievementQueryRepository.getMatchingExternalAchievementEntitiesForOneUser(
              eas.getUserId(),
              goalDefinition.getCode(),
              clientId,
              easStartDate.getTime(),
              easEndDate.getTime(),
              null,
              eas.getRecurringFrequency());

    } catch (ElasticsearchStatusException e) {
      log.error("Index not created yet {}", e.status());
      log.error("Index not created yet {}", e);
    }

    log.info("Received {} externalAchievementEntities", externalAchievementEntityList.size());

    log.info("Searching and creating map matching our criteria");

    ExternalAchievementEntity achievementEntity;
    if (externalAchievementEntityList.size() == 0) {

      achievementEntity =
          new ExternalAchievementEntity(
              goalDefinition.getCode(),
              clientId,
              eas.getUserId(),
              easStartDate,
              easEndDate,
              achievement,
              eas.getRecurring(),
              eas.getRecurringFrequency(),
              goalDefinition.getMetric(),
              achievementsMap,
              eas.getLastUpdatedBy(),
              deleted,
              eas.getDataType(),
              eas.getAchievementUpdatedDate());

    } else {
      achievementEntity = externalAchievementEntityList.get(0);
    }

    ExternalAchievementEntry externalAchievementObject =
        new ExternalAchievementEntry(
            achievement,
            achievementUpdateDate,
            typeOfAchievement,
            deleted,
            eas.getLastUpdatedBy(),
            eas.getDataType());
    externalAchievementObject.setLastUpdatedDate(
        goalsUtils.getOffsetCorrectedDate(
            externalAchievementObject.getLastUpdatedDate(), clientTimezone));
    achievementEntity =
        getNewAchievementsMapFromExisting(achievementEntity, externalAchievementObject);
    achievementEntity.setLastUpdatedDate(new Date());

    String achievementId =
        externalAchievementQueryRepository.saveExternalAchievement(achievementEntity);
    log.info("Save achievementEntity with id {}", achievementId);
    List<ExternalAchievementEntity> externalAchievementEntities =
        externalAchievementQueryRepository.findByIdsIn(Arrays.asList(achievementId), clientId);

    ExternalAchievementEntity savedEntity = externalAchievementEntities.get(0);
    ExternalAchievementApiObject externalAchievement =
        goalsUtils.getExternalAchievementFromEntity(savedEntity);
    return externalAchievement;
  }

  /*
   * This method modifies the existing achievements map and stores the required value and achievementupdateddate in the entity
   * It ensures that value is the latest value
   * */
  private ExternalAchievementEntity getNewAchievementsMapFromExisting(
      ExternalAchievementEntity achievementEntity,
      ExternalAchievementEntry externalAchievementObject) {
    log.info("In getNewAchievementsMapFromExisting");
    HashMap<Long, ExternalAchievementEntry> exisitingAchievementsMap =
        achievementEntity.getAchievementsMap();
    // Add the new entry to existing achievement map of the ExternalAchievementEntity
    exisitingAchievementsMap.put(
        externalAchievementObject.getLastUpdatedDate().getTime(), externalAchievementObject);
    // update the values outside the map with the latest
    achievementEntity.setAchievement(externalAchievementObject.getValue());
    achievementEntity.setAchievementUpdatedDate(externalAchievementObject.getLastUpdatedDate());
    achievementEntity.setLastUpdatedBy(externalAchievementObject.getLastUpdatedBy());
    achievementEntity.setDataType(externalAchievementObject.getDataType());
    achievementEntity.setLastUpdatedDate(new Date());
    Map<Long, ExternalAchievementEntry> nonDeletedAchievementsMap =
        exisitingAchievementsMap.entrySet().stream()
            .filter(entry -> entry.getValue().isDeleted() == false)
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    // if the incoming value is asking to delete the external achievement then roll it back to
    // previous non -deleted value
    if (exisitingAchievementsMap.size() > 1
        && externalAchievementObject.isDeleted()
        && nonDeletedAchievementsMap.size() > 0) {
      /*
      1. Sort map by date key
      2. Filter the deleted achievements
      3. Pick the last non deleted achievement
      4. Set the achievement in the entity
      * */

      log.debug(
          "There are {} achievements in nonDeletedAchievementsMap",
          nonDeletedAchievementsMap.size());
      List<Long> sortedKeys = new ArrayList<Long>(nonDeletedAchievementsMap.size());
      sortedKeys.addAll(nonDeletedAchievementsMap.keySet());
      Collections.sort(sortedKeys);
      log.debug("Sorted keys are: ", sortedKeys);
      Long dateKey = sortedKeys.get(sortedKeys.size() - 1);
      log.debug("New date key to be considered ", dateKey);
      achievementEntity.setAchievement(exisitingAchievementsMap.get(dateKey).getValue());
      achievementEntity.setAchievementUpdatedDate(new Date(dateKey));
      log.debug(
          "Setting achievement from previously undeleted data ",
          exisitingAchievementsMap.get(dateKey).getValue());
    }
    log.info("Returning from getNewAchievementsMapFromExisting");

    return achievementEntity;
  }

  /*
   * Bulk save external achievements for a given goal definition code.
   * This method also handles the grouping by logic to optimise bulk save
   * It save bulk writes to ES entities with common goal definition code & date range & recurring frequency
   * */
  @Override
  public BulkExternalAchievementResponse bulkSaveExternalAchievements(
      String clientId,
      StandardListRequest<BulkExternalAchievementRequest>
          externalAchievementSettingsStandardListRequest) {

    BulkExternalAchievementResponse bulkExternalAchievementResponse =
        new BulkExternalAchievementResponse();

    ArrayList<BulkExternalAchievementRequest> externalAchievementSettingsArrayList =
        externalAchievementSettingsStandardListRequest.getData();
    try {

      log.info(
          "In bulkSaveExternalAchievements for clientID {} and data of size {}",
          clientId,
          externalAchievementSettingsArrayList.size());
      /*
       * 1. Group the ExternalAchievementApiObject by goal definition code, start & end date
       * 2. Batch the grouped ExternalAchievementApiObject
       * 3. For each batch fetch if there any existing ExternalAchievementEntities
       * 4. Repeat the steps we followed in saveExternalAchievement
       *
       * */
      log.info("Grouping records on start & endate , gcode & rec frequency");
      Map<String, Map<Date, Map<Date, Map<String, List<ExternalAchievement>>>>>
          groupedByGoalAndStartAndEndDates =
              externalAchievementSettingsArrayList.stream()
                  .collect(
                      Collectors.groupingBy(
                          ExternalAchievement::getGoalDefinitionCode,
                          Collectors.groupingBy(
                              ExternalAchievement::getStartDate,
                              Collectors.groupingBy(
                                  ExternalAchievement::getEndDate,
                                  Collectors.groupingBy(
                                      ExternalAchievement::getRecurringFrequency)))));

      //        goalsUtils.prettyPrintStringFromJSON(groupedByGoalAndStartAndEndDates, "Grouped by
      // list");

      List<List<ExternalAchievement>> listsOfExternalAchievementSettingsList =
          batchGroupedByExternalAchievementSettings(groupedByGoalAndStartAndEndDates, clientId);

      //        goalsUtils.prettyPrintStringFromJSON(listsOfExternalAchievementSettingsList,
      // "listsOfExternalAchievementSettingsList");
      //        externalAchievementQueryRepository.bulkSaveExternalAchievementEntities(new
      // ArrayList<>());
      bulkExternalAchievementResponse.setStatus("success");
      List<BulkGoalAchievement> bulkGoalAchievementListBasedOnStatus =
          getBulkGoalAchievementListBasedOnStatus(
              externalAchievementSettingsArrayList, "success", "success");
      bulkExternalAchievementResponse.setResults(bulkGoalAchievementListBasedOnStatus);
    } catch (Exception e) {
      log.error("Exception in bulkSaveExternalAchievements", e);
      log.error("Error while saving external achievements {}", e.getLocalizedMessage());
      bulkExternalAchievementResponse.setStatus("error");
      List<BulkGoalAchievement> bulkGoalAchievementListBasedOnStatus =
          getBulkGoalAchievementListBasedOnStatus(
              externalAchievementSettingsArrayList, "error", e.getLocalizedMessage());
      bulkExternalAchievementResponse.setResults(bulkGoalAchievementListBasedOnStatus);
    }
    return bulkExternalAchievementResponse;
  }

  private List<BulkGoalAchievement> getBulkGoalAchievementListBasedOnStatus(
      List<BulkExternalAchievementRequest> bulkExternalAchievements,
      String status,
      String message) {
    List<BulkGoalAchievement> bulkGoalAchievementList =
        new ArrayList<>(bulkExternalAchievements.size());
    for (BulkExternalAchievementRequest bulkExternalAchievement : bulkExternalAchievements) {
      BulkGoalAchievement bulkGoalAchievement = new BulkGoalAchievement();
      bulkGoalAchievement.setMsg(message);
      bulkGoalAchievement.setRow(bulkExternalAchievement.getRow());
      bulkGoalAchievement.setStatus(status);
      bulkGoalAchievementList.add(bulkGoalAchievement);
    }
    return bulkGoalAchievementList;
  }

  /*
   * This method accepts group of entitySettings which share a common goalDefinitioncode, recurring frequency & date range
   * Fetches from ES if there are any matching entites for the above attributes else creates new entity
   * For the existing entites, it will read the previous uploaded achievement values and appends the new value to its map
   *
   * */
  private void saveExternalAchievementLists(
      String goalDefinitionCode,
      String recurringFrequency,
      Date startDate,
      Date endDate,
      List<ExternalAchievement> easList,
      String clientId) {
    log.info(
        "In saveExternalAchievementLists to save batch of {} ExternalAchievementApiObject",
        easList.size());

    GoalDefinition goalDefinition =
        configUtil.getGoalDefinitionByCode(clientId, goalDefinitionCode);

    TimeZone clientTimezone = configUtils.getClientTimezone(clientId);
    startDate = goalsUtils.getOffsetCorrectedDate(startDate, clientTimezone);
    endDate = goalsUtils.getOffsetCorrectedDate(endDate, clientTimezone);
    // correction to 23:59:58
    endDate = new Date(endDate.getTime() - 2000);

    log.info(
        "Corrected startDate {} & endDate {} for clientTimezeon {} amd goalDefinitionCode {} ",
        startDate,
        endDate,
        clientTimezone,
        goalDefinitionCode);

    if (goalDefinition == null) {
      log.error("Goal definition code " + goalDefinitionCode + " doesn't exist");
      throw new GoalManagementServiceException(
          "Goal definition code " + goalDefinitionCode + " doesn't exist");
    }

    if (goalDefinition.getType() != GoalType.external) {
      log.error(
          "Goal definition code " + goalDefinitionCode + " is not an external goal definition");
      throw new GoalManagementServiceException(
          "Goal definition code " + goalDefinitionCode + " is not an external goal definition");
    }

    log.info("Received goal definition code {} ", goalDefinitionCode);
    List<String> userIds =
        easList.stream().map(ExternalAchievement::getUserId).collect(Collectors.toList());

    // TODO: If there are no goal instance entities matching the date range, gD code, userIds &
    // recurring freq => Throw error

    List<ExternalAchievementEntity> matchingExternalAchievementEntities = new ArrayList<>();
    try {
      matchingExternalAchievementEntities =
          externalAchievementQueryRepository.getMatchingEntitiesForSetOfUsersAndAnyDateRange(
              userIds,
              goalDefinitionCode,
              clientId,
              startDate.getTime(),
              endDate.getTime(),
              recurringFrequency);
      log.info(
          "Received {} matching externalAchievementEntities",
          matchingExternalAchievementEntities.size());
    } catch (ElasticsearchStatusException e) {
      log.error(
          "Index not created yet. It will be created on the first insert. Ignore for now! {}",
          e.getLocalizedMessage(),
          e);
    } catch (Exception ex) {
      log.error("Exception while saveExternalAchievementLists", ex);
    }
    goalsUtils.prettyPrintStringFromJSON(
        matchingExternalAchievementEntities, "matchingExternalAchievementEntities");
    // Create a userId <=> ExternalAchievementEntity map of the existing
    // matchingExternalAchievementEntities
    Map<String, ExternalAchievementEntity> userIdAndEntityMap =
        matchingExternalAchievementEntities.stream()
            .collect(
                Collectors.toMap(
                    ExternalAchievementEntity::getUserId,
                    externalAchievementEntity -> externalAchievementEntity));

    log.info("Create userIdAndEntityMap of size {} ", userIdAndEntityMap.size());
    List<ExternalAchievementEntity> externalAchievementEntityListTobeSaved = new ArrayList<>();

    log.info(
        "Iterating over ExternalAchievementApiObject and creating externalAchievementEntityListTobeSaved");
    /*Iterate for each settings list and create/modify required entity to be saved*/
    for (ExternalAchievement eas : easList) {
      Double achievement = eas.getAchievement();
      Date achievementUpdateDate = eas.getAchievementUpdatedDate();
      achievementUpdateDate =
          goalsUtils.getOffsetCorrectedDate(achievementUpdateDate, clientTimezone);
      Boolean deleted = eas.getDeleted();
      String typeOfAchievement = eas.getTypeOfAchievement();
      HashMap<Long, ExternalAchievementEntry> achievementsMap = new HashMap<>();
      // Check if there's an existing matchin entity for this particular user (date range, goal code
      // & frequency are uniquely
      // grouped before this)
      ExternalAchievementEntity externalAchievementEntity = userIdAndEntityMap.get(eas.getUserId());
      if (externalAchievementEntity == null) {
        // No existing matchingExternalAchievementEntities found so creating new one
        Date easStartDate = goalsUtils.getOffsetCorrectedDate(eas.getStartDate(), clientTimezone);
        Date easEndDate = goalsUtils.getOffsetCorrectedDate(eas.getEndDate(), clientTimezone);
        easEndDate = new Date(easEndDate.getTime() - 2000);
        externalAchievementEntity =
            new ExternalAchievementEntity(
                goalDefinitionCode,
                clientId,
                eas.getUserId(),
                easStartDate,
                easEndDate,
                achievement,
                eas.getRecurring(),
                eas.getRecurringFrequency(),
                goalDefinition.getMetric(),
                achievementsMap,
                eas.getLastUpdatedBy(),
                deleted,
                eas.getDataType(),
                achievementUpdateDate);
      }
      ExternalAchievementEntry externalAchievementObject =
          new ExternalAchievementEntry(
              achievement,
              achievementUpdateDate,
              typeOfAchievement,
              deleted,
              eas.getLastUpdatedBy(),
              eas.getDataType());

      // We now have old matchingExternalAchievementEntities & new ones for the non exisiting ones
      externalAchievementEntity =
          getNewAchievementsMapFromExisting(externalAchievementEntity, externalAchievementObject);

      externalAchievementEntityListTobeSaved.add(externalAchievementEntity);
    }

    log.info(
        "Created externalAchievementEntityListTobeSaved of size {} ",
        externalAchievementEntityListTobeSaved.size());

    externalAchievementQueryRepository.bulkSaveExternalAchievementEntities(
        externalAchievementEntityListTobeSaved, clientId);
  }

  /*
   * This method batches entities grouped by steps in bulkSaveExternalAchievements
   * It saves each batch to the ES
   * */
  private List<List<ExternalAchievement>> batchGroupedByExternalAchievementSettings(
      Map<String, Map<Date, Map<Date, Map<String, List<ExternalAchievement>>>>>
          groupedByExternalAchievementSettings,
      String clientId) {

    log.info(
        "In batchGroupedByExternalAchievementSettings for groupedByExternalAchievementSettings");
    List<List<ExternalAchievement>> listsOfExternalAchievementSettingsList = new ArrayList<>();
    for (String goalDefinitionCode : groupedByExternalAchievementSettings.keySet()) {
      Map<Date, Map<Date, Map<String, List<ExternalAchievement>>>> dateMapMap =
          groupedByExternalAchievementSettings.get(goalDefinitionCode);

      for (Date startDate : dateMapMap.keySet()) {
        Map<Date, Map<String, List<ExternalAchievement>>> dateListMap = dateMapMap.get(startDate);

        for (Date endDate : dateListMap.keySet()) {
          Map<String, List<ExternalAchievement>> stringListMap = dateListMap.get(endDate);

          for (String recurringFrequency : stringListMap.keySet()) {
            List<ExternalAchievement> externalAchievementSettingsList =
                stringListMap.get(recurringFrequency);
            saveExternalAchievementLists(
                goalDefinitionCode,
                recurringFrequency,
                startDate,
                endDate,
                externalAchievementSettingsList,
                clientId);
            listsOfExternalAchievementSettingsList.add(externalAchievementSettingsList);
          }
        }
      }
    }
    return listsOfExternalAchievementSettingsList;
  }
}
