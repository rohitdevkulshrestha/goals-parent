package com.getvymo.platform.goals.app.service;

import in.vymo.core.config.model.goals.GoalDefinition;
import java.util.List;

public class GoalDefinitionAndAssignments {
  private GoalDefinition goalDefinition;
  private List<GoalAssignment> goalAssignments;

  public GoalDefinition getGoalDefinition() {
    return goalDefinition;
  }

  public void setGoalDefinition(GoalDefinition goalDefinition) {
    this.goalDefinition = goalDefinition;
  }

  public List<GoalAssignment> getGoalAssignments() {
    return goalAssignments;
  }

  public void setGoalAssignments(List<GoalAssignment> goalAssignments) {
    this.goalAssignments = goalAssignments;
  }
}
