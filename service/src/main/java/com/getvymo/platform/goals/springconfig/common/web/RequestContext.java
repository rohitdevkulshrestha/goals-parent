package com.getvymo.platform.goals.springconfig.common.web;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RequestContext {
  private String corrid;
  private String idempotencyKey;
  private String traceId;
  private String userId;
  private String deviceId;
  private Integer version;
  private Boolean simulated = Boolean.FALSE;
}
