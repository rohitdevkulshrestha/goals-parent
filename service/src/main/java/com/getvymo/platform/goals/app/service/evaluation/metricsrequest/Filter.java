package com.getvymo.platform.goals.app.service.evaluation.metricsrequest;

/** Created by debashish on 19/08/19. */
public abstract class Filter {
  private String type; // match, not_match, in, not_in, user, date

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public Filter(String type) {
    this.type = type;
  }

  public Filter() {}
}
