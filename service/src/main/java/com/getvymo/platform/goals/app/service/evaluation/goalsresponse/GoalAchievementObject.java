package com.getvymo.platform.goals.app.service.evaluation.goalsresponse;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.getvymo.platform.goals.app.service.utils.GoalsUtils;
import java.util.Date;

public class GoalAchievementObject {
  private Double value;
  private Integer progressPercentage;
  private Integer expectedProgress;
  private String goalProgressType;
  private String dataType;

  @JsonFormat(shape = JsonFormat.Shape.NUMBER)
  private Date lastUpdatedDate;

  public GoalAchievementObject(
      Double value,
      Integer progressPercentage,
      Integer expectedProgress,
      String goalProgressType,
      String dataType,
      Date lastUpdatedDate) {
    this.value = value;
    this.progressPercentage = progressPercentage;
    this.expectedProgress = expectedProgress;
    this.goalProgressType = goalProgressType;
    this.dataType = dataType;
    this.lastUpdatedDate = lastUpdatedDate;
  }

  public GoalAchievementObject() {}

  public Date getLastUpdatedDate() {
    return lastUpdatedDate;
  }

  public void setLastUpdatedDate(Date lastUpdatedDate) {
    this.lastUpdatedDate = lastUpdatedDate;
  }

  public String getGoalProgressType() {
    return goalProgressType;
  }

  public void setGoalProgressType(String goalProgressType) {
    this.goalProgressType = goalProgressType;
  }

  public Integer getProgressPercentage() {
    return progressPercentage;
  }

  public void setProgressPercentage(Integer progressPercentage) {
    this.progressPercentage = progressPercentage;
  }

  public Integer getExpectedProgress() {
    return expectedProgress;
  }

  public void setExpectedProgress(Integer expectedProgress) {
    this.expectedProgress = expectedProgress;
  }

  public Double getValue() {
    return value;
  }

  public void setValue(Double value) {
    this.value = GoalsUtils.round(value, 2);
  }

  public String getDataType() {
    return dataType;
  }

  public void setDataType(String dataType) {
    this.dataType = dataType;
  }
}
