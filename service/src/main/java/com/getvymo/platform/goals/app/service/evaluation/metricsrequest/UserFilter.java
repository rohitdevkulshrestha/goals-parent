package com.getvymo.platform.goals.app.service.evaluation.metricsrequest;

import java.util.List;

/** Created by debashish on 19/08/19. */
public class UserFilter extends Filter {
  private UserFilterEnum filterType;
  private List<String> users;

  public UserFilter(String type, UserFilterEnum filterType, List<String> users) {
    super(type);
    this.filterType = filterType;
    this.users = users;
  }

  public UserFilter(UserFilterEnum filterType, List<String> users) {
    this.filterType = filterType;
    this.users = users;
  }
  /*
  {type:'user',dimension:'user',filter_type:"team", users : []}
  */

  public UserFilter() {}

  public UserFilterEnum getFilterType() {
    return filterType;
  }

  public void setFilterType(UserFilterEnum filterType) {
    this.filterType = filterType;
  }

  public List<String> getUsers() {
    return users;
  }

  public void setUsers(List<String> users) {
    this.users = users;
  }
}
