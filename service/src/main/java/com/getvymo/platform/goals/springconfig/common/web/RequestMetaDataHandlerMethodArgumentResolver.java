package com.getvymo.platform.goals.springconfig.common.web;

import com.getvymo.platform.goals.springconfig.common.exceptionhandler.exceptions.business.IncompletePayloadHeaderException;
import com.getvymo.platform.goals.springconfig.constants.AppConstants;
import java.net.URI;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;
import org.zalando.problem.Status;

@Log4j2
public class RequestMetaDataHandlerMethodArgumentResolver implements HandlerMethodArgumentResolver {

  @Override
  public boolean supportsParameter(MethodParameter parameter) {
    return RequestContext.class.equals(parameter.getParameterType());
  }

  @Override
  public Object resolveArgument(
      MethodParameter parameter,
      ModelAndViewContainer mavContainer,
      NativeWebRequest webRequest,
      WebDataBinderFactory binderFactory)
      throws Exception {
    try {
      log.trace("Inside RequestMetaDataHandlerMethodArgumentResolver");
      RequestContext requestContext =
          new RequestContext(
              ThreadContext.get(AppConstants.CORRID),
              ThreadContext.get(AppConstants.IDEMPOTENCYKEY),
              ThreadContext.get(AppConstants.TRACEID),
              ThreadContext.get(AppConstants.USERID),
              ThreadContext.get(AppConstants.DEVICEID),
              -1,
              Boolean.valueOf(ThreadContext.get(AppConstants.SIMULATED)));
      return requestContext;
    } catch (Exception e) {
      throw new IncompletePayloadHeaderException(
          URI.create("https://getvymo.com/incomplete-payload"),
          "Bad or incomplete Header data",
          Status.BAD_REQUEST,
          e.getMessage());
    }
  }
}
