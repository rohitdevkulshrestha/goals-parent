package com.getvymo.platform.goals.app.service.evaluation.metricsrequest;

public abstract class SortBy {
  private String type; // metric, date
  private boolean asc;

  public SortBy(String type, boolean asc) {
    this.type = type;
    this.asc = asc;
  }

  public SortBy() {}

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public boolean isAsc() {
    return asc;
  }

  public void setAsc(boolean asc) {
    this.asc = asc;
  }
}
