package com.getvymo.platform.goals.app.api.external;

import com.getvymo.platform.goals.app.common.models.GoalAssigneeSettings;
import com.getvymo.platform.goals.app.common.models.GoalAssigneeSettingsWithoutVersion;
import com.getvymo.platform.goals.app.common.models.GoalAssignmentOperationMetaData;
import com.getvymo.platform.goals.app.common.models.StandardListRequest;
import com.getvymo.platform.goals.app.common.models.StandardListResponse;
import com.getvymo.platform.goals.app.service.ActivateVersionResponse;
import com.getvymo.platform.goals.app.service.GoalAssignment;
import com.getvymo.platform.goals.app.service.GoalAssignmentOperationVersion;
import com.getvymo.platform.goals.app.service.goalsmgmt.IGoalManagementService;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/goals/v1/clients/{clientId}/goalassignments")
public class GoalAssigneesRestController {
  @Autowired private IGoalManagementService iGoalManagementService;

  @PostMapping(name = "/", consumes = "application/json")
  public ActivateVersionResponse setGoalForUsersWithoutVersion(
      @Valid @RequestBody GoalAssigneeSettingsWithoutVersion goalAssigneeSettingsWithoutVersion,
      @Valid @PathVariable String clientId,
      @Valid @RequestParam List<String> userIds,
      @RequestParam String goalDefinitionCode,
      @RequestParam String userUpdating) {
    return iGoalManagementService.setGoalForUsersWithoutVersion(
        clientId, userIds, goalDefinitionCode, goalAssigneeSettingsWithoutVersion, userUpdating);
  }

  @PostMapping(value = "/with_version", consumes = "application/json")
  public List<GoalAssignment> setGoalForUsersWithVersion(
      @Valid @RequestBody GoalAssigneeSettings goalAssigneeSettings,
      @Valid @PathVariable String clientId,
      @Valid @RequestParam List<String> userIds,
      @RequestParam String goalDefinitionCode,
      @RequestParam String userUpdating,
      @RequestParam String version) {
    return iGoalManagementService.setGoalForUsersWithVersion(
        clientId, userIds, goalDefinitionCode, goalAssigneeSettings, version, userUpdating);
  }

  @PostMapping(value = "/bulk", consumes = "application/json")
  public List<GoalAssignment> bulkSetGoalForUsersWithVersion(
      @Valid @RequestBody StandardListRequest<GoalAssigneeSettings> goalAssigneeSettingsList,
      @Valid @PathVariable String clientId,
      @RequestParam String userUpdating,
      @RequestParam String version) {
    return iGoalManagementService.bulkSetGoalForUsersWithVersion(
        clientId, goalAssigneeSettingsList, version, userUpdating);
  }

  @GetMapping(path = "/set_version", consumes = "application/json")
  public ActivateVersionResponse setCurrentVersionForGoalAssignments(
      @RequestParam String version, @Valid @PathVariable String clientId) {
    return iGoalManagementService.activateGoalAssignmentOperationVersion(version, clientId);
  }

  @Deprecated
  @PostMapping(path = "/{goalInstanceId}", consumes = "application/json")
  public GoalAssignment editGoalInstance(
      @Valid @RequestBody GoalAssigneeSettings goalAssigneeSettings,
      @Valid @PathVariable String clientId,
      @Valid @PathVariable String goalInstanceId) {
    return iGoalManagementService.editGoalInstance(goalInstanceId, clientId, goalAssigneeSettings);
  }

  @DeleteMapping(name = "/", consumes = "application/json")
  public List<GoalAssignment> disableGoalForUsers(
      @Valid @PathVariable String clientId,
      @Valid @RequestParam List<String> userIds,
      @Valid @RequestParam String goalDefinitionCode,
      Date startDate,
      Date endDate) {
    return iGoalManagementService.disableGoalForUsers(
        clientId, userIds, goalDefinitionCode, startDate, endDate);
  }

  @GetMapping(path = "/users", consumes = "application/json")
  public StandardListResponse getAllGoalAssignments(
      @Valid @PathVariable String clientId,
      @Valid @RequestParam String userIds,
      @RequestParam String goalDefinitionId,
      @RequestParam(required = false) Date startDate,
      @RequestParam(required = false) Date endDate,
      @RequestParam(value = "pageNumber", required = false, defaultValue = "0") int pageNumber,
      @RequestParam(value = "pageSize", required = false, defaultValue = "20") int pageSize) {
    List<String> userIdsArray = Arrays.asList(userIds.split(","));
    return iGoalManagementService.getAllGoalAssignments(
        clientId, goalDefinitionId, userIdsArray, startDate, endDate, pageNumber, pageSize);
  }

  @GetMapping(path = "/users/{userId}", consumes = "application/json")
  public StandardListResponse getAllGoalAssignmentsForUser(
      @Valid @PathVariable String clientId,
      @Valid @PathVariable String userId,
      @RequestParam(value = "pageNumber", required = false, defaultValue = "0") int pageNumber,
      @RequestParam(value = "pageSize", required = false, defaultValue = "20") int pageSize) {
    return iGoalManagementService.getAllGoalAssignmentsForUser(
        clientId, userId, pageNumber, pageSize);
  }

  @PostMapping(path = "/version", consumes = "application/json")
  public GoalAssignmentOperationVersion createNewVersion(
      @Valid @RequestBody GoalAssignmentOperationMetaData auditInfo) {
    return iGoalManagementService.createGoalAssignmentOperationVersion(auditInfo);
  }
}
