package com.getvymo.platform.goals.app.common.models;

public class GoalAssignmentOperationMetaData {
  private String version;

  private GoalEntryType entryType;

  private String description;

  private String updater;

  public GoalAssignmentOperationMetaData(
      String version, GoalEntryType entryType, String description, String updater) {
    this.version = version;
    this.entryType = entryType;
    this.description = description;
    this.updater = updater;
  }

  public GoalAssignmentOperationMetaData() {}

  public String getVersion() {
    return version;
  }

  public void setVersion(String version) {
    this.version = version;
  }

  public GoalEntryType getEntryType() {
    return entryType;
  }

  public void setEntryType(GoalEntryType entryType) {
    this.entryType = entryType;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getUpdater() {
    return updater;
  }

  public void setUpdater(String updater) {
    this.updater = updater;
  }

  @Override
  public String toString() {
    return "GoalAssignmentOperationMetaData{"
        + "version='"
        + version
        + '\''
        + ", entryType="
        + entryType
        + ", description='"
        + description
        + '\''
        + ", updater='"
        + updater
        + '\''
        + '}';
  }
}
