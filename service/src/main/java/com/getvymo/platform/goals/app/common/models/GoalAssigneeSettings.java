package com.getvymo.platform.goals.app.common.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.getvymo.platform.goals.app.service.utils.GoalsUtils;
import java.util.Date;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(
    use = JsonTypeInfo.Id.NAME,
    include = JsonTypeInfo.As.PROPERTY,
    property = "goal_occurrence",
    visible = true)
@JsonSubTypes({
  @JsonSubTypes.Type(value = OneTimeGoalAssigneeSettings.class, name = "one_time"),
  @JsonSubTypes.Type(value = RecurringGoalAssigneeSettings.class, name = "recurring")
})
public abstract class GoalAssigneeSettings {

  @Enumerated(EnumType.STRING)
  private GoalOccurrence goalOccurrence;

  private Double target;

  private Integer row;

  @JsonFormat(shape = JsonFormat.Shape.NUMBER)
  private Date startDate;

  @JsonFormat(shape = JsonFormat.Shape.NUMBER)
  private Date endDate;

  private Boolean deleted;

  private String userId;

  private String goalDefinitionCode;

  public GoalAssigneeSettings() {}

  public GoalAssigneeSettings(
      GoalOccurrence goalOccurrence,
      Double target,
      Date startDate,
      Date endDate,
      Boolean deleted,
      String userId,
      String goalDefinitionCode) {
    super();
    this.goalOccurrence = goalOccurrence;
    this.target = target;
    this.startDate = startDate;
    this.endDate = endDate;
    this.deleted = deleted;
    this.userId = userId;
    this.goalDefinitionCode = goalDefinitionCode;
  }

  public Double getTarget() {
    return target;
  }

  public void setTarget(Double target) {
    this.target = GoalsUtils.round(target, 2);
  }

  public Date getStartDate() {
    return startDate;
  }

  public void setStartDate(Date startDate) {
    this.startDate = startDate;
  }

  public Date getEndDate() {
    return endDate;
  }

  public void setEndDate(Date endDate) {
    this.endDate = endDate;
  }

  public GoalOccurrence getGoalOccurrence() {
    return goalOccurrence;
  }

  public void setGoalOccurrence(GoalOccurrence goalOccurrence) {
    this.goalOccurrence = goalOccurrence;
  }

  public Boolean getDeleted() {
    return deleted;
  }

  public void setDeleted(Boolean deleted) {
    this.deleted = deleted;
  }

  @Override
  public String toString() {
    return "GoalAssigneeSettings{"
        + "goalOccurrence="
        + goalOccurrence
        + ", target="
        + target
        + ", startDate="
        + startDate
        + ", endDate="
        + endDate
        + ", deleted="
        + deleted
        + '}';
  }

  public String getGoalDefinitionCode() {
    return goalDefinitionCode;
  }

  public void setGoalDefinitionCode(String goalDefinitionCode) {
    this.goalDefinitionCode = goalDefinitionCode;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public Integer getRow() {
    return row;
  }

  public void setRow(Integer row) {
    this.row = row;
  }
}
