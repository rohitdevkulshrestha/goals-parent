package com.getvymo.platform.goals.app.service.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.getvymo.platform.goals.app.common.models.ComputedGoalDefinitionSettings;
import com.getvymo.platform.goals.app.common.models.ExternalGoalDefinitionSettings;
import com.getvymo.platform.goals.app.common.models.GoalAssigneeSettings;
import com.getvymo.platform.goals.app.common.models.GoalDefinitionSettings;
import com.getvymo.platform.goals.app.common.models.GoalOccurrence;
import com.getvymo.platform.goals.app.common.models.RecurringGoalAssigneeSettings;
import com.getvymo.platform.goals.app.common.models.VymoMetricGoalDefinitionSettings;
import com.getvymo.platform.goals.app.errors.GoalManagementServiceException;
import com.getvymo.platform.goals.app.persistence.dao.GoalAssigneeSettingsVersionsRepository;
import com.getvymo.platform.goals.app.persistence.dao.GoalAssignmentOperationVersionsRepository;
import com.getvymo.platform.goals.app.persistence.entities.ExternalAchievementEntity;
import com.getvymo.platform.goals.app.persistence.entities.GoalAssigneeSettingsVersionsEntity;
import com.getvymo.platform.goals.app.persistence.entities.GoalAssigneesEntity;
import com.getvymo.platform.goals.app.persistence.entities.GoalAssignmentOperationVersionsEntity;
import com.getvymo.platform.goals.app.persistence.entities.GoalInstanceEntity;
import com.getvymo.platform.goals.app.service.BulkGoalAssignment;
import com.getvymo.platform.goals.app.service.ExternalAchievementApiObject;
import com.getvymo.platform.goals.app.service.GoalAssignment;
import com.getvymo.platform.goals.app.service.UserGoalAssignment;
import com.getvymo.platform.goals.app.service.evaluation.goalsresponse.GoalAchievement;
import com.getvymo.platform.goals.app.service.evaluation.goalsresponse.GoalAchievementObject;
import com.getvymo.platform.goals.app.service.evaluation.goalsresponse.GoalsGroupedByDate;
import com.getvymo.platform.goals.app.service.evaluation.goalsresponse.GoalsGroupedByUser;
import com.getvymo.platform.goals.app.service.evaluation.goalsresponse.UserObject;
import com.getvymo.platform.goals.app.service.evaluation.metricsrequest.Filter;
import com.getvymo.platform.goals.app.service.evaluation.metricsrequest.IncludeDimensionFilter;
import com.getvymo.platform.goals.app.service.evaluation.metricsrequest.MatchDimensionFilter;
import com.getvymo.platform.goals.app.service.evaluation.metricsresponse.GroupedRecordSet;
import com.getvymo.platform.goals.app.service.evaluation.metricsresponse.MetricAchievementObject;
import com.getvymo.platform.goals.app.service.evaluation.metricsresponse.MetricsResponse;
import com.getvymo.platform.goals.app.service.evaluation.metricsresponse.MetricsResultObject;
import com.getvymo.platform.goals.app.utils.configclient.ConfigClient;
import com.getvymo.platform.goals.app.utils.configclient.ConfigUtil;
import com.getvymo.platform.goals.app.utils.configclient.ConfigUtils;
import in.vymo.configuration.api.ConfigurationClient;
import in.vymo.core.config.model.core.DATE_GRANULARITY;
import in.vymo.core.config.model.core.dimensionV2.ActivityAttributeDimension;
import in.vymo.core.config.model.core.dimensionV2.ActivityDimension;
import in.vymo.core.config.model.core.dimensionV2.ActivityVOAttributeDimension;
import in.vymo.core.config.model.core.dimensionV2.AggregateActivityAttributeDimension;
import in.vymo.core.config.model.core.dimensionV2.CalendarDimension;
import in.vymo.core.config.model.core.dimensionV2.Dimension;
import in.vymo.core.config.model.core.dimensionV2.UserDimension;
import in.vymo.core.config.model.core.dimensionV2.VOAttributeDimension;
import in.vymo.core.config.model.core.dimensionV2.VODimension;
import in.vymo.core.config.model.core.metric.Metric;
import in.vymo.core.config.model.goals.ComputedGoalDefinition;
import in.vymo.core.config.model.goals.GoalDefinition;
import in.vymo.core.config.model.goals.GoalMetricFilter;
import in.vymo.core.config.model.goals.GoalProgressTypeSettingMapping;
import in.vymo.core.config.model.goals.GoalType;
import in.vymo.core.config.model.goals.VymoMetricGoalDefinition;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.temporal.ChronoUnit;
import java.time.temporal.IsoFields;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TimeZone;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import javax.transaction.Transactional;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.mariuszgromada.math.mxparser.Argument;
import org.mariuszgromada.math.mxparser.Expression;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Log4j2
public class GoalsUtils {

  private static final int SHORT_STRING_LENGTH = 10;
  ConfigUtil configUtil = new ConfigUtil();
  @Autowired private GoalAssigneeSettingsVersionsRepository goalAssigneeSettingsVersionsRepository;

  @Autowired
  private GoalAssignmentOperationVersionsRepository goalAssignmentOperationVersionsRepository;

  @Autowired private ConfigUtils configUtils;
  private ConfigClient configClient;

  private ConfigurationClient client = configClient.client();

  private Map<String, List<GoalProgressTypeSettingMapping>>
      clientAndSortedGoalProgressTypeMappings = new HashMap<>();

  public static String generateRandomStringId() {
    return UUID.randomUUID().toString();
  }

  public static String generateRandomShortString() {
    return UUID.randomUUID().toString().replaceAll("-", "").substring(0, SHORT_STRING_LENGTH);
  }

  public static int getTimezoneOffset(Date date, TimeZone tz) {
    int offsetInMillis = tz.getOffset(date.getTime());
    return offsetInMillis;
  }

  public Date getOffsetCorrectedDate(Date date, TimeZone timeZone) {
    log.info("In getOffsetCorrectedDate, date {} & timezone is {}", date, timeZone);
    log.debug("Timezone is {} & name is {}", timeZone.toString(), timeZone.getDisplayName());
    int timezoneOffsetInMillis = getTimezoneOffset(date, timeZone);
    return new Date(date.getTime() - (long) timezoneOffsetInMillis);
  }

  public List<Filter> getGoalMetricFiltersFromEntities(
      List<GoalMetricFilter> goalMetricFilterEntities, String clientId) {
    log.info(
        "Inside getGoalMetricFiltersFromEntities for {} goalMetricFilterEntities",
        goalMetricFilterEntities.size());
    List<Filter> metricDimensionFilters = new ArrayList<>();
    if (goalMetricFilterEntities != null && !goalMetricFilterEntities.isEmpty()) {
      for (GoalMetricFilter goalMetricFilterEntity : goalMetricFilterEntities) {
        List<String> filterValues = goalMetricFilterEntity.getFilterValues();
        if (filterValues.isEmpty()) {
          return metricDimensionFilters;
        }
        String dimensionCode = goalMetricFilterEntity.getDimension();
        Dimension dimensionInfo = configUtil.getDimensionInfo(dimensionCode, clientId);
        if (dimensionInfo == null) {
          log.error("dimensionInfo is null for " + dimensionCode);
          return metricDimensionFilters;
        }
        String dimension = getDimensionType(dimensionInfo);
        String attribute = getAttributeByDimension(clientId, dimensionInfo);
        if (filterValues.size() > 1) {
          IncludeDimensionFilter includeDimensionFilter =
              new IncludeDimensionFilter("include", dimension, attribute, filterValues);
          metricDimensionFilters.add(includeDimensionFilter);
          log.info(
              "Created includeDimensionFilter for {} dimension {} filterValuesArray {} attribute",
              dimension,
              filterValues,
              attribute);
        } else {
          MatchDimensionFilter matchDimensionFilter =
              new MatchDimensionFilter("match", dimension, filterValues.get(0), attribute);
          metricDimensionFilters.add(matchDimensionFilter);
        }
        log.info(
            "Created matchDimensionFilter for {} dimension {} value, {} attribute",
            dimension,
            filterValues,
            attribute);
      }
    }

    log.info("Returning {} metricDimensionFilters", metricDimensionFilters.size());
    return metricDimensionFilters;
  }

  private String getAttributeByDimension(String clientId, Dimension dimensionInfo) {
    String type = dimensionInfo.getType();
    switch (type) {
      case "activity":
        {
          ActivityDimension activityDimension = (ActivityDimension) dimensionInfo;
          return null;
        }
      case "activity-vo-attribute":
        {
          ActivityVOAttributeDimension activityVOAttributeDimension =
              (ActivityVOAttributeDimension) dimensionInfo;
          return configUtil.getAttributeFromField(
              clientId, activityVOAttributeDimension.getField());
        }
      case "activity-attribute":
        {
          ActivityAttributeDimension activityAttributeDimension =
              (ActivityAttributeDimension) dimensionInfo;
          return configUtil.getAttributeFromField(clientId, activityAttributeDimension.getField());
        }
      case "vo":
        {
          VODimension voDimension = (VODimension) dimensionInfo;
          return null;
        }
      case "vo-attribute":
        {
          VOAttributeDimension voAttributeDimension = (VOAttributeDimension) dimensionInfo;
          return configUtil.getAttributeFromField(clientId, voAttributeDimension.getField());
        }
      case "aggregate-activity-attribute":
        {
          AggregateActivityAttributeDimension aggregateActivityAttributeDimension =
              (AggregateActivityAttributeDimension) dimensionInfo;
          return configUtil.getAttributeFromField(
              clientId, aggregateActivityAttributeDimension.getField());
        }
    }
    return dimensionInfo.getCode();
  }

  private String getDimensionType(Dimension dimensionInfo) {
    String type = dimensionInfo.getType();
    switch (type) {
      case "activity":
        {
          ActivityDimension activityDimension = (ActivityDimension) dimensionInfo;
          return activityDimension.getDimensionType();
        }
      case "activity-vo-attribute":
        {
          ActivityVOAttributeDimension activityVOAttributeDimension =
              (ActivityVOAttributeDimension) dimensionInfo;
          return "task_vo_attribute";
        }
      case "activity-attribute":
        {
          ActivityAttributeDimension activityAttributeDimension =
              (ActivityAttributeDimension) dimensionInfo;
          return "task_attribute";
        }
      case "vo":
        {
          VODimension voDimension = (VODimension) dimensionInfo;
          return voDimension.getDimensionType();
        }
      case "vo-attribute":
        {
          VOAttributeDimension voAttributeDimension = (VOAttributeDimension) dimensionInfo;
          return "vo_attribute";
        }
      case "aggregate-activity-attribute":
        {
          AggregateActivityAttributeDimension aggregateActivityAttributeDimension =
              (AggregateActivityAttributeDimension) dimensionInfo;
          return "aggregate_activity_attribute";
        }
      case "user":
        {
          UserDimension userDimension = (UserDimension) dimensionInfo;
          return userDimension.getDimensionType();
        }
      case "calendar":
        {
          CalendarDimension calendarDimension = (CalendarDimension) dimensionInfo;
          return calendarDimension.getDimensionType();
        }
    }
    return type;
  }

  public GoalAchievement getGoalAchievementFromResponseAndAssignment(
      List<GoalInstanceEntity> goalInstanceEntities,
      MetricsResponse metricsResponse,
      String goalAssigneeId,
      String userId,
      String clientId,
      List<String> progress,
      GoalInstanceEntity managerSelfGoalInstanceEntity,
      MetricAchievementObject managerSelfAchievement) {
    log.info("In getGoalAchievementFromResponseAndAssignment");
    Map<String, MetricAchievementObject> userAndAchievementMap = new HashMap<>();
    // goal definition will be same for all the goal instance entities
    GoalDefinition goalDefinition =
        configUtil.getGoalDefinitionByCode(
            clientId, goalInstanceEntities.get(0).getGoalDefinitionCode());
    try {
      if (Objects.isNull(metricsResponse) || Objects.isNull(metricsResponse.getResult())) {
        log.error("metricsResponse null for " + goalDefinition.getCode());
        throw new GoalManagementServiceException(
            "metricsResponse null for " + goalDefinition.getCode());
      }

      List<GroupedRecordSet> groupedRecordSets =
          metricsResponse.getResult().getGrouped_record_sets();
      // incase of grouped result set
      if (groupedRecordSets != null) {
        log.debug("Received groupedRecordSets of size -> {}", groupedRecordSets.size());
        log.debug(
            "Creating userAndAchievementMap for each subordinate userId and achievement object");
        log.debug("Goal definition metric is {}", goalDefinition.getMetric());
        for (GroupedRecordSet groupedRecordSet : groupedRecordSets) {
          String subordinateUserId = groupedRecordSet.getGroup_by_key().getCode();
          List<MetricAchievementObject> metricAchievementObjects = groupedRecordSet.getMetrics();
          for (MetricAchievementObject metricAchievementObject : metricAchievementObjects) {

            if (metricAchievementObject.getMetric().equals(goalDefinition.getMetric())) {
              userAndAchievementMap.put(subordinateUserId, metricAchievementObject);
            } else {
              log.debug(
                  "Not added {} to userAndAchievementMap as goalDefintionMetric {} is not same as  with "
                      + "metricAchievementObject metric {} ",
                  subordinateUserId,
                  goalDefinition.getMetric(),
                  metricAchievementObject.getMetric());
            }
          }
        }
      }

      log.debug("Getting metric achievement object for current user");
      for (MetricAchievementObject metricAchievementObject :
          metricsResponse.getResult().getMetrics()) {
        if (metricAchievementObject.getMetric().equals(goalDefinition.getMetric())) {
          userAndAchievementMap.put(userId, metricAchievementObject);
        }
      }

      GoalAchievement goalAchievement = new GoalAchievement();
      // Get current users' goal instance entity & corresponding achievement value
      GoalInstanceEntity usersGoalInstanceEntity =
          getUsersGoalInstanceEntity(goalInstanceEntities, userId);
      MetricAchievementObject usersGoalAchievementObject = userAndAchievementMap.get(userId);
      GoalAchievementObject userGoalAchievementObject =
          getGoalAchievementObjectFromGoalInstanceEntityAndAchievementValue(
              usersGoalInstanceEntity, usersGoalAchievementObject, progress);

      if ((Objects.nonNull(usersGoalAchievementObject)
              && Objects.nonNull(userGoalAchievementObject))
          || (Objects.nonNull(groupedRecordSets) && groupedRecordSets.size() > 0)) {
        goalAchievement.setGoalAssigneeId(goalAssigneeId);
        goalAchievement.setAchievement(userGoalAchievementObject);
      } else {
        log.error("Null userGoalAchievementObject received");
      }

      goalAchievement.setGoalAssigneeSettings(
          getGoalAssigneeSettingsFromGoalInstanceEntity(usersGoalInstanceEntity));
      // Goal definition would be same for all users
      goalAchievement.setGoalDefinitionCode(goalDefinition.getCode());
      goalAchievement.setGoalDefinitionSettings(
          getGoalDefinitionSettingsFromGoalDefinitionEntity(goalDefinition, clientId));
      goalAchievement.setUser(getUserObjectFromUserId(userId));
      goalAchievement.setStartDate(usersGoalInstanceEntity.getStartDate());
      goalAchievement.setEndDate(usersGoalInstanceEntity.getEndDate());

      // Get user subordinates goal achievement values grouped by user
      if (groupedRecordSets != null) {
        log.info("Setting user grouped by results");
        List<GoalsGroupedByUser> goalsGroupByResults =
            getGoalsGroupedByUserFromGoalInstanceEntitiesAndAchievementMap(
                goalInstanceEntities, userAndAchievementMap, progress);
        goalAchievement.setGroupedByUser(goalsGroupByResults);
      }
      if (Objects.nonNull(managerSelfAchievement)
          && Objects.nonNull(managerSelfGoalInstanceEntity)) {
        GoalsGroupedByUser managerSelfAchievementObject =
            getManagerSelfAchievement(
                managerSelfGoalInstanceEntity, managerSelfAchievement, progress);
        goalAchievement.setManagerSelfAchievement(managerSelfAchievementObject);
      }
      log.info("Returning goalAchievement");
      //        prettyPrintStringFromJSON(goalAchievement, "Returned goal achievement");
      return goalAchievement;
    } catch (Exception e) {
      log.error(
          "Exception while getGoalAchievementFromResponseAndAssignment " + goalDefinition.getCode(),
          e);
      throw new GoalManagementServiceException(
          "Exception while getGoalAchievementFromResponseAndAssignment "
              + goalDefinition.getCode());
    }
  }

  private GoalsGroupedByUser getManagerSelfAchievement(
      GoalInstanceEntity goalInstanceEntity,
      MetricAchievementObject metricAchievementObject,
      List<String> progress) {
    GoalsGroupedByUser goalsGroupByResult = new GoalsGroupedByUser();
    goalsGroupByResult.setGoalAssigneeSettings(
        getGoalAssigneeSettingsFromGoalInstanceEntity(goalInstanceEntity));

    if (metricAchievementObject != null) {
      GoalAchievementObject goalAchievementObject =
          getGoalAchievementObjectFromGoalInstanceEntityAndAchievementValue(
              goalInstanceEntity, metricAchievementObject, progress);
      if (goalAchievementObject == null) {
        return null;
      }
      goalsGroupByResult.setAchievement(goalAchievementObject);
    }
    goalsGroupByResult.setUser(getUserObjectFromUserId(goalInstanceEntity.getUserId()));
    GoalAssigneesEntity goalAssigneesEntity = goalInstanceEntity.getGoalAssignee();
    if (null != goalAssigneesEntity) {
      goalsGroupByResult.setGoalAssigneeId(goalAssigneesEntity.getId());
    }
    goalsGroupByResult.setStartDate(goalInstanceEntity.getStartDate());
    goalsGroupByResult.setEndDate(goalInstanceEntity.getEndDate());
    return goalsGroupByResult;
  }

  public GoalAchievement getGoalAchievementFromResponseAndAssignmentForGroupedByDate(
      GoalInstanceEntity goalInstanceEntity,
      MetricsResponse metricsResponse,
      String goalAssigneeId,
      String userId,
      GoalDefinition goalDefinition,
      String clientId,
      List<String> progress) {
    log.info(
        "In getGoalAchievementFromResponseAndAssignment for user {} and goalAssigneeId {}",
        userId,
        goalAssigneeId);
    // goal definition will be same for all the goal instance entities
    List<GroupedRecordSet> groupedRecordSets = metricsResponse.getResult().getGrouped_record_sets();
    List<GoalsGroupedByDate> goalsGroupedByDates = new ArrayList<>();
    GoalAchievement goalAchievement = new GoalAchievement();
    goalAchievement.setStartDate(goalInstanceEntity.getStartDate());
    goalAchievement.setEndDate(goalInstanceEntity.getEndDate());
    MetricAchievementObject totalAchievedMetricAchievementObject =
        metricsResponse.getResult().getMetrics().stream()
            .filter(
                metricAchievementObject ->
                    metricAchievementObject.getMetric().equals(goalDefinition.getMetric()))
            .findAny()
            .orElse(null);
    // incase of grouped result set
    // disbaling goals for computed and vo_total metric temporarily. Will fix ASAP
    if (!StringUtils.equals(goalDefinition.getMetric(), "computed")
        && !StringUtils.equals(goalDefinition.getMetric(), "vo_total")
        && (groupedRecordSets != null && !groupedRecordSets.isEmpty())) {
      log.info("Received groupedRecordSets of size -> {}", groupedRecordSets.size());
      log.info("Creating dateAndAchievementMap for each subordinate userId and achievement object");
      for (GroupedRecordSet groupedRecordSet : groupedRecordSets) {
        String recordedDate = groupedRecordSet.getGroup_by_key().getCode();
        List<MetricAchievementObject> metricAchievementObjects = groupedRecordSet.getMetrics();
        for (MetricAchievementObject metricAchievementObject : metricAchievementObjects) {
          if (metricAchievementObject.getMetric().equals(goalDefinition.getMetric())) {
            GoalsGroupedByDate eachGroupedByDateObject = new GoalsGroupedByDate();
            eachGroupedByDateObject.setDate(recordedDate);
            eachGroupedByDateObject.setValue(metricAchievementObject.getValue());
            eachGroupedByDateObject.setDataType(metricAchievementObject.getData_type());
            goalsGroupedByDates.add(eachGroupedByDateObject);
          }
        }
      }
    }

    // Total achieved value
    if (totalAchievedMetricAchievementObject != null) {
      goalAchievement.setAchievement(
          getGoalAchievementObjectFromGoalInstanceEntityAndAchievementValue(
              goalInstanceEntity, totalAchievedMetricAchievementObject, progress));
    }

    goalAchievement.setGoalAssigneeId(goalAssigneeId);
    goalAchievement.setGoalAssigneeSettings(
        getGoalAssigneeSettingsFromGoalInstanceEntity(goalInstanceEntity));

    // Goal definition would be same for all users
    goalAchievement.setGoalDefinitionCode(goalDefinition.getCode());
    goalAchievement.setGoalDefinitionSettings(
        getGoalDefinitionSettingsFromGoalDefinitionEntity(goalDefinition, clientId));
    goalAchievement.setUser(getUserObjectFromUserId(userId));
    // Get user subordinates goal achievement values grouped by user
    goalAchievement.setTrend(goalsGroupedByDates);
    log.info("Returning goalAchievement");
    //        prettyPrintStringFromJSON(goalAchievement, " goal Achievement from
    // getGoalAchievementFromResponseAndAssignmentForGroupedByDate");
    return goalAchievement;
  }

  public List<GoalsGroupedByUser> getGoalsGroupedByUserFromGoalInstanceEntitiesAndAchievementMap(
      List<GoalInstanceEntity> goalInstanceEntities,
      Map<String, MetricAchievementObject> userAndAchievementMap,
      List<String> progress) {
    log.info(
        "Getting getGoalsGroupedByUserFromGoalInstanceEntitiesAndAchievementMap for {} goalInstanceEntities and {} size "
            + "userAndAchievementMap",
        goalInstanceEntities.size(),
        userAndAchievementMap.size());
    List<GoalsGroupedByUser> goalsGroupByResults = new ArrayList<>();
    for (GoalInstanceEntity goalInstanceEntity : goalInstanceEntities) {
      GoalsGroupedByUser goalsGroupByResult = new GoalsGroupedByUser();
      goalsGroupByResult.setGoalAssigneeSettings(
          getGoalAssigneeSettingsFromGoalInstanceEntity(goalInstanceEntity));
      MetricAchievementObject userMetricAchievementObject =
          userAndAchievementMap.get(goalInstanceEntity.getUserId());
      if (userMetricAchievementObject == null) {
        continue;
      }
      if (userMetricAchievementObject != null) {
        Double achievementValue = userMetricAchievementObject.getValue();
        GoalAchievementObject goalAchievementObject =
            getGoalAchievementObjectFromGoalInstanceEntityAndAchievementValue(
                goalInstanceEntity, userMetricAchievementObject, progress);
        if (goalAchievementObject == null) {
          continue;
        }
        goalsGroupByResult.setAchievement(goalAchievementObject);
      }
      goalsGroupByResult.setUser(getUserObjectFromUserId(goalInstanceEntity.getUserId()));
      GoalAssigneesEntity goalAssigneesEntity = goalInstanceEntity.getGoalAssignee();
      if (null != goalAssigneesEntity) {
        goalsGroupByResult.setGoalAssigneeId(goalAssigneesEntity.getId());
      }
      goalsGroupByResult.setStartDate(goalInstanceEntity.getStartDate());
      goalsGroupByResult.setEndDate(goalInstanceEntity.getEndDate());
      goalsGroupByResults.add(goalsGroupByResult);
    }
    log.info(
        "Returing from getGoalsGroupedByUserFromGoalInstanceEntitiesAndAchievementMap with {} goalsGroupByResults",
        goalsGroupByResults.size());
    return goalsGroupByResults;
  }

  public GoalInstanceEntity getUsersGoalInstanceEntity(
      List<GoalInstanceEntity> goalInstanceEntities, String userId) {
    log.info(
        "Getting getUsersGoalInstanceEntity from list of goalInstanceEntities for userId -> {}",
        userId);
    for (GoalInstanceEntity goalInstanceEntity : goalInstanceEntities) {
      if (goalInstanceEntity.getUserId().equals(userId)) {
        log.info("Returning found goalInstanceEntity in getUsersGoalInstanceEntity");
        return goalInstanceEntity;
      }
    }
    throw new GoalManagementServiceException(
        "Not able to find corresponding goalInstance for userID " + userId);
  }

  public UserObject getUserObjectFromUserId(String userId) {
    log.debug("Getting getUserObjectFromUserId for userId -> {} ", userId);
    UserObject userObject = new UserObject(userId);
    return userObject;
  }

  @Transactional
  public GoalAssigneeSettings getGoalAssigneeSettingsFromGoalInstanceEntity(
      GoalInstanceEntity goalInstanceEntity) {
    log.info("Getting getGoalAssigneeSettingsFromGoalInstanceEntity");
    if (null == goalInstanceEntity) {
      log.warn(
          "goalInstanceEntity is null in getGoalAssigneeSettingsFromGoalInstanceEntity. So returning");
      return new RecurringGoalAssigneeSettings() {};
    }
    GoalAssigneesEntity goalAssigneesEntity = goalInstanceEntity.getGoalAssignee();
    GoalAssigneeSettings goalAssigneeSettings = new RecurringGoalAssigneeSettings() {};

    if (null != goalAssigneesEntity) {
      log.info(
          "Getting goalAssigneeSettingsVersionsEntity for goalAssigneeId "
              + goalAssigneesEntity.getId()
              + " for version "
              + goalAssigneesEntity.getCurrentVersion());
      GoalAssigneeSettingsVersionsEntity goalAssigneeSettingsVersionsEntity =
          goalAssigneeSettingsVersionsRepository.findByGoalAssigneesEntityAndVersion(
              goalAssigneesEntity, goalAssigneesEntity.getCurrentVersion());
      //            prettyPrintStringFromJSON(goalAssigneeSettingsVersionsEntity, "Got
      // goalAssigneeSettingsVersionsEntity for goalAssigneeId " + goalAssigneesEntity.getId() + "
      // for version " + goalAssigneesEntity.getCurrentVersion());
      if (null != goalAssigneeSettingsVersionsEntity) {
        log.info("populating goalAssigneeSettings from goalAssigneeSettingsVersionsEntity");
        goalAssigneeSettings.setStartDate(goalAssigneeSettingsVersionsEntity.getStartDate());
        goalAssigneeSettings.setEndDate(goalAssigneeSettingsVersionsEntity.getEndDate());
        goalAssigneeSettings.setTarget(goalInstanceEntity.getTarget());
        goalAssigneeSettings.setGoalDefinitionCode(goalInstanceEntity.getGoalDefinitionCode());
        goalAssigneeSettings.setUserId(goalAssigneesEntity.getUserId());

        RecurringGoalAssigneeSettings recurringGoalAssigneeSettings = null;

        GoalOccurrence goalOccurrence =
            goalAssigneeSettingsVersionsEntity.getRecurringFrequency() == null
                ? GoalOccurrence.one_time
                : GoalOccurrence.recurring;

        goalAssigneeSettings.setGoalOccurrence(goalOccurrence);

        if (goalInstanceEntity.isRecurring()) {
          recurringGoalAssigneeSettings = (RecurringGoalAssigneeSettings) goalAssigneeSettings;
          recurringGoalAssigneeSettings.setRecurringFrequency(
              goalAssigneeSettingsVersionsEntity.getRecurringFrequency());
          log.info(
              "Returing recurringGoalAssigneeSettings for getGoalAssigneeSettingsFromGoalInstanceEntity");
          return recurringGoalAssigneeSettings;
        }
        log.info(
            "Returing oneTimeGoalAssigneeSettings for getGoalAssigneeSettingsFromGoalInstanceEntity");
      }
    }
    return goalAssigneeSettings;
  }

  public GoalAssigneeSettings getGoalAssigneeSettingsFromGoalAssigneeSettingsVersionEntity(
      GoalAssigneeSettingsVersionsEntity goalAssigneeSettingsVersionsEntity) {

    log.info("Getting getGoalAssigneeSettingsFromGoalAssigneeSettingsVersionEntity");
    if (null == goalAssigneeSettingsVersionsEntity) {
      log.warn(
          "goalAssigneeSettingsVersionsEntity is null in getGoalAssigneeSettingsFromGoalInstanceEntity. So returning");
      return new RecurringGoalAssigneeSettings() {};
    }
    GoalAssigneesEntity goalAssigneesEntity =
        goalAssigneeSettingsVersionsEntity.getGoalAssigneesEntity();
    GoalAssigneeSettings goalAssigneeSettings = new RecurringGoalAssigneeSettings() {};

    if (null != goalAssigneesEntity) {

      goalAssigneeSettings.setStartDate(goalAssigneeSettingsVersionsEntity.getStartDate());
      goalAssigneeSettings.setEndDate(goalAssigneeSettingsVersionsEntity.getEndDate());
      goalAssigneeSettings.setTarget(goalAssigneeSettingsVersionsEntity.getTarget());
      goalAssigneeSettings.setUserId(goalAssigneeSettingsVersionsEntity.getUserId());
      goalAssigneeSettings.setGoalDefinitionCode(
          goalAssigneeSettingsVersionsEntity.getGoalDefinitionCode());
      RecurringGoalAssigneeSettings recurringGoalAssigneeSettings = null;

      GoalOccurrence goalOccurrence =
          goalAssigneeSettingsVersionsEntity.getRecurringFrequency() == null
              ? GoalOccurrence.one_time
              : GoalOccurrence.recurring;

      goalAssigneeSettings.setGoalOccurrence(goalOccurrence);

      if (goalAssigneeSettingsVersionsEntity.isRecurring()) {
        recurringGoalAssigneeSettings = (RecurringGoalAssigneeSettings) goalAssigneeSettings;
        recurringGoalAssigneeSettings.setRecurringFrequency(
            goalAssigneeSettingsVersionsEntity.getRecurringFrequency());
        log.info(
            "Returing recurringGoalAssigneeSettings for getGoalAssigneeSettingsFromGoalInstanceEntity");
        return recurringGoalAssigneeSettings;
      }
      log.info(
          "Returing oneTimeGoalAssigneeSettings for getGoalAssigneeSettingsFromGoalInstanceEntity");
    }
    return goalAssigneeSettings;
  }

  public GoalAchievementObject getGoalAchievementObjectFromGoalInstanceEntityAndAchievementValue(
      GoalInstanceEntity goalInstanceEntity,
      MetricAchievementObject metricAchievementObject,
      List<String> progress) {
    log.info("Getting getGoalAchievementObjectFromGoalInstanceEntityAndAchievementValue");
    GoalAchievementObject goalAchievementObject = new GoalAchievementObject();
    if (metricAchievementObject != null) {
      Double achievementValue = metricAchievementObject.getValue();
      goalAchievementObject.setDataType(metricAchievementObject.getData_type());
      Double target = goalInstanceEntity.getTarget();
      Double currentProgress = (double) achievementValue * 100 / (double) target;
      Double expectedProgress =
          calculateExpectedProgress(
              goalInstanceEntity.getStartDate(), goalInstanceEntity.getEndDate(), target);
      goalAchievementObject.setValue(achievementValue);
      goalAchievementObject.setExpectedProgress((int) Math.round(expectedProgress));
      goalAchievementObject.setProgressPercentage((int) Math.round(currentProgress));
      String progressType =
          getGoalProgressTypeFromExpectedAndCurrentProgress(
              Math.round(currentProgress), goalInstanceEntity.getClientId(), progress);
      if (StringUtils.equalsIgnoreCase("invalid", progressType)) {
        return null;
      }
      goalAchievementObject.setGoalProgressType(progressType);
      goalAchievementObject.setLastUpdatedDate(metricAchievementObject.getLastUpdatedDate());
      log.info("Returning the constructed for GoalAchievementObject");
    } else {
      log.warn("metricAchievementObject is null!");
    }
    return goalAchievementObject;
  }

  public ArrayList<GoalProgressTypeSettingMapping> getSortedGoalColorAndProgressBoundaries(
      ArrayList<GoalProgressTypeSettingMapping> goalColorAndProgressBoundaries) {
    // Sort the list
    Collections.sort(
        goalColorAndProgressBoundaries, Comparator.comparingInt(a -> Math.round(a.getThreshold())));
    return goalColorAndProgressBoundaries;
  }

  public String getGoalProgressTypeFromExpectedAndCurrentProgress(
      float currentProgress, String clientId, List<String> progress) {
    List<GoalProgressTypeSettingMapping> goalProgressTypeSettingMappings =
        clientAndSortedGoalProgressTypeMappings.get(clientId);

    if (null == goalProgressTypeSettingMappings) {
      goalProgressTypeSettingMappings = configUtils.getGoalProgressTypeSettingMappings(clientId);
      goalProgressTypeSettingMappings =
          getSortedGoalColorAndProgressBoundaries(
              (ArrayList<GoalProgressTypeSettingMapping>) goalProgressTypeSettingMappings);
      clientAndSortedGoalProgressTypeMappings.put(clientId, goalProgressTypeSettingMappings);
    }
    String progressType = currentProgress < 100 ? "not_completed" : "completed";
    for (GoalProgressTypeSettingMapping goalProgressTypeSettingMapping :
        goalProgressTypeSettingMappings) {
      log.info(
          "Current progress is {} and threshold is {} for type {}",
          currentProgress,
          goalProgressTypeSettingMapping.getThreshold(),
          goalProgressTypeSettingMapping.getGoalProgressType());
      if (currentProgress <= goalProgressTypeSettingMapping.getThreshold()) {
        log.info(
            "Fell in threshold {} for progress {}",
            goalProgressTypeSettingMapping.getThreshold(),
            currentProgress);
        if (Objects.isNull(progress) || progress.size() <= 0 || progress.contains(progressType)) {
          return goalProgressTypeSettingMapping.getGoalProgressType();
        } else {
          return "invalid";
        }
      }
    }
    log.info("Returning the highest threshold type");
    if (Objects.isNull(progress) || progress.size() <= 0 || progress.contains(progressType)) {
      return goalProgressTypeSettingMappings
          .get(goalProgressTypeSettingMappings.size() - 1)
          .getGoalProgressType();
    } else {
      return "invalid";
    }
  }

  public Double calculateExpectedProgress(Date startDate, Date endDate, Double target) {
    log.info(
        "Calculating expected progress for startDate -> {}, endDate -> {}, target -> {}",
        startDate,
        endDate,
        target);
    long startDateTime = startDate.getTime();
    long endDateTime = endDate.getTime();
    long todaysDateTime = new Date().getTime();
    double numerator = (Long.min(todaysDateTime, endDateTime) - startDateTime) * target;
    double denominator = endDateTime - startDateTime;
    double expectedProgress = numerator / denominator;
    log.info(
        "Returning the expected progress constructed from numerator -> {}, denominator -> {}",
        numerator,
        denominator);
    return Double.max(0, expectedProgress);
  }

  public List<GoalAssignment> getGoalAssignmentFromGoalAssigneeSettingsVersionEntities(
      List<GoalAssigneeSettingsVersionsEntity> goalAssigneeSettingsVersionsEntities,
      String clientId) {
    log.info(
        "IN getGoalAssignmentFromGoalAssigneeSettingsVersionEntities for {} goalAssigneeSettingsVersionsEntities",
        goalAssigneeSettingsVersionsEntities.size());
    List<GoalAssignment> goalAssignments = new ArrayList<GoalAssignment>();

    log.info(
        "Iterating over {} goalAssigneeSettingsVersionsEntities",
        goalAssigneeSettingsVersionsEntities.size());
    for (GoalAssigneeSettingsVersionsEntity goalAssigneeSettingsVersionsEntity :
        goalAssigneeSettingsVersionsEntities) {
      GoalAssigneesEntity goalAssigneesEntity =
          goalAssigneeSettingsVersionsEntity.getGoalAssigneesEntity();
      GoalAssignment goalAssignment = new GoalAssignment();
      GoalAssigneeSettings goalAssigneeSettings = new GoalAssigneeSettings() {};

      goalAssigneeSettings.setTarget(goalAssigneeSettingsVersionsEntity.getTarget());
      goalAssigneeSettings.setStartDate(goalAssigneeSettingsVersionsEntity.getStartDate());
      goalAssigneeSettings.setEndDate(goalAssigneeSettingsVersionsEntity.getEndDate());
      goalAssigneeSettings.setUserId(goalAssigneeSettingsVersionsEntity.getUserId());
      goalAssigneeSettings.setGoalDefinitionCode(
          goalAssigneeSettingsVersionsEntity.getGoalDefinitionCode());

      goalAssignment.setGoalAssigneeSettings(goalAssigneeSettings);
      goalAssignment.setDisabled(goalAssigneesEntity.getDeleted());
      goalAssignment.setGoalDefinitionCode(
          goalAssigneeSettingsVersionsEntity.getGoalDefinitionCode());
      goalAssignment.setActive(goalAssigneesEntity.getActive());
      goalAssignment.setUserId(goalAssigneeSettingsVersionsEntity.getUserId());
      goalAssignment.setLastUpdatedBy(goalAssigneeSettingsVersionsEntity.getLastUpdatedBy());
      goalAssignment.setCreatedBy(goalAssigneeSettingsVersionsEntity.getCreatedBy());
      goalAssignment.setVersion(goalAssigneeSettingsVersionsEntity.getVersion());

      goalAssignment.setClientId(clientId);

      goalAssignment.setId(goalAssigneesEntity.getId());

      goalAssignments.add(goalAssignment);
    }

    log.info(
        "Returning from getGoalAssignmentFromGoalAssigneeSettingsVersionEntities with {} goalAssignments",
        goalAssignments);

    return goalAssignments;
  }

  public List<String> getUserIdsWithoutGoals(
      List<GoalAssigneeSettingsVersionsEntity> existingGoalAssignments, List<String> allUserIds) {
    List<String> usersWithGoals = new ArrayList<String>();
    log.info(
        "Fetching getUserIdsWithoutGoals for {} existingGoalEntities and {} allUserIds",
        existingGoalAssignments.size(),
        allUserIds);
    // Fetch users with existing goals
    for (GoalAssigneeSettingsVersionsEntity gEntity : existingGoalAssignments) {
      usersWithGoals.add(gEntity.getUserId());
    }
    log.info("usersWithGoals are => {} ", usersWithGoals);
    List<String> usersWithoutGoals = new ArrayList<>(allUserIds);
    usersWithoutGoals.removeAll(usersWithGoals);
    log.info("usersWithoutGoals are => {} ", usersWithoutGoals);
    return usersWithoutGoals;
  }

  @Transactional
  public List<GoalAssigneeSettingsVersionsEntity> getExistingGoalAssigneeSettingsVersionEntities(
      Boolean isRecurring,
      List<String> userIds,
      String recurringFrequency,
      String goalDefinitionCode,
      Date startDate,
      Date endDate,
      String clientId) {
    log.info(
        "In getExistingGoalAssigneeSettingsVersionEntities for recurringFrequency {} startDate {} & endDate {}",
        recurringFrequency,
        startDate,
        endDate);
    // We format date to make native sql queries for non recurring goals
    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    List<GoalAssigneeSettingsVersionsEntity> existingGoalAssignments;
    if (isRecurring) {
      log.info(
          "Fetching goalAssigneeSettingsVersionsRepository.findLatestRowsForUserAndGoalDefinitionAndRecurringFrequency");
      existingGoalAssignments =
          goalAssigneeSettingsVersionsRepository
              .findLatestRowsForUserAndGoalDefinitionAndRecurringFrequency(
                  userIds,
                  recurringFrequency,
                  goalDefinitionCode,
                  dateFormat.format(startDate),
                  dateFormat.format(endDate),
                  clientId);
    } else {
      existingGoalAssignments =
          goalAssigneeSettingsVersionsRepository
              .findLatestRowsForUserAndGoalDefintionForOnetimeGoals(
                  userIds,
                  goalDefinitionCode,
                  dateFormat.format(startDate),
                  dateFormat.format(endDate),
                  clientId);
    }
    log.info(
        "Returing from getExistingGoalAssigneeSettingsVersionEntities after finding {} existingGoalAssignments ",
        existingGoalAssignments.size());
    return existingGoalAssignments;
  }

  @Transactional
  public void checkAndCreateVersion(String version) {
    if (version == null) {
      return;
    }

    GoalAssignmentOperationVersionsEntity existingEntity =
        goalAssignmentOperationVersionsRepository.findByVersion(version);

    if (existingEntity != null) {
      throw new GoalManagementServiceException("Version " + version + " already exists");
    }
    return;
  }

  /**
   * Returns the number of goal instances to be created for given start date & end date and time
   * granularity Will throw exception if the granularity doesn't fit exactly in the given time
   * bounds
   *
   * @param startDate
   * @param endDate
   * @param granularity
   * @return
   */
  public int getNumberOfGoalInstances(Date startDate, Date endDate, DATE_GRANULARITY granularity) {
    log.info(
        "In getNumberOfGoalInstances for  startDate {}, endDate {} , granularity {} ",
        startDate,
        endDate,
        granularity);
    long recurringDuration = endDate.getTime() - startDate.getTime();

    if (recurringDuration < 0) {
      throw new GoalManagementServiceException("Start date can't be after the end date");
    }

    int numberOfGoalInstances = 0;
    Calendar cal = Calendar.getInstance();
    cal.setTime(startDate);
    Date modifiedDate = null;

    switch (granularity) {
      case day:
        long diffInDays = TimeUnit.MILLISECONDS.toDays(recurringDuration);
        numberOfGoalInstances = (int) diffInDays;
        cal.add(Calendar.DATE, numberOfGoalInstances);
        modifiedDate = cal.getTime();
        break;

      case week:
        // Get diff in weeks
        // Add the diff to startdate and compare if it matches end date
        long diffInWeeks = TimeUnit.MILLISECONDS.toDays(recurringDuration) / 7;
        numberOfGoalInstances = (int) diffInWeeks;
        cal.add(Calendar.WEEK_OF_MONTH, numberOfGoalInstances);
        modifiedDate = cal.getTime();
        break;

      case month:
        long diffInMonths = getDateDiffInMonths(startDate, endDate);
        numberOfGoalInstances = (int) diffInMonths;
        cal.add(Calendar.MONTH, numberOfGoalInstances);
        modifiedDate = cal.getTime();
        break;

      case quarter:
        long diffInQuarters = getDateDiffInQuarters(startDate, endDate);
        numberOfGoalInstances = (int) diffInQuarters;
        cal.add(Calendar.MONTH, numberOfGoalInstances * 3);
        modifiedDate = cal.getTime();
        break;

      case year:
        long diffInYears = getDateDiffInYears(startDate, endDate);
        numberOfGoalInstances = (int) diffInYears;
        cal.add(Calendar.YEAR, numberOfGoalInstances);
        modifiedDate = cal.getTime();
        break;

      default:
        throw new GoalManagementServiceException("Granularity is not defined " + granularity);
    }
    if (modifiedDate.getTime() != endDate.getTime()) {
      throw new GoalManagementServiceException(
          "Recurring granularity doesn't fit with start & end date bounds");
    }
    log.info(
        "Returning from getNumberOfGoalInstances for {} numberOfGoalInstances",
        numberOfGoalInstances);
    return numberOfGoalInstances;
  }

  public Date roundOffMillisInDate(Date givenDate) {
    log.debug("In roundOffMillisInDate for {} date", givenDate);
    long requiredDate = givenDate.getTime();
    requiredDate = 1000 * (requiredDate / 1000);
    return new Date(requiredDate);
  }

  public long getDateDiffInQuarters(Date startDate, Date endDate) {
    log.debug("In getDateDiffInQuarters");
    return IsoFields.QUARTER_YEARS.between(
        new java.sql.Date(startDate.getTime()).toLocalDate(),
        new java.sql.Date(endDate.getTime()).toLocalDate());
  }

  public long getDateDiffInYears(Date startDate, Date endDate) {
    log.debug("In getDateDiffInYears");
    return ChronoUnit.YEARS.between(
        new java.sql.Date(startDate.getTime()).toLocalDate(),
        new java.sql.Date(endDate.getTime()).toLocalDate());
  }

  public long getDateDiffInMonths(Date startDate, Date endDate) {
    log.debug("In getDateDiffInMonths");
    return ChronoUnit.MONTHS.between(
        new java.sql.Date(startDate.getTime()).toLocalDate(),
        new java.sql.Date(endDate.getTime()).toLocalDate());
  }

  public List<GoalAssigneesEntity> createGoalAssigneeEntitiesForUsersWithoutGoalAssignments(
      List<String> userIds,
      String clientId,
      String goalDefinitionCode,
      String version,
      String createdBy,
      Boolean deleted) {
    log.info(
        "In createGoalAssigneeEntitiesForUsersWithoutGoalAssignments for userIds => {}, version => {}, and "
            + "goalDefinitionEntity code => {}",
        userIds,
        version,
        goalDefinitionCode);
    List<GoalAssigneesEntity> goalAssigneesEntities = new ArrayList<GoalAssigneesEntity>();
    for (String userId : userIds) {
      GoalAssigneesEntity goalAssigneesEntity =
          new GoalAssigneesEntity(version, userId, clientId, createdBy, deleted, false);
      goalAssigneesEntity.setGoalDefinitionCode(goalDefinitionCode);
      goalAssigneesEntities.add(goalAssigneesEntity);
    }
    log.info(
        "Returning from createGoalAssigneeEntitiesForUsersWithoutGoalAssignments after finding {} goalAssigneesEntities ",
        goalAssigneesEntities.size());
    return goalAssigneesEntities;
  }

  /**
   * Returns a map of start & end date boundaries like {startDate: '01/10/2019 00:00:00', endDate:
   * '31/10/2019 23:59:59'} for given no of instances and granularity. End dates end with 23:59:59
   * and start Dates end with 00:00:00
   *
   * @param startDate
   * @param endDate
   * @param granularity
   * @param noOfInstances
   * @return
   */
  public List<Map<String, Date>> getStartAndEndDateBoundaries(
      Date startDate,
      Date endDate,
      DATE_GRANULARITY granularity,
      int noOfInstances,
      String clientId) {

    log.info(
        "In getStartAndEndDateBoundaries for startDate {}, endDate {} , granularity {}, noOfInstances {} , clientId {}",
        startDate,
        endDate,
        granularity,
        noOfInstances,
        clientId);
    TimeZone zone = configUtils.getClientTimezone(clientId);
    log.debug("Timezone is {} & name is {}", zone.toString(), zone.getDisplayName());

    log.debug("In getStartAndEndDateBoundaries for {} noOfInstances", noOfInstances);
    log.debug("Start date is {} and end date is {}", startDate.toString(), endDate.toString());
    List<Map<String, Date>> listOfStartAndEndDates = new ArrayList<>();
    for (int i = 0; i < noOfInstances; i++) {
      Map<String, Date> startAndEndDates = new HashMap<>();
      log.debug("Start Date in loop is {}", startDate.toString());
      startAndEndDates.put("startDate", startDate);
      Date newEndDate = null;

      Calendar cal = Calendar.getInstance();
      cal.setTime(startDate);

      switch (granularity) {
        case day:
          cal.add(Calendar.DATE, 1);
          cal.add(Calendar.SECOND, -1);
          newEndDate = cal.getTime();
          startAndEndDates.put("endDate", newEndDate);
          break;

        case week:
          int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
          if (2 != dayOfWeek) {
            throw new GoalManagementServiceException("Weekly goals should start from Monday");
          }
          cal.add(Calendar.DATE, 7);
          cal.add(Calendar.SECOND, -1);
          newEndDate = cal.getTime();
          startAndEndDates.put("endDate", newEndDate);
          break;

        case month:
          int dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
          if (1 != dayOfMonth) {
            throw new GoalManagementServiceException(
                "Monthly goals should start from 1st of the month");
          }
          cal.add(Calendar.MONTH, 1);
          cal.add(Calendar.SECOND, -1);
          newEndDate = cal.getTime();
          startAndEndDates.put("endDate", newEndDate);

          break;

        case quarter:
          dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
          int monthOfTheYear = cal.get(Calendar.MONTH);
          if (1 != dayOfMonth) {
            throw new GoalManagementServiceException(
                "Quarterly goals should start from 1st of the month");
          }
          if (!(monthOfTheYear == 0
              || monthOfTheYear == 3
              || monthOfTheYear == 6
              || monthOfTheYear == 9)) {
            throw new GoalManagementServiceException(
                "Quarterly goals can start in the months of Jan, Apr, July or Oct");
          }
          cal.add(Calendar.MONTH, 3);
          cal.add(Calendar.SECOND, -1);
          newEndDate = cal.getTime();
          startAndEndDates.put("endDate", newEndDate);
          break;

        case year:
          dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
          if (1 != dayOfMonth) {
            throw new GoalManagementServiceException("Yearly goals should start from 1st of month");
          }
          cal.add(Calendar.YEAR, 1);
          cal.add(Calendar.SECOND, -1);
          newEndDate = cal.getTime();
          startAndEndDates.put("endDate", newEndDate);
          break;

        default:
          throw new GoalManagementServiceException("Granularity is not defined " + granularity);
      }
      // Added 1 sec for date limit to likes of 11:59:59 and start from 12:00:00
      Calendar nextStartCal = Calendar.getInstance();
      nextStartCal.setTime(newEndDate);
      log.debug("End date is {}", newEndDate.toString());
      nextStartCal.add(Calendar.SECOND, 1);
      startDate = nextStartCal.getTime();
      listOfStartAndEndDates.add(startAndEndDates);
    }
    log.debug("listOfStartAndEndDates are => ", listOfStartAndEndDates);
    listOfStartAndEndDates =
        getListOfStartAndEndDatesAfterOffsetCorrection(listOfStartAndEndDates, zone);
    return listOfStartAndEndDates;
  }

  /**
   * @param listOfStartAndEndDates
   * @param timeZone
   * @return
   */
  /* Modifies the map in place and returns the map*/
  public List<Map<String, Date>> getListOfStartAndEndDatesAfterOffsetCorrection(
      List<Map<String, Date>> listOfStartAndEndDates, TimeZone timeZone) {
    for (Map<String, Date> startAndEndDateMap : listOfStartAndEndDates) {
      Date startDateTemp = startAndEndDateMap.get("startDate");
      Date endDateTemp = startAndEndDateMap.get("endDate");
      log.debug(
          "Existing start {} & end dates {} ", startDateTemp.toString(), endDateTemp.toString());
      int timezoneOffsetInMillisForStartDate = getTimezoneOffset(startDateTemp, timeZone);
      // 2 * because to bring the
      Date newStartDate =
          new Date(startDateTemp.getTime() - (long) timezoneOffsetInMillisForStartDate);
      // setting it to 23:59:59
      int timezoneOffsetInMillisForEndDate = getTimezoneOffset(endDateTemp, timeZone);
      Date newEndDate =
          new Date((endDateTemp.getTime() - (long) timezoneOffsetInMillisForEndDate) - 1000);
      startAndEndDateMap.put("startDate", newStartDate);
      startAndEndDateMap.put("endDate", newEndDate);
      log.debug("New start {} & end dates {} ", newStartDate.toString(), newEndDate.toString());
    }

    return listOfStartAndEndDates;
  }

  /**
   * Get the goalInstance Entities to be saved with proper start & end date intervals for recurring
   * type For interval 01/10/19 - 30/09/2020, monthly goal frequency - returns 12 goal_instances
   *
   * @param recurringFrequency
   * @param clientId
   * @param userId
   * @param startDate
   * @param endDate
   * @param target
   * @param createdBy
   * @param updatedBy
   * @param numberOfGoalInstances
   * @param granularity
   * @return
   */
  private List<GoalInstanceEntity> getGoalInstanceEntitiesForRecurringType(
      String recurringFrequency,
      String clientId,
      String userId,
      Date startDate,
      Date endDate,
      Double target,
      String createdBy,
      String updatedBy,
      int numberOfGoalInstances,
      DATE_GRANULARITY granularity,
      String goalDefinitionCode,
      GoalAssigneesEntity goalAssigneesEntity) {
    log.info("In getGoalInstanceEntitiesForRecurringType");
    List<Map<String, Date>> startAndEndDateBoundaries =
        getStartAndEndDateBoundaries(
            startDate, endDate, granularity, numberOfGoalInstances, clientId);
    List<GoalInstanceEntity> goalInstanceEntities = new ArrayList<>();
    log.debug(
        "Got "
            + startAndEndDateBoundaries.size()
            + " date boundaries for "
            + numberOfGoalInstances
            + "goal instances to create");
    log.debug("Iterating over {} startAndEndDateBoundaries ", startAndEndDateBoundaries.size());
    for (Iterator<Map<String, Date>> eachEntryBounds = startAndEndDateBoundaries.iterator();
        eachEntryBounds.hasNext(); ) {
      Map<String, Date> startAndEndDateMap = (Map<String, Date>) eachEntryBounds.next();
      Date goalStartDate = startAndEndDateMap.get("startDate");
      Date goalEndDate = startAndEndDateMap.get("endDate");
      GoalInstanceEntity goalInstanceEntity =
          new GoalInstanceEntity(
              clientId,
              userId,
              goalStartDate,
              goalEndDate,
              target,
              createdBy,
              updatedBy,
              goalAssigneesEntity.getDeleted(),
              true,
              recurringFrequency,
              false);

      goalInstanceEntity.setGoalDefinitionCode(goalDefinitionCode);
      goalInstanceEntity.setGoalAssignee(goalAssigneesEntity);

      goalInstanceEntities.add(goalInstanceEntity);
    }
    log.info(
        "Returining from getGoalInstanceEntitiesForRecurringType with {} goalInstanceEntities ",
        goalInstanceEntities.size());
    return goalInstanceEntities;
  }

  public List<GoalAssigneeSettingsVersionsEntity>
      getOffsetCorrectedGoalAssigneeSettingsVersionsEntities(
          List<GoalAssigneeSettingsVersionsEntity> goalAssigneeSettingsVersionsEntities,
          TimeZone timeZone) {

    log.info(
        "In getOffsetCorrectedGoalAssigneeSettingsVersionsEntities for Timezone is {} & name is {}",
        timeZone.toString(),
        timeZone.getDisplayName());

    for (GoalAssigneeSettingsVersionsEntity goalAssigneeSettingsVersionsEntity :
        goalAssigneeSettingsVersionsEntities) {
      Date startDateTemp = goalAssigneeSettingsVersionsEntity.getStartDate();
      Date endDateTemp = goalAssigneeSettingsVersionsEntity.getEndDate();

      int timezoneOffsetInMillisForStartDate = getTimezoneOffset(startDateTemp, timeZone);
      Date newStartDate = new Date(startDateTemp.getTime() - timezoneOffsetInMillisForStartDate);
      int timezoneOffsetInMillisForEndDate = getTimezoneOffset(startDateTemp, timeZone);
      Date newEndDate = new Date(endDateTemp.getTime() - timezoneOffsetInMillisForEndDate);
      log.info(
          "Start date before offset correction: {} and after offset correction {}",
          startDateTemp,
          newStartDate);
      log.info(
          "End date before offset correction: {} and after offset correction {}",
          endDateTemp,
          newEndDate);

      goalAssigneeSettingsVersionsEntity.setStartDate(newStartDate);
      goalAssigneeSettingsVersionsEntity.setEndDate(newEndDate);
    }

    return goalAssigneeSettingsVersionsEntities;
  }

  public List<GoalInstanceEntity> createGoalInstancesFromGoalAssigneeSettingsVersionEntities(
      List<GoalAssigneeSettingsVersionsEntity> createdGoalAssigneeSettingsVersions,
      boolean isRecurring,
      String createdBy,
      String lastUpdatedBy,
      DATE_GRANULARITY granularity,
      String clientId,
      String goalDefinitionCode) {

    log.info(
        "In createGoalInstancesFromGoalAssigneeSettingsVersionEntities for {} createdGoalAssigneeSettingsVersions ",
        createdGoalAssigneeSettingsVersions.size());

    List<GoalInstanceEntity> goalInstanceEntities = new ArrayList<GoalInstanceEntity>();

    log.info("Iterating over createdGoalAssigneeSettingsVersions");
    for (GoalAssigneeSettingsVersionsEntity goalAssigneeSettingsVersionsEntity :
        createdGoalAssigneeSettingsVersions) {

      Date startDate = goalAssigneeSettingsVersionsEntity.getStartDate();
      Date endDate = goalAssigneeSettingsVersionsEntity.getEndDate();

      String userId = goalAssigneeSettingsVersionsEntity.getUserId();
      Double target = goalAssigneeSettingsVersionsEntity.getTarget();

      GoalAssigneesEntity goalAssigneesEntity =
          goalAssigneeSettingsVersionsEntity.getGoalAssigneesEntity();

      if (isRecurring) {
        String recurringFrequency = goalAssigneeSettingsVersionsEntity.getRecurringFrequency();
        int remainingInstancesToBeCreated =
            getNumberOfGoalInstances(startDate, endDate, granularity);
        List<GoalInstanceEntity> goalInstanceEntitiesForRecurringType =
            getGoalInstanceEntitiesForRecurringType(
                recurringFrequency,
                clientId,
                userId,
                startDate,
                endDate,
                target,
                createdBy,
                lastUpdatedBy,
                remainingInstancesToBeCreated,
                granularity,
                goalDefinitionCode,
                goalAssigneesEntity);

        goalInstanceEntities.addAll(goalInstanceEntitiesForRecurringType);
      } else {
        GoalInstanceEntity goalInstanceEntity =
            new GoalInstanceEntity(
                clientId,
                userId,
                startDate,
                endDate,
                target,
                createdBy,
                lastUpdatedBy,
                goalAssigneesEntity.getDeleted(),
                false,
                null,
                false);
        goalInstanceEntity.setGoalDefinitionCode(goalDefinitionCode);
        goalInstanceEntity.setGoalAssignee(goalAssigneesEntity);

        goalInstanceEntities.add(goalInstanceEntity);
      }
    }

    log.info(
        "Returning from createGoalInstancesFromGoalAssigneeSettingsVersionEntities with {} goalInstanceEntities ",
        goalInstanceEntities.size());
    return goalInstanceEntities;
  }

  public List<GoalAssigneeSettingsVersionsEntity> createNewGoalAssigneeSettingsVersionEntities(
      List<GoalAssigneesEntity> goalAssigneeEntities,
      String goalDefinitionCode,
      String recurringFrequency,
      Double target,
      Date startDate,
      Date endDate,
      Boolean isRecurring,
      String createdBy,
      String lastUpdatedBy,
      String version,
      String clientId,
      Boolean shouldDeleteGoalAssignment) {
    log.info(
        "In createNewGoalAssigneeSettingsVersionEntities for {} goalAssigneeEntities ",
        goalAssigneeEntities.size());
    List<GoalAssigneeSettingsVersionsEntity> newGoalAssigneeSettingsVersionsEntities =
        new ArrayList<GoalAssigneeSettingsVersionsEntity>();

    for (GoalAssigneesEntity goalAssigneesEntity : goalAssigneeEntities) {

      // Create goal_assignee_settings_version entity
      GoalAssigneeSettingsVersionsEntity assigneeSettingsVersionsEntity =
          new GoalAssigneeSettingsVersionsEntity(
              goalDefinitionCode,
              goalAssigneesEntity,
              recurringFrequency,
              target,
              startDate,
              endDate,
              isRecurring,
              version,
              lastUpdatedBy,
              goalAssigneesEntity.getUserId(),
              clientId,
              createdBy,
              shouldDeleteGoalAssignment);

      assigneeSettingsVersionsEntity.setGoalDefinitionCode(goalDefinitionCode);
      assigneeSettingsVersionsEntity.setGoalAssigneesEntity(goalAssigneesEntity);

      newGoalAssigneeSettingsVersionsEntities.add(assigneeSettingsVersionsEntity);
    }
    log.info(
        "Returning from createNewGoalAssigneeSettingsVersionEntities with {} newGoalAssigneeSettingsVersionsEntities ",
        newGoalAssigneeSettingsVersionsEntities.size());
    return newGoalAssigneeSettingsVersionsEntities;
  }

  public UserGoalAssignment getUserGoalAssignmentFromGoalAssigneeSettingsVersionEntity(
      GoalAssigneeSettingsVersionsEntity goalAssigneeSettingsVersionsEntity, String clientId) {

    log.info("In getUserGoalAssignmentFromGoalAssigneeSettingsVersionEntity");

    UserGoalAssignment userGoalAssignment = new UserGoalAssignment();
    log.info("Inside getUserGoalAssignmentFromGoalAssigneeSettingsVersionEntity");
    GoalAssigneesEntity goalAssigneesEntity =
        goalAssigneeSettingsVersionsEntity.getGoalAssigneesEntity();
    log.info(
        "Inside getUserGoalAssignmentFromGoalAssigneeSettingsVersionEntity goalAssigneesEntity"
            + goalAssigneesEntity);
    log.info(
        "Inside getUserGoalAssignmentFromGoalAssigneeSettingsVersionEntity goalAssigneeSettingsVersionsEntity"
            + goalAssigneeSettingsVersionsEntity.getGoalDefinitionCode());
    GoalDefinition goalDefinition =
        configUtil.getGoalDefinitionByCode(
            clientId, goalAssigneeSettingsVersionsEntity.getGoalDefinitionCode());
    if (goalDefinition == null) {
      log.error(
          "Goal definition code "
              + goalAssigneeSettingsVersionsEntity.getGoalDefinitionCode()
              + " doesn't exist");
      return userGoalAssignment;
    }
    GoalAssigneeSettings goalAssigneeSettings = new RecurringGoalAssigneeSettings() {};

    RecurringGoalAssigneeSettings recurringGoalAssigneeSettings = null;

    GoalOccurrence goalOccurrence =
        goalAssigneeSettingsVersionsEntity.getRecurringFrequency() == null
            ? GoalOccurrence.one_time
            : GoalOccurrence.recurring;

    goalAssigneeSettings.setTarget(goalAssigneeSettingsVersionsEntity.getTarget());
    goalAssigneeSettings.setStartDate(goalAssigneeSettingsVersionsEntity.getStartDate());
    goalAssigneeSettings.setEndDate(goalAssigneeSettingsVersionsEntity.getEndDate());
    goalAssigneeSettings.setGoalOccurrence(goalOccurrence);
    goalAssigneeSettings.setUserId(goalAssigneeSettingsVersionsEntity.getUserId());
    goalAssigneeSettings.setGoalDefinitionCode(
        goalAssigneeSettingsVersionsEntity.getGoalDefinitionCode());

    if (goalAssigneeSettingsVersionsEntity.isRecurring()) {
      recurringGoalAssigneeSettings = (RecurringGoalAssigneeSettings) goalAssigneeSettings;
      recurringGoalAssigneeSettings.setRecurringFrequency(
          goalAssigneeSettingsVersionsEntity.getRecurringFrequency());
      userGoalAssignment.setGoalAssigneeSettings(recurringGoalAssigneeSettings);
    } else {
      userGoalAssignment.setGoalAssigneeSettings(goalAssigneeSettings);
    }

    GoalType savedGoalType = goalDefinition.getType();

    if (savedGoalType == GoalType.vymoMetric) {
      VymoMetricGoalDefinition vymoMetricGoalDefinition = (VymoMetricGoalDefinition) goalDefinition;
      VymoMetricGoalDefinitionSettings vymoMetricGoalDefinitionSettings =
          new VymoMetricGoalDefinitionSettings(
              goalDefinition.getName(),
              goalDefinition.getDescription(),
              goalDefinition.getMetric(),
              goalDefinition.getScope(),
              goalDefinition.getType(),
              goalDefinition.getCode(),
              savedGoalType == GoalType.external,
              getShowAchievementList(goalDefinition, clientId),
              configUtil.getGoalOrder(goalDefinition, clientId),
              vymoMetricGoalDefinition.getMetricFilters(),
              vymoMetricGoalDefinition.getManagerDrillDownEnabled(),
              vymoMetricGoalDefinition.getHideShortfall());
      vymoMetricGoalDefinitionSettings.setMetricFilters(
          getGoalMetricFiltersPojoFromEntities(
              vymoMetricGoalDefinitionSettings.getMetricFilters(), clientId));
      vymoMetricGoalDefinitionSettings.setGoalDefinitionCode(goalDefinition.getCode());
      userGoalAssignment.setGoalDefinitionSettings(vymoMetricGoalDefinitionSettings);
    } else if (savedGoalType == GoalType.computed) {
      ComputedGoalDefinition computedGoalDefinition = (ComputedGoalDefinition) goalDefinition;
      ComputedGoalDefinitionSettings computedGoalDefinitionSettings =
          new ComputedGoalDefinitionSettings(
              goalDefinition.getName(),
              goalDefinition.getDescription(),
              goalDefinition.getMetric(),
              goalDefinition.getScope(),
              goalDefinition.getType(),
              goalDefinition.getCode(),
              savedGoalType == GoalType.external,
              getShowAchievementList(goalDefinition, clientId),
              configUtil.getGoalOrder(goalDefinition, clientId),
              computedGoalDefinition.getExpression(),
              computedGoalDefinition.getGoalDefinitionsInvolved(),
              computedGoalDefinition.getManagerDrillDownEnabled(),
              computedGoalDefinition.getHideShortfall());
      computedGoalDefinitionSettings.setGoalDefinitionCode(goalDefinition.getCode());
      userGoalAssignment.setGoalDefinitionSettings(computedGoalDefinitionSettings);
    } else {
      ExternalGoalDefinitionSettings externalGoalDefinitionSettings =
          new ExternalGoalDefinitionSettings(
              goalDefinition.getName(),
              goalDefinition.getDescription(),
              goalDefinition.getMetric(),
              goalDefinition.getScope(),
              goalDefinition.getType(),
              goalDefinition.getCode(),
              savedGoalType == GoalType.external,
              getShowAchievementList(goalDefinition, clientId),
              configUtil.getGoalOrder(goalDefinition, clientId),
              goalDefinition.getManagerDrillDownEnabled(),
              goalDefinition.getHideShortfall());
      externalGoalDefinitionSettings.setGoalDefinitionCode(goalDefinition.getCode());
      userGoalAssignment.setGoalDefinitionSettings(externalGoalDefinitionSettings);
    }

    userGoalAssignment.setUser(
        getUserObjectFromUserId(goalAssigneeSettingsVersionsEntity.getUserId()));
    userGoalAssignment.setGoalDefinitionId(goalDefinition.getCode());
    userGoalAssignment.setGoalAssigneeId(goalAssigneesEntity.getId());

    log.info("Returning from getUserGoalAssignmentFromGoalAssigneeSettingsVersionEntity");
    return userGoalAssignment;
  }

  private Boolean getShowAchievementList(GoalDefinition goalDefinition, String clientId) {
    String metric = goalDefinition.getMetric();
    Metric metricInfo = configUtil.getMetric(clientId, metric);
    if (metricInfo == null) {
      log.error("Metric info null for" + metric);
      return false;
    }
    return metricInfo.getShowAchievementList();
  }

  public GoalDefinitionSettings getGoalDefinitionSettingsFromGoalDefinitionEntity(
      GoalDefinition goalDefinition, String clientId) {
    log.info("Getting getGoalDefinitionSettingsFromGoalDefinitionEntity");
    GoalDefinitionSettings goalDefinitionSettings = new GoalDefinitionSettings() {};

    GoalType savedGoalType = goalDefinition.getType();

    if (savedGoalType == GoalType.vymoMetric) {
      VymoMetricGoalDefinition vymoMetricGoalDefinition = (VymoMetricGoalDefinition) goalDefinition;
      VymoMetricGoalDefinitionSettings vymoMetricGoalDefinitionSettings =
          new VymoMetricGoalDefinitionSettings(
              goalDefinition.getName(),
              goalDefinition.getDescription(),
              goalDefinition.getMetric(),
              goalDefinition.getScope(),
              goalDefinition.getType(),
              goalDefinition.getCode(),
              savedGoalType == GoalType.external,
              getShowAchievementList(goalDefinition, clientId),
              configUtil.getGoalOrder(goalDefinition, clientId),
              vymoMetricGoalDefinition.getMetricFilters(),
              vymoMetricGoalDefinition.getManagerDrillDownEnabled(),
              vymoMetricGoalDefinition.getHideShortfall());
      vymoMetricGoalDefinitionSettings.setMetricFilters(
          getGoalMetricFiltersPojoFromEntities(
              vymoMetricGoalDefinitionSettings.getMetricFilters(), clientId));
      vymoMetricGoalDefinitionSettings.setGoalDefinitionCode(goalDefinition.getCode());
      log.info(
          "Returing vymoMetricGoalDefinitionSettings for getGoalDefinitionSettingsFromGoalDefinitionEntity");
      return vymoMetricGoalDefinitionSettings;
    } else if (savedGoalType == GoalType.computed) {
      ComputedGoalDefinition computedGoalDefinition = (ComputedGoalDefinition) goalDefinition;
      ComputedGoalDefinitionSettings computedGoalDefinitionSettings =
          new ComputedGoalDefinitionSettings(
              goalDefinition.getName(),
              goalDefinition.getDescription(),
              goalDefinition.getMetric(),
              goalDefinition.getScope(),
              goalDefinition.getType(),
              goalDefinition.getCode(),
              savedGoalType == GoalType.external,
              getShowAchievementList(goalDefinition, clientId),
              configUtil.getGoalOrder(goalDefinition, clientId),
              computedGoalDefinition.getExpression(),
              computedGoalDefinition.getGoalDefinitionsInvolved(),
              computedGoalDefinition.getManagerDrillDownEnabled(),
              computedGoalDefinition.getHideShortfall());
      computedGoalDefinitionSettings.setGoalDefinitionCode(goalDefinition.getCode());
      log.info(
          "Returing ComputedGoalDefinitionSettings for getGoalDefinitionSettingsFromGoalDefinitionEntity");
      return computedGoalDefinitionSettings;
    } else {
      ExternalGoalDefinitionSettings externalGoalDefinitionSettings =
          new ExternalGoalDefinitionSettings(
              goalDefinition.getName(),
              goalDefinition.getDescription(),
              goalDefinition.getMetric(),
              goalDefinition.getScope(),
              goalDefinition.getType(),
              goalDefinition.getCode(),
              savedGoalType == GoalType.external,
              getShowAchievementList(goalDefinition, clientId),
              configUtil.getGoalOrder(goalDefinition, clientId),
              goalDefinition.getManagerDrillDownEnabled(),
              goalDefinition.getHideShortfall());
      externalGoalDefinitionSettings.setGoalDefinitionCode(goalDefinition.getCode());
      log.info(
          "Returing ExternalGoalDefinitionSettings for getGoalDefinitionSettingsFromGoalDefinitionEntity");
      return externalGoalDefinitionSettings;
    }
  }

  private List<GoalMetricFilter> getGoalMetricFiltersPojoFromEntities(
      List<GoalMetricFilter> goalMetricFilterEntities, String clientId) {
    List<GoalMetricFilter> goalMetricFilters = new ArrayList<>();
    if (goalMetricFilterEntities == null || goalMetricFilterEntities.isEmpty()) {
      return goalMetricFilters;
    }

    for (GoalMetricFilter goalMetricFilterEntity : goalMetricFilterEntities) {
      String dimensionCode = goalMetricFilterEntity.getDimension();
      Dimension dimensionInfo = configUtil.getDimensionInfo(dimensionCode, clientId);
      if (dimensionInfo == null) {
        log.error("dimensionInfo is null for " + dimensionCode);
        return goalMetricFilters;
      }
      GoalMetricFilter goalMetricFilter = new GoalMetricFilter();
      goalMetricFilter.setDimension(getDimensionType(dimensionInfo));
      goalMetricFilter.setFilterValues(goalMetricFilterEntity.getFilterValues());
      goalMetricFilters.add(goalMetricFilter);
    }
    return goalMetricFilters;
  }

  public List<BulkGoalAssignment> getGoalAssignmentFromGoalAssigneeAndSettings(
      List<GoalAssigneesEntity> goalAssigneesEntities,
      List<GoalAssigneeSettingsVersionsEntity> goalAssigneeSettingsVersionsEntities,
      String clientId,
      Integer beginningRowIndex) {
    log.info(
        "In getGoalAssignmentFromGoalAssigneeAndSettings for {} goalAssigneesEntities, {} and beginningRowIndex {} "
            + "goalAssigneeSettingsVersionsEntities",
        goalAssigneesEntities.size(),
        goalAssigneeSettingsVersionsEntities.size(),
        beginningRowIndex);
    List<BulkGoalAssignment> goalAssignments = new ArrayList<BulkGoalAssignment>();
    log.info("Iterating over goalAssigneesEntities");
    for (int i = 0; i < goalAssigneesEntities.size(); i++) {
      GoalAssigneesEntity goalAssigneesEntity = goalAssigneesEntities.get(i);
      GoalAssigneeSettingsVersionsEntity goalAssigneeSettingsVersionsEntity =
          goalAssigneeSettingsVersionsEntities.get(i);
      BulkGoalAssignment goalAssignment = new BulkGoalAssignment();
      GoalAssigneeSettings goalAssigneeSettings =
          getGoalAssigneeSettingsFromGoalAssigneeSettingsVersionEntity(
              goalAssigneeSettingsVersionsEntity);
      goalAssignment.setGoalAssigneeSettings(goalAssigneeSettings);
      goalAssignment.setDisabled(goalAssigneesEntity.getDeleted());
      goalAssignment.setGoalDefinitionCode(
          goalAssigneeSettingsVersionsEntity.getGoalDefinitionCode());
      goalAssignment.setActive(goalAssigneesEntity.getActive());
      goalAssignment.setUserId(goalAssigneeSettingsVersionsEntity.getUserId());
      goalAssignment.setLastUpdatedBy(goalAssigneeSettingsVersionsEntity.getLastUpdatedBy());
      goalAssignment.setCreatedBy(goalAssigneeSettingsVersionsEntity.getCreatedBy());
      goalAssignment.setVersion(goalAssigneeSettingsVersionsEntity.getVersion());

      goalAssignment.setClientId(clientId);

      goalAssignment.setId(goalAssigneesEntity.getId());

      goalAssignment.setMsg("success");
      goalAssignment.setRow(beginningRowIndex + i);
      goalAssignment.setStatus("success");

      goalAssignments.add(goalAssignment);
    }
    log.info(
        "Returning from getGoalAssignmentFromGoalAssigneeAndSettings with {} goalAssignments ",
        goalAssignments);
    return goalAssignments;
  }

  public GoalAssignment getGoalAssignnmentFromGoalInstanceEntity(
      GoalInstanceEntity gInstanceEntity) {
    log.info("In getGoalAssignnmentFromGoalInstanceEntity");
    GoalAssignment goalAssignment = new GoalAssignment();
    GoalAssigneeSettings goalAssigneeSettings = new GoalAssigneeSettings() {};

    goalAssigneeSettings.setTarget(gInstanceEntity.getTarget());
    goalAssigneeSettings.setStartDate(gInstanceEntity.getStartDate());
    goalAssigneeSettings.setEndDate(gInstanceEntity.getEndDate());
    goalAssigneeSettings.setUserId(gInstanceEntity.getUserId());
    goalAssigneeSettings.setGoalDefinitionCode(gInstanceEntity.getGoalDefinitionCode());

    goalAssignment.setGoalAssigneeSettings(goalAssigneeSettings);
    goalAssignment.setClientId(gInstanceEntity.getClientId());
    goalAssignment.setDisabled(gInstanceEntity.getDeleted());
    goalAssignment.setActive(gInstanceEntity.getActive());
    goalAssignment.setGoalDefinitionCode(gInstanceEntity.getGoalAssignee().getGoalDefinitionCode());
    goalAssignment.setId(String.valueOf(gInstanceEntity.getId()));
    goalAssignment.setUserId(gInstanceEntity.getUserId());
    goalAssignment.setLastUpdatedBy(gInstanceEntity.getLastUpdatedBy());
    goalAssignment.setCreatedBy(gInstanceEntity.getCreatedBy());
    log.info("Returning from getGoalAssignnmentFromGoalInstanceEntity");
    return goalAssignment;
  }

  // For users without goals we still need to fetch the metrics but had to show
  // the target as 0
  // We find the users without goalinstances and create dummy goal instances with
  // target 0
  public List<GoalInstanceEntity> getTotalGoalInstanceEntitiesIncludingMissingUsers(
      List<String> teamUserIds, List<GoalInstanceEntity> goalInstanceEntities) {
    log.info(
        "In getTotalGoalInstanceEntitiesIncludingMissingUsers for {} teamUserIds & {} goalInstanceEntities",
        teamUserIds.size(),
        goalInstanceEntities.size());
    List<GoalInstanceEntity> allGoalInstanceEntities = new ArrayList<>();
    ArrayList<String> clonedTeamUserIds = new ArrayList<>(teamUserIds);
    // return if both the sizes are same
    if (clonedTeamUserIds.size() == goalInstanceEntities.size()) {
      return goalInstanceEntities;
    }
    // append existing goalInstances
    log.debug("Appending {} existing goalInstanceEntities", goalInstanceEntities.size());
    allGoalInstanceEntities.addAll(goalInstanceEntities);

    // Get userIds with goals
    List<String> usersWithGoals =
        goalInstanceEntities.stream()
            .map(goalInstanceEntity -> goalInstanceEntity.getUserId())
            .collect(Collectors.toList());

    log.debug("There are {} users with goals", usersWithGoals.size());
    // Find the diff
    clonedTeamUserIds.removeAll(usersWithGoals);
    log.debug("There are {} users without goals", clonedTeamUserIds.size());

    // First GIE to get the meta info for dummy GIE
    GoalInstanceEntity sampleGIE = goalInstanceEntities.get(0);

    for (String userId : clonedTeamUserIds) {
      GoalInstanceEntity userGoalInstanceEntity =
          new GoalInstanceEntity(
              sampleGIE.getClientId(),
              userId,
              sampleGIE.getStartDate(),
              sampleGIE.getEndDate(),
              0d,
              sampleGIE.getCreatedBy(),
              sampleGIE.getLastUpdatedBy(),
              false,
              true,
              sampleGIE.getRecurringFrequency(),
              false);
      allGoalInstanceEntities.add(userGoalInstanceEntity);
    }
    log.info("Returning {} allGoalInstanceEntities", allGoalInstanceEntities.size());
    return allGoalInstanceEntities;
  }

  public ExternalAchievementApiObject getExternalAchievementFromEntity(
      ExternalAchievementEntity eae) {
    ExternalAchievementApiObject externalAchievement =
        new ExternalAchievementApiObject(
            eae.getGoalDefinitionCode(),
            eae.getClientId(),
            eae.getUserId(),
            eae.getStartDate(),
            eae.getEndDate(),
            eae.getAchievement(),
            eae.getRecurring(),
            eae.getRecurringFrequency(),
            eae.getCreatedBy(),
            eae.getLastUpdatedBy(),
            eae.getDeleted(),
            eae.getActive(),
            eae.getCreatedDate(),
            eae.getLastUpdatedDate());

    return externalAchievement;
  }

  public void prettyPrintStringFromJSON(Object jsonObject, String suffixString) {
    ObjectMapper mapper = new ObjectMapper();
    String s = null;
    try {
      s = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonObject);
    } catch (JsonProcessingException e) {
      log.info("Exception while prettyPrintStringFromJSON ", e);
    }
    log.info("Suffix string -> {}", suffixString);
    log.info(s);
  }

  public MetricsResultObject createAchievementDataForComputedGoal(
      Object result, Boolean isGroupby) {
    log.info("Inside createAchievementDataForComputedGoal");
    MetricsResultObject metricsResultObject = new MetricsResultObject();
    metricsResultObject.setMetrics(
        Arrays.asList(
            new MetricAchievementObject(
                "number", "computed", getAchievementValueFromObject(result), new Date())));
    metricsResultObject.setTotal_records_count(1);
    metricsResultObject.setType(isGroupby ? "grouped" : "terminal");
    log.info("Returning metricsResultObject");
    prettyPrintStringFromJSON(metricsResultObject, "metricsResultObject");
    return metricsResultObject;
  }

  public Double getAchievementValueFromObject(Object result) {
    Double achievement = 0d;
    if (result instanceof Integer || result instanceof Double) {
      achievement = Double.valueOf(result.toString());
    }
    return achievement;
  }

  public Object evaluateExpression(String exp, Map<String, Object> vars) {
    double result = 0;
    try {
      List<Argument> al = new ArrayList<>();
      for (Map.Entry<String, Object> entry : vars.entrySet()) {
        al.add(new Argument(entry.getKey(), Double.valueOf(entry.getValue().toString())));
      }
      Argument[] arr = new Argument[al.size()];
      arr = al.toArray(arr);
      Expression ex = new Expression(exp, arr);
      result = ex.calculate();
      if (Double.isNaN(result)) {
        result = 0d;
      }
    } catch (Exception e) {
      log.error("Error while evaluateExpression " + exp, e);
    }

    return result;
  }

  public static class GoalOrderComparator implements Comparator {
    public int compare(Object o1, Object o2) {
      UserGoalAssignment s1 = (UserGoalAssignment) o1;
      UserGoalAssignment s2 = (UserGoalAssignment) o2;
      if (Objects.nonNull(s1.getGoalDefinitionSettings().getGoalOrder())
          && Objects.nonNull(s2.getGoalDefinitionSettings().getGoalOrder())
          && s1.getGoalDefinitionSettings().getGoalOrder() > 0
          && s2.getGoalDefinitionSettings().getGoalOrder() > 0) {
        return s1.getGoalDefinitionSettings()
            .getGoalOrder()
            .compareTo(s2.getGoalDefinitionSettings().getGoalOrder());
      }
      return 0;
    }
  }

  public static double round(double input, int scale) {
    BigDecimal bigDecimal = new BigDecimal(input).setScale(scale, RoundingMode.HALF_EVEN);
    return bigDecimal.doubleValue();
  }

  public boolean isWithInRange(Date startDate, Date endDate, Date actualDate) {
    boolean result = false;
    if ((actualDate.equals(startDate) || actualDate.equals(endDate))
        || (actualDate.after(startDate) && actualDate.before(endDate))) {
      result = true;
    }
    return result;
  }

  public void validateNewGoalAssignmentWithExistingAssignments(
      List<GoalAssigneeSettingsVersionsEntity> existingGoalAssignments,
      GoalAssigneeSettings goalAssigneeSettings,
      String clientId) {
    log.info("In validateNewGoalAssignmentWithExistingAssignments");
    TimeZone clientTimezone = configUtils.getClientTimezone(clientId);
    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    DateFormat dateLogFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    for (GoalAssigneeSettingsVersionsEntity assignment : existingGoalAssignments) {
      Date newStartDate = roundOffMillisInDate(goalAssigneeSettings.getStartDate());
      Date newEndDate = roundOffMillisInDate(goalAssigneeSettings.getEndDate());
      // Correct dates as per client timezone
      Date offsetCorrectedStart = getOffsetCorrectedDate(newStartDate, clientTimezone);
      Date offsetCorrectedEnd = getOffsetCorrectedDate(newEndDate, clientTimezone);

      Date existingStartDate = assignment.getStartDate();
      Date existingEndDate = assignment.getEndDate();
      Date currentDate = getOffsetCorrectedDate(new Date(), clientTimezone);

      boolean isStartDateInRange =
          isWithInRange(existingStartDate, existingEndDate, offsetCorrectedStart);
      boolean isEndDateInRange =
          isWithInRange(existingStartDate, existingEndDate, offsetCorrectedEnd);

      log.info("New Start Date is {}", dateLogFormat.format(newStartDate));
      log.info("New End Date is {}", dateLogFormat.format(newEndDate));
      log.info("Offset corrected New Start Date is {}", dateLogFormat.format(offsetCorrectedStart));
      log.info("Offset corrected New End Date is {}", dateLogFormat.format(offsetCorrectedEnd));
      log.info("Current date is {}", dateLogFormat.format(currentDate));
      log.info("isStartDateInRange {} isEndDateInRange {}", isStartDateInRange, isEndDateInRange);

      // Don't allow to modify the targets for the dates that fall between the existing start and
      // end dates.
      // E.g
      // Existing Goal
      // SU Goal A 2021-04-01(start) 2021-08-01(August) 100(target)
      // New Goal
      // SU Goal A 2021-06-01(start) 2021-07-01(August) 100(target)
      // Assuming May as current month, New Goal Assignment will disable the existing one which
      // leads
      // to disable of goals for May and August
      if ((isStartDateInRange && isEndDateInRange)
          && newStartDate.after(existingStartDate)
          && newEndDate.before(existingEndDate)) {
        log.info("In Case 1 of validateNewGoalAssignmentWithExistingAssignments");
        throw new GoalManagementServiceException(
            "Goal Assignment already exists from "
                + dateFormat.format(existingStartDate)
                + " to "
                + dateFormat.format(existingEndDate));
      }

      // Allow to modify the targets for the dates starting from current date to any future date
      // after the existing goal end date
      // as this will disable current month/daily/yearly/weekly goals
      // E.g
      // Existing Goal
      // SU Goal A 2021-04-01(start) 2021-08-01(August) 100(target)
      // New Goal
      // SU Goal A 2021-06-01(start) 2021-10-01(Dec) 100(target)
      // Assuming May as current month, New Goal Assignment will disable the existing one which
      // leads
      // to disable of goals for May month
      // but allows the follow assignment
      // SU Goal A 2021-05-01(start) 2021-10-01(Dec) 100(target) because user is trying to modify
      // the goals
      // starting from the current date
      if (isStartDateInRange
          && newStartDate.after(currentDate)
          && newStartDate.after(existingStartDate)) {
        log.info("In Case 2 of validateNewGoalAssignmentWithExistingAssignments");
        throw new GoalManagementServiceException(
            "Goal Assignment already exists from "
                + dateFormat.format(existingStartDate)
                + " to "
                + dateFormat.format(existingEndDate));
      }

      // Don't to modify the targets for the dates that are before the existing assignment end date
      // as this will disable assignments for the dates after the new end date
      // E.g
      // Existing Goal
      // SU Goal A 2021-04-01(start) 2021-08-01(August) 100(target)
      // New Goal
      // SU Goal A 2021-01-01(start) 2021-06-01(Dec) 100(target)
      // Assuming May as current month, New Goal Assignment will disable the existing one which
      // leads
      // to disable of goals for july and august
      // but allows the follow assignment
      if (!isStartDateInRange
          && isEndDateInRange
          && newStartDate.before(existingStartDate)
          && newStartDate.before(existingEndDate)) {
        log.info("In Case 3 of validateNewGoalAssignmentWithExistingAssignments");
        throw new GoalManagementServiceException(
            "Goal Assignment already exists from "
                + dateFormat.format(existingStartDate)
                + " to "
                + dateFormat.format(existingEndDate));
      }
    }
  }
}
