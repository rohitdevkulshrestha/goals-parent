package com.getvymo.platform.goals.app.api.external;

import com.getvymo.platform.goals.app.common.models.GoalGroupSettings;
import com.getvymo.platform.goals.app.common.models.StandardListResponse;
import com.getvymo.platform.goals.app.service.goalsmgmt.IGoalManagementService;
import javax.validation.Valid;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/goals/v1/clients/{clientId}/goalGroups")
@Log4j2
public class GoalGroupRestController {
  @Autowired private IGoalManagementService iGoalManagementService;

  @GetMapping(path = "/{goalGroupCode}", consumes = "application/json")
  public GoalGroupSettings getGoalGroupByCode(
      @Valid @PathVariable(value = "goalGroupCode") String goalGroupCode,
      @Valid @PathVariable String clientId) {
    log.info("Fetching all goal groups for client", clientId, " and groupCode ", goalGroupCode);
    return iGoalManagementService.getGoalGroupDetails(clientId, goalGroupCode);
  }

  @SuppressWarnings("rawtypes")
  @GetMapping(path = "/all", consumes = "application/json")
  public StandardListResponse getAllGoalDefinitions(
      @Valid @PathVariable String clientId,
      @RequestParam(value = "pageNumber", required = false, defaultValue = "0") int pageNumber,
      @RequestParam(value = "pageSize", required = false, defaultValue = "20") int pageSize) {
    log.info("Fetching all goal groups for client", clientId);
    StandardListResponse slr = iGoalManagementService.getAllGoalGroups(clientId);
    slr.setPageNumber(pageNumber);
    slr.setPageSize(pageSize);
    return slr;
  }
}
