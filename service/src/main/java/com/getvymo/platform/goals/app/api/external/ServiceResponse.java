package com.getvymo.platform.goals.app.api.external;

import com.getvymo.platform.goals.app.errors.ErrorResponse;

public class ServiceResponse {
  private ErrorResponse error;
  private Object response;

  public ServiceResponse(Object response) {
    this.response = response;
  }
}
