package com.getvymo.platform.goals.springconfig.common.exceptionhandler;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.zalando.problem.spring.web.advice.ProblemHandling;

@ControllerAdvice
public class GlobalExceptionHandler implements ProblemHandling {
  // 400 bad request
  // 401 Unauthorized
  // 402 payment required
  // 403 forbidden
  // 404 resource not found, we will not be able to return this as sring itself will return this
  // 405 Method Not Allowed, gateway should provide this or container
  // 406 Not Acceptable
  // 409 Conflict
  // 410 Gone
  // 413 Payload Too Large
  // 406,415 Unsupported Media Type
  // 424 Failed Dependency
  // 500 internal error
  // 501 not implemented
  // 503,504 client should handle for hot deployment and retry as this is deterministic failure

  @Override
  public boolean isCausalChainsEnabled() {
    return true;
  }
}
