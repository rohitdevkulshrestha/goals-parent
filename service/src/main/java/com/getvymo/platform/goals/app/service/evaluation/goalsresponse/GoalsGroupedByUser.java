package com.getvymo.platform.goals.app.service.evaluation.goalsresponse;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.getvymo.platform.goals.app.common.models.GoalAssigneeSettings;
import java.util.Date;

public class GoalsGroupedByUser {
  private String goalAssigneeId;
  private GoalAssigneeSettings goalAssigneeSettings;
  private UserObject user;
  private GoalAchievementObject achievement;

  @JsonFormat(shape = JsonFormat.Shape.NUMBER)
  private Date startDate;

  @JsonFormat(shape = JsonFormat.Shape.NUMBER)
  private Date endDate;

  public GoalsGroupedByUser() {}

  public GoalsGroupedByUser(
      String goalAssigneeId,
      GoalAssigneeSettings goalAssigneeSettings,
      UserObject user,
      GoalAchievementObject achievement,
      Date startDate,
      Date endDate) {
    this.goalAssigneeId = goalAssigneeId;
    this.goalAssigneeSettings = goalAssigneeSettings;
    this.user = user;
    this.achievement = achievement;
    this.startDate = startDate;
    this.endDate = endDate;
  }

  public Date getStartDate() {
    return startDate;
  }

  public void setStartDate(Date startDate) {
    this.startDate = startDate;
  }

  public Date getEndDate() {
    return endDate;
  }

  public void setEndDate(Date endDate) {
    this.endDate = endDate;
  }

  public String getGoalAssigneeId() {
    return goalAssigneeId;
  }

  public void setGoalAssigneeId(String goalAssigneeId) {
    this.goalAssigneeId = goalAssigneeId;
  }

  public GoalAssigneeSettings getGoalAssigneeSettings() {
    return goalAssigneeSettings;
  }

  public void setGoalAssigneeSettings(GoalAssigneeSettings goalAssigneeSettings) {
    this.goalAssigneeSettings = goalAssigneeSettings;
  }

  public UserObject getUser() {
    return user;
  }

  public void setUser(UserObject user) {
    this.user = user;
  }

  public GoalAchievementObject getAchievement() {
    return achievement;
  }

  public void setAchievement(GoalAchievementObject achievement) {
    this.achievement = achievement;
  }
}
