package com.getvymo.platform.goals.app.common.models;

public class BulkExternalAchievementRequest extends ExternalAchievement {
  private String row;

  public String getRow() {
    return row;
  }

  public void setRow(String row) {
    this.row = row;
  }
}
